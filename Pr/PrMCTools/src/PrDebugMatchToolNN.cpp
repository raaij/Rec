// Include files
#include <limits>
#include <vector>
#include <map>
#include <algorithm>

// local
#include "PrDebugMatchToolNN.h"


#include "Event/MCTrackInfo.h"
#include "Event/MCParticle.h"
#include "Event/Track.h"
#include "Event/MCTrackInfo.h"

#include "Linker/LinkedTo.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrDebugMatchToolNN
//
// 2017-02-17 : Sevda
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY(PrDebugMatchToolNN)

//=============================================================================
void PrDebugMatchToolNN::fillTuple(const LHCb::Track& velo, const LHCb::Track& seed, const  std::vector<float>& vars ) const
{
  
  int found=  matchMCPart(velo, seed);
  Tuple tuple = nTuple("tuple","tuple");
  
  tuple->column("quality", found ).ignore();
  
  unsigned int i=0;
  for(auto var: vars ){
    tuple->column("var"+std::to_string(i), var).ignore();
    i++;
  }
  
  tuple->write().ignore();
}

//=============================================================================
int PrDebugMatchToolNN::matchMCPart(const LHCb::Track& velo, const LHCb::Track& seed) const 
{
  
  LinkedTo<LHCb::MCParticle, LHCb::Track> myLinkVelo ( evtSvc(), msgSvc(),LHCb::TrackLocation::Velo);
  LinkedTo<LHCb::MCParticle, LHCb::Track> myLinkSeed ( evtSvc(), msgSvc(),LHCb::TrackLocation::Seed);

  LHCb::MCParticle* mcPartV = myLinkVelo.first(  velo.key()  ); 
  MCTrackInfo trackInfo( evtSvc(), msgSvc() );


  int found=0;
  while(mcPartV != NULL){
    if(  !trackInfo.hasVeloAndT( mcPartV)){      
      mcPartV=myLinkVelo.next();
      continue; 
    }
    
    LHCb::MCParticle* mcPartS = myLinkSeed.first(  seed.key()  ); 
    while(mcPartS != NULL){
      if(mcPartV == mcPartS){
	
	if(11 == abs( mcPartV->particleID().pid())){      
	  found = -1;
	  break;
	}
	else {
	  found = 1;
	  break;
	}
      }
      else  mcPartS=myLinkSeed.next();
    }
    if(found) break;
    else mcPartV=myLinkVelo.next();
  }

  return found;
}
