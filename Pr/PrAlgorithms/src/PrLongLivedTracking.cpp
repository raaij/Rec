// Include files

#include <algorithm>
#include <cmath>


// from ROOT
#include <Math/CholeskyDecomp.h>
using ROOT::Math::CholeskyDecomp;

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Point3DTypes.h"

#include "STDet/DeSTDetector.h"
#include "STDet/DeUTSector.h"

#include "Event/Track.h"

#include "PrKernel/IPrDebugUTTool.h"
#include "TfKernel/RecoFuncs.h"
#include "TfKernel/IUTHitCreator.h"
#include "Event/StateParameters.h"
// local
#include "PrDownTrack.h"
#include "PrLongLivedTracking.h"

#include "weights/TMVA_PrLongLivedTracking_MLP.class.C"
//-----------------------------------------------------------------------------
// Implementation file for class : PrLongLivedTracking
//
// 2016-04-10: Michel De Cian and Adam Davis
//-----------------------------------------------------------------------------

namespace {

    auto lt_projection = [](auto&& proj) {
        return [proj=std::forward<decltype(proj)>(proj)](const auto& lhs, const auto& rhs)
               { return proj(lhs) < proj(rhs); };
    };

}

DECLARE_ALGORITHM_FACTORY( PrLongLivedTracking )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrLongLivedTracking::PrLongLivedTracking( const std::string& name,
                                          ISvcLocator* pSvcLocator)
: GaudiAlgorithm ( name , pSvcLocator ),
  m_downTime( 0 ),
  m_preselTime( 0 ),
  m_findMatchingHitsTime( 0 ),
  m_fitXProjectionTime( 0 ),
  m_fitAndRemoveTime( 0 ),
  m_xFitTime( 0 ),
  m_addUHitsTime( 0 ),
  m_addVHitsTime( 0 ),
  m_acceptCandidateTime( 0 ),
  m_storeTrackTime( 0 ),
  m_overlapTime( 0 ),
  m_printing( false ),
  m_magnetOff( false ),
  m_magFieldSvc(nullptr),
  m_utHitManager(nullptr),
  m_debugTool(nullptr),
  m_timerTool(nullptr),
  m_mvaReader(nullptr)
{
  declareProperty( "InputLocation" ,                 m_inputLocation        = ""                                          );
  declareProperty( "OutputLocation",                 m_outputLocation       = LHCb::TrackLocation::Downstream             );
  declareProperty( "ForwardLocation",                m_forwardLocation      = LHCb::TrackLocation::Forward                );
  declareProperty( "MatchLocation",                  m_matchLocation        = LHCb::TrackLocation::Match                  );
  declareProperty( "XPredTolConst",                  m_xPredTolConst        = 200. * Gaudi::Units::mm * Gaudi::Units::GeV );
  declareProperty( "XPredTolOffset",                 m_xPredTolOffset       = 6. * Gaudi::Units::mm                       );
  declareProperty( "TolMatchConst",                  m_tolMatchConst        = 20000.                                      );
  declareProperty( "TolMatchOffset",                 m_tolMatchOffset       = 1.5 *  Gaudi::Units::mm                     );
  declareProperty( "TolUConst",                      m_tolUConst            = 20000.0                                     );
  declareProperty( "TolUOffset",                     m_tolUOffset           = 2.5                                         );
  declareProperty( "TolVConst",                      m_tolVConst            = 2000.0                                      );
  declareProperty( "TolVOffset",                     m_tolVOffset           = 0.5                                         );

  declareProperty( "MaxWindowSize",                  m_maxWindow            = 10.0 * Gaudi::Units::mm                     );
  declareProperty( "MaxChi2",                        m_maxChi2              = 20.                                         );
  declareProperty( "MaxChi2ThreeHits",               m_maxChi2ThreeHits     = 10.0                                        );
  declareProperty( "MinUTx",                         m_minUTx               = 25. *  Gaudi::Units::mm                     );
  declareProperty( "MinUTy",                         m_minUTy               = 25. *  Gaudi::Units::mm                     );
  // Define parameters for MC09 field, zState = 9410
  declareProperty( "ZMagnetParams",                  m_zMagnetParams        = { 5379.88, -2143.93, 366.124, 119074,
        -0.0100333, -0.146055, 1260.96 } );

  declareProperty( "MomentumParams",                 m_momentumParams       = { 1217.77, 454.598, 3353.39 }               );
  declareProperty( "YParams",                        m_yParams              = {5.,2000.}                                  );
  declareProperty( "ZUT",                            m_zUT                  = 2485.* Gaudi::Units::mm                     );
  declareProperty( "ZUTa",                           m_zUTa                 = 2350.* Gaudi::Units::mm                     );

  declareProperty( "StateErrorX2",                   m_stateErrorX2         = 4.0                                         );
  declareProperty( "StateErrorY2",                   m_stateErrorY2         = 400.                                        );
  declareProperty( "StateErrorTX2",                  m_stateErrorTX2        = 6.e-5                                       );
  declareProperty( "StateErrorTY2",                  m_stateErrorTY2        = 1.e-4                                       );
  declareProperty( "StateErrorP",                    m_stateErrorP          = 0.15                                        );
  declareProperty( "MinPt",                          m_minPt                = 0. * Gaudi::Units::MeV                      );
  declareProperty( "MinMomentum",                    m_minMomentum          = 0. * Gaudi::Units::GeV                      );

  // -- Parameter to reject seed track which are likely ghosts
  declareProperty( "FisherCut",                      m_seedCut              = -1.0                                        );

  // -- Parameters for the cut on deltaP (momentum estimate from Seeding and Downstream kink)
  declareProperty( "MaxDeltaPConst",                 m_maxDeltaPConst       = 0.0                                         );
  declareProperty( "MaxDeltaPOffset",                m_maxDeltaPOffset      = 0.25                                        );

  // -- Parameters for correcting the predicted position
  declareProperty( "XCorrectionConst",               m_xCorrectionConst     = 23605.0                                     );
  declareProperty( "XCorrestionOffset",              m_xCorrectionOffset    = 0.4                                         );


  declareProperty( "MaxXTracks",                     m_maxXTracks           = 2                                           );
  declareProperty( "MaxChi2DistXTracks",             m_maxChi2DistXTracks   = 0.2                                         );
  declareProperty( "MaxXUTracks",                    m_maxXUTracks          = 2                                           );
  declareProperty( "FitXProjChi2Offset",             m_fitXProjChi2Offset   = 4.5                                         );
  declareProperty( "FitXProjChi2Const",              m_fitXProjChi2Const    = 35000.0                                     );

  // -- Tolerance for adding overlap hits
  declareProperty( "OverlapTol",                     m_overlapTol           = 2.0*Gaudi::Units::mm                        );
  declareProperty( "YTol",                           m_yTol                 = 2.0*Gaudi::Units::mm                        );
  // Change this in order to remove hits and T-tracks used for longtracks.
  // RemoveAll configures that everything is removed.
  // If false only hits and T-tracks from good longtracks are removed.
  // The criterion for this is the Chi2 of the longtracks from the fit.
  declareProperty( "RemoveUsed",                     m_removeUsed           = false                                       );
  declareProperty( "RemoveAll",                      m_removeAll            = false                                       );
  declareProperty( "LongChi2",                       m_longChi2             = 1.5                                         );

  //== debugging options
  declareProperty( "SeedKey",                        m_seedKey              = -1                                          );
  declareProperty( "WithDebugTool",                  m_withDebugTool        = false                                       );
  declareProperty( "DebugTool",                      m_debugToolName        = "PrDebugUTTruthTool"                       );
  declareProperty( "PrintTracks",                    m_printTracks          = false                                       );
  declareProperty( "TimingMeasurement",              m_doTiming             = false                                       );
  declareProperty( "ForceMCTrack",                   m_forceMCTrack         = false                                        );
  declareProperty( "TuneFisher",                     m_tuneFisher           = false                                       );

}
//=============================================================================
// Destructor
//=============================================================================
PrLongLivedTracking::~PrLongLivedTracking() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrLongLivedTracking::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Initialize" << endmsg;

  m_utHitManager = tool<Tf::UTStationHitManager <PrUTHit> >("PrUTStationHitManager");


  // -- For MVA
  std::vector<std::string> mvaNames =  { "chi2", "seedChi2", "p", "pt", "deltaP", "deviation", "initialChi2", "nHits", "highThresHits" };
  m_mvaReader =  new NNForLLT( mvaNames );


  if ( m_withDebugTool ) {
    m_debugTool = tool<IPrDebugUTTool>( m_debugToolName, this );
    //AD add efficiency vs step. This is recorded for every seed track, as this is the starting point.
    std::vector<std::string> steps;
    steps.push_back("InitEvent");
    steps.push_back("Fisher");
    steps.push_back("BeampipeCut");
    steps.push_back("Preselection");
    steps.push_back("FitXProjection");
    steps.push_back("AddUHits");
    steps.push_back("AddVHits");
    steps.push_back("FitAndRemove");
    steps.push_back("AcceptCandidate");
    steps.push_back("AddOverlapRegions");
    m_debugTool->initializeSteps(steps);
    steps.clear();//end AD

  }

  info() << "========================================================= "                        << endmsg
         << " XPredTol           = " << m_xPredTolConst       << "/p + " << m_xPredTolOffset    << endmsg
         << " TolMatchConst      = " << m_tolMatchConst       << "/p + " << m_tolMatchOffset    << endmsg
         << " TolUConst          = " << m_tolUConst           << "/p + " << m_tolUOffset        << endmsg
         << " TolVConst          = " << m_tolVConst           << "/p + " << m_tolVOffset        << endmsg
         << " MaxDeltaPConst     = " << m_maxDeltaPConst      << "/p + " << m_maxDeltaPOffset   << endmsg
         << " XCorrectionConst   = " << m_xCorrectionConst    << "/p + " << m_xCorrectionOffset << endmsg
         << " MaxXTracks         = " << m_maxXTracks          << endmsg
         << " MaxChi2DistXTracks = " << m_maxChi2DistXTracks  << endmsg
         << " MaxXUTracks        = " << m_maxXUTracks         << endmsg
         << " MaxChi2XProjection = " << m_fitXProjChi2Const   << "/p + " << m_fitXProjChi2Offset<< endmsg
         << " OverlapTol         = " << m_overlapTol          << endmsg
         << " SeedCut            = " << m_seedCut             << endmsg
         << " MaxChi2            = " << m_maxChi2             << endmsg
         << " MaxChi2ThreeHits   = " << m_maxChi2ThreeHits    << endmsg
         << " MaxWindowSize      = " << m_maxWindow           << endmsg
         << " RemoveUsed         = " << m_removeUsed          << endmsg
         << " RemoveAll          = " << m_removeAll           << endmsg
         << " LongChi2           = " << m_longChi2            << endmsg
         << " TimingMeasurements = " << m_doTiming            << endmsg
         <<  "========================================================= "   << endmsg;

  info() << "zMagnetParams ";
  for ( unsigned int kk = 0; m_zMagnetParams.size() > kk ; kk++) {
    info() << m_zMagnetParams[kk] << " ";
  }
  info() << endmsg << "momentumParams ";
  for ( unsigned int kk = 0; m_momentumParams.size() > kk ; kk++) {
    info() << m_momentumParams[kk] << " ";
  }
  info() << endmsg ;
  if ( 3 > m_zMagnetParams.size() ) {
    return Warning( "Not enough zMagnetParams" );
  }
  if ( 3 > m_momentumParams.size() ) {
    return Warning( "Not enough momentumParams" );
  }

  m_magFieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );


  if ( m_doTiming) {
    m_timerTool = tool<ISequencerTimerTool>( "SequencerTimerTool/Timer", this );
    m_timerTool->increaseIndent();
    m_downTime = m_timerTool->addTimer( "Execute" );
    m_preselTime = m_timerTool->addTimer( "getPreSelection" );
    m_findMatchingHitsTime = m_timerTool->addTimer( "FindMatchingHits" );
    m_fitXProjectionTime = m_timerTool->addTimer( "FitXProjection" );
    m_fitAndRemoveTime = m_timerTool->addTimer( "FitAndRemove" );
    m_xFitTime = m_timerTool->addTimer( "xFit" );
    m_addUHitsTime = m_timerTool->addTimer( "AddUHits" );
    m_addVHitsTime = m_timerTool->addTimer( "AddVHits" );
    m_acceptCandidateTime = m_timerTool->addTimer( "acceptCandidate" );
    m_overlapTime = m_timerTool->addTimer( "addOverlap" );
    m_storeTrackTime = m_timerTool->addTimer( "storeTrack" );
    m_timerTool->decreaseIndent();
  }

  // -- fill some helper variables
  m_addIsStereoHelper = { 0, 1, 1, 0 };

  // -- values for Fisher discriminant
  m_fishConst = -1.69860581797;
  m_fishCoefficients = { -0.241020410138, 3.03197732663e-07, -1.14400162824e-05,  0.126857153245, 0.122359738469 };

  m_matchingXHits.reserve(64);
  m_trackCandidates.reserve(16);
  m_goodXTracks.reserve(8);
  m_goodXUTracks.reserve(8);
  m_uHitsTemp.reserve(64);


  // -- that's a little ugly and should be improved in the future
  m_magPars = { m_zMagnetParams[0], m_zMagnetParams[1], m_zMagnetParams[2], m_zMagnetParams[3],
                m_zMagnetParams[4], m_zMagnetParams[5], m_zMagnetParams[6] };
  m_momPars = { m_momentumParams[0], m_momentumParams[1], m_momentumParams[2] };




  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PrLongLivedTracking::execute() {

  if ( m_doTiming ) m_timerTool->start( m_downTime );

  m_printing = msgLevel( MSG::DEBUG );
  if (m_printing) debug() << "==> Execute" << endmsg;

  // -- If no specified input location, get seed if it exists, else Tsa.
  if ( m_inputLocation.empty() ) {
    if ( exist<LHCb::Tracks>( LHCb::TrackLocation::Seed ) ) {
      m_inputLocation = LHCb::TrackLocation::Seed;
    } else {
      if ( exist<LHCb::Tracks>( LHCb::TrackLocation::Tsa ) ) {
        m_inputLocation = LHCb::TrackLocation::Tsa;
      } else {
        error() << "Could not find input location: " << LHCb::TrackLocation::Seed
                << " or " << LHCb::TrackLocation::Tsa << endmsg;
      }
    }
  }
  if ( m_withDebugTool ){ m_debugTool->resetflags();}//AD

  //==========================================================================
  // Prepare hits in UT, optional: remove used in PrForward.
  //==========================================================================
  ttCoordCleanup();

  //==========================================================================
  // Prepare the tracks, optional: removing the seends already used in Match.
  //==========================================================================
  LHCb::Tracks* inTracks   = get<LHCb::Tracks>( m_inputLocation );

  //== Local container of good track.
  std::vector<LHCb::Track*> myInTracks;
  myInTracks.reserve(inTracks->size());

  //=== Optional: Remove the seeds already used in Match, check by ancestor of Match tracks
  //=== Prepare T-Seeds
  prepareSeeds( inTracks, myInTracks );

  //==========================================================================
  // Get the output container
  //==========================================================================
  LHCb::Tracks* finalTracks = new LHCb::Tracks();
  finalTracks->reserve(100);
  put( finalTracks, m_outputLocation);

  const double magScaleFactor = m_magFieldSvc->signedRelativeCurrent() ;

  if( std::abs(magScaleFactor) > 1e-6 ){
    m_magnetOff = false;
  } else m_magnetOff = true;

  //==========================================================================
  // Main loop on tracks
  //==========================================================================
  Tf::UTStationHitManager<PrUTHit>::HitRange utcoords;
  if ( UNLIKELY( m_printing ) )  utcoords = m_utHitManager->hits();

  for ( LHCb::Track* tr : myInTracks ) {

    if( UNLIKELY(m_tuneFisher) && m_withDebugTool ){
      m_debugTool->tuneFisher( tr );
      continue;
    }

    if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("InitEvent",m_debugTool->isTrueTrack(tr,utcoords));}
    //AD, add debug step here to see if seed is reco'ble ad downstream

    // -- simple Fisher discriminant to reject bad seed tracks
    // -- tune this!
    //const double fisher = evaluateFisher( tr );


    //if( fisher < m_seedCut ) continue;
    if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("Fisher",m_debugTool->isTrueTrack(tr,utcoords));}
    //AD check fisher
    if ( 0 <= m_seedKey && m_seedKey == tr->key() ) m_printing = true;

    //TODO, remove the dependency on this.
    PrDownTrack track( tr, m_zUT, m_magPars, m_momPars, m_yParams, magScaleFactor*(-1) );

    if( std::abs( track.momentum()) < 1400 ) continue;

    // -- Veto particles coming from the beam pipe.
    if( insideBeampipe( track ) ) continue;
    if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("BeampipeCut",m_debugTool->isTrueTrack(tr,utcoords));}
    //AD
    const double deltaP = track.momentum() * track.state()->qOverP() - 1.;

    // --
    if ( UNLIKELY( m_printing ) ) {
      for ( PrUTHit* hit : utcoords ){
        if (hit->hit()->ignore()) continue;
        const double yTrack = track.yAtZ( 0. );
        const double tyTr   = track.slopeY();
        updateUTHitForTrackFast( hit, yTrack, tyTr );
      }

      info() << "Track " << tr->key()
             << format( " [%7.2f %7.2f GeV] x,y(UTa)%7.1f%7.1f dp/p%7.3f errXMag%6.1f YUT%6.1f",
                        .001/track.state()->qOverP(), .001*track.momentum(),
                        track.xAtZ( m_zUTa ), track.yAtZ( m_zUTa ), deltaP,
                        track.errXMag(), track.errYMag() )
             << endmsg;
      info() << format( " Y slope %8.6f computed %8.6f", track.state()->ty(), track.slopeY() )
             << endmsg;

      if ( m_debugTool ) m_debugTool->debugUTClusterOnTrack( tr, utcoords.begin(), utcoords.end() );
    }
    // --

    // -- tune this!
    // -- check for compatible momentum
    if ( maxDeltaP( track ) < fabs(deltaP) ) {
      if ( UNLIKELY( m_printing )) info() << "   --- deltaP " << deltaP << " -- rejected" << endmsg;
      if ( !m_magnetOff ) continue;
    }


    // -- Get hits in UT around a first track estimate
    getPreSelection( track );
    if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("Preselection",m_debugTool->isTrueTrack(tr,utcoords));}//AD
    // -- Need at least 3 hits and at least 1 stereo and 1 x hit
    if( 3 > m_preSelHits[0].size() +  m_preSelHits[1].size() + m_preSelHits[2].size() + m_preSelHits[3].size() ) continue;
    if( 1 > m_preSelHits[0].size() +  m_preSelHits[3].size() ) continue;
    if( 1 > m_preSelHits[1].size() +  m_preSelHits[2].size() ) continue;

    int nbOK = 0;
    int maxPoints = 0;
    m_trackCandidates.clear();
    m_matchingXHits.clear();

    //==============================================================
    // Try to find a candidate: X first, then UV.
    //==============================================================
    for( PrUTHit* myHit : m_xHits ){

      if ( myHit->hit()->testStatus( Tf::HitBase::UsedByPatDownstream ) ) continue;
      const double meanZ = myHit->z();
      const double posX  = myHit->x( );
      const int myPlane  = myHit->planeCode();

      track.startNewCandidate();


      // -- Create track estimate with one x hit
      const double slopeX = (track.xMagnet() - posX + track.sagitta( meanZ)) / (track.zMagnet() - meanZ);
      track.setSlopeX( slopeX );
      // -- now we know the slope better and can adapt the curvature
      // -- this effect is very small, but seems to be beneficial
      const double curvature =  1.6e-5 * ( track.state()->tx() - slopeX );
      track.setCurvature( curvature );

      // -----------------
      if ( UNLIKELY( m_printing )) {
        const double tolMatch = (std::abs(track.state()->p() / m_tolMatchConst) < 1. / (m_maxWindow - m_tolMatchOffset)) ?
          m_maxWindow : (m_tolMatchOffset + m_tolMatchConst / track.state()->p());
        info() << endmsg
               << format( "... start plane %1d x%8.2f z%8.1f slope%8.2f tolMatch%7.3f",
                          myPlane, posX, meanZ, 1000. * slopeX, tolMatch )
               << endmsg;
      }
      // -----------------


      // -- Fit x projection
      findMatchingHits( track, myPlane);

      if( !m_matchingXHits.empty() ){
        fitXProjection( track, myHit );
      }else{
        m_goodXTracks.clear();
        track.hits().push_back( myHit );
        m_goodXTracks.push_back( track );
      }
      if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("FitXprojection",m_debugTool->isTrueTrack(tr,track.hits()));}//AD

      // -- Take all xTracks into account whose chi2 is close to the best
      // -- until MaxXTracks is reached
      unsigned int maxI = 1;

      if( m_goodXTracks.size() > 1){
        for(unsigned int i = 1; i < m_goodXTracks.size() && i < m_maxXTracks; ++i){
          if(m_goodXTracks[i].chi2() - m_maxChi2DistXTracks < m_goodXTracks[0].chi2()) maxI = i;
        }
      }

      // -- Loop over good x tracks
      for( unsigned int i = 0; i < maxI; ++i){

        PrDownTrack& xTrack = m_goodXTracks[i];
        addUHits( xTrack, m_maxXUTracks );
        unsigned int maxJ = std::min( (unsigned int)m_maxXUTracks, (unsigned int)m_goodXUTracks.size() );
        if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("AddUHits",m_debugTool->isTrueTrack(tr,xTrack.hits()));}//AD
        // -- Loop over good xu tracks
        for(unsigned int j = 0; j < maxJ; j++){

          PrDownTrack& xuTrack = m_goodXUTracks[j];
          addVHits( xuTrack );
          if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("AddVHits",m_debugTool->isTrueTrack(tr,xuTrack.hits()));}//AD
          fitAndRemove<false>( xuTrack );
          if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("FitAndRemove",m_debugTool->isTrueTrack(tr,xuTrack.hits()));}//AD
          // -- Check if candidate is better than the old one
          if ( !acceptCandidate( xuTrack, maxPoints ) ) continue;
          if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("AcceptCandidate",m_debugTool->isTrueTrack(tr,xuTrack.hits()));}//AD
          m_trackCandidates.push_back( std::move(xuTrack) );
          ++nbOK;
        }

        // -- In case no U hits is found, search for V hit
        // -- but with larger search window
        if( xTrack.hits().size() > 1 && maxPoints < 4 && m_goodXUTracks.empty() ){
          addVHits( xTrack );
          if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("AddVHits",m_debugTool->isTrueTrack(tr,xTrack.hits()));}//AD
          fitAndRemove<false>( xTrack );
          if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("FitAndRemove",m_debugTool->isTrueTrack(tr,xTrack.hits()));}//AD
          // -- Check if candidate is better than the old one
          if ( !acceptCandidate( xTrack, maxPoints ) ) continue;
          if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("AcceptCandidate",m_debugTool->isTrueTrack(tr,xTrack.hits()));}//AD
          m_trackCandidates.push_back( std::move(xTrack) );
          ++nbOK;
        }
      }
    }

    // -- The 'nbOK' is just paranoia....
    if( m_trackCandidates.empty() || nbOK == 0) continue;

    // -- Now we have all possible candidates
    // -- Add overlap regions, fit again and flag
    // -- bad candidates
    for( PrDownTrack& track : m_trackCandidates ){

      addOverlapRegions( track );
      if( UNLIKELY(m_withDebugTool) ){m_debugTool->recordStepInProcess("AddOverlapRegions",m_debugTool->isTrueTrack(tr,track.hits()));}//AD
      if( m_withDebugTool && m_debugTool ){
        info() << "This track is: " << m_debugTool->isTrueTrack( track.track(), track.hits() ) << endmsg;
        m_debugTool->fracGoodHits( track.track(), track.hits() );
      }





      if( track.chi2() > m_maxChi2 ) track.setIgnore( true );
      if( track.firedLayers() < 3  ) track.setIgnore( true );
      if( insideBeampipe( track )  ) track.setIgnore( true );
      if( maxPoints > 3 && track.hits().size() < 4 ) track.setIgnore(true);

      if( track.ignore() ) continue;

      // -- Calculate the MVA
      double deviation = 0;

      for( auto& hit : track.hits() ){
        updateUTHitForTrackFast(hit, track.yAtZ(0.0), track.slopeY());
        deviation += std::abs( track.distance(hit) );
      }

      const int nHighThres = std::count_if( track.hits().begin(), track.hits().end(),
                                      [](const PrUTHit* hit){ return hit->hit()->sthit()->cluster().highThreshold(); });

      const double initialChi2 = track.initialChi2();
      const double deltaP = track.momentum() * track.state()->qOverP() - 1.;

      std::vector<double> vals ={ track.chi2(),
                                  track.track()->chi2PerDoF(),
                                  std::abs(track.momentum()),
                                  track.pt(),
                                  deltaP,
                                  deviation,
                                  initialChi2,
                                  static_cast<double>(track.hits().size()),
                                  static_cast<double>(nHighThres)};

      if( UNLIKELY(m_withDebugTool && m_debugTool)){
          const bool trueTrack = m_debugTool->isTrueTrack(track.track(),track.hits());
          m_debugTool->tuneFinalMVA( track.track(), trueTrack, vals);
      }

      const double mvaVal = m_mvaReader->GetMvaValue( vals );
      track.setMVAVal( mvaVal );
      if( track.mvaVal() < 0.05 ) track.setIgnore( true );
    }

    //std::sort( m_trackCandidates.begin(), m_trackCandidates.end(),
    //           [](const PrDownTrack& a, const PrDownTrack& b){ return a.chi2() < b.chi2();});

    std::sort( m_trackCandidates.begin(), m_trackCandidates.end(),
               [](const PrDownTrack& a, const PrDownTrack& b){ return a.mvaVal() > b.mvaVal();});


    for( PrDownTrack& track : m_trackCandidates ){

      if( track.ignore() ) continue;

      // -- Flag the hits of final track as used
      for( PrUTHit* hit: track.hits() ) {
        hit->hit()->setStatus( Tf::HitBase::UsedByPatDownstream, true );
      }

      if(  UNLIKELY(m_withDebugTool && m_debugTool) ) m_debugTool->chi2Tuple( std::abs( track.momentum() ), track.chi2(), track.hits().size());
      storeTrack( track, finalTracks, track.track() );
      break;
    }


  }


  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "Found " << finalTracks->size() << " tracks." << endmsg;

  if ( m_doTiming ) m_timerTool->stop( m_downTime );

  return StatusCode::SUCCESS;

}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode PrLongLivedTracking::finalize() {

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
//=========================================================================
//  Cleanup already used T-Seeds
//=========================================================================
void PrLongLivedTracking::prepareSeeds(LHCb::Tracks* inTracks, std::vector<LHCb::Track*>& myInTracks){

  LHCb::Tracks* match = getIfExists<LHCb::Tracks>( m_matchLocation );

  if ( NULL != match ) {
    if (!m_removeUsed) {
      myInTracks.insert( myInTracks.end(), inTracks->begin(), inTracks->end() );
    } else {
      if (m_printing) debug()<<"Remove seeds and tt hits from Match tracks"<<endmsg;
      for ( LHCb::Tracks::const_iterator itT = inTracks->begin();
            inTracks->end() != itT; itT++ ) {
        LHCb::Track* tr = (*itT);
        bool store =true;
        if ( m_printing ) debug() << "Seed " << tr->key();
        for( LHCb::Track* matchTr: *match ) {
          const SmartRefVector<LHCb::Track>& ancestors = matchTr->ancestors();
          for ( SmartRefVector<LHCb::Track>::const_iterator itA = ancestors.begin();
                ancestors.end() != itA; ++itA ) {
            const LHCb::Track* pt = (*itA);
            if ( tr == pt ) {
              if ( m_printing ) debug() << " is used in match " << matchTr->key();
              if ( m_removeAll || matchTr->chi2PerDoF() < m_longChi2 ) {
                if ( m_printing ) debug() << " good longtrack " << matchTr->key()<<endmsg;
                store = false;
                tagUsedUT( matchTr );
                break;
              }
              break;
            }
          }
          if ( !store ) break;
        }
        if (store) {
          myInTracks.push_back( tr );
          if ( m_printing ) debug() << " will be processed";
        }
        if ( m_printing ) debug() << endmsg;
      }
    }
  } else {  //== Copy tracks without ancestor...
    for(LHCb::Track* tr: *inTracks) {
      //== Ignore tracks with ancestor = forward...
      if ( m_removeUsed && 0 < tr->ancestors().size() ) continue;
      myInTracks.push_back( tr );
    }
  }
}
//=========================================================================
//  Cleanup already used UT coord
//=========================================================================
void PrLongLivedTracking::ttCoordCleanup ( ) {

  Tf::UTStationHitManager<PrUTHit>::HitRange utcoords = m_utHitManager->hits();
  for(PrUTHit* hit: utcoords) hit->hit()->setStatus( Tf::HitBase::UsedByPatDownstream, false );

  //== Tag hit used in forward
  if ( m_removeUsed ) {
    if ( exist<LHCb::Tracks>( m_forwardLocation ) ) {
      if (m_printing) debug()<<"Remove UT hits from Forward tracks from location "
                             <<m_forwardLocation <<endmsg;
      LHCb::Tracks* tracks = get<LHCb::Tracks>( m_forwardLocation );
      for(const LHCb::Track* tr: *tracks) {
        if (m_removeAll || tr->chi2PerDoF()<m_longChi2) tagUsedUT( tr );
      }
    }
  }
}
//=========================================================================
//  Tag used UT hits on this track.
//=========================================================================
void PrLongLivedTracking::tagUsedUT( const LHCb::Track* tr ) {

  Tf::UTStationHitManager<PrUTHit>::HitRange utcoords = m_utHitManager->hits();

  for(LHCb::LHCbID id: tr->lhcbIDs()) {
    if ( !id.isUT() ) continue;
    for(const PrUTHit* hit: utcoords) {
      if ( hit->hit()->lhcbID() == id ) {
        if (m_printing) debug()<<"tag hit as used "<<hit->hit()->lhcbID()<<endmsg;
        hit->hit()->setStatus( Tf::HitBase::UsedByPatMatch, true );
        break;
      }
    }
  }
}
//=========================================================================
//  Get the PreSelection
//=========================================================================
void PrLongLivedTracking::getPreSelection( PrDownTrack& track ) {

  if ( m_doTiming ) m_timerTool->start( m_preselTime );

  // - Max Pt around 100 MeV for strange particle decay -> maximum displacement is in 1/p.
  double xPredTol = m_xPredTolOffset;

  if (std::abs(track.momentum()) >  1e-6) xPredTol = m_xPredTolConst / std::abs( track.momentum() ) + m_xPredTolOffset;  // P dependance + overal tol.

  const double yTol = xPredTol;

  // -- a correction turns out to be beneficial
  // -- maybe to compensate tracks not coming from 0/0 (?)
  const double correction = xPosCorrection( track );

  for(int i = 0; i < 4; ++i) m_preSelHits[i].clear();

  m_xHits.clear();

  //== Collect all hits compatible with the extrapolation, region by region.
  if ( UNLIKELY( m_printing )) info() << "-- collect hits with tolerance " << xPredTol << endmsg;

  const double yTrack = track.yAtZ( 0. );
  const double tyTr   = track.slopeY();

  for(int iStation = 0; iStation < 2; ++iStation){
    for(int iLayer = 0; iLayer < 2; ++iLayer){

      auto range = m_utHitManager->sortedLayerHits(iStation,iLayer);

      if( range.empty() ) continue;

      const double zLayer   = range.front()->z();
      const double yPredLay = track.yAtZ( zLayer );
      const double xPredLay = track.xAtZ( zLayer );
      const double dxDy     = range.front()->hit()->dxDy();

      // -- this should sort of take the stereo angle and some tolerance into account.
      const double lowerBoundX = xPredLay - xPredTol - dxDy*yPredLay - 2.0;

      auto itHit = std::lower_bound( range.begin(), range.end(),
                                     lowerBoundX, Tf::compByX() );

      for( ; itHit != range.end(); ++itHit){

        PrUTHit* hit = *itHit;

        if( UNLIKELY( m_debugTool && m_forceMCTrack && !m_debugTool->isTrueHit( track.track(), hit ) ) ) continue;


        //if( !m_debugTool->isTrueHit( track.track(), hit ) ) continue;


        if( UNLIKELY( hit->hit()->ignore()) ) continue;


        const double yPos   = track.yAtZ( hit->z() );
        if ( !hit->hit()->isYCompatible( yPos, yTol ) ) continue;


        updateUTHitForTrackFast( hit, yTrack, tyTr);
        const double pos    = track.xAtZ( hit->z() ) - correction;

        // -- go from -x to +x
        // -- can break if we go out of the right bound
        if( xPredTol < pos - hit->x() ) continue;
        if( xPredTol < hit->x() - pos ) break;

        hit->hit()->setStatus( Tf::HitBase::UsedByPatDownstream, false );
        hit->setProjection( fabs( hit->x()-pos ) );

        m_preSelHits[2*iStation + iLayer].push_back(hit );

        if ( UNLIKELY( m_printing )) {
          info() << format( "  plane%2d z %8.2f x %8.2f pos %8.2f High%2d dist %8.2f",
                            hit->planeCode(), hit->z(), hit->x(), pos,
                            hit->hit()->sthit()->cluster().highThreshold(), hit->x() - pos);
          if ( m_debugTool ) m_debugTool->debugUTCluster( info(), hit );
          info() << endmsg;
        }
      }

      if( iStation == 1 && m_preSelHits[0].empty() && m_preSelHits[1].empty() ) break;

    }
  }


  std::sort(m_preSelHits[1].begin(), m_preSelHits[1].end(), Tf::increasingByProjection<>() );
  std::sort(m_preSelHits[2].begin(), m_preSelHits[2].end(), Tf::increasingByProjection<>() );

  // -- this is a little weird, but having a common vector of x-hits is better
  m_xHits = m_preSelHits[0];
  m_xHits.insert( m_xHits.end(), m_preSelHits[3].begin(), m_preSelHits[3].end() );
  std::sort( m_xHits.begin(),  m_xHits.end(),  Tf::increasingByProjection<>() );

  if ( m_doTiming ) m_timerTool->stop( m_preselTime );
}

//=========================================================================
//  Fit hits in x layers
//=========================================================================
void PrLongLivedTracking::xFit( PrDownTrack& track, const PrUTHit* hit1, const PrUTHit* hit2 ) {

  if ( m_doTiming ) m_timerTool->start( m_xFitTime );

  //== Fit, using the magnet point as constraint.
  double mat[6], rhs[3];
  mat[0] = 1./( track.errXMag() * track.errXMag() );
  mat[1] = 0.;
  mat[2] = 0.;
  rhs[0] = mat[0] * track.dxMagnet();//( m_magnetSave.x() - m_magnet.x() );
  rhs[1] = 0.;

  const double w1 = hit1->hit()->weight();
  const double w2 = hit2->hit()->weight();


  mat[0] += w1 + w2;
  mat[1] += w1 * (hit1->z() - track.zMagnet()) +  w2 * (hit2->z() - track.zMagnet());
  mat[2] += w1 * (hit1->z() - track.zMagnet())*(hit1->z() - track.zMagnet())
    + w2 * (hit2->z() - track.zMagnet())*(hit2->z() - track.zMagnet());

  rhs[0] += w1 *  track.distance( hit1 ) + w2 *  track.distance( hit2 );
  rhs[1] += w1 *  track.distance( hit1 ) * (hit1->z() - track.zMagnet())
    +  w2 *  track.distance( hit2 ) * (hit2->z() - track.zMagnet());

  CholeskyDecomp<double, 2> decomp(mat);
  if (UNLIKELY(!decomp)) {
    track.setChi2(1e42);
    if ( m_doTiming ) m_timerTool->stop( m_xFitTime );
    return;
  } else {
    decomp.Solve(rhs);
  }

  const double dx  = rhs[0];
  const double dsl = rhs[1];

  track.updateX( dx, dsl );

  const double chi2 = track.initialChi2() + w1* track.distance( hit1 )* track.distance( hit1 )
    +  w2* track.distance( hit2 )* track.distance( hit2 );

  track.setChi2( chi2 );

  if ( m_doTiming ) m_timerTool->stop( m_xFitTime );

}
//=========================================================================
//  Collect the hits in the other x layer
//=========================================================================
void PrLongLivedTracking::findMatchingHits( const PrDownTrack& track, const int plane) {

  if ( m_doTiming ) m_timerTool->start( m_findMatchingHitsTime );

  m_matchingXHits.clear();
  //search window = const1/momentum + const2
  double tol = (std::abs(track.state()->p() / m_tolMatchConst) < 1. / (m_maxWindow - m_tolMatchOffset)) ?
    m_maxWindow : (m_tolMatchOffset + m_tolMatchConst / track.state()->p());

  int planeToConsider = 0;
  if( plane == 0 ) planeToConsider = 3;
  if( m_preSelHits[planeToConsider].empty() ) return;
  const double xPred = track.xAtZ( m_preSelHits[planeToConsider].front()->z() );

  auto it = std::lower_bound( m_preSelHits[planeToConsider].begin(),
                              m_preSelHits[planeToConsider].end(), xPred - tol,
                              [](const PrUTHit* hit, const double uVal){ return hit->x() < uVal; });

  for( ; it != m_preSelHits[planeToConsider].end(); ++it ){
    PrUTHit* hit = *it;

    const double adist = std::abs( hit->x() - xPred );
    if ( adist > tol ) break;
    m_matchingXHits.push_back( hit );
  }

  std::sort( m_matchingXHits.begin(), m_matchingXHits.end(),
             lt_projection( [xPred](const PrUTHit* h) { return std::abs( h->x() - xPred ); } ) );

  if ( m_doTiming ) m_timerTool->stop( m_findMatchingHitsTime );

}
//=========================================================================
//  Add the U hits.
//=========================================================================
void PrLongLivedTracking::addUHits ( const PrDownTrack& track, const unsigned int maxNumTracks ) {

  if ( m_doTiming ) m_timerTool->start( m_addUHitsTime );

  m_goodXUTracks.clear();

  const double tol = m_tolUOffset + m_tolUConst / std::abs(track.momentum());

  // -- these numbers are a little arbitrary
  double minChi2 = 300;
  if( track.hits().size() == 1 ) minChi2 = 800;

  const double yTrack = track.yAtZ( 0. );
  const double tyTr   = track.slopeY();

  m_uHitsTemp.clear();

  // -- first select all hits, and then
  // -- accept until over a tolerance
  for(PrUTHit* hit: m_preSelHits[1]) {
    if( m_preSelHits[1].empty() ) break;
    updateUTHitForTrackFast( hit, yTrack, tyTr);

    const double dist = std::abs( track.distance( hit ) );
    if ( dist > tol ) continue;
    hit->setProjection( dist );
    m_uHitsTemp.push_back( hit );
  }

  if( m_uHitsTemp.empty() ) return;

  std::sort( m_uHitsTemp.begin(), m_uHitsTemp.end(), Tf::increasingByProjection<>());

  const double slopeX = track.slopeX();
  const double displX = track.displX();
  const double magnetX = track.xMagnet();

  PrDownTrack greatTrack( track );

  for( PrUTHit* hit : m_uHitsTemp){

    greatTrack.startNewXUCandidate(slopeX, displX, magnetX);

    greatTrack.hits().push_back( hit );
    fitAndRemove<true>( greatTrack );

    // -- it's sorted
    if ( greatTrack.chi2() > minChi2 ) break;
    if ( m_goodXUTracks.size() < maxNumTracks-1){
      m_goodXUTracks.push_back( greatTrack );
      greatTrack.hits().pop_back();
    }else{
      m_goodXUTracks.push_back( std::move(greatTrack) );
      break;
    }

  }

  if ( m_doTiming ) m_timerTool->stop( m_addUHitsTime );

}
//=========================================================================
//  Add the V hits. Take the one which has the best chi2
//=========================================================================
void PrLongLivedTracking::addVHits ( PrDownTrack& track ) {

  if ( m_doTiming ) m_timerTool->start( m_addVHitsTime );

  if( m_preSelHits[2].empty() ){
    if ( m_doTiming ) m_timerTool->stop( m_addVHitsTime );
    return;
  }

  double tol = m_tolVOffset + m_tolVConst/std::abs( track.momentum() );
  if( track.hits().size() == 2 )  tol = m_tolUOffset + m_tolUConst / std::abs(track.momentum());

  const double yTrack = track.yAtZ( 0. );
  const double tyTr   = track.slopeY();

  double minChi2 = 10000;

  PrUTHit* bestHit = nullptr;
  for(PrUTHit* hit: m_preSelHits[2]) {

    updateUTHitForTrackFast( hit, yTrack, tyTr);
    const double adist = std::abs( track.distance( hit ) );

    if( adist < tol ){
      track.hits().push_back( hit );
      fitAndRemove<true>( track );
      track.hits().pop_back();

      if( track.chi2() < minChi2){
        bestHit = hit;
        minChi2 = track.chi2();
      }
    }
  }

  if( bestHit != nullptr) track.hits().push_back( bestHit );

  track.sortFinalHits();

  if ( m_doTiming ) m_timerTool->stop( m_addVHitsTime );

}
//=========================================================================
//  Check if the new candidate is better than the old one
//=========================================================================
bool PrLongLivedTracking::acceptCandidate( PrDownTrack& track, int& maxPoints ){

  if ( m_doTiming ) m_timerTool->start( m_acceptCandidateTime );

  const int nbMeasureOK = track.hits().size();

  // -- use a tighter chi2 for 3 hit tracks
  // -- as they are more likely to be ghosts
  double maxChi2 = m_maxChi2;
  if( track.hits().size() == 3 ) maxChi2 = m_maxChi2ThreeHits;


  //== Enough mesures to have Chi2/ndof.
  if ( 3 > nbMeasureOK ) {
    if ( m_printing ) info() << " === not enough points" << endmsg;
    if ( m_doTiming ) m_timerTool->stop( m_acceptCandidateTime );
    return false;
  }

  if( 3 > track.firedLayers() ){
    if ( m_printing ) info() << " === not enough firedLayers" << endmsg;
    if ( m_doTiming ) m_timerTool->stop( m_acceptCandidateTime );
    return false;
  }

  //== Good enough Chi2/ndof
  if ( maxChi2 < track.chi2() ) {
    if ( m_printing ) info() << " === Chisq too big " << track.chi2() << endmsg;
    if ( m_doTiming ) m_timerTool->stop( m_acceptCandidateTime );
    return false;
  }

  const double deltaP = track.momentum() * track.state()->qOverP() - 1.;
  //== Compatible momentum
  if ( maxDeltaP( track ) < fabs(deltaP) ) {
    if ( m_printing ) info() << " === Deltap too big " << deltaP << endmsg;
    if ( !m_magnetOff ){
      if ( m_doTiming ) m_timerTool->stop( m_acceptCandidateTime );
      return false;
    }
  }

  //== Longest -> Keeep it
  if ( maxPoints > nbMeasureOK ) {
    if ( m_printing ) info() << " === less points than previous" << endmsg;
    if ( m_doTiming ) m_timerTool->stop( m_acceptCandidateTime );
    return false;
  }


  //== Count if enough with high threshold
  int nbHigh = 0;
  int nbUsed = 0;

  for(const PrUTHit* hit: track.hits()) {
    if ( hit->hit()->sthit()->cluster().highThreshold() ) ++nbHigh;
    if ( hit->hit()->testStatus( Tf::HitBase::UsedByPatDownstream )  ) ++nbUsed;
  }

  if ( 2 > nbHigh ) {
    if ( m_printing ) info() << " === not enough high threshold points" << endmsg;
    if ( m_doTiming ) m_timerTool->stop( m_acceptCandidateTime );
    return false;
  }
  if ( nbMeasureOK == nbUsed ) {
    if ( m_printing ) info() << " === is a clone" << endmsg;
    if ( m_doTiming ) m_timerTool->stop( m_acceptCandidateTime );
    return false;
  }

  if ( m_printing ) {
    info() << format( "  *** Good candidate ***  slope%8.2f displX%8.2f Y%8.2f Chi2%8.2f",
                      1000.*track.slopeX(), track.displX(), track.displY(), track.chi2() );
  }

  //== Better candidate.
  maxPoints = nbMeasureOK;
  if ( maxPoints > 4 ) maxPoints = 4;

  //== calculate pt and p
  const double momentum = std::abs(track.momentum());
  const double pt = track.pt();

  if (momentum<m_minMomentum){
    if ( m_doTiming ) m_timerTool->stop( m_acceptCandidateTime );
    return false;
  }

  if (pt<m_minPt){
    if ( m_doTiming ) m_timerTool->stop( m_acceptCandidateTime );
    return false;
  }

  track.sortFinalHits();

  if ( UNLIKELY( m_printing )) {
    for(PrUTHit* hit: track.hits()) {
      LHCb::STChannelID icID = hit->hit()->lhcbID().stID();
      double xCoord = hit->x() ;
      info() << "      UT Clus "
             << format( "(S%1d,L%1d,R%2d,S%2d,S%3d) x%7.1f  dist%7.3f High %1d",
                        icID.station(), icID.layer(), icID.detRegion(),
                        icID.sector(), icID.strip(), xCoord,
                        track.distance( hit ), hit->hit()->sthit()->cluster().highThreshold() ) ;
      if ( m_debugTool ) m_debugTool->debugUTCluster( info(), hit );
      info() << endmsg;
    }
  }

  if ( m_doTiming ) m_timerTool->stop( m_acceptCandidateTime );

  return true;
}

//=========================================================================
//  Store Track
//=========================================================================
void PrLongLivedTracking::storeTrack( PrDownTrack& track, LHCb::Tracks* finalTracks, LHCb::Track* tr ){

  if ( m_doTiming ) m_timerTool->start( m_storeTrackTime );

  counter("# Downstream tracks made")++;

  //=== Store the tracks
  //== new LHCb::Track
  LHCb::Track* dTr = new LHCb::Track();
  //== add ancestor
  dTr->addToAncestors( tr );
  //== Adjust flags
  dTr->setType(         LHCb::Track::Types::Downstream  );
  dTr->setHistory(      LHCb::Track::History::PrDownstream   );
  dTr->setPatRecStatus( LHCb::Track::PatRecStatus::PatRecIDs   );
  //== add Seed LHCbIDs
  dTr->addSortedToLhcbIDs(  tr->lhcbIDs()      );
  //== add new LHCbIDs
  for ( const PrUTHit* hit : track.hits() ){
    dTr->addToLhcbIDs( hit->hit()->lhcbID() );
    counter("#UT hits added")++;
  }


  //== add states
  // S. Stahl added 3 T-States

  // check for FPE and magnet off
  double QOverP = 1e-5;
  if (std::abs(track.momentum()) >  1e-6){
      QOverP = 1.0 / track.momentum();
  }

  //== create a state at zUTa
  LHCb::State ttState;
  ttState.setState( track.xAtZ( m_zUTa ),
                    track.yAtZ( m_zUTa ),
                    m_zUTa,
                    track.slopeX(),
                    track.slopeY(),
                    QOverP );
  Gaudi::TrackSymMatrix cov;
  cov(0,0) = m_stateErrorX2;
  cov(1,1) = m_stateErrorY2;
  cov(2,2) = m_stateErrorTX2;
  cov(3,3) = m_stateErrorTY2;
  double errQOverP = m_stateErrorP * QOverP;
  cov(4,4) = errQOverP * errQOverP;

  ttState.setCovariance( cov );
  dTr->addToStates( ttState );

  //== add seed states
  std::vector<LHCb::State*> newstates;
  newstates.reserve(3);
  newstates.push_back(tr->closestState(StateParameters::ZBegT).clone());
  newstates.push_back(tr->closestState(StateParameters::ZMidT).clone());
  // make sure we don't include same state twice
  if (std::abs(newstates[newstates.size() - 2]->z() -
               newstates.back()->z()) < 300.) {
    delete newstates.back();
    newstates.pop_back();
  }
  newstates.push_back(tr->closestState(StateParameters::ZEndT).clone());
  // make sure we don't include same state twice
  if (std::abs(newstates[newstates.size() - 2]->z() -
               newstates.back()->z()) < 300.) {
    delete newstates.back();
    newstates.pop_back();
  }

  // adjust q/p and its uncertainty
  for(LHCb::State* st: newstates) {
    st->covariance()(4,4) = errQOverP * errQOverP;
    st->setQOverP(QOverP);
  }
  dTr->addToStates(newstates);

  //== Save track
  finalTracks->insert( dTr );




  if ( m_doTiming ) m_timerTool->stop( m_storeTrackTime );

}

//=============================================================================
// Fit the projection in the zx plane, one hit in each x layer
//=============================================================================
void PrLongLivedTracking::fitXProjection( PrDownTrack& track, PrUTHit* firstHit){

  if ( m_doTiming ) m_timerTool->start( m_fitXProjectionTime );

  m_goodXTracks.clear();

  const double maxChi2 = m_fitXProjChi2Offset + m_fitXProjChi2Const/std::abs(track.momentum());

  // Catch if there is no second hit in other station
  PrUTHits::const_iterator itEnd = m_matchingXHits.end();
  for( PrUTHits::const_iterator iHit = m_matchingXHits.begin(); iHit != itEnd; ++iHit) {
    PrUTHit* hit = *iHit;

    track.startNewXCandidate();
    xFit( track, firstHit, hit );

    // -- It is sorted according to the projection
    // -- the chi2 will therefore only increase
    if( track.chi2() > maxChi2) break;
    track.hits().push_back( firstHit );
    track.hits().push_back( hit );

    // -- We can only move the last one
    // -- as the tracks before are 'recycled'
    if( m_goodXTracks.size() < m_maxXTracks-1){
      m_goodXTracks.push_back( track );
    }else{
      m_goodXTracks.push_back( std::move(track) );
      break;
    }

  }

  // -- If no suitable hit has been found
  // -- we just add the first one and make
  // -- it a track.
  if( m_goodXTracks.empty() ){
    track.hits().push_back( firstHit );
    m_goodXTracks.push_back( track );
    if ( m_doTiming ) m_timerTool->stop( m_fitXProjectionTime );
    return;
  }

  std::sort( m_goodXTracks.begin(), m_goodXTracks.end(),
             [](const PrDownTrack& a, const PrDownTrack& b){ return a.chi2() < b.chi2();});

  if ( m_doTiming ) m_timerTool->stop( m_fitXProjectionTime );

}
//=============================================================================
// This is needed for tracks which have more than one x hit in one layer
// Maybe we could make this smarter and do it for every track and add the 'second best'
// this, such that we do not need to loop over them again
//=============================================================================
void PrLongLivedTracking::addOverlapRegions( PrDownTrack& track ){

  if ( m_doTiming ) m_timerTool->start( m_overlapTime );

  bool hitAdded = false;

  const double yTrack = track.yAtZ( 0. );
  const double tyTr   = track.slopeY();

  for(int i = 0; i < 4; ++i){
    for( PrUTHit* hit : m_preSelHits[i] ){

      updateUTHitForTrackFast( hit, yTrack, tyTr );

      if ( hit->hit()->testStatus( Tf::HitBase::UsedByPatDownstream )) continue;
      if ( m_overlapTol > std::abs( track.distance( hit ) ) ) {
        double yTrack = track.yAtZ( hit->z() );

        if ( !hit->hit()->isYCompatible( yTrack, m_yTol ) ) continue;

        // -- check that z-position is different
        bool addHit = true;
        for(const PrUTHit* trackHit : track.hits() ){
          if( trackHit->planeCode() != hit->planeCode() ) continue;
          // -- the displacement in z between overlap modules is larger than 1mm
          if( std::abs( hit->z() - trackHit->z() ) < 1.0 ) addHit = false;
        }

        // -------------------------------------
        if(addHit){
          track.hits().push_back( hit );
          hitAdded = true;
        }

      }
    }
  }

  if ( hitAdded ) {
    track.sortFinalHits();
    fitAndRemove<false>( track );
  }

  if ( m_doTiming ) m_timerTool->stop( m_overlapTime );

}
//=============================================================================
// Evaluate the Fisher discriminant for the input tracks
//=============================================================================
/*
double PrLongLivedTracking::evaluateFisher( const LHCb::Track* track ){

  const unsigned int nbIT = std::count_if( track->lhcbIDs().begin(), track->lhcbIDs().end(),
                                           [](const LHCb::LHCbID id){ return id.isIT();});
  double nbITD =  static_cast<double>(nbIT);
  double lhcbIDSizeD = static_cast<double>(track->lhcbIDs().size());
  std::array<double,5> vals = { track->chi2PerDoF(), track->p(), track->pt(), nbITD, lhcbIDSizeD };

  return getFisher( vals );

}
*/

