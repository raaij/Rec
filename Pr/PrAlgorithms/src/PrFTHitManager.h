#ifndef PRFTHITMANAGER_H
#define PRFTHITMANAGER_H 1

// Include files
// from Gaudi
#include "PrKernel/PrHitManager.h"
#include "FTDet/DeFTDetector.h"
#include "GaudiKernel/RndmGenerators.h"

static const InterfaceID IID_PrFTHitManager ( "PrFTHitManager", 1, 0 );

/** @class PrFTHitManager PrFTHitManager.h
 *  Tool that transforms clusters into 'hits' (spatial positions) which are then used by the pattern
 *  recognition algorithms involving the FT.
 *
 *  @author Olivier Callot
 *  @date   2012-03-13
 */
class PrFTHitManager final : public PrHitManager {
public:

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_PrFTHitManager; }

  /// Standard constructor
  PrFTHitManager( const std::string& type,
                   const std::string& name,
                   const IInterface* parent);
  /** @brief Setup geometry of FT with hit zones for top / bottom / x-u-v-x layers
   */
  void buildGeometry() override;

  /** @brief Construct the hits and apply smearing to hit position (if enabled)
   */
  void decodeData() override;

protected:

 private:
  DeFTDetector* m_ftDet = nullptr;
};
#endif // PRFTHITMANAGER_H
