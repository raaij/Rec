#ifndef PRMATCHTOOLNN_H
#define PRMATCHTOOLNN_H 1

// Include files
#include <string>


#include "Event/Track.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiAlg/GaudiTool.h"

#include "IPrMatchTool.h"            // Interface
#include "PrKernel/IPrDebugMatchTool.h"


#include "weights/TMVAClassification_MLPMatching.class.C"

/** @class PrMatchToolNN PrMatchToolNN.h
 *
 * Match Velo and Seed tracks
 *
 * @author Manuel Schiller
 * @date 2012-01-31
 *	code cleanup, make sure all states are there for fit in HLT
 *
 * @author Johannes Albrecht
 * @date 2008-04-25
 * 	reorganize match as tool
 *
 * @author Olivier Callot
 * @date   2007-02-07
 *	initial implementation
 */


class PrMatchToolNN : public extends <GaudiTool, IPrMatchTool>
{
 public:

  using base_class::base_class;

  StatusCode initialize() override;
  StatusCode finalize() override;

  /// create match tracks from containers of velo and seed tracks
  StatusCode match(const LHCb::Tracks& velos,
                   const LHCb::Tracks& seeds,
                   LHCb::Tracks& matchs) const override;

  /// create a match track from a velo and a seed track
  StatusCode matchSingle(const LHCb::Track& velo,
                         const LHCb::Track& seed,
                         LHCb::Track& output ,
                         double& chi2) const override;

 public:

  /** @class MatchCandidate PrMatchNN.h
   *
   * Match candidate for PrMatcNNh algorithm
   *
   * @author Manuel Schiller
   * @date 2012-01-31
   * 	code cleanups
   *
   * @author Olivier Callot
   * @date   2007-02-07
   * 	initial implementation
   */
  class MatchCandidate
  {
  public:
  MatchCandidate(const LHCb::Track* vTr,
                 const LHCb::Track* sTr,
                 float dist) :
    m_vTr(vTr), m_sTr(sTr), m_dist(dist)
    { }

    ~MatchCandidate() { }

    const LHCb::Track* vTr() const { return m_vTr; }
    const LHCb::Track* sTr() const { return m_sTr; }
    float dist() const { return m_dist; }

private:
    const LHCb::Track* m_vTr;
    const LHCb::Track* m_sTr;
    float m_dist;
  };

 private:
  /// calculate matching chi^2
  float getChi2Match(const LHCb::State& vState,
                     const LHCb::State& sState,
                     std::array<float,6>& mLPReaderInput) const;

  /// merge velo and seed segment to output track
  void makeTrack(const LHCb::Track& velo,
                 const LHCb::Track& seed,
                 LHCb::Track& output,
                 float chi2) const;


  IPrAddUTHitsTool*       m_addUTHitsTool = nullptr;
  ITrackMomentumEstimate* m_fastMomentumTool = nullptr;
  IPrDebugMatchTool* m_matchDebugTool = nullptr;

  Gaudi::Property<std::string> m_addUTHitsToolName{ this, "AddUTHitsToolName", "PrAddUTHitsTool" };
  Gaudi::Property<std::string> m_fastMomentumToolName{ this, "FastMomentumToolName", "FastMomentumEstimate" };
  Gaudi::Property<std::string> m_matchDebugToolName {this, "MatchDebugToolName"  , "" };


  Gaudi::Property<std::vector<double>> m_zMagParams { this, "ZMagnetParams",
      { 5287.6, -7.98878, 317.683, 0.0119379, -1418.42} };
  Gaudi::Property<std::vector<double>> m_momParams { this, "MomentumParams",
      { 1.24386, 0.613179, -0.176015, 0.399949, 1.64664, -9.67158} };
  Gaudi::Property<std::vector<double>> m_bendYParams { this, "bendParamYParams",
      { -347.801, -42663.6} };

  // -- Parameters for matching in y-coordinate
  Gaudi::Property<float> m_zMatchY {        this, "zMatchY", 10000. * Gaudi::Units::mm };
  // -- Tolerances
  Gaudi::Property<float> m_dxTol {          this, "dxTol",         8. * Gaudi::Units::mm };
  Gaudi::Property<float> m_dxTolSlope {     this, "dxTolSlope",   80. * Gaudi::Units::mm };
  Gaudi::Property<float> m_dyTol {          this, "dyTol",         6. * Gaudi::Units::mm };
  Gaudi::Property<float> m_dyTolSlope {     this, "dyTolSlope",  300. * Gaudi::Units::mm };
  Gaudi::Property<float> m_fastYTol {       this, "FastYTol",    250.                    };

  // -- The main cut values
  Gaudi::Property<float> m_maxChi2 {        this, "MaxMatchChi2", 15.0   };
  Gaudi::Property<float> m_minNN {          this, "MinMatchNN",    0.25  };
  Gaudi::Property<float> m_maxdDist {       this, "MaxdDist",      0.10  };

  Gaudi::Property<bool> m_addUT {           this, "AddUTHits", true  };

  IClassifierReader*  m_MLPReader;

  typedef std::pair<const LHCb::Track*, const LHCb::State*> TrackStatePair;
  typedef std::vector<TrackStatePair> TrackStatePairs;

};

#endif // PRMATCHTOOL_H

// vim: sw=4:tw=78:ft=cpp
