// Include files

// local
#include "PrHitDecoding.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrHitDecoding
//
// 2016-05-23 : Thomas Nikodem
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( PrHitDecoding )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrHitDecoding::PrHitDecoding( const std::string& name,
                                        ISvcLocator* pSvcLocator)
: GaudiAlgorithm ( name , pSvcLocator ),
  m_hitManager(nullptr)
{
  declareProperty( "HitManagerName",     m_hitManagerName  = "PrFTHitManager" );
}
//=============================================================================
// Destructor
//=============================================================================
PrHitDecoding::~PrHitDecoding() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrHitDecoding::initialize() {

  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  m_hitManager = tool<PrHitManager>( m_hitManagerName );
  m_hitManager->buildGeometry();

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PrHitDecoding::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  // -- Decode the data in the T-stations
  m_hitManager->decodeData();

  return StatusCode::SUCCESS;
}

//=============================================================================
