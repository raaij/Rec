// Include files

// from Gaudi
#include "Event/FTLiteCluster.h"
#include "GaudiKernel/IRndmGenSvc.h"
// local
#include "PrFTHitManager.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrFTHitManager
//
// 2012-03-13 : Olivier Callot
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( PrFTHitManager )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrFTHitManager::PrFTHitManager( const std::string& type,
                                const std::string& name,
                                const IInterface* parent )
: PrHitManager ( type, name , parent )
{
  declareInterface<PrFTHitManager>(this);
}

//=========================================================================
//  Create the zones in the hit manager, 'zone' is a method of the base class
//=========================================================================
void PrFTHitManager::buildGeometry ( ) {
  // -- only build geometry once.
  if(m_geometryBuilt) return;

  // Retrieve and initialize DeFT (no test: exception in case of failure)
  m_ftDet = getDet<DeFTDetector>( DeFTDetectorLocation::Default );
  if(UNLIKELY(! m_ftDet ) ){
    error()<<" Cannot load the FTDetector from default location "<<endmsg;
    return;
  }
  if( m_ftDet->version() < 61 ) {
    error() << "This version requires FTDet v6.1 or higher" << endmsg;
    return;
  }

  // Loop over all layers
  for( auto station : m_ftDet->stations() ) {
    for( auto layer : station->layers() ) {
      int id = 4*(station->stationID() - 1) + layer->layerID();

      DetectorSegment seg(0, layer->globalZ(), layer->dxdy(), layer->dzdy(), 0., 0.);
      float xmax = 0.5*layer->sizeX();
      float ymax = 0.5*layer->sizeY();

      //The setGeometry defines the z at y=0, the dxDy and the dzDy, as well as the isX properties of the zone.
      //This is important, since these are used in the following.
      //They are set once for each zone in this method.
      MakeZone( 2*id,   seg, -xmax, xmax, -25., ymax ); // Small overlap (25 mm) for stereo layers
      MakeZone( 2*id+1, seg, -xmax, xmax, -ymax, 25. ); // Small overlap (25 mm) for stereo layers

      //----> Debug zones
      if( msgLevel(MSG::DEBUG)){
        debug() << "Layer " << id << " z " << zone(2*id).z()
                << " angle " << zone(2*id).dxDy() << endmsg;
      }
      //----> Debug zones
    }
  }
  
  m_geometryBuilt = true;
}


//=========================================================================
//  Decode an event.
//=========================================================================
void PrFTHitManager::decodeData ( ) {

  if(m_decodedData) return;
  
  typedef FastClusterContainer<LHCb::FTLiteCluster,int> FTLiteClusters;
  FTLiteClusters* clusters = get<FTLiteClusters>( LHCb::FTLiteClusterLocation::Default );
  if ( msgLevel( MSG::DEBUG) ) debug() << "Retrieved " << clusters->size() << " clusters" << endmsg;

  const DeFTMat* mat = nullptr;
  unsigned int prevMatID = 99999999;
  for ( auto clus : *clusters ) {
    if( clus.channelID().uniqueMat() != prevMatID ) {
      mat = m_ftDet->findMat( clus.channelID() );
      prevMatID = mat->elementID().uniqueMat();
    }

    double fraction = clus.fraction();
    LHCb::FTChannelID id = clus.channelID();

    int lay  = 4*(id.station()-1) + id.layer();
    int zone = int(mat->isBottom());
    int code = 2*lay + zone;

    auto endPoints = mat->endPoints(id, fraction);
    double invdy = 1./(endPoints.first.y()-endPoints.second.y());
    double dxdy  = (endPoints.first.x()-endPoints.second.x())*invdy;
    double dzdy  = (endPoints.first.z()-endPoints.second.z())*invdy;
    float x0     = endPoints.first.x()-dxdy*endPoints.first.y();
    float z0     = endPoints.first.z()-dzdy*endPoints.first.y();
    float yMin   = std::min(endPoints.first.y(), endPoints.second.y());
    float yMax   = std::max(endPoints.first.y(), endPoints.second.y());

    float errX = 0.170; // TODO: this should be ~80 micron; get this from a tool
    addHitInZone( code, LHCb::LHCbID( id ), x0, z0, dxdy, dzdy, yMin, yMax, errX , zone, lay );
    if ( msgLevel(MSG::VERBOSE) )
      verbose() << " .. hit " << id << " code=" << code << " x=" << x0 << " z=" << z0 << endmsg;
  }

  for ( unsigned int lay = 0; nbZones() > lay ; ++lay ) {
    std::sort( getIterator_Begin(lay), getIterator_End(lay), PrHit::LowerByX0() );
  }
  m_decodedData = true;
}
//=============================================================================
