#ifndef PRMATCHNN_H
#define PRMATCHNN_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

#include "Event/Track.h"
#include "GaudiKernel/IRegistry.h"
#include "IPrMatchTool.h"

/** @class PrMatchNN PrMatchNN.h
 *  Match Velo and Seed tracks
 *
 *  @author Michel De Cian (migration to Upgrade)
 *  @date 2013-11-15
 *
 *  @author Olivier Callot
 *  @date   2007-02-07
 */

class PrMatchNN : public GaudiAlgorithm
{
public:

  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;
  
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:

  Gaudi::Property<std::string> m_veloLocation {this, "VeloInput"  , LHCb::TrackLocation::Velo };
  Gaudi::Property<std::string> m_seedLocation {this, "SeedInput"  , LHCb::TrackLocation::Seed };
  Gaudi::Property<std::string> m_matchLocation {this, "MatchOutput"  , LHCb::TrackLocation::Match };
  Gaudi::Property<std::string> m_matchToolName {this, "MatchToolName"  , "PrMatchToolNN" };

  IPrMatchTool* m_matchTool = nullptr;

  
};

#endif // PRMATCH_H

