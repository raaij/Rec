// $Id: IPrDebugUTTool.h,v 1.1 2008-12-04 09:07:35 cattanem Exp $
#ifndef PATKERNEL_IPATDEBUGUTTOOL_H 
#define PATKERNEL_IPATDEBUGUTTOOL_H 1

// Include files
#include "PrKernel/PrUTHit.h"
#include "TfKernel/UTStationHitManager.h"
// Forward declarations
class MsgStream;

namespace LHCb{
  class Track;
}

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_IPrDebugUTTool ( "IPrDebugUTTool", 1, 0 );

/** @class IPrDebugUTTool IPrDebugUTTool.h PrKernel/IPrDebugUTTool.h
 *  
 *
 *  @author Olivier Callot
 *  @date   2007-10-22
 */
class IPrDebugUTTool : virtual public IAlgTool {
public: 

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IPrDebugUTTool; }

  virtual void debugUTClusterOnTrack( const LHCb::Track* track, 
                                      const PrUTHits::const_iterator beginCoord,
                                      const PrUTHits::const_iterator endCoord   ) = 0;
  
  virtual void debugUTCluster( MsgStream& msg, const PrUTHit* hit ) = 0;
  
  virtual bool isTrueHit( const LHCb::Track* track, const PrUTHit* hit) = 0;

  virtual double fracGoodHits( const LHCb::Track* track, const PrUTHits& hits) = 0;
  
  virtual bool isTrueTrack( const LHCb::Track* track, const PrUTHits& hits) = 0;

  virtual bool isTrueTrack( const LHCb::Track* track, const Tf::UTStationHitManager<PrUTHit>::HitRange& hits) = 0;//AD, overload
  
  virtual void chi2Tuple( const double p, const double chi2, const unsigned int nHits) = 0;
  
  //added by AD 2/1/16 for efficiency vs step

  virtual void initializeSteps(std::vector<std::string> steps) = 0;//initialize all steps in the process  

  virtual void recordStepInProcess(std::string step, bool result) = 0;//record the result of a step in the process
  
  virtual void resetflags() = 0;//reset all flags

  virtual void forceMCHits(PrUTHits& hits, LHCb::Track* track) = 0;//Special. Force only MC matched hits in the track.

  virtual void tuneFisher( const LHCb::Track* seedTrack) = 0;
  
  virtual void tuneDeltaP( const LHCb::Track* seedTrack, const double deltaP, const double momentum) = 0;
  
  virtual void tuneFinalMVA( const LHCb::Track* seedTrack, const bool goodTrack, std::vector<double> vals) = 0;
  
  
  virtual void getMagnetError( const LHCb::Track* seedTrack) = 0;
  

protected:

private:

};
#endif // PATKERNEL_IPATDEBUGUTTOOL_H
