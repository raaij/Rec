#ifndef PRFITTOOL_H 
#define PRFITTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "GaudiKernel/Point3DTypes.h"

#include "LinParFit.h"

static const InterfaceID IID_PrFitTool ( "PrFitTool", 1, 0 );

/** @class PrFitTool PrFitTool.h
 *  
 *
 *  @author Olivier Callot
 *  @date   2006-12-08
 */
class PrFitTool : public GaudiTool {
public: 

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_PrFitTool; }

  /// Standard constructor
  PrFitTool( const std::string& type, 
           const std::string& name,
           const IInterface* parent);

  bool fitLine( const std::vector<Gaudi::XYZPoint>& hit, int mode, 
                double z0, double& a, double& b);
  
  bool fitParabola( const std::vector<Gaudi::XYZPoint>& hit, int mode, 
                    double z0, double& a, double& b, double& c);
  
  bool fitCubic( const std::vector<Gaudi::XYZPoint>& hit, int mode, 
                 double x0, double& a, double& b, double& c, double& d);


protected:

private:

  LinParFit<double> m_fit2;
  LinParFit<double> m_fit3;
  LinParFit<double> m_fit4;

};
#endif // PRFITTOOL_H