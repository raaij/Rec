#ifndef TRACKINTERFACES_ITRACKFITTER_H 
#define TRACKINTERFACES_ITRACKFITTER_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"
// from LHCbKernel
#include "Kernel/ParticleID.h"

// range v3
#include <range/v3/utility/any.hpp>
#include <functional>

// Forward declarations
namespace LHCb {
 class Track;
}

/** @class ITrackFitter ITrackFitter.h TrackInterfaces/ITrackFitter.h
 *  
 *  Interface for a track fitting tool.
 *
 *  @author Jose A. Hernando, Eduardo Rodrigues
 *  @date   2005-05-25
 *
 *  @author Rutger van der Eijk  07-04-1999
 *  @author Mattiew Needham 
 */
class ITrackFitter : public extend_interfaces<IAlgTool>
{

public: 

  DeclareInterfaceID( ITrackFitter, 3, 0 );

  /// Create an instance of the accelerator cache
  virtual ranges::v3::any createCache() const { return ranges::v3::any{}; }

  /// Fit a track 
  virtual StatusCode fit( LHCb::Track& track,
                          LHCb::ParticleID pid = LHCb::ParticleID(211) ) const = 0;

  /// Fit a track
  virtual StatusCode fit_r( LHCb::Track& track,
                            ranges::v3::any& accelCache,
                            LHCb::ParticleID pid = LHCb::ParticleID(211) ) const = 0;

  // Fit a track
  virtual StatusCode operator() (LHCb::Track& track) const = 0;
  
  // Fit a batch of tracks
  virtual void operator() (std::vector<std::reference_wrapper<LHCb::Track>>& track) const = 0;
};

#endif // TRACKINTERFACES_ITRACKFITTER_H
