#ifndef TRACKINTERFACES_ITRACKEXTRASELECTOR_H
#define TRACKINTERFACES_ITRACKEXTRASELECTOR_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// Forward Declaration - from TrackInterfaces
struct ITrackExtrapolator;

/** @class ITrackExtraSelector ITrackExtraSelector.h TrackInterfaces/ITrackExtraSelector
 *
 *  Interface class to select which extrapolator to use.
 *
 */

struct ITrackExtraSelector: extend_interfaces<IAlgTool> {

  DeclareInterfaceID( ITrackExtraSelector, 2 , 0 );

  virtual const ITrackExtrapolator* select( double zStart,
                                            double zEnd ) const = 0;

};

#endif // TRACKINTERFACES_ITRACKEXTRASELECTOR_H
