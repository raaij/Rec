#ifndef TRACKINTERFACES_ISTATECORRECTIONTOOL_H
#define TRACKINTERFACES_ISTATECORRECTIONTOOL_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// from LHCbKernel
#include "Kernel/ParticleID.h"

// Forward declarations
class Material;
namespace LHCb {
  class State;
}


/** @class IStateCorrectionTool IStateCorrectionTool.h TrackInterfaces/IStateCorrectionTool.h
 *
 *  Interface for state correction tools
 *
 *  @author Eduardo Rodrigues
 *  @date   2006-08-18
 */
struct IStateCorrectionTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IStateCorrectionTool, 2, 0 );
  /// Correct a State
  virtual void correctState( LHCb::State& state,
                             const Material* material,
                             double wallThickness = 0,
                             bool upstream = true,
                             LHCb::ParticleID pid = LHCb::ParticleID(211) ) const = 0;


};
#endif // TRACKINTERFACES_ISTATECORRECTIONTOOL_H
