#ifndef TRACKINTERFACES_ITRACKMATCH_H
#define TRACKINTERFACES_ITRACKMATCH_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

//forward declarations
namespace LHCb {
  class Track;
}


/** @class ITrackMatch ITrackMatch.h TrackInterfaces/ITrackMatch.h
 *
 *
 *  @author Jose A. Hernando
 *  @date   2007-06-16
 */
struct ITrackMatch : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( ITrackMatch, 2, 0 );

  virtual StatusCode match(const LHCb::Track& track0,
                           const LHCb::Track& track1,
                           LHCb::Track& matchTrack,
                           double& quality,
                           double& quality2) = 0;

};
#endif // TRACKINTERFACES_IMATCHTVELOTRACKS_H
