#ifndef TRAJPROJECTOR_H 
#define TRAJPROJECTOR_H 1

// Include files
#include "TrackProjector.h"


/** @class TrajProjector TrajProjector.h TrajProjector.h
 *  
 *  Projects into the measurement space
 *
 *  @author Jeroen van Tilburg
 *  @date   2006-02-21
 */

template <typename T>
struct TrajProjector : TrackProjector {

  /// FIXME/TODO: project given a traj instead of a state

  /// Standard constructor
  TrajProjector( const std::string& type, 
                 const std::string& name,
                 const IInterface* parent );

};

#endif // TRAJPROJECTOR_H
