#ifndef TRACKPROJECTORS_TRACKPROJECTOR_H
#define TRACKPROJECTORS_TRACKPROJECTOR_H 1

// Include files

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/SystemOfUnits.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackProjector.h"

// TrackVectorFit
#include "TrackVectorFit/TrackVectorFit.h"

class IMagneticFieldSvc;
struct ITrajPoca;

/** @class TrackProjector TrackProjector.h TrackProjectors/TrackProjector.h
 *
 *  TrackProjector is the base class implementing methods
 *  from the ITrackProjector interface.
 *
 *  @author Jose Hernando
 *  @author Eduardo Rodrigues
 *  @date   2005-03-10
 */

class TrackProjector : public extends<GaudiTool, ITrackProjector> {

public:
   using base_class::base_class;

  /// ::initialize
  StatusCode initialize() override;

  /// Project the reference vector
  StatusCode project( const LHCb::StateVector& state, const LHCb::Measurement& meas ) override;

  /// Project a state onto a measurement.
  StatusCode project( const LHCb::State& state, const LHCb::Measurement& meas ) override;

  /// Project the state vector in this fitnode and update projection matrix and reference residual
  virtual StatusCode projectReference( LHCb::FitNode& node ) const override;
  virtual StatusCode projectReference( LHCb::Node& node ) const override;
  virtual StatusCode projectReference (Tr::TrackVectorFit::Node& n) const override;

  /// Retrieve the derivative of the residual wrt. the alignment parameters
  /// of the measurement. The details of the alignment transformation are
  /// defined in AlignTraj.
  Derivatives alignmentDerivatives(const LHCb::StateVector& state,
					   const LHCb::Measurement& meas,
                                   const Gaudi::XYZPoint& pivot) const override;

  /// Retrieve the projection matrix H of the (last) projection
  const Gaudi::TrackProjectionMatrix& projectionMatrix() const { return m_H ; }

  /// Retrieve the chi squared of the (last) projection
  double chi2() const;

  /// Retrieve the residual of the (last) projection
  double residual() const override { return m_residual ; }

  /// Retrieve the error on the residual of the (last) projection
  double errResidual() const override { return m_errResidual; }

  /// Retrieve the error on the measurement of the (last) projection
  double errMeasure() const override { return m_errMeasure ; }

  double doca () const { return m_doca; }

  Gaudi::XYZVector pocaVector () const { return m_unitPocaVector; }

  StatusCode project (
    const LHCb::StateVector& statevector,
    LHCb::Measurement& meas,
    Gaudi::TrackProjectionMatrix& H,
    double& residual,
    double& errMeasure,
    Gaudi::XYZVector& unitPocaVector,
    double& doca,
    double& sMeas
  ) const;

protected:
  mutable double m_residual = 0;
  mutable double m_errResidual = 0;
  mutable double m_errMeasure = 0;
  mutable Gaudi::TrackProjectionMatrix m_H;
  mutable double m_sMeas   = 0;
  mutable double m_sState  = 0;
  mutable double m_doca  = 0;
  mutable Gaudi::XYZVector m_unitPocaVector ;
  Gaudi::Property<bool> m_useBField { this,  "UseBField", false };                  /// Create StateTraj with or without BField information.
  Gaudi::Property<double> m_tolerance{ this, "Tolerance", 0.0005*Gaudi::Units::mm };   ///< Required accuracy of the projection
  IMagneticFieldSvc* m_pIMF = nullptr; ///< Pointer to the magn. field service
  ITrajPoca*         m_poca = nullptr; ///< Pointer to the ITrajPoca interface

};
#endif // TRACKPROJECTORS_TRACKPROJECTOR_H
