#pragma once

// Tr::TrackVectorFit
#include "TrackVectorFit/TrackVectorFit.h"

// Gaudi et al.
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbMath/LHCbMath.h"
#include "TrackKernel/TrackTraj.h"

// Event
#include "Event/TrackFunctor.h"
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Event/OTMeasurement.h"
#include "Event/State.h"

// Interface base class
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackProjectorSelector.h"
#include "TrackInterfaces/ITrackProjector.h"
#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/IMaterialLocator.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

// Forward declarations
struct ITrackManipulator;
struct IMaterialLocator;
struct ITrackExtrapolator;

namespace LHCb {
  class Track;
  class State;
  class TrackFitResult;
}

/** @class TrackVectorFitter TrackVectorFitter.h
 *  @author Daniel Campora
 *  @date   2017-02-13
 *  reusing much of TrackMasterFitter
 */
struct TrackVectorFitter : public extends<GaudiTool, ITrackFitter> {
  // In Gaudi future, this should be a thread allocated object
  mutable Tr::TrackVectorFit::TrackVectorFit m_fit;
  // Code profiling
  // mutable bool m_firstCall;

  Gaudi::Property<std::string> m_inputLocation { this,  "InputLocation" , "" };

  // All properties in alphabetical order
  Gaudi::Property<bool> m_addDefaultRefNodes {this, "AddDefaultReferenceNodes", true};
  Gaudi::Property<bool> m_applyEnergyLossCorrections {this, "ApplyEnergyLossCorr", true};
  Gaudi::Property<bool> m_applyMaterialCorrections {this, "ApplyMaterialCorrections", true};
  Gaudi::Property<double> m_chi2Outliers {this, "Chi2Outliers", 9.0};
  Gaudi::Property<double> m_errorX {this, "ErrorX", 20.0 * Gaudi::Units::mm};
  Gaudi::Property<double> m_errorY {this, "ErrorY", 20.0 * Gaudi::Units::mm};
  Gaudi::Property<double> m_errorTx {this, "ErrorTx", 0.1};
  Gaudi::Property<double> m_errorTy {this, "ErrorTy", 0.1};
  Gaudi::Property<std::vector<double>> m_errorQoP {this, "ErrorQoP", {0.0, 0.01}};
  Gaudi::Property<bool> m_fillExtraInfo {this, "FillExtraInfo", true};
  Gaudi::Property<bool> m_makeMeasurements {this, "MakeMeasurements", false};
  Gaudi::Property<bool> m_makeNodes {this, "MakeNodes", false};
  Gaudi::Property<double> m_maxDeltaChi2Converged {this, "MaxDeltaChiSqConverged", 0.01};
  Gaudi::Property<double> m_maxMomentumForScattering {this, "MaxMomentumForScattering", 500. * Gaudi::Units::GeV};
  Gaudi::Property<unsigned> m_maxUpdateTransports {this, "MaxUpdateTransports", 10};
  Gaudi::Property<unsigned> m_memManagerStorageIncrements {this, "MemManagerStorageIncrements", 2048};
  Gaudi::Property<double> m_minMomentumForELossCorr {this, "MinMomentumELossCorr", 10. * Gaudi::Units::MeV};
  Gaudi::Property<double> m_minMomentumForScattering {this, "MinMomentumForScattering", 100. * Gaudi::Units::MeV};
  Gaudi::Property<size_t> m_minNumMuonHits {this, "MinNumMuonHitsForOutlierRemoval", 4};
  Gaudi::Property<size_t> m_minNumTHits {this, "MinNumTHitsForOutlierRemoval", 6};
  Gaudi::Property<size_t> m_minNumTTHits {this, "MinNumTTHitsForOutlierRemoval", 3};
  Gaudi::Property<size_t> m_minNumVeloPhiHits {this, "MinNumVeloPhiHitsForOutlierRemoval", 3};
  Gaudi::Property<size_t> m_minNumVeloRHits {this, "MinNumVeloRHitsForOutlierRemoval", 3};
  Gaudi::Property<unsigned> m_numFitIter {this, "NumberFitIterations", 10};
  Gaudi::Property<unsigned> m_numOutlierIter {this, "MaxNumberOutliers", 2};
  Gaudi::Property<double> m_scatteringP {this, "MomentumForScattering", -1};
  Gaudi::Property<double> m_scatteringPt {this, "TransverseMomentumForScattering", 400. * Gaudi::Units::MeV};
  Gaudi::Property<bool> m_stateAtBeamLine {this, "StateAtBeamLine", true};
  Gaudi::Property<bool> m_updateMaterial {this, "UpdateMaterial", false};
  Gaudi::Property<bool> m_updateTransport {this, "UpdateTransport", true};
  Gaudi::Property<bool> m_upstream {this, "FitUpstream", true};
  Gaudi::Property<bool> m_useSeedStateErrors {this, "UseSeedStateErrors", false};
  // Tool Handles  
  ToolHandle<ITrackExtrapolator> m_extrapolator; // Extrapolator
  ToolHandle<ITrackExtrapolator> m_veloExtrapolator; // Extrapolator for Velo-only tracks
  ToolHandle<IMeasurementProvider> m_measProvider;
  ToolHandle<IMaterialLocator> m_materialLocator;
  ToolHandle<ITrackProjectorSelector> m_projectorSelector;
  // Attributes
  std::array<size_t, 5> m_minNumHits;

  void printAttributes () const {
    debug() << "m_addDefaultRefNodes " << m_addDefaultRefNodes << endmsg;
    debug() << "m_applyEnergyLossCorrections " << m_applyEnergyLossCorrections << endmsg;
    debug() << "m_applyMaterialCorrections " << m_applyMaterialCorrections << endmsg;
    debug() << "m_chi2Outliers " << m_chi2Outliers << endmsg;
    debug() << "m_errorTx " << m_errorTx << endmsg;
    debug() << "m_errorTy " << m_errorTy << endmsg;
    debug() << "m_errorX " << m_errorX << endmsg;
    debug() << "m_errorY " << m_errorY << endmsg;
    debug() << "m_fillExtraInfo " << m_fillExtraInfo << endmsg;
    debug() << "m_makeMeasurements " << m_makeMeasurements << endmsg;
    debug() << "m_makeNodes " << m_makeNodes << endmsg;
    debug() << "m_maxDeltaChi2Converged " << m_maxDeltaChi2Converged << endmsg;
    debug() << "m_maxMomentumForScattering " << m_maxMomentumForScattering << endmsg;
    debug() << "m_maxUpdateTransports " << m_maxUpdateTransports << endmsg;
    debug() << "m_minMomentumForELossCorr " << m_minMomentumForELossCorr << endmsg;
    debug() << "m_minMomentumForScattering " << m_minMomentumForScattering << endmsg;
    debug() << "m_minNumMuonHits " << m_minNumMuonHits << endmsg;
    debug() << "m_minNumTHits " << m_minNumTHits << endmsg;
    debug() << "m_minNumTTHits " << m_minNumTTHits << endmsg;
    debug() << "m_minNumVeloPhiHits " << m_minNumVeloPhiHits << endmsg;
    debug() << "m_minNumVeloRHits " << m_minNumVeloRHits << endmsg;
    debug() << "m_numFitIter " << m_numFitIter << endmsg;
    debug() << "m_numOutlierIter " << m_numOutlierIter << endmsg;
    debug() << "m_scatteringP " << m_scatteringP << endmsg;
    debug() << "m_scatteringPt " << m_scatteringPt << endmsg;
    debug() << "m_stateAtBeamLine " << m_stateAtBeamLine << endmsg;
    debug() << "m_updateMaterial " << m_updateMaterial << endmsg;
    debug() << "m_updateTransport " << m_updateTransport << endmsg;
    debug() << "m_upstream " << m_upstream << endmsg;
    debug() << "m_useSeedStateErrors " << m_useSeedStateErrors << endmsg;
  }

  LHCb::ParticleID m_pid;

  TrackVectorFitter (
    const std::string& type,
    const std::string& name,
    const IInterface* parent
  );

  virtual StatusCode initialize () override;
  
  virtual StatusCode finalize () override;

  // Note: Use 
  // void LHCb::Track::setFitStatus (const LHCb::Track::FitStatus &value)
  // to update the status of the tracks, in both operator() calls
  virtual StatusCode operator() (LHCb::Track& track) const override;
  
  // Deprecated
  virtual StatusCode fit (
    LHCb::Track& track,
    LHCb::ParticleID
  ) const override {
    // Ignore pid
    return operator()(track);
  }

  virtual StatusCode fit_r (LHCb::Track& track,
    ranges::v3::any&,
    LHCb::ParticleID pid
  ) const override {
    return fit(track, pid);
  }

  void operator() (std::vector<std::reference_wrapper<LHCb::Track>>& tracks) const override;

  void populateTracks (
    std::list<std::reference_wrapper<Tr::TrackVectorFit::Track>>& tracks
  ) const;

  void generateFitResult (LHCb::Track& track) const;

  void generateAndSetSeedCovariance (Tr::TrackVectorFit::Track& t) const;

  double closestToBeamLine (const LHCb::State& state) const;

  StatusCode makeNodes (LHCb::Track& track) const;

  void populateRefVectors (Tr::TrackVectorFit::Track& t) const;

  void projectAndUpdate (
    Tr::TrackVectorFit::Track& t,
    const bool& doUpdateMaterialCorrections = true,
    const bool& doUpdateTransport = true
  ) const;

  StatusCode initializeRefStates (LHCb::Track& track) const;
  
  void projectReference (Tr::TrackVectorFit::Track& t) const;

  void updateMaterialCorrections (Tr::TrackVectorFit::Track& t) const;

  void updateTransport (Tr::TrackVectorFit::Track& t) const;

  bool removeWorstOutlier (Tr::TrackVectorFit::Track& t) const;

  bool removeWorstOutlierSimplified (Tr::TrackVectorFit::Track& t) const;

  void determineStates (Tr::TrackVectorFit::Track& t) const;

  void fillExtraInfo (Tr::TrackVectorFit::Track& t) const;

  inline StatusCode failureInfo (const std::string& comment) const {
    if (msgLevel(MSG::DEBUG)) {
      debug() << "TrackVectorFitter failure: " + comment << endmsg;
    }
    return StatusCode::FAILURE;
  }

  inline unsigned hitType (const LHCb::Measurement::Type& type) const {
    enum HitType {VeloR=0, VeloPhi=1, TT=2, T=3, Muon=4, Unknown=5};

    switch (type) {
      case LHCb::Measurement::VeloR:
      case LHCb::Measurement::VeloLiteR:
      case LHCb::Measurement::VP:
        return VeloR;

      case LHCb::Measurement::VeloLitePhi:
      case LHCb::Measurement::VeloPhi:
        return VeloPhi;
      
      case LHCb::Measurement::TT:
      case LHCb::Measurement::TTLite:
      case LHCb::Measurement::UT:
      case LHCb::Measurement::UTLite:
        return TT;
      
      case LHCb::Measurement::IT:
      case LHCb::Measurement::OT:
      case LHCb::Measurement::ITLite:
      case LHCb::Measurement::FT:
        return T;
      
      case LHCb::Measurement::Muon:
        return Muon;
      
      case LHCb::Measurement::Unknown:
      case LHCb::Measurement::Calo:
      case LHCb::Measurement::Origin:
      default:
        return Unknown;
    }
  }

  inline unsigned nActiveMeasurements (
    const Tr::TrackVectorFit::Track& t,
    const LHCb::Measurement::Type& type
  ) const {
    unsigned rc (0);
    for (const auto& n : t.nodes()) {
      if (n.node().type() == LHCb::Node::HitOnTrack && n.node().measurement().type() == type) {
        ++rc;
      }
    }
    return rc ;
  }

  inline unsigned nActiveOTTimes (
    const Tr::TrackVectorFit::Track& t
  ) const {
    unsigned rc (0);
    for (const auto& n : t.nodes()) {
      if (n.node().type() == LHCb::Node::HitOnTrack && n.node().measurement().type() == LHCb::Measurement::OT) {
        const LHCb::OTMeasurement* otmeas = static_cast<const LHCb::OTMeasurement*>(&(n.node().measurement()));
        if (otmeas->driftTimeStrategy() == LHCb::OTMeasurement::FitDistance ||
            otmeas->driftTimeStrategy() == LHCb::OTMeasurement::FitTime) {
          ++rc;
        }
      }
    }
    return rc ;
  }

  inline const ITrackExtrapolator* extrapolator (LHCb::Track::Types tracktype) const {
    if (tracktype == LHCb::Track::Velo || tracktype == LHCb::Track::VeloR) {
      return &(*m_veloExtrapolator);
    }
    return &(*m_extrapolator);
  }
};
