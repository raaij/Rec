#ifndef TRACKTOOLS_STATEDETAILEDBETHEBLOCHENERGYCORRECTIONTOOL_H
#define TRACKTOOLS_STATEDETAILEDBETHEBLOCHENERGYCORRECTIONTOOL_H 1

// Include files
// -------------

#include <unordered_map>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/PhysicalConstants.h"

// from TrackInterfaces
#include "TrackInterfaces/IStateCorrectionTool.h"

// From PartProp
#include "Kernel/IParticlePropertySvc.h"

// from TrackEvent
#include "Event/State.h"

/** @class StateDetailedBetheBlochEnergyCorrectionTool
 *
 *  This state correction tool applies a dE/dx energy loss correction
 *  with the full version of the Bethe-Bloch equation.
 *
 *  @author Stephanie Hansmann-Menzemer
 *  @date   2008-05-02
 *
 */
class StateDetailedBetheBlochEnergyCorrectionTool : public extends<GaudiTool, IStateCorrectionTool> {
public:
  /// Standard constructor
  using base_class::base_class;

  StatusCode initialize() override;

  /// Correct a State for dE/dx energy losses with a simplified Bethe-Bloch equiaton
  void correctState( LHCb::State& state,
                     const Material* material,
                     double wallThickness = 0,
                     bool upstream = true,
                     LHCb::ParticleID pid = LHCb::ParticleID(211) ) const override;

private:
  // Job options
  Gaudi::Property<double> m_energyLossCorr { this,  "EnergyLossFactor",  1.0 };     ///< tunable energy loss correction
  Gaudi::Property<double> m_maxEnergyLoss { this,  "MaximumEnergyLoss", 100. * Gaudi::Units::MeV };      ///< maximum energy loss in dE/dx correction


  LHCb::IParticlePropertySvc* m_pp = nullptr;   /// particle property service

  typedef std::unordered_map<unsigned, double> PID2MassMap;
  enum class Mat { X0, C, X1, a, m, DensityFactor, LogI };
  typedef std::unordered_map<const Material*, std::tuple<
      double, double, double, double, double, double, double> >
      Material2FactorMap;

  mutable PID2MassMap m_pid2mass;
  mutable Material2FactorMap m_material2factors;
  mutable PID2MassMap::iterator m_lastCachedPID = end(m_pid2mass);
  mutable Material2FactorMap::iterator m_lastCachedMaterial = end(m_material2factors);
};
#endif // TRACKTOOLS_STATEDETAILEDBETHEBLOChENERGYCORRECTIONTOOL_H
