#ifndef TRACKTOOLS_STATETHINMSCORRECTIONTOOL_H
#define TRACKTOOLS_STATETHINMSCORRECTIONTOOL_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from TrackInterfaces
#include "TrackInterfaces/IStateCorrectionTool.h"

// from TrackEvent
#include "Event/State.h"

/** @class StateThinMSCorrectionTool StateThinMSCorrectionTool.h
 *
 *  This state correction tool applies a multiple scattering correction
 *  in the approximation of a thin scatter
 *
 *  @author Eduardo Rodrigues
 *  @date   2006-08-21
 *  (Original code taken from the master extrapolator)
 */
class StateThinMSCorrectionTool final : public extends<GaudiTool, IStateCorrectionTool> {
public:

  /// Standard constructor
  using base_class::base_class;

  /// Correct a State for multiple scattering in the approximation of a thin scatter
  void correctState( LHCb::State& state,
                     const Material* material,
                     double wallThickness = 0,
                     bool upstream = true,
                     LHCb::ParticleID pid = LHCb::ParticleID(211) ) const override;

private:
  // Job options
  Gaudi::Property<double> m_msff2{ this, "MSFudgeFactor2" , 1.0 };    ///< fudge factor for multiple scattering errors

};
#endif // TRACKTOOLS_STATETHINMSCORRECTIONTOOL_H
