#ifndef TRACKINITFIT_H
#define TRACKINITFIT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
// from TrackInterfaces
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackStateInit.h"

/** @class TrackInitFit
 *
 *
 *  @author K Holubyev
 *  @date   2009-11-14
 */
class TrackInitFit final : public extends<GaudiTool, ITrackFitter> {
public:
  /// Standard constructor
  using base_class::base_class;

  StatusCode initialize() override;

  //! fit a track
  StatusCode fit( LHCb::Track& track, LHCb::ParticleID pid = LHCb::ParticleID(211)) const override;

  // Fit a track
  virtual StatusCode operator() (LHCb::Track& track) const override {
    return fit(track);
  }
  
  // Fit a batch of tracks
  virtual void operator() (std::vector<std::reference_wrapper<LHCb::Track>>& tracks) const override {
    std::for_each(tracks.begin(), tracks.end(), [&] (LHCb::Track& track) {
      fit(track);
    });
  }

  /// fit a track
  StatusCode fit_r( LHCb::Track& track,
                    ranges::v3::any&,
                    LHCb::ParticleID pid ) const override
  {
    return fit( track, pid );
  }

private:

  Gaudi::Property<std::string> m_initToolName { this,"Init", "TrackStateInitTool" };
  Gaudi::Property<std::string> m_fitToolName { this,"Fit", "TrackMasterFitter" };

  ITrackStateInit* m_initTrack = nullptr;
  ITrackFitter* m_fitTrack = nullptr;

};
#endif // TRACKINITFIT_H
