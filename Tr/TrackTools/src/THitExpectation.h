#ifndef _THitExpectation_H
#define _THitExpectation_H

/** @class THitExpectation THitExpectation.h
 *
 * Implementation of TTHitExpectation tool
 * see interface header for description
 *
 *  @author M.Needham
 *  @date   22/5/2007
 */

#include "GaudiAlg/GaudiTool.h"
#include "TrackInterfaces/IHitExpectation.h"

#include <string>

namespace LHCb{
  class Track;
  class State;
}

struct ITrackExtrapolator;
class IMagneticFieldSvc;

class THitExpectation : public extends<GaudiTool, IHitExpectation>  {

public:

  /** constructer */
  using base_class::base_class;

  /** intialize */
  StatusCode initialize() override;

protected:

  Tf::Tsa::Parabola xParabola(const LHCb::Track& aTrack, const double z) const;

  Tf::Tsa::Line xLine(const LHCb::Track& aTrack, const double z) const;

  Tf::Tsa::Line yLine(const LHCb::Track& aTrack, const double z) const;

private:

  double curvature(const LHCb::State& aState) const;

  IMagneticFieldSvc* m_pIMF = nullptr;
  ITrackExtrapolator* m_extrapolator = nullptr;

  Gaudi::Property<std::string> m_extrapolatorName {this, "extrapolatorName", "TrackParabolicExtrapolator" };

};



#endif
