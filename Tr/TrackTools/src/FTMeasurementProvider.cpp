/** @class FTMeasurementProvider FTMeasurementProvider.cpp
 *
 * Implementation of FTMeasurementProvider
 * see interface header for description
 *
 *  @author Wouter Hulsbergen
 *  @date   30/12/2005
 */

#include "TrackInterfaces/IMeasurementProvider.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IIncidentListener.h"

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "Event/FTMeasurement.h"
#include "Event/StateVector.h"
#include "FTDet/DeFTDetector.h"
#include "Event/TrackParameters.h"
#include "TrackKernel/TrackTraj.h"
#include "Event/FTLiteCluster.h"

class FTMeasurementProvider final : public extends< GaudiTool,
                                                    IMeasurementProvider,
                                                    IIncidentListener >
{
public:

  /// constructer
  using base_class::base_class;

  StatusCode initialize() override;
  LHCb::Measurement* measurement( const LHCb::LHCbID& id,
                                  bool localY=false ) const override;
  LHCb::Measurement* measurement( const LHCb::LHCbID& id,
                                  const LHCb::ZTrajectory& refvector,
                                  bool localY=false) const override;
  inline LHCb::FTMeasurement* ftmeasurement( const LHCb::LHCbID& id ) const  ;

  void handle ( const Incident& incident ) override;
  const FastClusterContainer<LHCb::FTLiteCluster,int>* clusters() const;

  void addToMeasurements( range_of_ids ids,
                          std::vector<LHCb::Measurement*>& measurements,
                          const LHCb::ZTrajectory& reftraj) const override ;

  StatusCode load( LHCb::Track&  ) const override {
    return Error( "sorry, MeasurementProviderBase::load not implemented" );
  }

private:
  const DeFTDetector* m_det = nullptr;
  mutable FastClusterContainer<LHCb::FTLiteCluster,int>* m_clusters;
} ;

//=============================================================================
// Declare to tool factory
//=============================================================================

DECLARE_TOOL_FACTORY( FTMeasurementProvider )

//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------

StatusCode FTMeasurementProvider::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if( sc.isFailure() ) { return Error( "Failed to initialize!", sc ); }

  // Retrieve the detector element
  m_det = getDet<DeFTDetector>( DeFTDetectorLocation::Default ) ;

  // reset pointer to list of clusters at beginevent
  incSvc()->addListener(this, IncidentType::BeginEvent);

  return sc;
}

//-----------------------------------------------------------------------------
/// Handle a begin-event incidence: Make sure clusters are reloaded.
//-----------------------------------------------------------------------------

void FTMeasurementProvider::handle ( const Incident& incident )
{
  if ( IncidentType::BeginEvent == incident.type() ) m_clusters = nullptr ;
}

//-----------------------------------------------------------------------------
/// Load clusters from the TES
//-----------------------------------------------------------------------------

const FastClusterContainer<LHCb::FTLiteCluster,int>* FTMeasurementProvider::clusters() const
{
  /// If there is a new event, get the clusters and sort them according to their channel ID
  if( UNLIKELY(!m_clusters) ){
    m_clusters = getIfExists<FastClusterContainer<LHCb::FTLiteCluster,int> >
                 ( LHCb::FTLiteClusterLocation::Default );
    if(!m_clusters) error() << "Could not find FTLiteClusters at: "
                            << LHCb::FTLiteClusterLocation::Default << endmsg;
  }
  return m_clusters ;
}

//-----------------------------------------------------------------------------
/// Create a measurement
//-----------------------------------------------------------------------------
LHCb::FTMeasurement* FTMeasurementProvider::ftmeasurement( const LHCb::LHCbID& id ) const {

  if( !id.isFT() ) {
    error() << "Not an FT measurement" << endmsg ;
    return nullptr;
  }
  /// The clusters are sorted, so we can use a binary search (lower bound search)
  /// to find the element corresponding to the channel ID
  const auto& c = *clusters();
  auto itH = std::lower_bound( c.begin(),  c.end(), id.ftID(),
                      [](const LHCb::FTLiteCluster clus, const LHCb::FTChannelID id){
                        return clus.channelID() < id;
                      });
  return ( itH != c.end() ) ?  new LHCb::FTMeasurement( (*itH), *m_det ) : nullptr;
}

//-----------------------------------------------------------------------------
/// Return the measurement
//-----------------------------------------------------------------------------
LHCb::Measurement* FTMeasurementProvider::measurement( const LHCb::LHCbID& id,
                                                       bool /*localY*/ ) const {
  return ftmeasurement(id) ;
}

//-----------------------------------------------------------------------------
/// Create a measurement with statevector. For now very inefficient.
//-----------------------------------------------------------------------------

LHCb::Measurement* FTMeasurementProvider::measurement( const LHCb::LHCbID& id,
                                                       const LHCb::ZTrajectory& /* reftraj */,
                                                       bool /*localY*/ ) const {
  // default implementation
  return ftmeasurement(id) ;
}

//-----------------------------------------------------------------------------
/// Create measurements for list of LHCbIDs
//-----------------------------------------------------------------------------

void FTMeasurementProvider::addToMeasurements( range_of_ids ids,
                                               std::vector<LHCb::Measurement*>& measurements,
                                               const LHCb::ZTrajectory& reftraj) const
{
  std::transform( begin(ids), end(ids), std::back_inserter(measurements),
                  [&](const LHCb::LHCbID& id)
                  { return measurement(id,reftraj,false) ; });
}
