#ifndef TRACKTOOLS_CountVeloTracks_H
#define TRACKTOOLS_CountVeloTracks_H

//-----------------------------------------------------------------------------
/** @class CountVeloTracks CountVeloTracks.h
 *
 *  Tool for counting the distinct VELO tracks in an event
 *
 *  Based on code written by M Needham
 *
 *  @author David Hutchcroft David.Hutchcroft@cern.ch
 *
 *  @date   21/1/2011
 */
//-----------------------------------------------------------------------------

#include "GaudiAlg/GaudiTool.h"
#include "Kernel/ICountContainedObjects.h"

struct CountVeloTracks : extends<GaudiTool, ICountContainedObjects>
{

  /// constructor
  using base_class::base_class;

  /** Returns number of distinct VELO tracks contributing to the container
  *
  *  @param tracks reference to LHCb::Tracks container
  *
  *  @return number of distinct VELO tracks
  */
  unsigned int nObj ( const ObjectContainerBase * cont ) const override;

};

#endif // TRACKTOOLS_CountVeloTracks_H
