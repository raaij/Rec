#pragma once

using namespace Mem::View;

// ---------------
// get methods
// ---------------

template<>
inline const PRECISION& Node::get<Op::NodeParameters, Op::ReferenceResidual> () const {
  return *(m_nodeParameters.m_referenceResidual);
}

template<>
inline const PRECISION& Node::get<Op::NodeParameters, Op::ErrMeasure> () const {
  return *(m_nodeParameters.m_errorMeasure);
}

template<>
inline const TrackVector& Node::get<Op::NodeParameters, Op::ReferenceVector> () const {
  return m_nodeParameters.m_referenceVector;
}

template<>
inline const TrackVector& Node::get<Op::NodeParameters, Op::ProjectionMatrix> () const {
  return m_nodeParameters.m_projectionMatrix;
}

template<>
inline const PRECISION& Node::get<Op::Forward, Op::Chi2> () const {
  return m_node->forwardChi2();
}

template<>
inline const TrackVector& Node::get<Op::Forward, Op::StateVector> () const {
  return m_node->forwardStateVector();
}

template<>
inline const TrackSymMatrix& Node::get<Op::Forward, Op::Covariance> () const {
  return m_node->forwardCovariance();
}

template<>
inline const TrackMatrix<25>& Node::get<Op::Forward, Op::TransportMatrix> () const {
  return m_forwardTransportMatrix;
}

template<>
inline const PRECISION& Node::get<Op::Backward, Op::Chi2> () const {
  return m_node->backwardChi2();
}

template<>
inline const TrackVector& Node::get<Op::Backward, Op::StateVector> () const {
  return m_node->backwardStateVector();
}

template<>
inline const TrackSymMatrix& Node::get<Op::Backward, Op::Covariance> () const {
  return m_node->backwardCovariance();
}

template<>
inline const TrackMatrix<25>& Node::get<Op::Backward, Op::TransportMatrix> () const {
  return m_backwardTransportMatrix;
}

template<>
inline const TrackVector& Node::get<Op::Smooth, Op::StateVector> () const {
  return m_smoothState.m_state;
}

template<>
inline const TrackSymMatrix& Node::get<Op::Smooth, Op::Covariance> () const {
  return m_smoothState.m_covariance;
}

template<>
inline const PRECISION& Node::get<Op::Smooth, Op::Residual> () const {
  return *(m_smoothState.m_residual);
}

template<>
inline const PRECISION& Node::get<Op::Smooth, Op::ErrResidual> () const {
  return *(m_smoothState.m_errResidual);
}

// -------------------------
// ref getters for the above
// -------------------------

template<>
inline PRECISION& Node::get<Op::NodeParameters, Op::ReferenceResidual> () {
  return *(m_nodeParameters.m_referenceResidual);
}

template<>
inline PRECISION& Node::get<Op::NodeParameters, Op::ErrMeasure> () {
  return *(m_nodeParameters.m_errorMeasure);
}

template<>
inline TrackVector& Node::get<Op::NodeParameters, Op::ReferenceVector> () {
  return m_nodeParameters.m_referenceVector;
}

template<>
inline TrackVector& Node::get<Op::NodeParameters, Op::ProjectionMatrix> () {
  return m_nodeParameters.m_projectionMatrix;
}

template<>
inline PRECISION& Node::get<Op::Forward, Op::Chi2> () {
  return m_node->forwardChi2();
}

template<>
inline TrackVector& Node::get<Op::Forward, Op::StateVector> () {
  return m_node->forwardStateVector();
}

template<>
inline TrackSymMatrix& Node::get<Op::Forward, Op::Covariance> () {
  return m_node->forwardCovariance();
}

template<>
inline TrackMatrix<25>& Node::get<Op::Forward, Op::TransportMatrix> () {
  return m_forwardTransportMatrix;
}

template<>
inline PRECISION& Node::get<Op::Backward, Op::Chi2> () {
  return m_node->backwardChi2();
}

template<>
inline TrackVector& Node::get<Op::Backward, Op::StateVector> () {
  return m_node->backwardStateVector();
}

template<>
inline TrackSymMatrix& Node::get<Op::Backward, Op::Covariance> () {
  return m_node->backwardCovariance();
}

template<>
inline TrackMatrix<25>& Node::get<Op::Backward, Op::TransportMatrix> () {
  return m_backwardTransportMatrix;
}

template<>
inline TrackVector& Node::get<Op::Smooth, Op::StateVector> () {
  return m_smoothState.m_state;
}

template<>
inline TrackSymMatrix& Node::get<Op::Smooth, Op::Covariance> () {
  return m_smoothState.m_covariance;
}

template<>
inline PRECISION& Node::get<Op::Smooth, Op::Residual> () {
  return *(m_smoothState.m_residual);
}

template<>
inline PRECISION& Node::get<Op::Smooth, Op::ErrResidual> () {
  return *(m_smoothState.m_errResidual);
}
