#pragma once

#include <vector>
#include <functional>
#include "MemStore.h"
#include "MemIterator.h"
#include "MemView.h"
#include "MemViewMatrix.h"
#include "VectorConfiguration.h"
#include "AlignedAllocator.h"
#include "scalar/Math.h"

// External
#include "Event/Measurement.h"
#include "Event/OTMeasurement.h"
#include "Event/Track.h"
#include "Event/Node.h"
#include "Event/ChiSquare.h"
#include "Event/TrackFitResult.h"
#include "Event/TrackTypes.h"

#include "GaudiKernel/boost_allocator.h"
#include "GaudiKernel/VectorMap.h"
#include "LHCbMath/ValueWithError.h"
#include "LHCbMath/MatrixManip.h"
#include "LHCbMath/ValueWithError.h"
#include "Math/SMatrix.h"

namespace Tr {

namespace TrackVectorFit {

/**
 * @brief      TrackVectorFit version of TrackFitEvent::FitNode.
 */
struct FitNode : public LHCb::Node {
  Mem::View::State m_forwardState;
  Mem::View::State m_backwardState;
  mutable bool m_forwardValid  = false;
  mutable bool m_backwardValid = false;
  mutable LHCb::State m_forwardLHCbState;
  mutable LHCb::State m_backwardLHCbState;

  FitNode () = default;
  FitNode (const double& z, const LHCb::State::Location& location = LHCb::State::LocationUnknown) : LHCb::Node(z, location) {}
  FitNode (LHCb::Measurement*& meas) : LHCb::Node(meas) {}

  inline const Mem::View::TrackVector& forwardStateVector () const { return m_forwardState.m_updatedState; }
  inline const Mem::View::TrackSymMatrix& forwardCovariance () const { return m_forwardState.m_updatedCovariance; }
  inline const double& forwardChi2 () const { return *m_forwardState.m_chi2; }
  inline const Mem::View::TrackVector& backwardStateVector () const { return m_backwardState.m_updatedState; }
  inline const Mem::View::TrackSymMatrix& backwardCovariance () const { return m_backwardState.m_updatedCovariance; }
  inline const double& backwardChi2 () const { return *m_backwardState.m_chi2; }

  inline Mem::View::TrackVector& forwardStateVector () { return m_forwardState.m_updatedState; }
  inline Mem::View::TrackSymMatrix& forwardCovariance () { return m_forwardState.m_updatedCovariance; }
  inline double& forwardChi2 () { return *m_forwardState.m_chi2; }
  inline Mem::View::TrackVector& backwardStateVector () { return m_backwardState.m_updatedState; }
  inline Mem::View::TrackSymMatrix& backwardCovariance () { return m_backwardState.m_updatedCovariance; }
  inline double& backwardChi2 () { return *m_backwardState.m_chi2; }

  inline LHCb::State& forwardState () {
    if (not m_forwardValid) {
      m_forwardLHCbState = LHCb::State {
        (Gaudi::TrackVector) forwardStateVector(),
        (Gaudi::TrackSymMatrix) forwardCovariance(),
        z(),
        state().location()
      };
      m_forwardValid = true;
    }
    return m_forwardLHCbState;
  }

  inline const LHCb::State& forwardState () const {
    if (not m_forwardValid) {
      m_forwardLHCbState = LHCb::State {
        (Gaudi::TrackVector) forwardStateVector(),
        (Gaudi::TrackSymMatrix) forwardCovariance(),
        z(),
        state().location()
      };
      m_forwardValid = true;
    }
    return m_forwardLHCbState;
  }

  inline LHCb::State& backwardState () {
    if (not m_backwardValid) {
      m_backwardLHCbState = LHCb::State {
        (Gaudi::TrackVector) backwardStateVector(),
        (Gaudi::TrackSymMatrix) backwardCovariance(),
        z(),
        state().location()
      };
      m_backwardValid = true;
    }
    return m_backwardLHCbState;
  }

  inline const LHCb::State& backwardState () const {
    if (not m_backwardValid) {
      m_backwardLHCbState = LHCb::State {
        (Gaudi::TrackVector) backwardStateVector(),
        (Gaudi::TrackSymMatrix) backwardCovariance(),
        z(),
        state().location()
      };
      m_backwardValid = true;
    }
    return m_backwardLHCbState;
  }
};

/**
 * @brief      Track with smart pointers to the storage of forward / backward fitted states.
 */
class FitTrack : public LHCb::Track {
  std::shared_ptr<Mem::Store> m_forwardStore;
  std::shared_ptr<Mem::Store> m_backwardStore;
public:
  using LHCb::Track::Track;

  void assignMemManagers ( std::shared_ptr<Mem::Store>& forwardStore,
                           std::shared_ptr<Mem::Store>& backwardStore)
  {
    m_forwardStore = forwardStore;
    m_backwardStore = backwardStore;
  }
};

struct Node {
  _aligned Gaudi::TrackVector m_transportVector; // Transport vector for propagation from previous node to this one
  _aligned Gaudi::TrackSymMatrix m_noiseMatrix; // Noise in propagation from previous node to this one
  Mem::View::TrackMatrix<25> m_forwardTransportMatrix;
  Mem::View::TrackMatrix<25> m_backwardTransportMatrix;
  Mem::View::SmoothState m_smoothState;
  Mem::View::NodeParameters m_nodeParameters;

  unsigned m_index;
  int m_ndof = 0;
  int m_ndofBackward = 0;

  FitNode* m_node;
  PRECISION m_deltaEnergy = ((PRECISION) 0.0); // Change in energy in propagation from previous node to this one

  Node (FitNode* node, const unsigned& index) : m_index(index), m_node(node) {}
  Node (const Node& copy) = default;

  // Const getters
  inline const Gaudi::TrackVector& transportVector () const { return m_transportVector; }
  inline const Gaudi::TrackSymMatrix& noiseMatrix () const { return m_noiseMatrix; }
  inline const PRECISION& refResidual () const {
    assert(m_nodeParameters.m_basePointer != nullptr);
    return *m_nodeParameters.m_referenceResidual;
  }
  inline const PRECISION& deltaEnergy () const { return m_deltaEnergy; }
  inline const FitNode& node () const { return *m_node; }
  inline PRECISION chi2 () const {
    assert(m_node->m_forwardState.m_basePointer != nullptr && m_node->m_backwardState.m_basePointer != nullptr);
    return std::max(*(m_node->m_forwardState.m_chi2), *(m_node->m_backwardState.m_chi2));
  }
  inline PRECISION smoothChi2 () const {
    assert(m_smoothState.m_basePointer != nullptr);
    const double& res = *(m_smoothState.m_residual);
    const double& err = *(m_smoothState.m_errResidual);
    return (res*res) / (err*err);
  }

  // Reference getters
  inline FitNode& node () { return *m_node; }

  // Setters
  inline void setTransportVector (const Gaudi::TrackVector& transportVector) { m_transportVector = transportVector; }
  inline void setNoiseMatrix (const Gaudi::TrackSymMatrix& noiseMatrix) { m_noiseMatrix = noiseMatrix; }
  inline void setRefResidual (const PRECISION& res) {
    assert(m_nodeParameters.m_basePointer != nullptr);
    (*m_nodeParameters.m_referenceResidual) = res;
  }
  inline void setDeltaEnergy (const PRECISION& e) { m_deltaEnergy = e; }
  inline void setTransportMatrix (const Gaudi::TrackMatrix& tm) {
    m_forwardTransportMatrix.copy(tm);
  }
  inline void calculateAndSetInverseTransportMatrix (const Gaudi::TrackMatrix& tm) {
    Scalar::Math::invertMatrix(tm, m_backwardTransportMatrix);
  }
  inline void setRefVector (const Gaudi::TrackVector& rv) {
    assert(m_nodeParameters.m_basePointer != nullptr);
    m_nodeParameters.m_referenceVector.copy(rv);
  }
  inline void setProjection (
    const Gaudi::TrackProjectionMatrix& pm,
    const PRECISION& rr,
    const PRECISION& em
  ) {
    assert(m_nodeParameters.m_basePointer != nullptr);
    m_nodeParameters.m_projectionMatrix.copy(pm);
    *m_nodeParameters.m_referenceResidual = rr;
    *m_nodeParameters.m_errorMeasure = em;
  }
  inline void resetProjection () {
    assert(m_nodeParameters.m_basePointer != nullptr);
    for (unsigned i=0; i<5; ++i) {
      m_nodeParameters.m_projectionMatrix[i] = 0.0;
    }
    *m_nodeParameters.m_referenceResidual = 0.0;
    *m_nodeParameters.m_errorMeasure = 0.0;
  }

  // The class U type specifier is conditional to S
  // One could do the same with a partially specialized class,
  // but here we want a function
  template<class R, class S,
    class U =
      typename std::conditional<std::is_same<S, Op::Covariance>::value,       Mem::View::TrackSymMatrix,
      typename std::conditional<std::is_same<S, Op::StateVector>::value,      Mem::View::TrackVector,
      typename std::conditional<std::is_same<S, Op::ReferenceVector>::value,  Mem::View::TrackVector,
      typename std::conditional<std::is_same<S, Op::ProjectionMatrix>::value, Mem::View::TrackVector,
      typename std::conditional<std::is_same<S, Op::TransportMatrix>::value,  Mem::View::TrackMatrix<25>, PRECISION>::type>::type>::type>::type>::type
  >
  inline const U& get () const;

  template<class R, class S,
    class U =
      typename std::conditional<std::is_same<S, Op::Covariance>::value,       Mem::View::TrackSymMatrix,
      typename std::conditional<std::is_same<S, Op::StateVector>::value,      Mem::View::TrackVector,
      typename std::conditional<std::is_same<S, Op::ReferenceVector>::value,  Mem::View::TrackVector,
      typename std::conditional<std::is_same<S, Op::ProjectionMatrix>::value, Mem::View::TrackVector,
      typename std::conditional<std::is_same<S, Op::TransportMatrix>::value,  Mem::View::TrackMatrix<25>, PRECISION>::type>::type>::type>::type>::type
  >
  inline U& get ();
};

#include "NodeGetters.h"

struct Track {
  std::vector<Node, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>> m_nodes;

  // Active measurements from this node
  unsigned m_forwardUpstream;
  unsigned m_backwardUpstream;

  // Chi square of track
  PRECISION m_forwardFitChi2 = ((PRECISION) 0.0);
  PRECISION m_backwardFitChi2 = ((PRECISION) 0.0);
  PRECISION m_savedChi2 = ((PRECISION) 0.0);

  // Some other things
  int m_ndof = 0;
  int m_savedndof = 0;
  int m_nTrackParameters;
  bool m_savedPrefit;
  unsigned m_iterationsToConverge;
  unsigned m_numberOfOutlierIterations;

  // Initial covariances
  Gaudi::TrackSymMatrix m_seedCovariance;

  LHCb::Track* m_track;
  unsigned m_index;

  // TODO Move this to the fit level: One can just issue one std::copy_n, and just copy a big chunk of memory
  //      from a container of just smoothed states to a container of just refvectors
  inline void updateRefVectors () {
    std::for_each(m_nodes.begin(), m_nodes.end(), [] (Node& n) {
      n.get<Op::NodeParameters, Op::ReferenceVector>().copy(n.get<Op::Smooth, Op::StateVector>());
    });
  }

  inline void initializeForwardUpstream () {
    m_forwardUpstream = 0;
    for (unsigned i=0; i<m_nodes.size(); ++i) {
      if (m_nodes[i].node().type() == LHCb::Node::HitOnTrack) {
        m_forwardUpstream = i;
        return;
      }
    }
  }

  inline void initializeBackwardUpstream () {
    m_backwardUpstream = m_nodes.size() - 1;
    for (unsigned i=0; i<m_nodes.size(); ++i) {
      const unsigned reverse_i = m_nodes.size() - 1 - i;
      if (m_nodes[reverse_i].node().type() == LHCb::Node::HitOnTrack) {
        m_backwardUpstream = i;
        return;
      }
    }
  }

  Track (LHCb::Track& track, const unsigned& trackIndex)
  : m_track(&track), m_index(trackIndex) {
    // Generate VectorFit nodes
    unsigned nodeIndex = 0;
    m_nodes.reserve(m_track->fitResult()->nodes().size());
    for (auto*& node : m_track->fitResult()->nodes()) {
      m_nodes.emplace_back(static_cast<FitNode*>(node), nodeIndex++);
    }

    // Find the first HitOnTrack node for forward and backward
    initializeForwardUpstream();
    initializeBackwardUpstream();
  }

  Track (LHCb::Track& track) : Track(track, 0) {}

  Track (const Track& track) = default;

  inline void updateUpstream () {
    // A node became an outlier. Find out if this
    // has any impact on the upstream defined nodes.
    if (m_nodes[m_forwardUpstream].node().type() == LHCb::Node::Outlier) {
      initializeForwardUpstream();
    }

    if (m_nodes[m_nodes.size() - 1 - m_backwardUpstream].node().type() == LHCb::Node::Outlier) {
      initializeBackwardUpstream();
    }
  }

  inline const LHCb::Track& track () const {
    return *m_track;
  }

  inline LHCb::Track& track () {
    return *m_track;
  }

  inline std::vector<Node, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>&
  nodes () {
    return m_nodes;
  }

  inline const std::vector<Node, aligned_allocator<PRECISION, sizeof(PRECISION)*VECTOR_WIDTH>>&
  nodes () const {
    return m_nodes;
  }

  inline void setSeedCovariance (const Gaudi::TrackSymMatrix& seedCovariance) {
    m_seedCovariance = seedCovariance;
  }

  inline void setNTrackParameters (const int& nTrackParameters) {
    m_nTrackParameters = nTrackParameters;
  }

  inline int nTrackParameters () const {
    return m_nTrackParameters;
  }

  inline PRECISION chi2 () const {
    return std::max(m_forwardFitChi2, m_backwardFitChi2);
  }

  inline PRECISION minChi2 () const {
    return std::min(m_forwardFitChi2, m_backwardFitChi2);
  }

  inline int ndof () const {
    return m_ndof;
  }

  /**
   * @brief Saves the current chi2 for posterior checks.
   */
  inline void saveChi2 () {
    m_savedChi2 = track().chi2();
    m_savedndof = m_ndof;
  }

  inline PRECISION chi2Difference () const {
    return std::abs(m_savedChi2 - track().chi2());
  }

  inline bool isPrefit () const {
    return std::any_of(m_nodes.begin(), m_nodes.end(), [] (const Node& n) {
      const auto& node = n.node();

      if (not node.hasMeasurement() or node.measurement().type() != LHCb::Measurement::OT) {
        return false;
      }

      const auto* otmeas = dynamic_cast<const LHCb::OTMeasurement*>(&(node.measurement())) ;
      return otmeas and otmeas->driftTimeStrategy() == LHCb::OTMeasurement::PreFit;
    });
  }

  inline void savePrefit () {
    m_savedPrefit = isPrefit();
  }

  inline bool wasPrefit () const {
    return m_savedPrefit;
  }

  inline void setIterationsToConverge (const unsigned& i) {
    m_iterationsToConverge = i;
  }

  inline void saveNumberOfOutlierIterations (const unsigned& n) {
    m_numberOfOutlierIterations = n;
  }

  inline unsigned numberOfOutlierIterations () {
    return m_numberOfOutlierIterations;
  }

  /**
   * @brief Calculates the intermediate chi squares and updates the TrackFitResult object.
   * @details This routine calculates the chisquare contributions from
   *          different segments of the track. It uses the chisquare
   *          contributions from the bi-directional kalman fit. Summing these
   *          leads to a real chisquare only if the contributions are
   *          uncorrelated. For a Velo-TT-T track you can then calculate:
   *
   *          - the chisquare of the T segment and the T-TT segment by using the
   *          'upstream' contributions
   *
   *          - the chisquare of the Velo segment and the Velo-TT segment by
   *          using the 'downstream' contributions
   *
   *          Note that you cannot calculate the contribution of the TT segment
   *          seperately (unless there are no T or no Velo hits). Also, if
   *          there are Muon hits, you cannot calculate the T station part, so
   *          for now this only works for tracks without muon hits.
   */
  inline void calculateChi2Types () {
    LHCb::TrackFitResult& fitResult = *(track().fitResult());

    auto veloBegin = m_nodes.end();
    auto ttBegin = m_nodes.end();
    auto tBegin = m_nodes.end();
    auto muonBegin = m_nodes.end();
    auto veloEnd = m_nodes.end();
    auto ttEnd = m_nodes.end();
    auto tEnd = m_nodes.end();
    auto muonEnd = m_nodes.end();

    for (auto it = m_nodes.begin(); it != m_nodes.end(); ++it) {
      auto& n = *it;
      if (n.node().type() == LHCb::Node::HitOnTrack) {
        switch (n.node().measurement().type()) {
          case LHCb::Measurement::VP:
          case LHCb::Measurement::VeloR:
          case LHCb::Measurement::VeloPhi:
          case LHCb::Measurement::VeloLiteR:
          case LHCb::Measurement::VeloLitePhi:
          case LHCb::Measurement::Origin:
            if (veloBegin == m_nodes.end()) {
              veloBegin = it;
            }
            veloEnd = it;
            break;

          case LHCb::Measurement::TT:
          case LHCb::Measurement::TTLite:
          case LHCb::Measurement::UT:
          case LHCb::Measurement::UTLite:
            if (ttBegin == m_nodes.end()) {
              ttBegin = it;
            }
            ttEnd = it;
            break;

          case LHCb::Measurement::OT:
          case LHCb::Measurement::IT:
          case LHCb::Measurement::ITLite:
          case LHCb::Measurement::FT:
            if (tBegin == m_nodes.end()) {
              tBegin = it;
            }
            tEnd = it;
            break;

          case LHCb::Measurement::Muon:
            if (muonBegin == m_nodes.end()) {
              muonBegin = it;
            }
            muonEnd = it;
            break;

          case LHCb::Measurement::Unknown:
          case LHCb::Measurement::Calo:
          default:
            break;

        }
      }
    }

    const bool upstream = m_nodes.front().node().z() > m_nodes.back().node().z();
    auto chilambda = [] (const bool& upstream, const decltype(muonBegin)& itBackward, const decltype(muonBegin)& itForward) {
      return upstream ?
        LHCb::ChiSquare(itForward->get<Op::Forward, Op::Chi2>(), itForward->m_ndof) :
        LHCb::ChiSquare(itBackward->get<Op::Backward, Op::Chi2>(), itBackward->m_ndofBackward);
    };

    // Set all the ChiSquare objects in TrackFitResult
    fitResult.setChi2(LHCb::ChiSquare(chi2(), ndof()));

    if (muonBegin != m_nodes.end()) fitResult.setChi2Muon(chilambda(upstream, muonBegin, muonEnd));
    else                            fitResult.setChi2Muon(LHCb::ChiSquare(0.0, 0));
    if (tBegin != m_nodes.end())    fitResult.setChi2Downstream(chilambda(upstream, tBegin, tEnd));
    else                            fitResult.setChi2Downstream(fitResult.chi2Muon());
    if (veloBegin != m_nodes.end()) fitResult.setChi2Velo(chilambda(not upstream, veloBegin, veloEnd));
    else                            fitResult.setChi2Velo(LHCb::ChiSquare(0.0, 0));
    if (ttBegin != m_nodes.end())   fitResult.setChi2Upstream(chilambda(not upstream, ttBegin, ttEnd));
    else                            fitResult.setChi2Upstream(fitResult.chi2Velo());
    if (tBegin != m_nodes.end())    fitResult.setChi2Long(chilambda(not upstream, tBegin, tEnd));
    else                            fitResult.setChi2Long(fitResult.chi2Upstream());
  }
};

namespace Mem {

namespace View {

template<unsigned long N>
inline std::ostream& operator<< (std::ostream& s, const TrackMatrix<N>& v) {
  for (unsigned i=0; i<N; ++i) {
    s << v[i];
    if (i != N-1) s << " ";
  }
  return s;
}

inline std::ostream& operator<< (std::ostream& s, const TrackVector& v) {
  if (v.m_basePointer != nullptr) {
    for (unsigned i=0; i<5; ++i) {
      s << v[i];
      if (i!=4) s << " ";
    }
  } else {
    s << "uninitialized";
  }
  return s;
}

inline std::ostream& operator<< (std::ostream& s, const TrackSymMatrix& v) {
  if (v.m_basePointer != nullptr) {
    for (unsigned i=0; i<15; ++i) {
      s << v[i];
      if (i!=14) s << " ";
    }
  } else {
    s << "uninitialized";
  }
  return s;
}

}

}

inline std::ostream& operator<< (std::ostream& s, const Node& n) {
  const auto& node = n.node();

  s << "Node " << "#" << n.m_index << "\n";
  if (n.node().m_forwardState.m_basePointer != nullptr) {
  s << " Forward state: " << n.get<Tr::TrackVectorFit::Op::Forward, Tr::TrackVectorFit::Op::StateVector>()
    << ", covariance: " << n.get<Tr::TrackVectorFit::Op::Forward, Tr::TrackVectorFit::Op::Covariance>()
    << ", chi2: " << n.get<Tr::TrackVectorFit::Op::Forward, Tr::TrackVectorFit::Op::Chi2>() << "\n";
  }
  if (n.node().m_backwardState.m_basePointer != nullptr) {
  s << " Backward state: " << n.get<Tr::TrackVectorFit::Op::Backward, Tr::TrackVectorFit::Op::StateVector>()
    << ", covariance: " << n.get<Tr::TrackVectorFit::Op::Backward, Tr::TrackVectorFit::Op::Covariance>()
    << ", chi2: " << n.get<Tr::TrackVectorFit::Op::Backward, Tr::TrackVectorFit::Op::Chi2>() << "\n";
  }
  if (n.m_smoothState.m_basePointer != nullptr) {
  s << " Smoothed state: " << n.get<Tr::TrackVectorFit::Op::Smooth, Tr::TrackVectorFit::Op::StateVector>()
    << ", covariance: " << n.get<Tr::TrackVectorFit::Op::Smooth, Tr::TrackVectorFit::Op::Covariance>() << "\n"
    << " Total chi2: " << n.get<Tr::TrackVectorFit::Op::Forward, Tr::TrackVectorFit::Op::Chi2>() << ", "
      << n.get<Tr::TrackVectorFit::Op::Backward, Tr::TrackVectorFit::Op::Chi2>() << "\n";
  }
  s << " hasInfoUpstream: -, -\n";

  s << " Node"
    << " type " << node.type();
  if (node.hasMeasurement()) {
    s << " measurement of type " << node.measurement().type()
      << " with LHCbID " << node.measurement().lhcbID().channelID();
  }
  s << " at z " << node.z() << "\n"
    << " transport matrix ";
  if (n.get<Tr::TrackVectorFit::Op::Forward, Tr::TrackVectorFit::Op::TransportMatrix>().m_basePointer != nullptr) {
    s << n.get<Tr::TrackVectorFit::Op::Forward, Tr::TrackVectorFit::Op::TransportMatrix>();
  }
  s << "\n inverse transport matrix ";
  if (n.get<Tr::TrackVectorFit::Op::Backward, Tr::TrackVectorFit::Op::TransportMatrix>().m_basePointer != nullptr) {
    s << n.get<Tr::TrackVectorFit::Op::Backward, Tr::TrackVectorFit::Op::TransportMatrix>();
  }
  if (n.m_smoothState.m_basePointer != nullptr) {
    s << "\n residual " << n.get<Tr::TrackVectorFit::Op::Smooth, Tr::TrackVectorFit::Op::Residual>()
      << " errResidual " << n.get<Tr::TrackVectorFit::Op::Smooth, Tr::TrackVectorFit::Op::ErrResidual>();
  }
  if (n.m_nodeParameters.m_basePointer != nullptr) {
    s << " projectionMatrix " << n.get<Tr::TrackVectorFit::Op::NodeParameters, Tr::TrackVectorFit::Op::ProjectionMatrix>()
      << " refVector " << n.get<Tr::TrackVectorFit::Op::NodeParameters, Tr::TrackVectorFit::Op::ReferenceVector>()
      << " refResidual " << n.get<Tr::TrackVectorFit::Op::NodeParameters, Tr::TrackVectorFit::Op::ReferenceResidual>()
      << " errMeasure " << n.get<Tr::TrackVectorFit::Op::NodeParameters, Tr::TrackVectorFit::Op::ErrMeasure>();
  }

  return s;
}

}

}

template<class T, unsigned long N>
inline std::ostream& operator<< (std::ostream& s, const std::array<T, N>& v) {
  for (unsigned i=0; i<N; ++i) {
    s << v[i];
    if (i != N-1) s << " ";
  }
  return s;
}
