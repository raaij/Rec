#pragma once

#include <array>
#include "VectorConfiguration.h"
#include "Math/SVector.h"

#include "LHCbMath/MatrixManip.h"
#include "Event/StateVector.h"

namespace Tr {

namespace TrackVectorFit {

namespace Mem {

namespace View {

template<unsigned long N>
struct TrackMatrix {
  constexpr static unsigned size = N;
  PRECISION* m_basePointer = 0x0;
  TrackMatrix () = default;
  TrackMatrix (PRECISION* m_basePointer) : m_basePointer(m_basePointer) {}
  inline void setBasePointer (const TrackMatrix<N>& v) { m_basePointer = v.m_basePointer; }
  inline void setBasePointer (PRECISION* m_basePointer) { this->m_basePointer = m_basePointer; }
  inline PRECISION& operator[] (const unsigned i) { return m_basePointer[i * VECTOR_WIDTH]; }
  inline PRECISION operator[] (const unsigned i) const { return m_basePointer[i * VECTOR_WIDTH]; }

  /**
   * @brief      Copies v into its state
   *             Assumes m_basePointer is well defined
   */
  inline void copy (const TrackMatrix<N>& m) {
    for (unsigned i=0; i<N; ++i) {
      this->operator[](i) = m[i];
    }
  }

  inline void operator+= (const TrackMatrix<N>& m) {
    for (unsigned i=0; i<N; ++i) {
      this->operator[](i) += m[i];
    }
  }

  template<class __GAUDITYPE>
  inline void copy (const __GAUDITYPE& gm) {
    for (unsigned i=0; i<N; ++i) {
      this->operator[](i) = gm.Array()[i];
    }
  }

  template<class __GAUDITYPE>
  inline void operator+= (const __GAUDITYPE& gm) {
    for (unsigned i=0; i<N; ++i) {
      this->operator[](i) += gm.Array()[i];
    }
  }
};

struct TrackVector : public TrackMatrix<5> {
  TrackVector () = default;
  TrackVector (PRECISION* m_basePointer) : TrackMatrix<5>(m_basePointer) {}

  inline void copy (const TrackVector& v) { TrackMatrix<5>::copy(static_cast<TrackMatrix<5>>(v)); }
  inline void operator+= (const TrackVector& v) { TrackMatrix<5>::operator+=(static_cast<TrackMatrix<5>>(v)); }

  inline void copy (const Gaudi::TrackVector& v) { TrackMatrix<5>::template copy<Gaudi::TrackVector>(v); }
  inline void copy (const Gaudi::TrackProjectionMatrix& v) { TrackMatrix<5>::template copy<Gaudi::TrackProjectionMatrix>(v); }
  inline void operator+= (const Gaudi::TrackVector& v) { TrackMatrix<5>::template operator+=<Gaudi::TrackVector>(v); }
  
  inline operator Gaudi::TrackVector () const {
    return Gaudi::TrackVector{this->operator[](0), this->operator[](1), this->operator[](2), this->operator[](3), this->operator[](4)};
  }

  inline operator Gaudi::TrackProjectionMatrix () const {
    Gaudi::TrackProjectionMatrix pm;
    for (unsigned i=0; i<5; ++i) {
      pm.Array()[i] = this->operator[](i);
    }
    return pm;
  }
};

struct TrackSymMatrix : public TrackMatrix<15> {
  TrackSymMatrix () = default;
  TrackSymMatrix (PRECISION* m_basePointer) : TrackMatrix<15>(m_basePointer) {}

  inline void copy (const TrackSymMatrix& v) { TrackMatrix<15>::copy(static_cast<TrackMatrix<15>>(v)); }
  inline void operator+= (const TrackSymMatrix& v) { TrackMatrix<15>::operator+=(static_cast<TrackMatrix<15>>(v)); }
  
  inline void copy (const Gaudi::TrackSymMatrix& v) { TrackMatrix<15>::template copy<Gaudi::TrackSymMatrix>(v); }
  inline void operator+= (const Gaudi::TrackSymMatrix& v) { TrackMatrix<15>::template operator+=<Gaudi::TrackSymMatrix>(v); }
  
  inline PRECISION& operator() (const unsigned row, const unsigned col) {
    return row>col ?
      m_basePointer[(row*(row+1)/2 + col) * VECTOR_WIDTH] :
      m_basePointer[(col*(col+1)/2 + row) * VECTOR_WIDTH];
  }

  inline PRECISION operator() (const unsigned row, const unsigned col) const {
    return row>col ?
      m_basePointer[(row*(row+1)/2 + col) * VECTOR_WIDTH] :
      m_basePointer[(col*(col+1)/2 + row) * VECTOR_WIDTH];
  }
  
  inline operator Gaudi::TrackSymMatrix () const {
    Gaudi::TrackSymMatrix t;
    for (unsigned i=0; i<15; ++i) {
      t.Array()[i] = this->operator[](i);
    }
    return t;

    // TODO - Find a suitable constructor in place
    // return Gaudi::TrackSymMatrix(
    //   ROOT::Math::SVector<PRECISION, 15>{
    //     this->operator[](0), this->operator[](1), this->operator[](2), this->operator[](3), this->operator[](4),
    //     this->operator[](5), this->operator[](6), this->operator[](7), this->operator[](8), this->operator[](9),
    //     this->operator[](10), this->operator[](11), this->operator[](12), this->operator[](13), this->operator[](14)
    //   }
    // );
  }
};

// Some operators
inline Gaudi::TrackVector operator- (const Gaudi::TrackVector& mA, const TrackVector& mB) {
  const PRECISION* A = mA.Array();
  return Gaudi::TrackVector {
    A[0]-mB[0],
    A[1]-mB[1],
    A[2]-mB[2],
    A[3]-mB[3],
    A[4]-mB[4]
  };
}

}

}

}

}
