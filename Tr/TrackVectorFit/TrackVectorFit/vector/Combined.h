#pragma once

#include "../Types.h"
#include "Predict.h"
#include "Update.h"

namespace Tr {

namespace TrackVectorFit {

namespace Vector {

template<class T>
struct fit {
  template<unsigned W>
  static inline void op (
    std::array<Sch::Item, W>& n,
    fp_ptr_64_const rv,
    fp_ptr_64_const pm,
    fp_ptr_64_const rr,
    fp_ptr_64_const em,
    fp_ptr_64_const tm,
    fp_ptr_64_const last_us,
    fp_ptr_64_const last_uc,
    fp_ptr_64 us,
    fp_ptr_64 uc,
    fp_ptr_64 chi2
  ) {
    predict<T>::template op<W> (
      n,
      tm,
      last_us,
      last_uc,
      us,
      uc
    );

    update<W> (
      n,
      rv,
      pm,
      rr,
      em,
      us,
      uc,
      chi2
    );
  }
};

}

}

}
