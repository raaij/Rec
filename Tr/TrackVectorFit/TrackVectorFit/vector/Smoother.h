#pragma once

#include "../Types.h"
#include "Math.h"

namespace Tr {

namespace TrackVectorFit {

namespace Vector {

template<long unsigned W>
inline uint16_t smoother (
  fp_ptr_64_const s1,
  fp_ptr_64_const s2,
  fp_ptr_64_const c1,
  fp_ptr_64_const c2,
  fp_ptr_64 ss,
  fp_ptr_64 sc
) {
  return MathCommon<W>::average (
    s1,
    c1,
    s2,
    c2,
    ss,
    sc
  );
}

template<long unsigned W>
inline void updateResiduals (
  const std::array<Sch::Item, W>& n,
  fp_ptr_64_const rv,
  fp_ptr_64_const pm,
  fp_ptr_64_const rr,
  fp_ptr_64_const em,
  fp_ptr_64_const ss,
  fp_ptr_64_const sc,
  fp_ptr_64 res,
  fp_ptr_64 errRes
) {
  MathCommon<W>::updateResiduals (
    n,
    rv,
    pm,
    rr,
    em,
    ss,
    sc,
    res,
    errRes
  );
}

}

}

}
