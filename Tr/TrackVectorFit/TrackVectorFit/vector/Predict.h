#pragma once

#include "../Types.h"
#include "../ArrayGen.h"
#include "Math.h"

namespace Tr {

namespace TrackVectorFit {

namespace Vector {

template<class T>
struct predict {
  template<unsigned W>
  static inline void op (
    const std::array<Sch::Item, W>& n,
    fp_ptr_64_const tm,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  );
};

template<>
struct predict<Op::Forward> {
  template<unsigned W>
  static inline void op (
    const std::array<Sch::Item, W>& n,
    fp_ptr_64_const tm,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  ) {
    _aligned const std::array<PRECISION, 15*W> nm = ArrayGen::getCurrentNoiseMatrix(n);
    _aligned const std::array<PRECISION, 5*W> tv = ArrayGen::getCurrentTransportVector(n);

    Math<Op::Forward>::predictState<W> (
      tm,
      tv,
      us,
      ps
    );

    Math<Op::Forward>::predictCovariance<W> (
      tm,
      nm,
      uc,
      pc
    );
  }
};

template<>
struct predict<Op::Backward> {
  template<unsigned W>
  static inline void op (
    const std::array<Sch::Item, W>& n,
    fp_ptr_64_const tm,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  ) {
    _aligned const std::array<PRECISION, 5*W> tv = ArrayGen::getPreviousTransportVector(n);
    _aligned std::array<PRECISION, 15*W> nm = ArrayGen::getPreviousNoiseMatrix(n);

    Math<Op::Backward>::predictState<W> (
      tm,
      tv,
      us,
      ps
    );

    Math<Op::Backward>::predictCovariance<W> (
      tm,
      nm,
      uc,
      pc
    );
  }
};

}

}

}
