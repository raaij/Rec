#pragma once

#include "../Types.h"
#include "Math.h"

namespace Tr {

namespace TrackVectorFit {

namespace Vector {

template<unsigned W>
inline void update (
  const std::array<Sch::Item, W>& n,
  fp_ptr_64_const rv,
  fp_ptr_64_const pm,
  fp_ptr_64_const rr,
  fp_ptr_64_const em,
  fp_ptr_64 us,
  fp_ptr_64 uc,
  fp_ptr_64 chi2
) {
  MathCommon<W>::update (
    rv,
    pm,
    rr,
    em,
    us,
    uc,
    chi2,
    n
  );
}

}

}

}
