#pragma once

#include <vector>
#include <array>
#include <iostream>
#include "MemStore.h"
#include "MemIterator.h"
#include "MemView.h"
#include "Types.h"
#include "Scheduler.h"
#include "scalar/Predict.h"
#include "scalar/Update.h"
#include "scalar/Combined.h"
#include "scalar/Smoother.h"
#include "vector/Predict.h"
#include "vector/Update.h"
#include "vector/Combined.h"
#include "vector/Smoother.h"

namespace Tr {

namespace TrackVectorFit {

struct TrackVectorFit {
  unsigned m_memManagerStorageIncrements;
  bool m_debug;
  // The three schedulers
  std::list<Sch::Blueprint<VECTOR_WIDTH>> m_scheduler_pre;
  std::list<Sch::Blueprint<VECTOR_WIDTH>> m_scheduler_main;
  std::list<Sch::Blueprint<VECTOR_WIDTH>> m_scheduler_post;
  // VectorFit MemManagers local to this TrackVectorFit
  Mem::Store m_smoothStore;
  Mem::Store m_nodeParametersStore;
  Mem::Store m_transportForwardStore;
  Mem::Store m_transportBackwardStore;
  Mem::Store m_auxStore;
  Mem::Iterator m_forwardNodeIterator;
  Mem::Iterator m_forwardStoreIterator;
  Mem::Iterator m_smoothStoreIterator;
  Mem::ReverseIterator m_forwardMainStoreIterator;
  // VectorFit MemManagers with track ownership
  std::shared_ptr<Mem::Store> m_forwardStore;
  std::shared_ptr<Mem::Store> m_backwardStore;

  TrackVectorFit (
    const unsigned& memManagerStorageIncrements = 1024,
    const bool& debug = false
  );

  void operator() (
    Track& track
  );

  void operator() (
    std::list<std::reference_wrapper<Track>>& tracks
  );

  inline void resetStores () {
    // Reserve new node store
    m_forwardStore = std::make_shared<Mem::Store>(Mem::Store(Mem::View::State::size, m_memManagerStorageIncrements));
    m_backwardStore = std::make_shared<Mem::Store>(Mem::Store(Mem::View::State::size, m_memManagerStorageIncrements));

    // Only reset stores not resetted in initializeBasePointers
    m_smoothStore.reset();
    m_auxStore.reset();
    m_nodeParametersStore.reset();
  }

  inline void initializeBasePointers (
    std::list<std::reference_wrapper<Track>>& tracks
  ) {
    m_transportForwardStore.reset();
    m_transportBackwardStore.reset();

    if (tracks.size() >= VECTOR_WIDTH) {
      // Schedule what we are going to do
      m_scheduler_pre  = Sch::StaticScheduler<Sch::Pre, VECTOR_WIDTH>::generate(tracks);
      m_scheduler_main = Sch::StaticScheduler<Sch::Main, VECTOR_WIDTH>::generate(tracks);
      m_scheduler_post = Sch::StaticScheduler<Sch::Post, VECTOR_WIDTH>::generate(tracks);

      // Populates the base pointers with the scheduler provided
      auto populateBasePointers =
      [&] (
        decltype(m_scheduler_pre)& sch,
        const bool& populateForward,
        const bool& populateBackward
      ) {
        std::for_each(sch.begin(), sch.end(), [&] (Sch::Blueprint<VECTOR_WIDTH>& s) {
          auto& pool = s.pool;
          const auto& action = s.action;

          m_nodeParametersStore.getNewVector();
          m_forwardStore->getNewVector();
          m_backwardStore->getNewVector();
          m_smoothStore.getNewVector();
          if (populateForward)  { m_transportForwardStore.getNewVector(); }
          if (populateBackward) { m_transportBackwardStore.getNewVector(); }

          for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
            if (action[i]) {
              pool[i].node->m_nodeParameters.setBasePointer(m_nodeParametersStore.getLastVector() + i);
              pool[i].node->node().m_forwardState.setBasePointer(m_forwardStore->getLastVector() + i);
              pool[i].node->node().m_backwardState.setBasePointer(m_backwardStore->getLastVector() + i);
              // Note: Do NOT update smooth pointer here (we still need that)

              if (populateForward) {
                pool[i].node->get<Op::Forward, Op::TransportMatrix>().setBasePointer(m_transportForwardStore.getLastVector() + i);
              }

              if (populateBackward) {
                pool[i].node->get<Op::Backward, Op::TransportMatrix>().setBasePointer(m_transportBackwardStore.getLastVector() + i);
              }
            }
          }
        });
      };

      // Align the stores to point to new vectors, and initialize iterator attributes
      m_forwardStore->align();
      m_backwardStore->align();
      m_smoothStore.align();
      m_nodeParametersStore.align();
      m_forwardNodeIterator = Mem::Iterator(m_nodeParametersStore, true);
      m_forwardStoreIterator = Mem::Iterator(*m_forwardStore, true);
      m_smoothStoreIterator = Mem::Iterator(m_smoothStore, true);

      // Populate all pointers
      populateBasePointers(m_scheduler_pre, false, true);
      // Add one extra vector to avoid swapping out elements
      m_forwardStore->getNewVector();
      m_backwardStore->getNewVector();
      populateBasePointers(m_scheduler_main, true, true);
      // Initialize the reverse mainstore iterators here
      m_forwardMainStoreIterator = Mem::ReverseIterator(*m_forwardStore);
      // Add one extra vector to avoid swapping out elements
      m_forwardStore->getNewVector();
      m_backwardStore->getNewVector();
      populateBasePointers(m_scheduler_post, true, false);
    } else {
      // Align the stores to point to new vectors, and initialize iterator attributes
      m_forwardStore->align();
      m_backwardStore->align();
      m_smoothStore.align();
      m_nodeParametersStore.align();
      m_forwardNodeIterator = Mem::Iterator(m_nodeParametersStore, true);
      m_forwardStoreIterator = Mem::Iterator(*m_forwardStore, true);
      m_smoothStoreIterator = Mem::Iterator(m_smoothStore, true);

      // Just populate the base pointers of all nodes
      std::for_each(tracks.begin(), tracks.end(), [&] (Track& track) {
        std::for_each(track.m_nodes.begin(), track.m_nodes.end(), [&] (Node& node) {
          node.m_nodeParameters.setBasePointer(m_nodeParametersStore.getNextElement());
          node.get<Op::Forward, Op::TransportMatrix>().setBasePointer(m_transportForwardStore.getNextElement());
          node.get<Op::Backward, Op::TransportMatrix>().setBasePointer(m_transportBackwardStore.getNextElement());
          node.node().m_forwardState.setBasePointer(m_forwardStore->getNextElement());
          node.node().m_backwardState.setBasePointer(m_backwardStore->getNextElement());
          m_smoothStore.getNextElement();
          // Note: Do NOT update the smooth pointer here (we still need that)
        });
      });
    }
  }

  void populateNodes (
    Track& track
  );
};

}

}
