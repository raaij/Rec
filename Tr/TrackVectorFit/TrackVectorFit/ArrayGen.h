#pragma once

#include <type_traits>
#include "Types.h"
#include "Scheduler.h"
#include "VectorConfiguration.h"
#include "SimdTransposeHelper.h"

// ------------------
// Array constructors
// ------------------

namespace Tr {

namespace TrackVectorFit {

struct ArrayGen {
  template<size_t W>
  static inline constexpr uint16_t mask () {
    return (W==16 ? 0xFFFF : (W==8 ? 0xFF : (W==4 ? 0x0F : 0x03)));
  }
  
  template<size_t W>
  static inline constexpr std::array<PRECISION, 5*W> getPreviousTransportVector (
    const std::array<Sch::Item, W>& nodes
  ) {
    return transpose_helper<5>::get<W>([&nodes](int j){ return (nodes[j].node+1)->transportVector().Array(); });
  }

  template<size_t W>
  static inline constexpr std::array<PRECISION, 5*W> getCurrentTransportVector (
    const std::array<Sch::Item, W>& nodes
  ) {
    return transpose_helper<5>::get<W>([&nodes](int j){ return nodes[j].node->transportVector().Array(); });
  }

  template<size_t W>
  static inline constexpr std::array<PRECISION, 15*W> getPreviousNoiseMatrix (
    const std::array<Sch::Item, W>& nodes
  ) {
    return transpose_helper<15>::get<W>([&nodes](int j){ return (nodes[j].node+1)->noiseMatrix().Array(); });
  }

  template<size_t W>
  static inline constexpr std::array<PRECISION, 15*W> getCurrentNoiseMatrix (
    const std::array<Sch::Item, W>& nodes
  ) {
    return transpose_helper<15>::get<W>([&nodes](int j){ return nodes[j].node->noiseMatrix().Array(); });
  }
};

}

}
