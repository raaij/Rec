#pragma once

#include "../Types.h"
#include "Math.h"

namespace Tr {

namespace TrackVectorFit {

namespace Scalar {

inline void transportCovariance (
  const TrackMatrix<25>& tm,
  const TrackSymMatrix& uc,
  TrackSymMatrix& pc
) {
  bool isLine = tm[4]==0;
  if (!isLine) {
    Math::similarity_5_5(tm, uc, pc);
  } else {
    pc.copy(uc);

    if (tm[2] != 0 or tm[8] != 0) {
      pc(0,0) += 2 * uc(2,0) * tm[2] + uc(2,2) * tm[2] * tm[2];
      pc(2,0) += uc(2,2) * tm[2];
      pc(1,1) += 2 * uc(3,1) * tm[8] + uc(3,3) * tm[8] * tm[8];
      pc(3,1) += uc(3,3) * tm[8];
      pc(1,0) += uc(2,1) * tm[2] + uc(3,0) * tm[8] + uc(3,2) * tm[2] * tm[8];
      pc(2,1) += uc(3,2) * tm[8];
      pc(3,0) += uc(3,2) * tm[2];
    }
  }
}

inline void transportCovariance (
  const TrackMatrix<25>& tm,
  const Gaudi::TrackSymMatrix& uc,
  TrackSymMatrix& pc
) {
  bool isLine = tm[4]==0;
  if (!isLine) {
    Math::similarity_5_5(tm, uc.Array(), pc);
  } else {
    pc.copy(uc);

    if (tm[2] != 0 or tm[8] != 0) {
      pc(0,0) += 2 * uc(2,0) * tm[2] + uc(2,2) * tm[2] * tm[2];
      pc(2,0) += uc(2,2) * tm[2];
      pc(1,1) += 2 * uc(3,1) * tm[8] + uc(3,3) * tm[8] * tm[8];
      pc(3,1) += uc(3,3) * tm[8];
      pc(1,0) += uc(2,1) * tm[2] + uc(3,0) * tm[8] + uc(3,2) * tm[2] * tm[8];
      pc(2,1) += uc(3,2) * tm[8];
      pc(3,0) += uc(3,2) * tm[2];
    }
  }
}

// Initialise
template<class T>
inline void initialise (
  Node& node,
  const Gaudi::TrackSymMatrix& covariance
) {
  node.get<T, Op::StateVector>().copy(node.get<Op::NodeParameters, Op::ReferenceVector>());
  node.get<T, Op::Covariance>().copy(covariance);
}

template<class T, bool U>
inline void predict (
  Node& node,
  const Node& prevnode
);

template<>
inline void predict<Op::Forward, false> (
  Node& node,
  const Node& prevnode
) {
  node.get<Op::Forward, Op::StateVector>().copy(node.get<Op::NodeParameters, Op::ReferenceVector>());
  node.get<Op::Forward, Op::Covariance>().copy(prevnode.get<Op::Forward, Op::Covariance>());
}

template<>
inline void predict<Op::Forward, true> (
  Node& node,
  const Node& prevnode
) {
  node.get<Op::Forward, Op::StateVector>().copy(node.transportVector());
  for (int i=0; i<5; ++i) {
    for (int j=0; j<5; ++j) {
      node.get<Op::Forward, Op::StateVector>()[i] += 
        node.get<Op::Forward, Op::TransportMatrix>()[5*i + j] *
        prevnode.get<Op::Forward, Op::StateVector>()[j];
    }
  }

  transportCovariance(
    node.get<Op::Forward, Op::TransportMatrix>(),
    prevnode.get<Op::Forward, Op::Covariance>(),
    node.get<Op::Forward, Op::Covariance>());
  node.get<Op::Forward, Op::Covariance>() += node.noiseMatrix();
}

template<>
inline void predict<Op::Backward, false> (
  Node& node,
  const Node& prevnode
) {
  node.get<Op::Backward, Op::StateVector>().copy(node.get<Op::Forward, Op::StateVector>());
  node.get<Op::Backward, Op::Covariance>().copy(prevnode.get<Op::Backward, Op::Covariance>());
}

template<>
inline void predict<Op::Backward, true> (
  Node& node,
  const Node& prevnode
) {
  Gaudi::TrackVector temp_sub;
  for (int i=0; i<5; ++i) {
    temp_sub[i] = prevnode.get<Op::Backward, Op::StateVector>()[i] - prevnode.transportVector().Array()[i];
  };

  for (int i=0; i<5; ++i) {
    node.get<Op::Backward, Op::StateVector>()[i] = node.get<Op::Backward, Op::TransportMatrix>()[5*i] * temp_sub[0];
    for (int j=1; j<5; ++j) {
      node.get<Op::Backward, Op::StateVector>()[i] += node.get<Op::Backward, Op::TransportMatrix>()[5*i + j] * temp_sub[j];
    }
  }

  Gaudi::TrackSymMatrix temp_cov;
  for (int i=0; i<15; ++i) {
    temp_cov.Array()[i] = prevnode.get<Op::Backward, Op::Covariance>()[i] + prevnode.noiseMatrix().Array()[i];
  }

  transportCovariance (
    node.get<Op::Backward, Op::TransportMatrix>(),
    temp_cov,
    node.get<Op::Backward, Op::Covariance>()
  );
}

}

}

}
