#pragma once

#include "../Types.h"
#include "Predict.h"
#include "Update.h"

namespace Tr {

namespace TrackVectorFit {

namespace Scalar {

template<class T>
inline void fit (
  Node& node,
  const Gaudi::TrackSymMatrix& covariance
) {
  node.get<T, Op::StateVector>().copy(node.get<Op::NodeParameters, Op::ReferenceVector>());
  node.get<T, Op::Covariance>().copy(covariance);

  update<T>(node);
}

template<class T, bool U>
inline void fit (
  Node& node,
  const Node& prevnode
) {
  predict<T, U>(node, prevnode);
  update<T>(node);
}

}

}

}
