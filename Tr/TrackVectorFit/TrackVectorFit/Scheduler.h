#pragma once

#include <bitset>
#include <ostream>
#include "Types.h"

namespace Tr {

namespace TrackVectorFit {

namespace Sch {

struct Item {
  Track* track;
  unsigned trackIndex;
  std::vector<Node, aligned_allocator<PRECISION, ALIGNMENT>>::iterator node;
  std::vector<Node, aligned_allocator<PRECISION, ALIGNMENT>>::iterator lastnode;

  Item () = default;
  Item (const Item& copy) = default;
  Item (
    Track* track,
    const unsigned& nodeIndex,
    const unsigned& lastnodeIndex
  ) : track(track) {
    trackIndex = track->m_index;
    node     = track->m_nodes.begin() + nodeIndex;
    lastnode = track->m_nodes.begin() + lastnodeIndex;
  }
};

template<size_t N>
std::bitset<N> reverse (const std::bitset<N>& set) {
  std::bitset<N> result;
  for (size_t i = 0; i < N; i++) {
    result[i] = set[N-i-1];
  }
  return result;
}

template<size_t N>
struct Blueprint {
  std::bitset<N> in;
  std::bitset<N> out;
  std::bitset<N> action;
  std::array<Item, N> pool;

  Blueprint () = default;
  Blueprint (const Blueprint& copy) = default;
  Blueprint (
    const std::bitset<N>& in,
    const std::bitset<N>& out,
    const std::bitset<N>& action,
    const std::array<Item, N>& pool
  ) : in(in), out(out), action(action), pool(pool) {}

  friend inline std::ostream& operator<< (
    std::ostream& cout,
    const Blueprint<N>& blueprint
  ) {
    cout << reverse<N>(blueprint.in) << " "
      << reverse<N>(blueprint.out) << " "
      << reverse<N>(blueprint.action) << " ";

    auto& pool = blueprint.pool;
    cout << "{ ";
    for (unsigned j=0; j<pool.size(); ++j) {
      cout << pool[j].trackIndex << "-" << pool[j].node->m_index << " ";
    }
    cout << "}";

    return cout;
  }

  friend inline std::ostream& operator<< (
    std::ostream& cout,
    const std::list<Blueprint<N>>& plan
  ) {
    unsigned i=0;
    for (auto& blueprint : plan) {
      cout << "#" << i++ << ": " << blueprint << std::endl;
    }
    return cout;
  }
};

template<size_t N>
struct StaticSchedulerCommon {
  // Generic generate method
  template<class F0, class F1, class F2>
  static std::list<Blueprint<N>>
  generate (
    std::list<std::reference_wrapper<Track>>& tracks,
    const F0& getSize,
    const F1& getCurrentElement,
    const F2& getLastElement
  ) {
    if (N > tracks.size()) {
      std::cout << "Scheduler: N <= tracks.size(), things probably won't work" << std::endl;
    }

    // Order tracks
    std::vector<std::reference_wrapper<Track>> ordered_tracks (tracks.begin(), tracks.end());
    std::sort(ordered_tracks.begin(), ordered_tracks.end(), [&getSize] (const Track& t0, const Track& t1) {
      const size_t size0 = getSize(t0);
      const size_t size1 = getSize(t1);
      return size0 > size1;
    });

    // Initialise
    std::list<Blueprint<N>> plan;
    std::bitset<N> in; // Note: Default constructor, it's initialized with zeroes
    std::bitset<N> out;
    std::bitset<N> action = std::bitset<N>().set();
    std::array<Item, N> pool;
    std::vector<unsigned> slots (N);
    std::iota(slots.begin(), slots.end(), 0);
    auto trackIterator = ordered_tracks.begin();

    // Iterate over all tracks, add what we need
    while (trackIterator != ordered_tracks.end()) {
      Track& track = *trackIterator;

      if (getSize(track) > 0) {
        // Add the element to a free slot
        const unsigned slot = slots.back();
        slots.pop_back();
        in[slot] = true;
        pool[slot] = Item (
          &track,
          getCurrentElement(track),
          getLastElement(track)
        );
      }

      while (slots.empty()) {
        // Set out mask for tracks where
        // this is the last node to process
        out = false;
        for (size_t i=0; i<N; ++i) {
          Item& s = pool[i];
          if (s.node + 1 == s.lastnode) {
            out[i] = 1;
            slots.push_back(i);
          }
        }

        // Add to plan
        plan.emplace_back(in, out, action, pool);

        // Initialise in, and prepare nodes for next iteration
        in = false;
        std::for_each (pool.begin(), pool.end(), [] (Item& s) {
          if (s.node + 1 != s.lastnode) {
            ++s.node;
          }
        });
      }
      
      ++trackIterator;
    }

    // Iterations with no full vectors
    if (slots.size() < N) {
      // make processingIndices the indices that didn't finish yet
      std::vector<unsigned> processingIndices (N);
      std::iota(processingIndices.begin(), processingIndices.end(), 0);
      for (auto it=slots.rbegin(); it!=slots.rend(); ++it) {
        processingIndices.erase(processingIndices.begin() + *it);
      }

      auto processingIndicesEnd = processingIndices.end();
      while (processingIndicesEnd != processingIndices.begin()) {
        // Set out and action mask
        action = false;
        out = false;
        std::for_each (processingIndices.begin(), processingIndicesEnd, [&] (const unsigned& i) {
          Item& s = pool[i];
          action[i] = 1;
          if (s.node + 1 == s.lastnode) {
            out[i] = true;
          }
        });

        // Adding to the plan
        plan.emplace_back(in, out, action, pool);

        // Initialise in, and prepare nodes for next iteration
        in = false;
        processingIndicesEnd = std::remove_if(processingIndices.begin(), processingIndicesEnd, [&] (const unsigned& i) {
          Item& s = pool[i];
          if (s.node + 1 == s.lastnode) return true;
          ++s.node;
          return false;
        });
      }
    }

    return plan;
  }
};

// Specializable generate method
class Pre;
class Main;
class Post;

template<class T, size_t N>
struct StaticScheduler {
  StaticScheduler () = default;
  StaticScheduler (const StaticScheduler& copy) = default;

  static std::list<Blueprint<N>>
  generate (
    std::list<std::reference_wrapper<Track>>& tracks
  );
};

template<size_t N>
struct StaticScheduler<Pre, N> {
  static std::list<Blueprint<N>>
  generate (
    std::list<std::reference_wrapper<Track>>& tracks
  ) {
    return StaticSchedulerCommon<N>::generate (
      tracks,
      [] (const Track& t) { return (t.m_forwardUpstream + 1); },
      [] (const Track&) { return 0; },
      [] (const Track& t) { return t.m_forwardUpstream + 1; }
    );
  }
};

template<size_t N>
struct StaticScheduler<Main, N> {
  static std::list<Blueprint<N>>
  generate (
    std::list<std::reference_wrapper<Track>>& tracks
  ) {
    return StaticSchedulerCommon<N>::generate (
      tracks,
      [] (const Track& t) { return t.m_nodes.size() - t.m_forwardUpstream - t.m_backwardUpstream; },
      [] (const Track& t) { return t.m_forwardUpstream + 1; },
      [] (const Track& t) { return t.m_nodes.size() - t.m_backwardUpstream - 1; }
    );
  }
};

template<size_t N>
struct StaticScheduler<Post, N> {
  static std::list<Blueprint<N>>
  generate (
    std::list<std::reference_wrapper<Track>>& tracks
  ) {
    return StaticSchedulerCommon<N>::generate (
      tracks,
      [] (const Track& t) { return (t.m_backwardUpstream + 1); },
      [] (const Track& t) { return t.m_nodes.size() - t.m_backwardUpstream - 1; },
      [] (const Track& t) { return t.m_nodes.size(); }
    );
  }
};

}

struct SwapStore {
  Mem::View::State store;
  TrackVector& state;
  TrackSymMatrix& covariance;

  SwapStore (const Mem::View::State& store, TrackVector& state, TrackSymMatrix& covariance) :
    store(store), state(state), covariance(covariance) {}
};

}

}
