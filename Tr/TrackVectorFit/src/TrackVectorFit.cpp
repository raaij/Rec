#include "TrackVectorFit/TrackVectorFit.h"

using namespace Tr::TrackVectorFit;

TrackVectorFit::TrackVectorFit (
  const unsigned& memManagerStorageIncrements,
  const bool& debug
) : m_memManagerStorageIncrements(memManagerStorageIncrements),
  m_debug(debug) {
  m_smoothStore = Mem::Store(Mem::View::SmoothState::size, m_memManagerStorageIncrements);
  m_nodeParametersStore = Mem::Store(Mem::View::NodeParameters::size, m_memManagerStorageIncrements);
  m_transportForwardStore = Mem::Store(Mem::View::TrackMatrix<25>::size, m_memManagerStorageIncrements);
  m_transportBackwardStore = Mem::Store(Mem::View::TrackMatrix<25>::size, m_memManagerStorageIncrements);
  m_auxStore = Mem::Store(20, m_memManagerStorageIncrements);
}

void TrackVectorFit::operator() (
  std::list<std::reference_wrapper<Track>>& tracks
) {
  // Initialize the smooth state iterator here
  Mem::Iterator smoothMainStoreIterator (m_smoothStoreIterator);

  if (tracks.size() >= VECTOR_WIDTH) {
    if (m_debug) {
      std::cout << "Scheduler pre iterations:" << std::endl
        << m_scheduler_pre << std::endl
        << "Scheduler main iterations:" << std::endl
        << m_scheduler_main << std::endl
        << "Scheduler post iterations:" << std::endl
        << m_scheduler_post << std::endl;
    }

    // Iterators for all stores
    Mem::Iterator forwardTransportIterator (m_transportForwardStore);
    Mem::ReverseIterator backwardTransportIterator (m_transportBackwardStore);
    Mem::ReverseIterator backwardNodeIterator (m_nodeParametersStore);
    Mem::ReverseIterator backwardStoreIterator (*m_backwardStore);

    // Iterators initialized from the object state
    Mem::Iterator forwardNodeIterator (m_forwardNodeIterator);
    Mem::Iterator forwardStoreIterator (m_forwardStoreIterator);
    Mem::ReverseIterator forwardMainStoreIterator (m_forwardMainStoreIterator);

    // Forward fit
    //  Pre iterations
    if (m_debug) {
      std::cout << std::endl << "Calculating forward fit pre iterations" << std::endl;
    }
    std::for_each (m_scheduler_pre.begin(), m_scheduler_pre.end(), [&] (Sch::Blueprint<VECTOR_WIDTH>& s) {
      const auto& in = s.in;
      const auto& action = s.action;
      auto& pool = s.pool;

      if (m_debug) {
        std::cout << s << std::endl;
      }

      // Predict (always in a new vector, no need to swap)
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (action[i]) {
          auto& node = *(pool[i].node);
          if (in[i]) {
            Scalar::initialise<Op::Forward>(node, pool[i].track->m_seedCovariance);
          } else {
            auto& prevnode = *(pool[i].node-1);
            Scalar::predict<Op::Forward, false>(node, prevnode);
          }
        }
      }

      // Update
      Mem::View::State current (forwardStoreIterator.nextVector());
      Mem::View::NodeParameters currentParameters (forwardNodeIterator.nextVector());

      Vector::update<VECTOR_WIDTH> (
        pool,
        currentParameters.m_referenceVector.m_basePointer,
        currentParameters.m_projectionMatrix.m_basePointer,
        currentParameters.m_referenceResidual,
        currentParameters.m_errorMeasure,
        current.m_updatedState.m_basePointer,
        current.m_updatedCovariance.m_basePointer,
        current.m_chi2
      );
    });

    // Forward fit
    //  Main iterations
    if (m_debug) {
      std::cout << std::endl << "Calculating forward fit main iterations" << std::endl;
    }
    std::list<SwapStore> swaps;
    auto lastVector = forwardStoreIterator.nextVector();
    std::for_each (m_scheduler_main.begin(), m_scheduler_main.end(), [&] (Sch::Blueprint<VECTOR_WIDTH>& s) {
      const auto& in  = s.in;
      const auto& out = s.out;
      auto& pool = s.pool;

      if (m_debug) {
        std::cout << s << std::endl;
      }

      // Feed the data we need in our vector
      if (in.any()) {
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (in[i]) {
            Mem::View::State memslot (lastVector + i);
            memslot.m_updatedState.copy((pool[i].node-1)->get<Op::Forward, Op::StateVector>());
            memslot.m_updatedCovariance.copy((pool[i].node-1)->get<Op::Forward, Op::Covariance>());
          }
        }
      }

      // predict and update
      const auto& currentVector = forwardStoreIterator.nextVector();
      Mem::View::State last (lastVector);
      Mem::View::State current (currentVector);
      const auto& tm = forwardTransportIterator.nextVector();
      Mem::View::NodeParameters currentParameters (forwardNodeIterator.nextVector());

      Vector::fit<Op::Forward>::op<VECTOR_WIDTH> (
        pool,
        currentParameters.m_referenceVector.m_basePointer,
        currentParameters.m_projectionMatrix.m_basePointer,
        currentParameters.m_referenceResidual,
        currentParameters.m_errorMeasure,
        tm,
        last.m_updatedState.m_basePointer,
        last.m_updatedCovariance.m_basePointer,
        current.m_updatedState.m_basePointer,
        current.m_updatedCovariance.m_basePointer,
        current.m_chi2
      );

      // Move requested data out
      if (out.any()) {
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (out[i]) {
            auto& node = *(pool[i].node);

            Mem::View::State memslot (m_auxStore.getNextElement());
            Mem::View::State currentslot (currentVector + i);
            memslot.m_updatedState.copy(currentslot.m_updatedState.m_basePointer);
            memslot.m_updatedCovariance.copy(currentslot.m_updatedCovariance.m_basePointer);
            swaps.push_back(
              SwapStore(
                memslot,
                node.get<Op::Forward, Op::StateVector>(),
                node.get<Op::Forward, Op::Covariance>()
              )
            );
          }
        }
      }

      lastVector = currentVector;
    });

    // Restore updated state
    for (auto& swap : swaps) {
      swap.state.copy(swap.store.m_updatedState);
      swap.covariance.copy(swap.store.m_updatedCovariance);
    }

    // Forward fit
    //  Post iterations
    if (m_debug) {
      std::cout << std::endl << "Calculating forward fit post iterations" << std::endl;
    }
    lastVector = forwardStoreIterator.nextVector();
    std::for_each (m_scheduler_post.begin(), m_scheduler_post.end(), [&] (Sch::Blueprint<VECTOR_WIDTH>& s) {
      const auto& in  = s.in;
      const auto& out = s.out;
      auto& pool = s.pool;

      if (m_debug) {
        std::cout << s << std::endl;
      }

      // Feed the data we need in our vector
      if (in.any()) {
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (in[i]) {
            Mem::View::State memslot (lastVector + i);
            memslot.m_updatedState.copy((pool[i].node-1)->get<Op::Forward, Op::StateVector>());
            memslot.m_updatedCovariance.copy((pool[i].node-1)->get<Op::Forward, Op::Covariance>());
          }
        }
      }

      // predict and update
      const auto& currentVector = forwardStoreIterator.nextVector();
      Mem::View::State last (lastVector);
      Mem::View::State current (currentVector);
      const auto& tm = forwardTransportIterator.nextVector();
      Mem::View::NodeParameters currentParameters (forwardNodeIterator.nextVector());

      Vector::fit<Op::Forward>::op<VECTOR_WIDTH> (
        pool,
        currentParameters.m_referenceVector.m_basePointer,
        currentParameters.m_projectionMatrix.m_basePointer,
        currentParameters.m_referenceResidual,
        currentParameters.m_errorMeasure,
        tm,
        last.m_updatedState.m_basePointer,
        last.m_updatedCovariance.m_basePointer,
        current.m_updatedState.m_basePointer,
        current.m_updatedCovariance.m_basePointer,
        current.m_chi2
      );

      // Move requested data out and update data pointers
      if (out.any()) {
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (out[i]) {
            auto& node = *(pool[i].node);

            Mem::View::State memslot (m_auxStore.getNextElement());
            Mem::View::State currentslot (currentVector + i);
            memslot.m_updatedState.copy(currentslot.m_updatedState.m_basePointer);
            memslot.m_updatedCovariance.copy(currentslot.m_updatedCovariance.m_basePointer);
            node.get<Op::Forward, Op::StateVector>().setBasePointer(memslot.m_updatedState);
            node.get<Op::Forward, Op::Covariance>().setBasePointer(memslot.m_updatedCovariance);
          }
        }
      }

      lastVector = currentVector;
    });

    // Backward fit and smoother
    //  Pre iterations
    if (m_debug) {
      std::cout << "Calculating backward fit pre iterations" << std::endl;
    }
    std::for_each (m_scheduler_post.rbegin(), m_scheduler_post.rend(), [&] (Sch::Blueprint<VECTOR_WIDTH>& s) {
      const auto& in = s.out;
      const auto& action = s.action;
      auto& pool = s.pool;

      // Predict (always in a new vector, no need to swap)
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (action[i]) {
          auto& node = *(pool[i].node);
          if (in[i]) {
            Scalar::initialise<Op::Backward>(node, pool[i].track->m_seedCovariance);
          } else {
            auto& prevnode = *(pool[i].node+1);
            Scalar::predict<Op::Backward, false>(node, prevnode);
          }
        }
      }

      if (m_debug) {
        std::cout << s << std::endl;
      }

      // Update
      Mem::View::State current (backwardStoreIterator.previousVector());
      Mem::View::NodeParameters currentParameters (backwardNodeIterator.previousVector());

      Vector::update<VECTOR_WIDTH> (
        pool,
        currentParameters.m_referenceVector.m_basePointer,
        currentParameters.m_projectionMatrix.m_basePointer,
        currentParameters.m_referenceResidual,
        currentParameters.m_errorMeasure,
        current.m_updatedState.m_basePointer,
        current.m_updatedCovariance.m_basePointer,
        current.m_chi2
      );
    });

    // Backward fit and smoother
    //  Main iterations
    if (m_debug) {
      std::cout << "Calculating backward fit and smoother main iterations" << std::endl;
    }
    lastVector = backwardStoreIterator.previousVector();
    std::for_each (m_scheduler_main.rbegin(), m_scheduler_main.rend(), [&] (Sch::Blueprint<VECTOR_WIDTH>& s) {
      const auto& in  = s.out;
      const auto& out = s.in;
      const auto& action = s.action;
      auto& pool = s.pool;

      if (m_debug) {
        std::cout << s << std::endl;
      }

      // Feed the data we need in our vector
      if (in.any()) {
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (in[i]) {
            Mem::View::State memslot (lastVector + i);
            memslot.m_updatedState.copy((pool[i].node+1)->get<Op::Backward, Op::StateVector>());
            memslot.m_updatedCovariance.copy((pool[i].node+1)->get<Op::Backward, Op::Covariance>());
          }
        }
      }

      // Update pointers of smooth state
      const auto& smootherVector = smoothMainStoreIterator.nextVector();
      for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
        if (action[i]) {
          pool[i].node->m_smoothState.setBasePointer(smootherVector + i);
        }
      }

      // predict and update
      const auto& currentVector = backwardStoreIterator.previousVector();
      Mem::View::State last (lastVector);
      Mem::View::State current (currentVector);
      const auto& tm = backwardTransportIterator.previousVector();
      Mem::View::NodeParameters currentParameters (backwardNodeIterator.previousVector());

      Vector::predict<Op::Backward>::op<VECTOR_WIDTH> (
        pool,
        tm,
        last.m_updatedState.m_basePointer,
        last.m_updatedCovariance.m_basePointer,
        current.m_updatedState.m_basePointer,
        current.m_updatedCovariance.m_basePointer
      );

      // Do the smoother *here*
      // This benefits from cache locality,
      // and removes the need of doing swapping to restore
      // the updated states
      
      // Fetch the corresponding forward and smooth state, and update it
      Mem::View::State forward (forwardMainStoreIterator.previousVector());
      Mem::View::SmoothState smooth (smootherVector);

      // Smoother
      const uint16_t smoother_result = Vector::smoother<VECTOR_WIDTH> (
        forward.m_updatedState.m_basePointer,
        forward.m_updatedCovariance.m_basePointer,
        current.m_updatedState.m_basePointer,      // These are still the predicted state
        current.m_updatedCovariance.m_basePointer, // and covariance
        smooth.m_state.m_basePointer,
        smooth.m_covariance.m_basePointer
      );

      // Update residuals
      Vector::updateResiduals (
        pool,
        currentParameters.m_referenceVector.m_basePointer,
        currentParameters.m_projectionMatrix.m_basePointer,
        currentParameters.m_referenceResidual,
        currentParameters.m_errorMeasure,
        smooth.m_state.m_basePointer,
        smooth.m_covariance.m_basePointer,
        smooth.m_residual,
        smooth.m_errResidual
      );

      // Update
      Vector::update<VECTOR_WIDTH> (
        pool,
        currentParameters.m_referenceVector.m_basePointer,
        currentParameters.m_projectionMatrix.m_basePointer,
        currentParameters.m_referenceResidual,
        currentParameters.m_errorMeasure,
        current.m_updatedState.m_basePointer,
        current.m_updatedCovariance.m_basePointer,
        current.m_chi2
      );

      // Set errors
      if (smoother_result != ArrayGen::mask<VECTOR_WIDTH>()) {
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (action[i] && !(smoother_result >> i) & 0x01) {
            pool[i].track->track().setFitStatus(LHCb::Track::FitFailed);
          }
        }
      }

      if (m_debug) {
        if (smoother_result != ArrayGen::mask<VECTOR_WIDTH>()) {
          std::cout << "Smoother errors: " << smoother_result << std::endl;
        }
      }

      // Move requested data out and update data pointers
      if (out.any()) {
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (out[i]) {
            auto& node = *(pool[i].node);

            Mem::View::State memslot (m_auxStore.getNextElement());
            Mem::View::State currentslot (currentVector + i);
            memslot.m_updatedState.copy(currentslot.m_updatedState.m_basePointer);
            memslot.m_updatedCovariance.copy(currentslot.m_updatedCovariance.m_basePointer);
            node.get<Op::Backward, Op::StateVector>().setBasePointer(memslot.m_updatedState);
            node.get<Op::Backward, Op::Covariance>().setBasePointer(memslot.m_updatedCovariance);
          }
        }
      }

      lastVector = currentVector;
    });

    // Backward fit
    //  Post iterations
    if (m_debug) {
      std::cout << "Calculating backward fit post iterations" << std::endl;
    }
    lastVector = backwardStoreIterator.previousVector();
    std::for_each (m_scheduler_pre.rbegin(), m_scheduler_pre.rend(), [&] (Sch::Blueprint<VECTOR_WIDTH>& s) {
      const auto& in  = s.out;
      const auto& out = s.in;
      auto& pool = s.pool;

      if (m_debug) {
        std::cout << s << std::endl;
      }

      // Feed the data we need in our vector
      if (in.any()) {
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (in[i]) {
            Mem::View::State memslot (lastVector + i);
            memslot.m_updatedState.copy((pool[i].node+1)->get<Op::Backward, Op::StateVector>());
            memslot.m_updatedCovariance.copy((pool[i].node+1)->get<Op::Backward, Op::Covariance>());
          }
        }
      }

      // predict and update
      const auto& currentVector = backwardStoreIterator.previousVector();
      Mem::View::State last (lastVector);
      Mem::View::State current (currentVector);
      const auto& tm = backwardTransportIterator.previousVector();
      Mem::View::NodeParameters currentParameters (backwardNodeIterator.previousVector());

      Vector::fit<Op::Backward>::op<VECTOR_WIDTH> (
        pool,
        currentParameters.m_referenceVector.m_basePointer,
        currentParameters.m_projectionMatrix.m_basePointer,
        currentParameters.m_referenceResidual,
        currentParameters.m_errorMeasure,
        tm,
        last.m_updatedState.m_basePointer,
        last.m_updatedCovariance.m_basePointer,
        current.m_updatedState.m_basePointer,
        current.m_updatedCovariance.m_basePointer,
        current.m_chi2
      );

      // Move requested data out and update data pointers
      if (out.any()) {
        for (unsigned i=0; i<VECTOR_WIDTH; ++i) {
          if (out[i]) {
            auto& node = *(pool[i].node);

            Mem::View::State memslot (m_auxStore.getNextElement());
            Mem::View::State currentslot (currentVector + i);
            memslot.m_updatedState.copy(currentslot.m_updatedState.m_basePointer);
            memslot.m_updatedCovariance.copy(currentslot.m_updatedCovariance.m_basePointer);
            node.get<Op::Backward, Op::StateVector>().setBasePointer(memslot.m_updatedState);
            node.get<Op::Backward, Op::Covariance>().setBasePointer(memslot.m_updatedCovariance);
          }
        }
      }

      lastVector = currentVector;
    });
  } else {
    // Forward fit
    std::for_each(tracks.begin(), tracks.end(), [&] (Track& track) {
      auto& nodes = track.m_nodes;
      Node& node = nodes.front();
      Scalar::fit<Op::Forward>(node, track.m_seedCovariance);

      // Before m_forwardUpstream
      for (unsigned i=1; i<=track.m_forwardUpstream; ++i) {
        Node& node = nodes[i];
        Node& prevnode = nodes[i-1];
        Scalar::fit<Op::Forward, false>(node, prevnode);
      }

      // After m_forwardUpstream
      for (unsigned i=track.m_forwardUpstream+1; i<nodes.size(); ++i) {
        Node& node = nodes[i];
        Node& prevnode = nodes[i-1];
        Scalar::fit<Op::Forward, true>(node, prevnode);
      }
    });

    // Backward fit
    std::for_each(tracks.begin(), tracks.end(), [&] (Track& track) {
      auto& nodes = track.m_nodes;
      Node& node = nodes.back();
      Scalar::fit<Op::Backward>(node, track.m_seedCovariance);

      // Before m_backwardUpstream
      for (unsigned i=1; i<=track.m_backwardUpstream; ++i) {
        const int element = track.m_nodes.size() - i - 1;
        Node& prevnode = nodes[element + 1];
        Node& node = nodes[element];
        Scalar::fit<Op::Backward, false>(node, prevnode);
      }

      // After m_backwardUpstream
      for (unsigned i=track.m_backwardUpstream+1; i<nodes.size(); ++i) {
        const int element = track.m_nodes.size() - i - 1;
        Node& prevnode = nodes[element + 1];
        Node& node = nodes[element];
        Scalar::predict<Op::Backward, true>(node, prevnode);

        // Do the smoother
        if (i < (track.m_nodes.size() - track.m_forwardUpstream - 1)) {
          node.m_smoothState.setBasePointer(smoothMainStoreIterator.nextElement());
          const bool smoothSuccess = Scalar::smoother<true, true>(node);
          Scalar::updateResiduals(node);

          if (not smoothSuccess) {
            track.track().setFitStatus(LHCb::Track::FitFailed);
          }
        }

        Scalar::update<Op::Backward>(node);
      }
    });
  }

  // Calculate the chi2 a posteriori
  std::for_each(tracks.begin(), tracks.end(), [&] (Track& track) {
    auto& nodes = track.m_nodes;
    
    // Reset the values
    track.m_ndof = -track.m_nTrackParameters;
    auto ndofBackward = -track.m_nTrackParameters;
    track.m_forwardFitChi2  = ((PRECISION) 0.0);
    track.m_backwardFitChi2 = ((PRECISION) 0.0);

    for (unsigned k=0; k<nodes.size(); ++k) {
      Node& node = nodes[k];
      if (node.node().type() == LHCb::Node::HitOnTrack) {
        track.m_forwardFitChi2 += node.get<Op::Forward, Op::Chi2>();
        ++track.m_ndof;
      }
      node.get<Op::Forward, Op::Chi2>() = track.m_forwardFitChi2;
      node.m_ndof = track.m_ndof;

      // Backwards chi2
      Node& backwardNode = nodes[nodes.size() - 1 - k];
      if (backwardNode.node().type() == LHCb::Node::HitOnTrack) {
        track.m_backwardFitChi2 += backwardNode.get<Op::Backward, Op::Chi2>();
        ++ndofBackward;
      }
      backwardNode.get<Op::Backward, Op::Chi2>() = track.m_backwardFitChi2;
      backwardNode.m_ndofBackward = ndofBackward;
    }
  });

  // Not upstream iterations
  std::for_each(tracks.begin(), tracks.end(), [&] (Track& track) {
    for (unsigned j=0; j<=track.m_forwardUpstream; ++j) {
      track.m_nodes[j].m_smoothState.setBasePointer(smoothMainStoreIterator.nextElement());
      Scalar::smoother<false, true>(track.m_nodes[j]);
      Scalar::updateResiduals(track.m_nodes[j]);
    }

    for (unsigned j=0; j<=track.m_backwardUpstream; ++j) {
      const unsigned element = track.m_nodes.size() - j - 1;
      track.m_nodes[element].m_smoothState.setBasePointer(smoothMainStoreIterator.nextElement());
      Scalar::smoother<true, false>(track.m_nodes[element]);
      Scalar::updateResiduals(track.m_nodes[element]);
    }
  });

  // Set the LHCb::Node information accordingly
  std::for_each(tracks.begin(), tracks.end(), [] (Track& track) {
    // TODO - Either the min or max chi2 could be picked up here.
    track.track().setChi2AndDoF(track.minChi2(), track.ndof());
  });
}

/**
 * @brief Populate the nodes with the calculated information.
 */
void TrackVectorFit::populateNodes (
  Track& track
) {
  FitTrack* fitTrack = static_cast<FitTrack*>(track.m_track);
  fitTrack->assignMemManagers(m_forwardStore, m_backwardStore);

  std::for_each(track.nodes().begin(), track.nodes().end(), [] (Node& n) {
    auto& node = n.node();

    node.setErrMeasure(n.get<Op::NodeParameters, Op::ErrMeasure>());
    node.setProjectionMatrix((Gaudi::TrackProjectionMatrix) n.get<Op::NodeParameters, Op::ProjectionMatrix>());
    node.setResidual(n.get<Op::Smooth, Op::Residual>());
    node.setErrResidual(n.get<Op::Smooth, Op::ErrResidual>());
    
    const auto smoothStateVector = (Gaudi::TrackVector) n.get<Op::Smooth, Op::StateVector>();
    node.setRefVector(smoothStateVector);
    node.setState(
      smoothStateVector,
      (Gaudi::TrackSymMatrix) n.get<Op::Smooth, Op::Covariance>(),
      node.z());
  });
}
