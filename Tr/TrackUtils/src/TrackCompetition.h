#ifndef _TrackCompetition_H_
#define _TrackCompetition_H_

/** @class TrackCompetition TrackCompetition.h
 *
 *  Copy a container of tracks. By default do not copy tracks that failed the fit
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

namespace LHCb{
  class Track;
}

class TrackCompetition: public GaudiAlgorithm {

public:

  // Constructor
  TrackCompetition(const std::string& name,
              ISvcLocator* pSvcLocator);

  StatusCode execute() override;

private:

  std::string m_inputLocation;
  std::string m_outputLocation;
  double m_fracUsed;

};

#endif
