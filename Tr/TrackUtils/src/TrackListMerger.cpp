/** @class TrackListMerger TrackListMerger.h
 *
 *  Merge different track lists.
 *
 *  @author Wouter Hulsbergen
 *  @date   05/01/2010
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include <string>
#include <vector>
#include "Event/Track.h"


class TrackListMerger final : public GaudiAlgorithm {

public:

  // Constructors and destructor
  TrackListMerger(const std::string& name,
              ISvcLocator* pSvcLocator);
  StatusCode execute() override;

private:
  std::vector<std::string> m_inputLocations;
  std::string m_outputLocation;
};

DECLARE_ALGORITHM_FACTORY( TrackListMerger )

TrackListMerger::TrackListMerger(const std::string& name,
                       ISvcLocator* pSvcLocator):
  GaudiAlgorithm(name, pSvcLocator)
{
  // constructor
  declareProperty( "inputLocations",  m_inputLocations) ;
  declareProperty( "outputLocation", m_outputLocation) ;
}

StatusCode TrackListMerger::execute()
{
  LHCb::Track::Selection* tracksout = new LHCb::Track::Selection() ;
  put( tracksout, m_outputLocation) ;

  // loop
  for( const auto& ilist : m_inputLocations ) {
    for (const auto& track : get<LHCb::Track::Range>(ilist) ) {
      // make sure the track is not yet there!
      if( std::find( tracksout->begin(),tracksout->end(),track) == tracksout->end() )
	     tracksout->insert( track ) ;
    }
  }
  return StatusCode::SUCCESS;
}
