// Include files 

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// from LHCbEvent
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/MCHit.h"
#include "Event/Track.h"
#include "Event/State.h"
#include "Event/MCTrackInfo.h"

#include "Linker/LinkedTo.h"

// local
#include "PatLongLivedParams.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PatLongLivedParams
//
// 2002-11-02 : Olivier Callot
// 2016-09-01 : Michel De Cian
//-----------------------------------------------------------------------------

DECLARE_ALGORITHM_FACTORY( PatLongLivedParams )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PatLongLivedParams::PatLongLivedParams( const std::string& name,
                                      ISvcLocator* pSvcLocator)
: GaudiTupleAlg ( name , pSvcLocator ),
  m_fitTool(nullptr)
{
  declareProperty( "NTupleName",     m_tupleName  = "Track" );
  declareProperty( "ZTT1",           m_zTT1       = 2469.0*Gaudi::Units::mm );
  declareProperty( "ZRef",           m_zRef       = 9410.0*Gaudi::Units::mm );
  declareProperty( "zMagnetParams",  m_zMagParams );
  declareProperty( "momentumParams", m_momParams  );
  declareProperty( "curvatureParams", m_curvatureParams  );
  declareProperty( "yParams", m_yParams  );
  declareProperty( "yParams2", m_yParams2  );
  declareProperty( "seedLocation",   m_inputLocationSeed = LHCb::TrackLocation::Seed  );
  declareProperty( "determineResolution",   m_resolution = false  );

  m_nEvent = 0;
  m_nTrack = 0;
}

//=============================================================================
// Destructor
//=============================================================================
PatLongLivedParams::~PatLongLivedParams() {}

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode PatLongLivedParams::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;
  
  m_zMagPar.init( "zMagnet"  , m_zMagParams );
  m_momPar.init ( "momentum" , m_momParams );
  m_curvature.init ( "curvature" , m_curvatureParams );
  m_yPar.init ( "yPar" , m_yParams );
  m_yPar2.init ( "yPar2" , m_yParams2 );
  
  m_fitTool = tool<FitTool>( "FitTool" );
  
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PatLongLivedParams::execute() {
  debug() << "==> Execute" << endmsg;

  m_nEvent++;

  // -- As this algorithm is the only one running, even the usual counter is not included.
  if( m_nEvent % 100 == 0 ) info() << "Event " << m_nEvent << endmsg;
  

  if( m_resolution ){
    resolution();
    return StatusCode::SUCCESS;
  }
  
  MCTrackInfo trackInfo( evtSvc(), msgSvc() );

  LHCb::MCParticles* partCtnr = get<LHCb::MCParticles>( LHCb::MCParticleLocation::Default );

  // Get the IT and TT hits
  LHCb::MCHits* itHits = get<LHCb::MCHits>( LHCb::MCHitLocation::IT );
  LHCb::MCHits* ttHits = get<LHCb::MCHits>( LHCb::MCHitLocation::TT );

  // Get the OT hits
  LHCb::MCHits* otHits = get<LHCb::MCHits>( LHCb::MCHitLocation::OT );

  LHCb::MCParticles::const_iterator pItr;
  const LHCb::MCParticle* part;
  SmartRefVector<LHCb::MCVertex> vDecay;
  
  const LHCb::MCParticle* kShort  = 0;
  const LHCb::MCVertex*   kDecay  = 0;
  
  // A container for used hits
  std::vector<Gaudi::XYZPoint> trHits;
  std::vector<Gaudi::XYZPoint> TTHits;
  
  for ( pItr = partCtnr->begin(); partCtnr->end() != pItr; pItr++ ) {
    part = *pItr;

    if(  !trackInfo.hasT( part ) || !trackInfo.hasTT( part ) ) continue;
    // -- optimise for downstream tracks.
    if( trackInfo.hasVelo( part ) ) continue;
    
    
    // Find the Pi from the kShort
    if ( 211 == std::abs(part->particleID().pid())  ) { 
      kDecay = part->originVertex();
      if ( 0 == kDecay ) continue;
      kShort = kDecay->mother();
      if ( 0 == kShort ) continue;
      if ( 310 != kShort->particleID().pid() ) continue;
      const LHCb::MCVertex* origin = kShort->originVertex();
      if ( 0 == origin ) continue;
      if ( 100. < origin->position().R() ) continue;  // Kshorts from close the beam line

      bool hasInteractionVertex = false;
      SmartRefVector<LHCb::MCVertex> endV = part->endVertices();
      for ( SmartRefVector<LHCb::MCVertex>::const_iterator itV = endV.begin() ;
            endV.end() != itV; itV++ ) {
        if ( (*itV)->position().z() < 9500. ) hasInteractionVertex = true;
      }
      if ( hasInteractionVertex ) continue;
    
      debug() << "--- Found pi key " << part->key() << endmsg;
    
      TTHits.clear();
      trHits.clear();
   
      // Get the IT hits
      for ( LHCb::MCHits::const_iterator iHitIt = itHits->begin() ; 
            itHits->end() != iHitIt ; iHitIt++ ) {
        if ( (*iHitIt)->mcParticle() ==  part ) {
          trHits.push_back( (*iHitIt)->midPoint() );
        }
      }

      // Get the TT hits
      for ( LHCb::MCHits::const_iterator iHittt = ttHits->begin() ; 
            ttHits->end() != iHittt ; iHittt++ ) {
        if ( (*iHittt)->mcParticle() ==  part ) {
          TTHits.push_back( (*iHittt)->midPoint() );
        }
      }

      // Get the OT hits
      for ( LHCb::MCHits::const_iterator oHitIt = otHits->begin() ; 
            otHits->end() != oHitIt ; oHitIt++ ) {
        if ( (*oHitIt)->mcParticle() ==  part ) {
          trHits.push_back( (*oHitIt)->midPoint() );
        }
      }
      if ( 3  > TTHits.size() || 12 > trHits.size() )  continue;

      debug() << "=== Found a good K0S Decay : " 
              << kShort->key() << " decay at " 
              << format( "%7.1f %7.1f %7.1f", 
                         kDecay->position().x(),
                         kDecay->position().y(),
                         kDecay->position().z() )
              << endmsg;
      debug() << " pion momentum = " << part->momentum().R() / Gaudi::Units::GeV << " GeV"
              << endmsg;
      
      //== Fill ntuple
      double pz = kShort->momentum().Z();
      double kSlXT = kShort->momentum().X() / pz;
      double kSlYT = kShort->momentum().Y() / pz;

      Tuple tTrack = nTuple( m_tupleName, m_tupleName );

      m_nTrack++;

      tTrack->column( "KVertX", kDecay->position().X() );
      tTrack->column( "KVertY", kDecay->position().Y() );
      tTrack->column( "KVertZ", kDecay->position().Z() );
      tTrack->column( "KMoment", kShort->momentum().R() );
      tTrack->column( "KSlopeX", kSlXT );
      tTrack->column( "KSlopeY", kSlYT );

      //== Fit the Pi+
      pz = part->momentum().Z();
      tTrack->column( "moment",  part->momentum().R() );
      tTrack->column( "slopeX",  part->momentum().X() / pz );
      tTrack->column( "slopeY",  part->momentum().Y() / pz );
    
      //== Fit the TT area
      double axt, bxt, ayt, byt, dz;
      double axt2,bxt2,cxt2;
      
      debug() << "  TT: ";
      m_fitTool->fitLine( TTHits, 0, m_zTT1, axt, bxt );
      m_fitTool->fitLine( TTHits, 1, m_zTT1, ayt, byt );
      m_fitTool->fitParabola( TTHits, 0, m_zTT1, axt2, bxt2, cxt2 );
      debug() << format( " x %7.1f tx %7.4f   y %7.1f ty %7.4f ",
                         axt, bxt, ayt, byt ) 
              << endmsg;
      tTrack->column( "axt" , axt );
      tTrack->column( "bxt", bxt );
      tTrack->column( "ayt" , ayt );
      tTrack->column( "byt", byt );
      tTrack->column( "axt2", axt2 );
      tTrack->column( "bxt2", bxt2 );
      tTrack->column( "cxt2", cxt2 );

      std::vector<Gaudi::XYZPoint>::const_iterator itP;
      if ( msgLevel( MSG::DEBUG ) ) {
        for ( itP = TTHits.begin(); TTHits.end() > itP; itP++ ) {
          dz = (*itP).z()-m_zTT1;
          debug() << format( "    : %7.1f %7.1f %7.1f  dx %7.3f  dy %7.3f",
                             (*itP).x(), (*itP).y(), (*itP).z(),
                             (*itP).x()-(axt+bxt*dz),
                             (*itP).y()-(ayt+byt*dz)
                             ) << endmsg;
        }
      }
      
      double ax, bx, cx, dx, ay, by;
      m_fitTool->fitCubic( trHits, 0, m_zRef, ax, bx, cx, dx );
      m_fitTool->fitLine(  trHits, 1, m_zRef, ay, by );
      tTrack->column( "ax", ax );
      tTrack->column( "bx", bx );
      tTrack->column( "cx", cx );
      tTrack->column( "dx", dx );
      tTrack->column( "ay", ay );
      tTrack->column( "by", by );
      
      
      
      

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << format( "  tr: x%7.1f %7.4f %7.3f %7.3f  y%7.1f %7.4f", 
                           ax, bx, 1.e6*cx, 1.e9*dx, ay, by ) << endmsg;
        
        for ( itP = trHits.begin(); trHits.end() > itP; itP++ ) {
          dz = (*itP).z()-m_zRef;
          debug() << format( "    : %7.1f %7.1f %7.1f  dx %7.3f  dy %7.3f",
                             (*itP).x(), (*itP).y(), (*itP).z(),
                             (*itP).x()-(ax + bx*dz + cx*dz*dz + dx*dz*dz*dz),
                             (*itP).y()-(ay + by*dz)
                             ) << endmsg;
        }
      }
      
      double zMagnet = (axt-bxt*m_zTT1 - (ax-bx*m_zRef) ) / (bx-bxt);
      tTrack->column( "zMagnet", zMagnet );
      double zMagnety = (ayt-byt*m_zTT1 - (ay-by*m_zRef) ) / (by-byt);
      tTrack->column( "zMagnety", zMagnety );

      const double yMagnet = ayt + (zMagnet-m_zTT1)*byt;
      
      double dSlope =  fabs(bx - bxt);
      //double dSlope2 =  (bx - bxt);
      tTrack->column("dSlope",dSlope);
      m_zMagPar.setFun( 0, 1. );
      m_zMagPar.setFun( 1, by*by );
      m_zMagPar.setFun( 2, bx*bx );
      m_zMagPar.setFun( 3, 1.0/part->momentum().R() );
      m_zMagPar.setFun( 4, std::abs(ax) );
      m_zMagPar.setFun( 5, std::abs(ay) );
      m_zMagPar.setFun( 6, std::abs(by) );

      double dSlope2 =  (bx - bxt);
      for ( itP = TTHits.begin(); TTHits.end() > itP; itP++ ) {
        Tuple tTrack2 = nTuple( "Track2", "Track2" );
          
        dz = (*itP).z()-m_zTT1;
        double dx2 = (*itP).x()-(axt+bxt*dz);
        tTrack2->column( "dx2", dx2 );
        tTrack2->column( "dz2", dz );
        tTrack2->column( "dSlope2", dSlope2);
        tTrack2->column( "moment2", part->momentum().R() );
        
        tTrack2->write();
      }
      
      //m_curvature.setFun(0,0.0);
      m_curvature.setFun(0,dSlope2);
      
      double cEst = m_curvature.sum();
      m_curvature.addEvent(cxt2-cEst);
      tTrack->column("curvature",cxt2);
      tTrack->column("dSlope2",dSlope2);
      
      double zEst = m_zMagPar.sum();
      tTrack->column( "zEst", zEst );
      m_zMagPar.addEvent( zMagnet-zEst );

      dSlope = std::abs(dSlope);
      
      double dp = dSlope * part->momentum().R() - m_momPar.sum();
      // -- this is a little obscure, the numbers ( '5' and '2000' ) are determined below.
      // -- the numbers used here might be a bit outdated
      double bytPred = by + 5. * by * fabs(by) * (bx-bxt) * (bx-bxt);
      double aytPred = ay + (zMagnet-m_zRef)*by + (m_zTT1-zMagnet)*bytPred;
      aytPred -= 2000*by*(bx-bxt)*(bx-bxt);
      
      tTrack->column( "bytPred", bytPred );
      tTrack->column( "aytPred", aytPred );
      
      tTrack->column( "xMagnet", axt + ( zMagnet - m_zTT1 ) * bxt );
      tTrack->column( "yMagnet", ayt + ( zMagnet - m_zTT1 ) * byt );

      // -- this determines the numbers quoted above
      // -- the precise parametrisations are a bit 'black magic'
      m_yPar.setFun(0, by*std::abs(by)*dSlope*dSlope );
      m_yPar.addEvent( byt - by - m_yPar.sum() );
      
      // -- This is how by is determined for OT tracks 
      // -- see LHCb-PUB-2017-001
      //const double byExp = ay / ( m_zRef + ( m_yParams[0] * fabs(by) * zMagnet + m_yParams[2] )* dSlope2  );
      
      const double yMagnetExp = ay + (zMagnet - m_zRef) * by;
      
      m_yPar2.setFun(0, by*dSlope*dSlope );
      m_yPar2.addEvent( yMagnet - yMagnetExp - m_yPar2.sum() );
      
      // -- and the momentum parametrisations
      m_momPar.setFun( 0, 1. );
      m_momPar.setFun( 1, bx * bx );
      m_momPar.setFun( 2, by * by );
      //m_momPar.setFun( 3, bx*bx*bx*bx );
      m_momPar.addEvent( dp );
      tTrack->column( "PredMom", m_momPar.sum() / dSlope );
      
      double xMag = ax + ( zEst - m_zRef ) * bx;
      tTrack->column( "xPred", xMag * m_zTT1 / zEst );
      tTrack->write();
   
      double slopeX = xMag / zMagnet;
      tTrack->column( "slopeX", slopeX );
   
      debug() << format( "  zMag%7.1f xMag%7.1f yMag%7.1f back%7.1f",
                         zMagnet,
                         axt + ( zMagnet - m_zTT1 ) * bxt,
                         ayt + ( zMagnet - m_zTT1 ) * byt,
                         ay  + ( zMagnet - m_zRef ) * by )
              << endmsg;
    }
  }
  return StatusCode::SUCCESS;
}
//=============================================================================
//  Determine resolution
//=============================================================================
void PatLongLivedParams::resolution(){
  
  LinkedTo<LHCb::MCParticle, LHCb::Track> mySeedLink ( evtSvc(), msgSvc(),LHCb::TrackLocation::Seed);
  MCTrackInfo trackInfo( evtSvc(), msgSvc() );

  // Get the IT and TT hits
  LHCb::MCHits* itHits = get<LHCb::MCHits>( LHCb::MCHitLocation::IT );
  LHCb::MCHits* ttHits = get<LHCb::MCHits>( LHCb::MCHitLocation::TT );

  // Get the OT hits
  LHCb::MCHits* otHits = get<LHCb::MCHits>( LHCb::MCHitLocation::OT );

  LHCb::Tracks* inTracks   = getIfExists<LHCb::Tracks>( m_inputLocationSeed );
  
  if( inTracks == nullptr ){ 
    error() << "Could not find tracks in " << m_inputLocationSeed << endmsg;
    return;
  }

  // A container for used hits
  std::vector<Gaudi::XYZPoint> trHits;
  std::vector<Gaudi::XYZPoint> TTHits;

  for( LHCb::Track* track : *inTracks){
    
    const LHCb::MCParticle* mcSeedPart = mySeedLink.first( track->key() );
    if( mcSeedPart == nullptr ) continue;
    if( std::abs(mcSeedPart->particleID().pid()) == 11 ) continue;
   
    if(  !trackInfo.hasT( mcSeedPart) || !trackInfo.hasTT( mcSeedPart ) ) continue;
    // -- optimise for downstream tracks
    if( trackInfo.hasVelo( mcSeedPart ) ) continue;
    
    const LHCb::MCVertex* kDecay = mcSeedPart->originVertex();
    if ( nullptr == kDecay ) continue;
    const LHCb::MCParticle* kShort = kDecay->mother();
    if ( nullptr == kShort ) continue;
    
    // -- Ks and Lambda
    if ( 310 != std::abs(kShort->particleID().pid()) && 3122 != std::abs(kShort->particleID().pid()) ) continue;
    
    const LHCb::MCVertex* origin = kShort->originVertex();
    if ( nullptr == origin ) continue;
    if ( 100. < origin->position().R() ){
      debug()<< "Too far from beampipe" << endmsg;
      continue;  // particles from close the beam line
    }
    
    bool hasInteractionVertex = false;
    SmartRefVector<LHCb::MCVertex> endV = mcSeedPart->endVertices();
    for ( SmartRefVector<LHCb::MCVertex>::const_iterator itV = endV.begin() ;
          endV.end() != itV; itV++ ) {
      if ( (*itV)->position().z() < 9500. ) hasInteractionVertex = true;
    }
    if ( hasInteractionVertex ){
      debug()<< "Interaction vertex found. skipping" << endmsg;
      continue;
    }
    
    TTHits.clear();
    trHits.clear();
    
    // Get the IT hits
    for ( LHCb::MCHits::const_iterator iHitIt = itHits->begin() ; 
          itHits->end() != iHitIt ; iHitIt++ ) {
      if ( (*iHitIt)->mcParticle() ==  mcSeedPart ) {
        trHits.push_back( (*iHitIt)->midPoint() );
      }
    }
    
    // Get the TT hits
    for ( LHCb::MCHits::const_iterator iHittt = ttHits->begin() ; 
          ttHits->end() != iHittt ; iHittt++ ) {
      if ( (*iHittt)->mcParticle() ==  mcSeedPart ) {
        TTHits.push_back( (*iHittt)->midPoint() );
      }
    }
    
    // Get the OT hits
    for ( LHCb::MCHits::const_iterator oHitIt = otHits->begin() ; 
          otHits->end() != oHitIt ; oHitIt++ ) {
      if ( (*oHitIt)->mcParticle() ==  mcSeedPart ) {
        trHits.push_back( (*oHitIt)->midPoint() );
      }
    }
    if ( 4  > TTHits.size() || 11 > trHits.size() )  continue;
    
    //== Fit the TT area
    double axt, bxt, ayt, byt;
    //double axt2,bxt2,cxt2;
    
    debug() << "  TT: ";
    m_fitTool->fitLine( TTHits, 0, m_zTT1, axt, bxt );
    m_fitTool->fitLine( TTHits, 1, m_zTT1, ayt, byt );
    //m_fitTool->fitParabola( TTHits, 0, m_zTT1, axt2, bxt2, cxt2 );

    double ax, bx, cx, dx, ay, by;
    m_fitTool->fitCubic( trHits, 0, m_zRef, ax, bx, cx, dx );
    m_fitTool->fitLine(  trHits, 1, m_zRef, ay, by );

    const double zMagnet = (axt-bxt*m_zTT1 - (ax-bx*m_zRef) ) / (bx-bxt);
    const double xMagnet = axt + ( zMagnet - m_zTT1 ) * bxt;
    const double yMagnet = ayt + ( zMagnet - m_zTT1 ) * byt;
        
    LHCb::State* state = &track->closestState( 10000. );

    const double zMagnetExp =m_zMagParams [0] + 
      m_zMagParams[1] * state->ty() * state->ty() +  
      m_zMagParams[2] * state->tx() * state->tx() +
      m_zMagParams[3] * 1/state->p() +
      m_zMagParams[4] * std::abs( state->x() ) +
      m_zMagParams[5] * std::abs( state->y() ) +
      m_zMagParams[6] * std::abs( state->ty() );

    
    const double dzExp        = zMagnetExp - state->z();
    const double xMagnetExp   = state->x() + dzExp * state->tx();
    const double bxtExp       = xMagnetExp / zMagnetExp;
    const double dSlope       = std::abs( bxtExp - state->tx() );
    const double dSlope2      = dSlope*dSlope;
   
    // -- Number of IT hits
    const unsigned int nbIT = std::count_if( track->lhcbIDs().begin(), track->lhcbIDs().end(), 
                                             [](const LHCb::LHCbID id){ return id.isIT();});


    // -- this is how it is done in PatDownTrack for OT tracks.
    // -- see LHCb-PUB-2017-001
    double byExp = state->y() / ( state->z() + 
                                  ( m_yParams[0] * std::abs(state->ty()) * zMagnet + m_yParams2[0] )* dSlope2  );
    
    if( nbIT > 4 ) byExp      = state->ty();
    const double bytExp       = byExp*(1.0+m_yParams[0]*dSlope2*std::abs(byExp));
    const double yMagnetExp   = state->y() + dzExp * byExp - m_yParams2[0] * byExp * dSlope2;
    const double deltaZMagnet = zMagnet - zMagnetExp;
    const double deltaXMagnet = xMagnet - xMagnetExp;
    const double deltaYMagnet = yMagnet - yMagnetExp;

    // -- Check difference in momentum
    const double p =  mcSeedPart->p();
    const double pExp = (m_momParams[0] + m_momParams[1] * state->tx() * state->tx() +m_momParams[2] * state->ty() * state->ty() ) / dSlope  * -1;

    Tuple resoTuple = nTuple( "resoTuple", "resoTuple" );
    resoTuple->column( "zMagnet", zMagnet );
    resoTuple->column( "yMagnet", yMagnet );
    resoTuple->column( "xMagnet", xMagnet );
    resoTuple->column( "zMagnetExp", zMagnetExp );
    resoTuple->column( "xMagnetExp", xMagnetExp );
    resoTuple->column( "yMagnetExp", yMagnetExp );
    resoTuple->column( "deltaZMagnet", deltaZMagnet );
    resoTuple->column( "deltaXMagnet", deltaXMagnet );
    resoTuple->column( "deltaYMagnet", deltaYMagnet );
    resoTuple->column( "dSlope", dSlope );
    resoTuple->column( "deltaP", p - std::abs(pExp) );
    resoTuple->column( "pTrue", p );
    resoTuple->column( "nIT", double(nbIT) );
    resoTuple->column( "ay", ay );
    resoTuple->column( "by", by );
    resoTuple->column( "byExp", state->ty() );
    resoTuple->column( "byt", byt );
    resoTuple->column( "bytExp", bytExp );
    resoTuple->column( "ayt", ayt );
        
    resoTuple->write();
    
  }
  

}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode PatLongLivedParams::finalize() {

  debug() << "==> Finalize" << endmsg;

  MsgStream& msg = info() << "============================================" << endmsg;
  msg << "  Processed " << m_nEvent << " events and " << m_nTrack << " tracks. " << endmsg;
  msg << "============================================" << endmsg;
  m_zMagPar.updateParameters( msg );
  m_momPar.updateParameters(  msg );
  m_curvature.updateParameters(  msg );
  m_yPar.updateParameters(  msg );
  m_yPar2.updateParameters(  msg );
  
  std::cout << std::endl;
  m_zMagPar.printPythonParams( name() );
  m_momPar.printPythonParams(  name() );
  m_curvature.printPythonParams(  name() );
  m_yPar.printPythonParams(  name() );
  m_yPar2.printPythonParams(  name() );
  
  std::cout << std::endl;

  m_zMagPar.printParams( "PatLongLivedTracking" );
  m_momPar.printParams(  "PatLongLivedTracking" );
  m_curvature.printParams(  "PatLongLivedTracking" );
  m_yPar.printParams(  "PatLongLivedTracking" );
  m_yPar2.printParams(  "PatLongLivedTracking" );
  std::cout << std::endl;

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}
//=============================================================================
