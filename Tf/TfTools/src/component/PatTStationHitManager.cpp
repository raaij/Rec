//From TfKernel
#include "TfKernel/OTHit.h"
#include "TfKernel/RecoFuncs.h"

// local
#include "PatKernel/PatTStationHitManager.h"

DECLARE_TOOL_FACTORY( PatTStationHitManager )

void PatTStationHitManager::prepareHits() const
{
  if ( allHitsPrepared() ) return;
  //explicitly trigger decoding -- this will implicitly set allHitsPrepared!
  Tf::TStationHitManager<PatForwardHit>::prepareHits();
  // and now sort, and flag next/prev OT hit
  for (Tf::TStationID sta=0; sta<maxStations(); ++sta) {
    for (Tf::TLayerID lay=0; lay<maxLayers(); ++lay) {
      for (Tf::OTRegionID reg=0; reg<maxOTRegions(); ++reg) {
        PatFwdHit* prevHit = nullptr;
        double lastCoord = -10000000.;
        for ( auto& hit : hits(sta,lay,reg) ) {
          if ( hit->hit()->xAtYEq0() - lastCoord < 3. ) {
            hit->setHasPrevious( true );
            prevHit->setHasNext( true ); // For the first iteration, when prevHit == nullptr,
                                         // the intial value of lastCoord is such that this
                                         // branch is never taken, and thus there is no
                                         // nullptr dereference here.
          }
          lastCoord = hit->hit()->xAtYEq0();
          prevHit = hit;
        }
      }
    }
  }
}
