#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/IIncidentSvc.h"

#include "TfKernel/DefaultVeloPhiHitManager.h"


//-----------------------------------------------------------------------------
// Implementation file for class : DefaultVeloPhiHitManager
//
// 2007-08-07 : Kurt Rinnert <kurt.rinnert@cern.ch>
//-----------------------------------------------------------------------------
namespace Tf {

DECLARE_TOOL_FACTORY( DefaultVeloPhiHitManager )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DefaultVeloPhiHitManager::DefaultVeloPhiHitManager(const std::string& type,
    const std::string& name,
    const IInterface* parent)
  : DefaultVeloHitManager<DeVeloPhiType,VeloPhiHit,2>(type, name, parent)
{
  declareInterface<DefaultVeloPhiHitManager>(this);
}


} // namespace Tf
