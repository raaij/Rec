// $Id: RichRingRecTransformTool.h,v 1.2 2009-06-05 17:21:35 jonrob Exp $
#ifndef RICHRINGRECTRANSFORMTOOL_H
#define RICHRINGRECTRANSFORMTOOL_H 1

// Include files
// from Gaudi
#include "RichRingRec/RichRingRecToolBase.h"
#include "RichRingRec/IRichRingRecTransformTool.h"            // Interface

namespace Rich
{
  namespace Rec
  {
    namespace TemplateRings
    {

      /** @class RichRingRecTransformTool RichRingRecTransformTool.h
       *
       *
       *  @author Sajan EASO
       *  @date   2007-06-12
       */
      class RichRingRecTransformTool : public RichRingRecToolBase,
                                       virtual public IRichRingRecTransformTool {
      public:
        /// Standard constructor
        RichRingRecTransformTool( const std::string& type,
                                  const std::string& name,
                                  const IInterface* parent);

        ~RichRingRecTransformTool( ); ///< Destructor

        double CartToPolRad(double x, double y, double aXc, double aYc ) override;
        double CartToPolPhi(double x, double y, double aXc, double aYc ) override;
        double CartToPolRadFromTHNum(int aHitNum, int aTrackNum , int rad) override;
        double CartToPolPhiFromTHNum(int aHitNum, int aTrackNum , int rad) override;
        double getYHitCoord(int aHitNum, int aTrackNum , int rad, int iRich);
        int  SearchIndexInVect(VD aVect, double aVal) override;
        double XDiffValue(int aHitNum, int aTrackNum, int  rad, int iRich) override;
        double YDiffValue(int aHitNum, int aTrackNum, int  rad, int iRich) override;
        VD GetCorrelation (VD aTar, VD aTemp) override;

        VI FindPeakInRP(VVD aPolVec, int rad) override;
        VI getMeanPeakPolarInvA(VVD aPolVec, double aThrValue );



        int RichDetNumFromRadiator(int rad) override;
        int RichDetNumFromHitZ(double GlobalZHit) override;
        void InitRichRingRecTransformTool();
        VI getTrackIndexLimits( int irad) override;

        void setRadiusToBGParam();
        void setBGToRadiusParam();
        void setRadiusFitLimits ();
        double  getBGFromRadius(int irad, double aRadius) override;
        double getRadiusFromBG(int irad, double aBG) override;
        double TrackExpectedRadiusValue(int itk, int irad, int PartType) override;
        double TrackExpectedRadiusFromTruePidCode(int itk, int irad,
                                                  int aTruePidCode) override;
        double getTrackMassFromRadius(int itk, int irad, double aRadius ) override;
        VD TrackExpectedRadii(int itk, int irad) override;
        bool CheckBGThreshold( double aMass, int itk, int irad) override;
        double  MaxRadiusFit( int irad) override;

      protected:

      private:

        // local variables

        int m_NumPeakSearch;


        VD m_AgelRadiusToBGParam;
        VD m_AgelBGToRadiusParam;

        VD m_R1gRadiusToBGParam;
        VD m_R1gBGToRadiusParam;

        VD m_R2gRadiusToBGParam;
        VD m_R2gBGToRadiusParam;


        double  m_AgelMinRadiusFit;
        double  m_AgelMaxRadiusFit;
        double  m_R1gMinRadiusFit;
        double  m_R1gMaxRadiusFit;
        double  m_R2gMinRadiusFit;
        double  m_R2gMaxRadiusFit;
        double   m_AgelMinBGFit;
        double   m_AgelMaxBGFit;
        double   m_R1gMinBGFit;
        double   m_R1gMaxBGFit;
        double   m_R2gMinBGFit;
        double   m_R2gMaxBGFit;
        VD m_MinBGFit;

        double   m_AgelBGTolerence;
        double   m_R1gBGTolerence;
        double   m_R2gBGTolerence;


      };

    }
  }
}

#endif // RICHRINGRECTRANSFORMTOOL_H
