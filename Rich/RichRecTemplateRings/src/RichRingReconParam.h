// $Id: RichRingReconParam.h,v 1.5 2009-12-16 13:42:49 seaso Exp $
#ifndef RICHRINGRECONPARAM_H
#define RICHRINGRECONPARAM_H 1

// Include files
// from Gaudi
#include "RichRingRec/RichRingRecToolBase.h"
#include "RichRingRec/IRichRingReconParam.h"            // Interface

namespace Rich
{
  namespace Rec
  {
    namespace TemplateRings
    {

      /** @class RichRingReconParam RichRingReconParam.h
       *
       *
       *  @author Sajan EASO
       *  @date   2007-06-12
       */
      class RichRingReconParam : public RichRingRecToolBase,
                                 virtual public IRichRingReconParam {
      public:
        /// Standard constructor
        RichRingReconParam( const std::string& type,
                            const std::string& name,
                            const IInterface* parent);

        ~RichRingReconParam( ); ///< Destructor


        double YAgelShift()  override {  return m_YAgelShift;}
        int MinRadiator()  override {  return m_MinRadiator;}
        int MaxRadiator()  override {  return m_MaxRadiator;}
        bool  ActivateSingleEvHisto() override {  return  m_ActivateSingleEvHisto;}
        int MinTrackNumForDebug() override {    return m_MinTrackNumForDebug;}
        int MaxTrackNumForDebug() override {    return m_MaxTrackNumForDebug;}
        bool ActivateSingleTrackDebug()  override {  return m_ActivateSingleTrackDebug;}
        std::string RingRecHistoPath()  override {  return m_RingRecHistoPath;}
        bool ActivateRandomPhiLocalScaling()  override {  return m_ActivateRandomPhiLocalScaling;}
        bool ActivateMCCompareMassPrint()  override {  return m_ActivateMCCompareMassPrint;}
        bool ActivateMaxNumberOfTrackSegmentsInRadiator() override
        {  return m_ActivateMaxNumberOfTrackSegmentsInRadiator;}
        int MaxNumTrackSegmentsInAerogel() override
        {    return m_MaxNumTrackSegmentsInAerogel;}
        int MaxNumTrackSegmentsInR1gas() override
        {    return m_MaxNumTrackSegmentsInR1gas;}
        int MaxNumTrackSegmentsInR2gas() override
        {    return m_MaxNumTrackSegmentsInR2gas;}
        //  bool WriteOutAuxiliaryDebugHisto()  {    return m_WriteOutAuxiliaryDebugHisto;}
        // std::string RichDebugAuxHistoFileName()  {    return m_RichDebugAuxHistoFileName;}

        double MinTrackMomentumSelectInAerogel() override {return m_MinTrackMomentumSelectInAerogel;}


        double MinTrackMomentumSelectInRich1Gas() override {return m_MinTrackMomentumSelectInRich1Gas;}

        double MinTrackMomentumSelectInRich2Gas() override {return m_MinTrackMomentumSelectInRich2Gas;}
        bool ActivateWithoutTrackMomentumInfo() override {return m_ActivateWithoutTrackMomentumInfo;
        }



        StatusCode initialize() override;


      protected:

      private:


        double m_YAgelShift;


        int  m_MinRadiator;
        int  m_MaxRadiator;
        bool m_ActivateSingleEvHisto;
        int m_MinTrackNumForDebug;
        int m_MaxTrackNumForDebug;
        bool m_ActivateSingleTrackDebug;
        std::string m_RingRecHistoPath;

        //  bool m_WriteOutAuxiliaryDebugHisto;
        // std::string m_RichDebugAuxHistoFileName;
        bool m_ActivateRandomPhiLocalScaling;
        bool m_ActivateMCCompareMassPrint;
        bool m_ActivateMaxNumberOfTrackSegmentsInRadiator;
        int  m_MaxNumTrackSegmentsInAerogel;
        int  m_MaxNumTrackSegmentsInR1gas;
        int  m_MaxNumTrackSegmentsInR2gas;

        double m_MinTrackMomentumSelectInAerogel;
        double m_MinTrackMomentumSelectInRich1Gas;
        double m_MinTrackMomentumSelectInRich2Gas;

        bool m_ActivateWithoutTrackMomentumInfo;



      };

    }
  }
}

#endif // RICHRINGRECONPARAM_H
