
//---------------------------------------------------------------------------
/** @file RichTracklessRingFilterAlg.h
 *
 *  Header file for algorithm class : Rich::Rec::TracklessRingFilterAlg
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   10/01/2003
 */
//---------------------------------------------------------------------------

#ifndef RICHRECALGORITHMS_RichTracklessRingFilterAlg_H
#define RICHRECALGORITHMS_RichTracklessRingFilterAlg_H 1

// Array properties. Must be first..
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Base class
#include "RichRecBase/RichRecAlgBase.h"

// Event
#include "Event/RichRecStatus.h"
#include "Event/RichRecRing.h"

// Rich Utils
#include "RichUtils/RichMap.h"

namespace Rich
{
  namespace Rec
  {

    //---------------------------------------------------------------------------
    /** @class TracklessRingFilterAlg RichTracklessRingFilterAlg.h
     *
     *  Reads in a set of trackless RichRecRing objects and produces a filtered
     *  container of the best rings
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   12/06/2008
     */
    //---------------------------------------------------------------------------

    class TracklessRingFilterAlg final : public Rich::Rec::AlgBase
    {

    public:

      /// Standard constructor
      TracklessRingFilterAlg( const std::string& name, ISvcLocator* pSvcLocator );

      StatusCode initialize() override; ///< Algorithm initialisation
      StatusCode execute() override;    ///< Algorithm execution

    private:

      /// Input location in TES for rings
      std::string m_inputRings;

      /// Output location in TES for rings
      std::string m_outputRings;

      /// Minimum number of hits on selected rings
      RadiatorArray<unsigned int> m_minNumHits = {{}};

      /// Minimum average hit probability for selected rings
      RadiatorArray<double> m_minAvProb = {{}};

    };

  }
}

#endif // RICHRECALGORITHMS_RichTracklessRingFilterAlg_H
