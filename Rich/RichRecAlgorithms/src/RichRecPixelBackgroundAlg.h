
//---------------------------------------------------------------------------
/** @file RichRecPixelBackgroundAlg.h
 *
 *  Header file for algorithm class : Rich::Rec::PixelBackgroundAlg
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   10/01/2003
 */
//---------------------------------------------------------------------------

#ifndef RICHRECALGORITHMS_RichRecPixelBackgroundAlg_H
#define RICHRECALGORITHMS_RichRecPixelBackgroundAlg_H 1

// Base class
#include "RichRecBase/RichRecAlgBase.h"

// interfaces
#include "RichRecInterfaces/IRichPixelBackgroundEsti.h"

namespace Rich
{
  namespace Rec
  {

    //---------------------------------------------------------------------------
    /** @class PixelBackgroundAlg RichRecPixelBackgroundAlg.h
     *
     *  Algorithm to run the pixel background algorithm for all segments
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   10/01/2003
     */
    //---------------------------------------------------------------------------

    class PixelBackgroundAlg final : public Rich::Rec::AlgBase
    {

    public:

      /// Standard constructor
      PixelBackgroundAlg( const std::string& name, ISvcLocator* pSvcLocator );

      StatusCode initialize() override; ///< Algorithm initialisation
      StatusCode execute() override;    ///< Algorithm execution

    private:

      /// background tool
      const IPixelBackgroundEsti * m_bkgTool = nullptr;

    };

  }
}

#endif // RICHRECALGORITHMS_RichRecPixelBackgroundAlg_H
