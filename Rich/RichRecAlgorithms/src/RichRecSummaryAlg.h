
//---------------------------------------------------------------------------
/** @file RichRecSummaryAlg.h
 *
 *  Header file for algorithm class : Rich::Rec::SummaryAlg
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   10/01/2003
 */
//---------------------------------------------------------------------------

#ifndef RICHRECALGORITHMS_RichRecSummaryAlg_H
#define RICHRECALGORITHMS_RichRecSummaryAlg_H 1

// Gaudi (must be first)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Base class
#include "RichRecBase/RichRecAlgBase.h"

// Event
#include "Event/RichRecStatus.h"
#include "Event/ProcStatus.h"
#include "Event/RichSummaryTrack.h"

// tool interfaces
#include "RichRecInterfaces/IRichTrackSelector.h"
#include "RichRecInterfaces/IRichCherenkovAngle.h"
#include "RichRecInterfaces/IRichCherenkovResolution.h"
#include "RichRecInterfaces/IRichExpectedTrackSignal.h"

namespace Rich
{
  namespace Rec
  {

    //---------------------------------------------------------------------------
    /** @class SummaryAlg RichRecSummaryAlg.h
     *
     *  Algorithm to fill the reconstruction summary data objects for the RICH
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   07/06/2006
     *
     *  @todo Review if we need to store all RichSmartIDs associated to a pixel 
     *        or if the primary ID is enough ?
     */
    //---------------------------------------------------------------------------

    class SummaryAlg final : public Rich::Rec::AlgBase
    {

    public:

      /// Standard constructor
      SummaryAlg( const std::string& name, ISvcLocator* pSvcLocator );

      StatusCode initialize() override;    // Algorithm initialization
      StatusCode execute() override;    // Algorithm execution

    private:   // Private data members

      /// Rich Cherenkov angle calculator tool
      const ICherenkovAngle * m_ckAngle = nullptr;

      /// Cherenkov angle resolution tool
      const ICherenkovResolution * m_ckAngleRes = nullptr;

      /// Pointer to RichExpectedTrackSignal tool
      const IExpectedTrackSignal * m_tkSignal = nullptr;

      /// Track selector
      const ITrackSelector * m_trSelector = nullptr;

      /// Location to store the summary tracks
      std::string m_summaryLoc;

      /// Number of sigmas to select photons, for each radiator
      RadiatorArray<double> m_nSigma = {{}};

    };

  }
}

#endif // RICHRECALGORITHMS_RichRecSummaryAlg_H
