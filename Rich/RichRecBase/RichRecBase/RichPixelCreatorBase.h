
//---------------------------------------------------------------------------------
/** @file RichPixelCreatorBase.h
 *
 *  Header file for tool base class : Rich::Rec::PixelCreatorBase
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   20/04/2005
 */
//---------------------------------------------------------------------------------

#ifndef RICHRECBASE_RICHPIXELCREATORBASE_H
#define RICHRECBASE_RICHPIXELCREATORBASE_H 1

// STL
#include <functional>
#include <vector>
#include <array>

// Gaudi
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

// base class
#include "RichRecBase/RichRecToolBase.h"

// interfaces
#include "RichInterfaces/IRichPixelSuppressionTool.h"
#include "RichInterfaces/IRichPixelClusteringTool.h"
#include "RichInterfaces/IRichSmartIDTool.h"
#include "RichInterfaces/IRichRawBufferToSmartIDsTool.h"
#include "RichRecInterfaces/IRichPixelCreator.h"
#include "RichRecInterfaces/IRichRecGeomTool.h"

// Rich Utils
#include "RichUtils/RichStatDivFunctor.h"
#include "RichUtils/RichMap.h"
#include "RichUtils/RichHashMap.h"
#include "RichUtils/BoostArray.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Boost
#ifdef __INTEL_COMPILER                   // Disable ICC remark from Boost
#define BOOST_MULTI_ARRAY_NO_GENERATORS 0 // zero used for undefined preprocessing identifier "BOOST_MULTI_ARRAY_NO_GENERATORS"
#endif
#include "boost/multi_array.hpp"
#ifdef __INTEL_COMPILER
#undef BOOST_MULTI_ARRAY_NO_GENERATORS
#endif

namespace Rich
{
  namespace Rec
  {

    //---------------------------------------------------------------------------------------
    /** @class PixelCreatorBase RichPixelCreatorBase.h RichRecBase/RichPixelCreatorBase.h
     *
     *  Base class for all RichRecPixel creator tools.
     *
     *  Implements common functionaility needed by all concrete implementations.
     *  Derived classes must implement the methods newPixels and buildPixel using
     *  whatever means appropriate for that implementation.
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   20/04/2005
     *
     *  @todo Find a better way to handle the filling of the pixel iterators, that avoids a
     *        seperate loop and if possible the maps
     */
    //---------------------------------------------------------------------------------------

    class PixelCreatorBase : public Rich::Rec::ToolBase,
                             virtual public IPixelCreator,
                             virtual public IIncidentListener
    {

    public:

      /// Standard constructor
      PixelCreatorBase( const std::string& type,
                        const std::string& name,
                        const IInterface* parent );

      // Initialize method
      virtual StatusCode initialize() override;

      // Finalize method
      virtual StatusCode finalize() override;

      // Implement the handle method for the Incident service.
      // This is used to inform the tool of software incidents.
      virtual void handle( const Incident& incident ) override;

    public: // methods from interface

      // Returns a pointer to the RichRecPixels
      LHCb::RichRecPixels * richPixels() const override;

      // Access the range for the pixels in the given RICH detector
      IPixelCreator::PixelRange range( const Rich::DetectorType rich ) const override;

      // Access the range for the pixels in the given RICH detector and PD panel
      IPixelCreator::PixelRange range( const Rich::DetectorType rich,
                                       const Rich::Side         panel ) const override;

      // Access the begin iterator for the pixels in the given RICH PD
      IPixelCreator::PixelRange range( const LHCb::RichSmartID pdID ) const override;

      // Form all possible RichRecPixels from RawBuffer
      // The most efficient way to make all RichRecPixel objects in the event.
      StatusCode newPixels() const override;

      // Clear the current transient event data
      void clear() const override;

    private: // helper classes

      /** @class SortByRegion RichPixelCreatorBase.h RichRecBase/RichPixelCreatorBase.h
       *
       *  Class to sort the RichRecPixels according to detector regions
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   20/04/2005
       */
      class SortByRegion final
        : std::binary_function<const LHCb::RichRecPixel*,const LHCb::RichRecPixel*,bool>
      {
      public:
        /// Method to return true if RichRecPixel p1 should be listed before p2
        inline bool operator() ( const LHCb::RichRecPixel * p1,
                                 const LHCb::RichRecPixel * p2 ) const
        {
          return ( p1->pdPixelCluster().primaryID().dataBitsOnly().key() <
                   p2->pdPixelCluster().primaryID().dataBitsOnly().key() );
        }
      };

      /** @class PixStats RichPixelCreatorBase.h RichRecBase/RichPixelCreatorBase.h
       *
       *  Simple class to hold pixel/cluster creation statistics
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   20/04/2005
       */
      class PixStats
      {
      public:
        PixStats() = default;
      public:
        unsigned long long numClusters{0};     ///< Number of selected PD pixel clusters
        unsigned long long numPixels{0};       ///< Number of selected raw PD pixels
        unsigned long long rejectedPixels{0};  ///< number of rejected PD pixels
      public:
        /// Has this tally seen some pixels
        bool hasSomeStats() const noexcept { return numPixels>0 || rejectedPixels>0 ; }
      };

    protected: // methods

      /// Initialise for a new event
      virtual void InitNewEvent() const;

      /// Finalise current event
      virtual void FinishEvent() const;

      /// Read the pixels and fill the RICH and panel begin and end iterators
      void fillIterators() const;

      /// Reset all iterators to default values
      void resetIterators() const;

      /// Sort the RichRecPixel container into detector regions
      inline void sortPixels() const
      {
        std::stable_sort( richPixels()->begin(), richPixels()->end(), SortByRegion() );
      }

      /// Access the final RichRecPixel location in the TES
      inline const std::string & pixelLocation() const noexcept
      {
        return m_richRecPixelLocation;
      }

      /// Is book keeping to be performed
      inline bool bookKeep() const noexcept { return m_bookKeep; }

      /** Check if a given RICH detector is to be used
       *
       *  @param rich The RICH detector type
       *
       *  @return boolean indicating if the given RICH detector is active
       *  @retval true  RICH detector is active
       *  @retval false RICH detector is not in use
       */
      inline bool useDetector( const Rich::DetectorType rich ) const noexcept
      {
        return m_usedDets[rich];
      }

      /** Check the status of the given RICH PD pixel identifier (RichSmartID)
       *
       *  @param id The PD pixel RichSmartID to check
       *
       *  @return boolean indicating if the given channel is active
       *  @retval true  channel is active
       *  @retval false channel is not in use
       */
      inline bool pixelIsOK( const LHCb::RichSmartID id ) const noexcept
      {
        const auto ok = id.isValid();
        if ( UNLIKELY(!ok) ) { warning() << "Invalid ID " << id << endmsg; }
        return ok;
      }

      /** Check the status of the given RICH PD identifier (RichSmartID)
       *
       *  @param id The PD RichSmartID to check
       *
       *  @return boolean indicating if the given channel is active
       *  @retval true  PD is active
       *  @retval false PD is not in use
       */
      inline bool pdIsOK( const LHCb::RichSmartID id ) const
      {
        return ( id.isValid() &&                             // Valid PD ID
                 useDetector(id.rich()) &&                   // This RICH is in use
                 ( !m_pdCheck || m_richSys->pdIsActive(id) ) // If required, check PD is alive
                 );
      }

      /** Save a given pixel to the TES container
       *
       *  @param pix Pointer to the RichRecPixel to save
       */
      inline void savePixel( LHCb::RichRecPixel * pix ) const
      {
        richPixels()->insert( pix );
        ++(m_hitCount[pix->detector()].numClusters);
        m_hitCount[pix->detector()].numPixels += pix->pdPixelCluster().size();
        m_hasBeenCalled = true;
      }

      /** Apply PD pixel suppression, if configured to do so
       *
       *  @param pdID    RichSmartID for PD
       *  @param smartIDs Vector of pixel smartIDs for this PD
       *
       *  @return Reference to the cleaned SmartID container
       */
      inline LHCb::RichSmartID::Vector
      applyPixelSuppression( const LHCb::RichSmartID pdID,
                             const LHCb::RichSmartID::Vector & smartIDs ) const
      {
        // If no pixel suppression, just return orginals
        if ( !m_applyPixelSuppression ) { return smartIDs; }
        else
        {
          // apply cleaning
          // copied container for 'cleaned' IDs since we might remove
          // some, and we cannot change the raw data from smartIDdecoder()
          auto cleanedSmartIDs = smartIDs;
          // start size
          const auto startSize = smartIDs.size();
          // which rich
          const auto rich = pdID.rich();
          // clean
          pdSuppTool(rich)->applyPixelSuppression(pdID,cleanedSmartIDs);
          // some stats
          m_hitCount[rich].rejectedPixels += (startSize-cleanedSmartIDs.size());
          // return
          return cleanedSmartIDs;
        }
      }

      /// Build a new RichRecPixel from an Rich::PDPixelCluster
      virtual LHCb::RichRecPixel * buildPixel ( const Rich::PDPixelCluster& cluster ) const;

      /// Build a new RichRecPixel from a single LHCb::RichSmartID
      virtual LHCb::RichRecPixel * buildPixel( const LHCb::RichSmartID & id ) const;

      /// Access the RichSmartIDTool
      inline const ISmartIDTool * smartIDTool() const
      {
        if (!m_idTool) { acquireTool( "RichSmartIDTool", m_idTool, nullptr, true ); }
        return m_idTool;
      }

      /// Access the RichSmartIDDecoder
      inline const Rich::DAQ::IRawBufferToSmartIDsTool * smartIDdecoder() const
      {
        if (!m_decoder) { acquireTool( "RichSmartIDDecoder", m_decoder, nullptr, true ); }
        return m_decoder;
      }

      /// Access the geometry tool
      inline const IGeomTool * geomTool() const noexcept
      {
        return m_geomTool;
      }

    protected: // data

      /// Flag to signify all pixels have been formed
      mutable bool m_allDone = false;

      /// Map between RichSmartID and the associated RichRecPixel
      mutable Rich::HashMap< LHCb::RichSmartID::KeyType, LHCb::RichRecPixel* > m_pixelExists;

      /// Map indicating if a given RichSmartID has been considered already
      mutable Rich::HashMap< LHCb::RichSmartID::KeyType, bool > m_pixelDone;

    private: // definitions

      /// ND array type
      template< typename TYPE, unsigned int N >
      using NDArray = boost::multi_array<TYPE,N>;

    private: // data

      /// Pointer to RICH system detector element
      const DeRichSystem * m_richSys = nullptr;

      /// PD occupancy tools
      mutable DetectorArray<const Rich::DAQ::IPixelSuppressionTool*> m_pdOcc = {{nullptr,nullptr}};

      /// PD clustering tools
      mutable DetectorArray<const Rich::DAQ::IPixelClusteringTool*> m_pdClus = {{nullptr,nullptr}};

      /// Pointer to RichSmartID tool
      mutable const ISmartIDTool * m_idTool = nullptr;

      /// Raw Buffer Decoding tool
      mutable const Rich::DAQ::IRawBufferToSmartIDsTool * m_decoder = nullptr;

      /// Geometry tool
      const IGeomTool * m_geomTool = nullptr;

      /// Pointer to RichRecPixels
      mutable LHCb::RichRecPixels * m_pixels = nullptr;

      /// Flag to turn on or off the book keeping features to save cpu time.
      bool m_bookKeep;

      /// Flag to turn on or off the explicit checking of the PD status
      bool m_pdCheck;

      /** @brief Flag to turn on/off use of clusters in pixel reconstruction.
       *  If set to true cluster finding will be run, and a single RichRecPixel will be created for each cluster.
       *  If false, cluster finding will STILL be run but each hit will be made into a single RichRecPixel.
       *  In both cases, the associated cluster for each RichRecPixel is still set. */
      DetectorArray<bool> m_clusterHits = {{}};

      /** @brief Flag to suppress the finding of clusters entirely (for speed)
       *  If set to true, no clusters will be found and the associated cluster pointers are not set. */
      bool m_noClusterFinding;

      /// Flags for which RICH detectors to create pixels for
      DetectorArray<bool> m_usedDets = {{}};

      /// Location of RichRecPixels in TES
      std::string m_richRecPixelLocation;

      /// Begin iterators for various Rich and panel combinations
      mutable NDArray<LHCb::RichRecPixels::iterator,2> m_begins { boost::extents[Rich::NRiches][Rich::NPDPanelsPerRICH] };

      /// End iterators for various Rich and panel combinations
      mutable NDArray<LHCb::RichRecPixels::iterator,2> m_ends   { boost::extents[Rich::NRiches][Rich::NPDPanelsPerRICH] };

      /// Begin iterators for each RICH
      mutable DetectorArray<LHCb::RichRecPixels::iterator> m_richBegin = {{}};

      /// End iterators for each RICH
      mutable DetectorArray<LHCb::RichRecPixels::iterator> m_richEnd = {{}};

      /// PD range map type
      using PDItPair = std::pair<LHCb::RichRecPixels::iterator,LHCb::RichRecPixels::iterator>;
      using PDItMap  = Rich::Map<const LHCb::RichSmartID,PDItPair>;

      /// iterators for each PD
      mutable PDItMap m_pdIts;

      /// Hit count tally
      mutable DetectorArray<PixStats> m_hitCount = {{}};

      /// Event count
      mutable unsigned long long m_Nevts = 0;

      /// Flag to indicate if the tool has been used in a given event
      mutable bool m_hasBeenCalled = false;

      /// Should PD occupancy be monitored
      bool m_applyPixelSuppression;

      /// Maximum number of pixels per event
      unsigned int m_maxPixels;

    private: // methods

      /// Printout the pixel creation statistics
      void printStats() const;

      /// returns a pointer to the PD suppression tool for the given RICH
      inline const Rich::DAQ::IPixelSuppressionTool *
      pdSuppTool( const Rich::DetectorType rich ) const
      {
        if ( !m_pdOcc[rich] )
        {
          acquireTool( "PixelSuppress"+Rich::text(rich), m_pdOcc[rich], this );
        }
        return m_pdOcc[rich];
      }

      /// returns a pointer to the PD clustering tool for the given RICH
      inline const Rich::DAQ::IPixelClusteringTool *
      pdClusTool( const Rich::DetectorType rich ) const
      {
        if ( !m_pdClus[rich] )
        {
          acquireTool( "PixelCreatorClustering",
                       "PixelClustering"+Rich::text(rich), m_pdClus[rich], this );
        }
        return m_pdClus[rich];
      }

    };

  }
} // RICH

#endif // RICHRECBASE_RICHPIXELCREATORBASE_H
