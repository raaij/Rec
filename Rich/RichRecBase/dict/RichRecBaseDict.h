#ifndef DICT_RICHRECBASEDICT_H 
#define DICT_RICHRECBASEDICT_H 1

#include "Event/RichRecSegment.h"
#include "Event/RichRecPixel.h"
#include "Event/RichRecTrack.h"
#include "Event/RichRecRing.h"
#include "Event/RichRecPhoton.h"
#include "Event/RichRecStatus.h"
#include "RichRecUtils/RichRecPhotonKey.h"
#include "RichRecUtils/RichRadCorrLocalPositions.h"
#include "RichRecBase/FastRingFitter.h"

// instantiate some templated classes, to get them into the dictionary
namespace 
{
  struct _Instantiations 
  {
    // nothing to do
  };
}

#endif // DICT_RICHRECBASEDICT_H
