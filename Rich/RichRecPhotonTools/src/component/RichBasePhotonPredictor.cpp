
//-----------------------------------------------------------------------------
/** @file RichBasePhotonPredictor.cpp
 *
 *  Implementation file for tool : Rich::Rec::BasePhotonPredictor
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   26/07/2007
 */
//-----------------------------------------------------------------------------

// Array properties. Must be first..
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// local
#include "RichBasePhotonPredictor.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Rec;

//-----------------------------------------------------------------------------

// Standard constructor
BasePhotonPredictor::
BasePhotonPredictor( const std::string& type,
                     const std::string& name,
                     const IInterface* parent )
  : Rich::Rec::ToolBase( type, name, parent )
{
  // initialise
  m_scale  .fill( 0 );
  m_maxROI2.fill( 0 );
  m_minROI2.fill( 0 );

  // interface
  declareInterface<IPhotonPredictor>(this);

  // default values                            Aero    R1Gas   R2Gas
  // job options
  declareProperty( "MinTrackROI", m_minROI = { 110,     0,       0 } );
  declareProperty( "MaxTrackROI", m_maxROI = { 390,    86,     165 } );
  declareProperty( "NSigma",      m_nSigma = { 5.5,   5.5,    11.5 } );
}

StatusCode BasePhotonPredictor::initialize()
{
  // Initialise base class
  const StatusCode sc = ToolBase::initialize();
  if ( sc.isFailure() ) { return sc; }

  // debugging
  //setProperty( "OutputLevel", MSG::VERBOSE );

  // get tools
  acquireTool( "RichCherenkovAngle",          m_ckAngle  );
  acquireTool( "RichRecGeometry",             m_geomTool );
  acquireTool( "RichCherenkovResolution",     m_ckRes    );
  acquireTool( "RichParticleProperties",  m_richPartProp );

  // Are we upgrade detector ?
  DeRich1 * rich1DE = getDet<DeRich1>( DeRichLocations::Rich1 );
  const bool isUpgrade = rich1DE->RichGeometryConfig() == 1;

  // Setup scaling parameters
  std::vector<double> ckThetaMax; // Scaling parameter - Max CK theta point
  std::vector<double> sepGMax;    // Scaling parameter - Max separation point
  // RICH1 Gas rings are larger in the upgrade ...
  if ( !isUpgrade )
  {
    ckThetaMax = { 0.24, 0.052, 0.03 };
    sepGMax    = { 342,  75,     130 };
  }
  else
  {
    ckThetaMax = { 0.24, 0.052, 0.03 };
    sepGMax    = { 342,  98.2,   130 };
  }

  // loop over radiators
  for ( const auto rad : Rich::radiators() )
  {
    // cache some numbers
    m_minROI2[rad] = std::pow(m_minROI[rad],2);
    m_maxROI2[rad] = std::pow(m_maxROI[rad],2);
    m_scale[rad] = (ckThetaMax[rad]/sepGMax[rad]);
    if ( msgLevel(MSG::DEBUG) )
    {
      // printout for this rad
      auto trad = Rich::text(rad);
      trad.resize(8,' ');
      debug() << trad << " : Sep. range     "
              << boost::format("%5.1f") % m_minROI[rad] << " -> "
              << boost::format("%5.1f") % m_maxROI[rad] << " mm  : Tol. "
              << boost::format("%5.1f") % m_nSigma[rad] << " # sigma" << endmsg;
    }
  }

  m_pidTypes = m_richPartProp->particleTypes();
  _ri_debug << "Particle types considered = " << m_pidTypes << endmsg;

  return sc;
}

