
//-----------------------------------------------------------------------------
/** @file RichSimplePhotonPredictor.cpp
 *
 *  Implementation file for tool : Rich::Rec::SimplePhotonPredictor
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

// local
#include "RichSimplePhotonPredictor.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Rec;

//-----------------------------------------------------------------------------

DECLARE_TOOL_FACTORY( SimplePhotonPredictor )

// Standard constructor
SimplePhotonPredictor::
SimplePhotonPredictor( const std::string& type,
                       const std::string& name,
                       const IInterface* parent )
  : BasePhotonPredictor ( type, name, parent )
{
  m_Nselected.fill(0);
  m_Nreject.fill(0);
}

StatusCode SimplePhotonPredictor::finalize()
{

  if ( m_Nselected[Rich::Aerogel]  > 0 ||
       m_Nselected[Rich::Rich1Gas] > 0 ||
       m_Nselected[Rich::Rich2Gas] > 0 )
  {
    // statistical tool
    const PoissonEffFunctor occ("%10.2f +-%7.2f");

    // printout stats
    info() << "======================================================================" << endmsg
           << "           Pixel/Segment combination selection summary : " << endmsg
           << "  Aerogel   : Selected "
           << occ(m_Nselected[Rich::Aerogel],m_Nselected[Rich::Aerogel]+m_Nreject[Rich::Aerogel])
           << " % of possible candidates" << endmsg
           << "  Rich1Gas  : Selected "
           << occ(m_Nselected[Rich::Rich1Gas],m_Nselected[Rich::Rich1Gas]+m_Nreject[Rich::Rich1Gas])
           << " % of possible candidates" << endmsg
           << "  Rich2Gas  : Selected "
           << occ(m_Nselected[Rich::Rich2Gas],m_Nselected[Rich::Rich2Gas]+m_Nreject[Rich::Rich2Gas])
           << " % of possible candidates" << endmsg
           << "======================================================================" << endmsg;
  }

  // Execute base class method
  return BasePhotonPredictor::finalize();
}

// fast decision on whether a photon is possible
bool
SimplePhotonPredictor::photonPossible( LHCb::RichRecSegment * segment,
                                       LHCb::RichRecPixel * pixel ) const
{

  // Default to not selected
  bool OK = false;

  // which radiator
  const auto rad = segment->trackSegment().radiator();

  // Are they in the same Rich detector ?
  if ( segment->trackSegment().rich() == pixel->detector() )
  {

    // Hit seperation criteria : based on global coordinates
    const double sep2 = m_geomTool->trackPixelHitSep2(segment, pixel);
    if ( sep2 < m_maxROI2[rad] && sep2 > m_minROI2[rad] )
    {
      OK = true;
    }

  }

  if ( OK ) { ++m_Nselected[rad]; }
  else      { ++m_Nreject[rad];   }

  return OK;
}

