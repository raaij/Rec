
//-----------------------------------------------------------------------------
/** @file RichSepVCKthetaPhotonPredictor.cpp
 *
 *  Implementation file for tool : Rich::Rec::SepVCKthetaPhotonPredictor
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   01/06/2005
 */
//-----------------------------------------------------------------------------

// Array properties. Must be first..
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// local
#include "RichSepVCKthetaPhotonPredictor.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Rec;

//-----------------------------------------------------------------------------

DECLARE_TOOL_FACTORY( SepVCKthetaPhotonPredictor )

// Standard constructor
SepVCKthetaPhotonPredictor::
SepVCKthetaPhotonPredictor( const std::string& type,
                            const std::string& name,
                            const IInterface* parent )
  : BasePhotonPredictor( type, name, parent )
{
  m_Nselected.fill(0);
  m_Nreject.fill(0);
  declareProperty( "TolerenceFactor", m_tolF = { 25, 15, 40 } );
}

StatusCode SepVCKthetaPhotonPredictor::finalize()
{

  if ( m_Nselected[Rich::Aerogel]  > 0 ||
       m_Nselected[Rich::Rich1Gas] > 0 ||
       m_Nselected[Rich::Rich2Gas] > 0 )
  {

    // statistical tool
    const PoissonEffFunctor occ("%10.2f +-%7.2f");

    // printout stats
    info() << "=================================================================" << endmsg
           << "  Pixel/Segment combination selection summary :-" << endmsg
           << "    Aerogel   : "
           << occ(m_Nselected[Rich::Aerogel],m_Nselected[Rich::Aerogel]+m_Nreject[Rich::Aerogel])
           << " % of possible candidates" << endmsg
           << "    Rich1Gas  : "
           << occ(m_Nselected[Rich::Rich1Gas],m_Nselected[Rich::Rich1Gas]+m_Nreject[Rich::Rich1Gas])
           << " % of possible candidates" << endmsg
           << "    Rich2Gas  : "
           << occ(m_Nselected[Rich::Rich2Gas],m_Nselected[Rich::Rich2Gas]+m_Nreject[Rich::Rich2Gas])
           << " % of possible candidates" << endmsg
           << "=================================================================" << endmsg;
  }

  // Execute base class method
  return BasePhotonPredictor::finalize();
}

// fast decision on whether a photon is possible
bool
SepVCKthetaPhotonPredictor::photonPossible( LHCb::RichRecSegment * segment,
                                            LHCb::RichRecPixel * pixel ) const
{

  // Default to not selected
  bool OK = false;

  // which radiator
  const auto rad = segment->trackSegment().radiator();

  // Are they in the same Rich detector ?
  if ( segment->trackSegment().rich() == pixel->detector() )
  {

    // segment / hit separation squared
    const double sep2 = m_geomTool->trackPixelHitSep2(segment,pixel);

    // Check overall boundaries
    if ( sep2 < m_maxROI2[rad] && sep2 > m_minROI2[rad] )
    {

      // Cache separation
      const double sep = sqrt(sep2);

      // Loop over mass hypos and check finer grained boundaries
      for ( Rich::Particles::const_iterator hypo = m_pidTypes.begin();
            hypo != m_pidTypes.end(); ++hypo )
      {

        // expected separation, scales linearly with expected CK angle
        const double expSep = m_ckAngle->avgCherenkovTheta(segment,*hypo) * m_scale[rad];

        // is this pixel/segment pair in the accepted range
        const double dsep = fabs(sep-expSep);
        if ( dsep < m_tolF[rad] )
        {
          OK = true;
          //if ( msgLevel(MSG::VERBOSE) )
          //{
          //  verbose() << "  -> " << *hypo << " fabs(sep-expSep)="
          //            << dsep << " PASSED tol=" << m_tolF[rad] << endmsg;
          //}
          break;
        }
        //if ( msgLevel(MSG::VERBOSE) && !OK )
        //{
        //  verbose() << "  -> " << *hypo << " fabs(sep-expSep)="
        //            << dsep << " FAILED tol=" << m_tolF[rad] << " -> reject" << endmsg;
        //}

      } // loop over hypos

    } // overall boundary check
      //else if ( msgLevel(MSG::VERBOSE) )
      //{
      //  verbose() << "  -> sep2=" << sep2
      //            << " FAILED overall boundary check " << m_minROI2[rad] << "->" << m_maxROI2[rad]
      //            << " -> reject" << endmsg;
      //}

  } // same detector
  //else if ( msgLevel(MSG::VERBOSE) )
  //{
  //  verbose() << "  -> " << " FAILED RICH detector check -> reject" << endmsg;
  //}

  if ( OK ) { ++m_Nselected[rad]; }
  else      { ++m_Nreject[rad];   }

  return OK;
}

