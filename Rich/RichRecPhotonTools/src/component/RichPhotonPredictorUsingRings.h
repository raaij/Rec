
//----------------------------------------------------------------------------------
/** @file RichPhotonPredictorUsingRings.h
 *
 *  Header file for tool : Rich::Rec::PhotonPredictorUsingRings
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//----------------------------------------------------------------------------------

#ifndef RICHRECTOOLS_RICHPHOTONPREDICTORUSINGRINGS_H
#define RICHRECTOOLS_RICHPHOTONPREDICTORUSINGRINGS_H 1

// base class
#include "RichBasePhotonPredictor.h"

// from Gaudi
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

namespace Rich
{
  namespace Rec
  {

    //----------------------------------------------------------------------------------
    /** @class PhotonPredictorUsingRings RichPhotonPredictorUsingRings.h
     *
     *  Tool which performs the association between RichRecSegments and
     *  RichRecPixels to form RichRecPhotons.  This particular instance uses
     *  RichRecRing objects (which must already exist in the TES) to only select
     *  pixel/segment combinations that form a valid Ring candidate.
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   15/03/2002
     */
    //----------------------------------------------------------------------------------

    class PhotonPredictorUsingRings final : public BasePhotonPredictor,
                                            virtual public IIncidentListener
    {

    public: // Methods for Gaudi Framework

      /// Standard constructor
      PhotonPredictorUsingRings( const std::string& type,
                                 const std::string& name,
                                 const IInterface* parent );

      // Initialize method
      StatusCode initialize() override;

    public: // methods (and doxygen comments) inherited from public interface

      // Is it possible to make a photon candidate using this segment and pixel.
      bool photonPossible( LHCb::RichRecSegment * segment,
                           LHCb::RichRecPixel * pixel ) const override;

      // Implement the handle method for the Incident service.
      void handle( const Incident& incident ) override;

    private: // methods

      /// Initialise for a new event
      void InitNewEvent();

      /// Retuns pointer RichRecRings
      LHCb::RichRecRings * richRings() const;

    private: // private data

      /// Pointer to RichRecRings
      mutable LHCb::RichRecRings * m_recRings = nullptr;

      /// location of rings in TES
      std::string m_ringLoc;

    };

    inline void PhotonPredictorUsingRings::InitNewEvent()
    {
      // Initialise navigation data
      m_recRings = nullptr;
    }

    inline LHCb::RichRecRings *
    PhotonPredictorUsingRings::richRings() const
    {
      if ( !m_recRings )
      {
        m_recRings = get<LHCb::RichRecRings>( m_ringLoc );
        _ri_debug << "Located " << m_recRings->size()
                  << " RichRecRings at " << m_ringLoc
                  << endmsg;
      }
      return m_recRings;
    }

  }
}

#endif // RICHRECTOOLS_RICHPHOTONPREDICTORUSINGRINGS_H

