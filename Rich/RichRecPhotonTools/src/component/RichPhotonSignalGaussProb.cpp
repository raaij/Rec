
//-----------------------------------------------------------------------------
/** @file RichPhotonSignalGaussProb.cpp
 *
 *  Implementation file for tool : Rich::Rec::PhotonSignalGaussProb
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

// local
#include "RichPhotonSignalGaussProb.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Rec;

namespace
{
  static const double root_two_pi  = std::sqrt( 2.0 * M_PI );
  constexpr    double two_over_pi2 = 2.0 / ( M_PI * M_PI );
  constexpr    double oneOverTwoPI = 1.0 / ( 2.0  * M_PI );
}

//-----------------------------------------------------------------------------

// Standard constructor
PhotonSignalGaussProb::PhotonSignalGaussProb( const std::string& type,
                                              const std::string& name,
                                              const IInterface* parent )
  : ToolBase( type, name, parent )
{
  // init
  m_radiusCurv.fill(0);
  m_pixelArea.fill(0);
  m_scaleFactor.fill(0);
  m_factor.fill(0);
  // interface
  declareInterface<IPhotonSignal>(this);
  // JOs
  declareProperty( "MinExpArg", m_minArg = -650 );
  // CRJ : Longer term this should not be a job option, but taken from the DB
  declareProperty( "Rich2PixelSignalScaleFactor", m_rich2PixelSignalScaleFactor = 4.0 );
}

StatusCode PhotonSignalGaussProb::initialize()
{

  // Sets up various tools and services
  const StatusCode sc = ToolBase::initialize();
  if ( sc.isFailure() ) { return sc; }

  // force debug
  //setProperty( "OutputLevel", MSG::VERBOSE );

  // Acquire instances of tools
  acquireTool( "RichCherenkovAngle",      m_ckAngle );
  acquireTool( "RichExpectedTrackSignal", m_signal  );
  acquireTool( "RichCherenkovResolution", m_ckRes   );
  acquireTool( "RichParticleProperties",  m_richPartProp );

  // PID types
  m_pidTypes = m_richPartProp->particleTypes();

  // Get Rich Detector elements
  const auto * Rich1DE = getDet<DeRich1>( DeRichLocations::Rich1 );
  const auto * Rich2DE = getDet<DeRich2>( DeRichLocations::Rich2 );

  // Rich System
  m_richSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );

  // Mirror radii of curvature in mm
  m_radiusCurv[Rich::Rich1] = Rich1DE->sphMirrorRadius();
  m_radiusCurv[Rich::Rich2] = Rich2DE->sphMirrorRadius();

  // Upgrade ?
  const bool pmtActivate = ( Rich1DE->RichPhotoDetConfig() == Rich::PMTConfig );

  // These numbers should come from the DB eventually. Not be job options.
  const std::string pname = "Rich2BrunelPidPixelSignalScaleFactor";
  m_rich2PixelSignalScaleFactor = 
    ( Rich1DE->exists(pname) ? Rich1DE->param<double>(pname) : 1.0 );

  m_scaleFactor[Rich::Rich1] = 4.0;
  m_scaleFactor[Rich::Rich2] = ( !pmtActivate ? 4.0 : 4.0 * m_rich2PixelSignalScaleFactor );

  // exp params
  m_expMinArg = myexp( m_minArg );

  // Setup incident services
  incSvc()->addListener( this, IncidentType::BeginEvent );

  // cache the factor for each RICH
  for ( const auto det : { Rich::Rich1, Rich::Rich2 } )
  {
    m_factor[det] = m_scaleFactor[det] / std::pow( m_radiusCurv[det], 2 );
  }

  // Informational Printout
  if ( msgLevel(MSG::DEBUG) )
  {
    debug() << " Mirror radii of curvature = "
            << m_radiusCurv[Rich::Rich1] << " " << m_radiusCurv[Rich::Rich2]
            << endmsg;
  }

  return sc;
}

void PhotonSignalGaussProb::handle ( const Incident& )
{
  // only subscribe to one sort of incident, so no need to check type
  m_prefillDone = false;
}

void PhotonSignalGaussProb::prefillPredictedPixelSignal() const
{
  if ( !m_prefillDone )
  {
    // Loop over all photons
    for ( auto * photon : *(richPhotons()) )
    {
      // Compute the ID independent term
      const auto A = _predictedPixelSignal(photon);
      // Loop over the mass hypos and compute and fill each value
      for ( const auto id : m_pidTypes )
      {
        // If not already done, fill this hypo
        if ( !photon->expPixelSignalPhots().dataIsValid(id) )
        {
          const auto sig = A * _predictedPixelSignal(photon,id);
          photon->setExpPixelSignalPhots( id, sig );
        }
      }
    }
    m_prefillDone = true;
  }
}

double
PhotonSignalGaussProb::predictedPixelSignal( LHCb::RichRecPhoton * photon,
                                             const Rich::ParticleIDType id ) const
{
  // Protect against below threshold hypothesis
  if ( id == Rich::BelowThreshold ) return 0;

  if ( !photon->expPixelSignalPhots().dataIsValid(id) )
  {
    // Compute the expected pixel contribution
    // See note LHCB/98-040 page 10 equation 16 for the details of where this comes from

    const auto pixelSignal = _predictedPixelSignal(photon) * _predictedPixelSignal(photon,id);

    // save final result
    photon->setExpPixelSignalPhots( id, pixelSignal );
  }

  return photon->expPixelSignalPhots( id );
}

double PhotonSignalGaussProb::signalProbFunc( const double thetaDiff,
                                              const double thetaExpRes ) const
{
  // See note LHCB/98-040 page 11 equation 18
  const auto expArg = -0.5 * std::pow(thetaDiff/thetaExpRes,2);
  return ( ( expArg>m_minArg ? myexp(expArg) : m_expMinArg ) /
                     ( root_two_pi*thetaExpRes ) );
}

double
PhotonSignalGaussProb::signalProb( LHCb::RichRecPhoton * photon,
                                   const Rich::ParticleIDType id ) const
{
  // Protect against below threshold hypothesis
  if ( id == Rich::BelowThreshold ) return 0.0;

  // Expected Cherenkov theta angle
  const auto thetaExp = m_ckAngle->avgCherenkovTheta(photon->richRecSegment(),id);
  if ( thetaExp < 0.000001 ) return 0.0;

  // Expected Cherenkov theta angle resolution
  const auto thetaExpRes = m_ckRes->ckThetaResolution(photon->richRecSegment(),id);

  // The difference between reco and expected
  const auto thetaDiff = photon->geomPhoton().CherenkovTheta() - thetaExp;

  // return the expected signal contribution
  return this->signalProbFunc(thetaDiff,thetaExpRes) * oneOverTwoPI;
}

double
PhotonSignalGaussProb::scatterProb( LHCb::RichRecPhoton * photon,
                                    const Rich::ParticleIDType id ) const
{
  // Protect against below threshold hypothesis
  if ( id == Rich::BelowThreshold ) return 0.0;

  if ( Rich::Aerogel == photon->richRecSegment()->trackSegment().radiator() )
  {

    // Expected Cherenkov theta angle
    const auto thetaExp = m_ckAngle->avgCherenkovTheta( photon->richRecSegment(), id );
    if ( thetaExp < 0.000001 ) return 0.0;

    // Reconstructed Cherenkov theta angle
    const auto thetaRec = photon->geomPhoton().CherenkovTheta();

    // Compute the scattering
    double fbkg = 0.0;
    if ( thetaRec < thetaExp )
    {
      fbkg = ( myexp(17.0*thetaRec) - 1.0 ) / ( myexp(17.0*thetaExp) - 1.0 );
    }
    else if ( thetaRec < 0.5*M_PI + thetaExp - 0.04 )
    {
      fbkg = std::pow( vdt::fast_cos( thetaRec - thetaExp + 0.04 ), 2 ) / 0.9984;
    }
    else
    {
      return 0.0;
    }

    // return the probability
    return two_over_pi2 * fbkg;
  }

  return 0.0;
}

//-----------------------------------------------------------------------------

DECLARE_TOOL_FACTORY( PhotonSignalGaussProb )

//-----------------------------------------------------------------------------
