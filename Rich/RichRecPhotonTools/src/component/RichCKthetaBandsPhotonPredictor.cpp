
//-----------------------------------------------------------------------------
/** @file RichCKthetaBandsPhotonPredictor.cpp
 *
 *  Implementation file for tool : Rich::Rec::CKthetaBandsPhotonPredictor
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   26/07/2007
 */
//-----------------------------------------------------------------------------

// Array properties. Must be first..
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// local
#include "RichCKthetaBandsPhotonPredictor.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Rec;

//-----------------------------------------------------------------------------

// Standard constructor
CKthetaBandsPhotonPredictor::
CKthetaBandsPhotonPredictor( const std::string& type,
                             const std::string& name,
                             const IInterface* parent )
  : BasePhotonPredictor( type, name, parent ) { }

// fast decision on whether a photon is possible
bool
CKthetaBandsPhotonPredictor::photonPossible( LHCb::RichRecSegment * segment,
                                             LHCb::RichRecPixel * pixel ) const
{

  // Default to not selected
  bool OK = false;

  // cache reference to track segment (avoids multiple obj pointer lookup checks).
  const auto & tkSeg = segment->trackSegment();

  // Are they in the same Rich detector ?
  if ( tkSeg.rich() == pixel->detector() )
  {

    // which radiator
    const auto rad = tkSeg.radiator();

    // segment / hit separation squared
    //const auto sep2 = m_geomTool->trackPixelHitSep2(segment,pixel);

    // ---------------------------------------------------------------------------
    // WARNING - Pasting code from virtual call here for speed
    // ---------------------------------------------------------------------------

    // Pixel position, in local HPD coords corrected for average radiator distortion
    const auto & pixP = pixel->radCorrLocalPositions().position(rad);

    // segment position ray traced to HPD panel, in local HPD coords
    //const auto & segP = segment->pdPanelHitPointLocal();

    // Global check
    // const bool isR1  = ( Rich::Rich1 == pixel->detector() );
    // const bool isPos = ( isR1 ? pixP.y() > 0 : pixP.x() > 0 );
    // if ( ( isR1 && // RICH1
    //        ( ( (  isPos && segment->photonsInYPlus()  ) ||
    //            ( !isPos && segment->photonsInYMinus() ) ) ) ) // ||
    //          // pixP.y()*segP.y() > 0 ) )
    //      || // RICH2
    //      ( ( (  isPos && segment->photonsInXPlus()  ) ||
    //          ( !isPos && segment->photonsInXMinus() ) ) ) ) // ||
      // pixP.x()*segP.x() > 0 ) )
    {

      // The segment hit point in local coordiates on the PD plane
      const auto & segPanelPnt =
        segment->pdPanelHitPointLocal(pixel->pdPixelCluster().panel());
      
      // compute the seperation squared
      const auto sep2 = ( std::pow( (pixP.x()-segPanelPnt.x()), 2 ) +
                          std::pow( (pixP.y()-segPanelPnt.y()), 2 ) );
      
      // Check overall boundaries
      if ( sep2 < m_maxROI2[rad] && sep2 > m_minROI2[rad] )
      {

        // estimated CK theta
        const auto ckThetaEsti = std::sqrt(sep2)*m_scale[rad];
        //_ri_verbo << " -> CK theta Esti " << ckThetaEsti << endmsg;

        // Loop over mass hypos and check finer grained boundaries
        for ( const auto hypo : m_pidTypes )
        {

          // expected CK theta
          const auto expCKtheta = m_ckAngle->avgCherenkovTheta(segment,hypo);
          // expected CK theta resolution
          const auto expCKres   = m_ckRes->ckThetaResolution(segment,hypo);

          //_ri_verbo << "   -> " << expCKtheta << " " << expCKres << " " << m_nSigma[rad] << endmsg;

          // is this pixel/segment pair in the accepted range
          if ( fabs(expCKtheta-ckThetaEsti) < m_nSigma[rad]*expCKres )
          {
            OK = true;
            break;
          }

        } // loop over hypos

      } // boundary check

    } // Global check

  } // detector check

  return OK;
}

//-----------------------------------------------------------------------------

DECLARE_TOOL_FACTORY( CKthetaBandsPhotonPredictor )

//-----------------------------------------------------------------------------
