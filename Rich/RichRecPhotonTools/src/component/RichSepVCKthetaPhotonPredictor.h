
//-----------------------------------------------------------------------------
/** @file RichSepVCKthetaPhotonPredictor.h
 *
 *  Header file for tool : Rich::Rec::SepVCKthetaPhotonPredictor
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   01/06/2005
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECTOOLS_RichSepVCKthetaPhotonPredictor_H
#define RICHRECTOOLS_RichSepVCKthetaPhotonPredictor_H 1

// base class
#include "RichBasePhotonPredictor.h"

namespace Rich
{
  namespace Rec
  {

    //-----------------------------------------------------------------------------
    /** @class SepVCKthetaPhotonPredictor RichSepVCKthetaPhotonPredictor.h
     *
     *  Tool which performs the association between RichRecTracks and
     *  RichRecPixels to form RichRecPhotons.
     *
     *  This particular implementation uses a cut range per radiator,
     *  on the seperation between the pixel and ray-traced track impact point,
     *  that scales with the expected Cherenkov angle. The hit is required to be
     *  in range for at least one possible meass hypothesis.
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   01/06/2005
     */
    //-----------------------------------------------------------------------------

    class SepVCKthetaPhotonPredictor final : public BasePhotonPredictor
    {

    public: // Methods for Gaudi Framework

      /// Standard constructor
      SepVCKthetaPhotonPredictor( const std::string& type,
                                  const std::string& name,
                                  const IInterface* parent );

      // Finalize method
      StatusCode finalize() override;

    public: // methods (and doxygen comments) inherited from public interface

      // Is it possible to make a photon candidate using this segment and pixel.
      bool photonPossible( LHCb::RichRecSegment * segment,
                           LHCb::RichRecPixel * pixel ) const override;

    private: // private data

      RadiatorArray<double> m_tolF = {{}}; ///< Region of tolerance in seperation

      // debug counting numbers

      /// Number of selected combinations for each radiator
      mutable RadiatorArray<unsigned long long> m_Nselected = {{}};
      /// Number of rejected combinations for each radiator
      mutable RadiatorArray<unsigned long long> m_Nreject = {{}};

    };

  }
}

#endif // RICHRECTOOLS_RichSepVCKthetaPhotonPredictor_H
