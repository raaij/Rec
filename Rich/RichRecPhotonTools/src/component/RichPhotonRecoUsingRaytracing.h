//-----------------------------------------------------------------------------
/** @file RichPhotonRecoUsingRaytracing.h
 *
 *  Header file for tool : Rich::Rec::PhotonRecoUsingRaytracing
 *
 *  @author Claus P Buszello
 *  @date   2008-11-01
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECTOOLS_RichPhotonRecoUsingRaytracing_H
#define RICHRECTOOLS_RichPhotonRecoUsingRaytracing_H 1

// STL
#include <iostream>
#include <sstream>
#include <array>

// Base class
#include "RichPhotonRecoBase.h"

// interfaces
#include "RichRecInterfaces/IRichPhotonReconstruction.h"
#include "RichInterfaces/IRichSmartIDTool.h"
#include "RichRecInterfaces/IRichMassHypothesisRingCreator.h"
#include "RichRecInterfaces/IRichCherenkovAngle.h"
#include "RichInterfaces/IRichParticleProperties.h"
#include "RichInterfaces/IRichRayTracing.h"

// Kernel
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"

// RichKernel
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/RichGeomPhoton.h"

// RichDet
#include "RichDet/Rich1DTabFunc.h"

// GSL
#include "gsl/gsl_math.h"

// VDT
#include "vdt/atan2.h"

namespace Rich
{
  namespace Rec
  {

    //-----------------------------------------------------------------------------
    /** @class PhotonRecoUsingRaytracing RichPhotonRecoUsingRaytracing.h
     *
     *  Rich detector tool which reconstructs photons from track segments
     *  and smartIDs or global hit positions.
     *
     *  Attempts to do this in a very fast way, by estimating the CK photon angle
     *  using the track and photon hit positions on the detector plane.
     *
     *  @author Claus Buszello Claus.Buszello@cern.ch
     *  @date   2008-11-01
     */
    //-----------------------------------------------------------------------------

    class PhotonRecoUsingRaytracing final : public PhotonRecoBase,
                                            virtual public IPhotonReconstruction
    {

    public: // Methods for Gaudi Framework

      /// Standard constructor
      PhotonRecoUsingRaytracing( const std::string& type,
                                 const std::string& name,
                                 const IInterface* parent);

      // Initialization of the tool after creation
      StatusCode initialize() override;

      // tool finalization
      StatusCode finalize() override;

    public: // methods (and doxygen comments) inherited from interface

      // Reconstructs the geometrical photon candidate for a given RichTrackSegment
      // and RichSmartID channel identifier
      bool reconstructPhoton ( const LHCb::RichRecSegment * segment,
                               const LHCb::RichRecPixel * pixel,
                               LHCb::RichGeomPhoton& gPhoton ) const override;

    private: // data

      /// RichSmartID tool
      const ISmartIDTool* m_idTool = nullptr;

      const ICherenkovAngle * m_ckAngle = nullptr; ///< Cherenkov angle tool

      const Rich::IRayTracing * m_raytrace = nullptr; ///< raytracing tool

      RadiatorArray<double> m_ERLSet = {{}};
      RadiatorArray<double> m_maxdiff = {{}};
      RadiatorArray<int> m_maxiter = {{}};

      double m_damping;

      double m_ERL;

      bool m_failiter;

      /// Cached working photon for ray-tracing
      mutable LHCb::RichGeomPhoton m_photon;

      /// cached ray tracing mode
      LHCb::RichTraceMode m_mode;

    };

  }
}

#endif // RICHRECTOOLS_RichPhotonRecoUsingRaytracing_H
