
// local
#include "RichSignalPhotonYields.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

SignalPhotonYields::SignalPhotonYields( const std::string& name,
                                        ISvcLocator* pSvcLocator )
  : MultiTransformer ( name, pSvcLocator,
                       { KeyValue{ "DetectablePhotonYieldLocation",   PhotonYieldsLocation::Detectable },
                         KeyValue{ "DetectablePhotonSpectraLocation", PhotonSpectraLocation::Detectable },
                         KeyValue{ "GeomEffsLocation",                GeomEffsLocation::Default } },
                       { KeyValue{ "SignalPhotonYieldLocation",       PhotonYieldsLocation::Signal },
                         KeyValue{ "SignalPhotonSpectraLocation",     PhotonSpectraLocation::Signal } } )
{ 
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

OutData
SignalPhotonYields::operator()( const PhotonYields::Vector& detYields,
                                const PhotonSpectra::Vector& detSpectra,
                                const GeomEffs::Vector& geomEffs ) const
{
  // make the data to return
  OutData data;
  auto & yieldV   = std::get<PhotonYields::Vector> (data);
  auto & spectraV = std::get<PhotonSpectra::Vector>(data);

  // reserve sizes
  yieldV.reserve  ( detYields.size() );
  spectraV.reserve( detYields.size() );

  // Loop over input data
  for ( const auto && data : Ranges::Zip(detYields,detSpectra,geomEffs) )
  {
    const auto & detYield = std::get<0>(data);
    const auto & detSpec  = std::get<1>(data);
    const auto & geomEff  = std::get<2>(data);

    // Create the signal photon spectra, starting from the detectable spectra
    spectraV.emplace_back( detSpec );
    auto & sigSpectra = spectraV.back();

    // create the yield data
    yieldV.emplace_back();
    auto & yields = yieldV.back();

    // Loop over PID types
    for ( const auto id : activeParticles() )
    {
      if ( id != Rich::BelowThreshold ) 
      {
        //_ri_verbo << std::setprecision(9) 
        //          << id << " detYield " << detYield[id] << " geomEff " << geomEff[id] << endmsg;
        // Scale the detectable yields by the geom eff
        yields.setData( id, detYield[id] * geomEff[id] );
        for ( auto & i : sigSpectra.energyDist(id) ) { i *= geomEff[id]; }
      }
    }
    
  }

  // return the new data
  return data;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( SignalPhotonYields )

//=============================================================================
