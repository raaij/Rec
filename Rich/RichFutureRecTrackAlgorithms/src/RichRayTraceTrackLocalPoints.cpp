
// local
#include "RichRayTraceTrackLocalPoints.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

RayTraceTrackLocalPoints::RayTraceTrackLocalPoints( const std::string& name, 
                                                      ISvcLocator* pSvcLocator )
  : Transformer ( name, pSvcLocator,
                  { KeyValue{ "TrackGlobalPointsLocation", SpacePointLocation::SegmentsGlobal } },
                  { KeyValue{ "TrackLocalPointsLocation",  SpacePointLocation::SegmentsLocal  } } )
{
  declareProperty( "SmartIDTool", m_idTool );
}

//=============================================================================

StatusCode RayTraceTrackLocalPoints::initialize()
{
  // Sets up various tools and services
  auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // load tools
  sc = sc && m_idTool.retrieve();

  // return
  return sc;
}

//=============================================================================

SegmentPanelSpacePoints::Vector
RayTraceTrackLocalPoints::operator()( const SegmentPanelSpacePoints::Vector& gPoints ) const 
{
  SegmentPanelSpacePoints::Vector lPoints;
  lPoints.reserve( gPoints.size() );

  // Convert the global points
  for ( const auto & gPoint : gPoints )
  {
    if ( gPoint.ok() )
    {
      // convert the global info to local
      lPoints.emplace_back( m_idTool.get()->globalToPDPanel( gPoint.point() ),
                            m_idTool.get()->globalToPDPanel( gPoint.point(Rich::left)  ),
                            m_idTool.get()->globalToPDPanel( gPoint.point(Rich::right) ),
                            gPoint.bestSide(), gPoint.closestPD() ); 
    }
    else
    {
      // save a default object
      lPoints.emplace_back( );
    }
  }
  
  return lPoints;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( RayTraceTrackLocalPoints )

//=============================================================================
