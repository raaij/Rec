
//---------------------------------------------------------------------------------
/** @file RichBaseTrSegMaker.h
 *
 *  Header file for tool : Rich::Future::Rec::BaseTrSegMaker
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   14/01/2002
 */
//---------------------------------------------------------------------------------

#pragma once

// STL
#include <vector>
#include <array>

// from Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/SystemOfUnits.h"

// base class and interface
#include "RichFutureRecBase/RichRecAlgBase.h"
#include "RichRecInterfaces/IRichTrSegMaker.h"

// Event model
#include "Event/Track.h"
#include "Event/State.h"

// LHCbKernel
#include "Kernel/RichSmartID.h"

// RichDet
#include "RichDet/DeRich.h"
#include "RichDet/DeRichRadiator.h"
#include "RichDet/DeRichBeamPipe.h"

// GSL
#include "gsl/gsl_math.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      //---------------------------------------------------------------------------------
      /** @class BaseTrSegMaker RichBaseTrSegMaker.h
       *
       *  Base class to tools that create RichTrackSegments from Tracks.
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   14/01/2002
       */
      //---------------------------------------------------------------------------------

      class BaseTrSegMaker : public AlgBase
      {

      public: // Methods for Gaudi Framework

        /// Standard Constructor
        BaseTrSegMaker( const std::string& name,
                        ISvcLocator* pSvcLocator );

        /// Initialization after creation
        virtual StatusCode initialize() override;

      protected: // methods

        /// Access the DeRichBeamPipe objects, creating as needed on demand
        inline const DeRichBeamPipe* deBeam( const Rich::RadiatorType rad ) const
        {
          const auto rich = ( Rich::Rich2Gas == rad ? Rich::Rich2 : Rich::Rich1 );
          return m_deBeam[rich];
        }

        /// Test flag to see if beam pipe intersections should be checked
        inline bool checkBeamPipe( const Rich::RadiatorType rad ) const noexcept
        {
          return m_checkBeamP[rad];
        }

        /// Test flag to see if a given radiator is in use
        inline bool usedRads( const Rich::RadiatorType rad ) const noexcept
        {
          return m_usedRads[rad];
        }

      private: // data

        /// RICH beampipe object for each radiator
        DetectorArray<const DeRichBeamPipe*> m_deBeam = {{}};

        /// Check for beam pipe intersections in each radiator ?
        Gaudi::Property< RadiatorArray<bool> > m_checkBeamP 
        { this, "CheckBeamPipe", { false, true, false } };

        /// Flags to turn on/off individual radiators
        Gaudi::Property< RadiatorArray<bool> > m_usedRads
        { this, "Radiators", { false, true, true  } };

      };

    }
  }
}
