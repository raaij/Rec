
#pragma once

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/PhysicalConstants.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"

// Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichFutureUtils/RichGeomPhoton.h"
#include "RichUtils/ZipRange.h"

// Rec Utils
#include "RichRecUtils/RichRadCorrLocalPositions.h"

// Interfaces
#include "RichFutureInterfaces/IRichRayTracing.h"
#include "RichInterfaces/IRichSmartIDTool.h"

// VDT
#include "vdt/sincos.h"

namespace Rich
{
  namespace Future
  {

    // Use the functional framework
    using namespace Gaudi::Functional;

    namespace Rec
    {

      /** @class RayTraceCherenkovCones RichRayTraceCherenkovCones.h
       *
       *  Creates the Cherenkov cones for each segment and mass hypothesis,
       *  using photon raytracing.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */

      class RayTraceCherenkovCones final :
        public Transformer< MassHypoRingsVector( const LHCb::RichTrackSegment::Vector&,
                                                 const CherenkovAngles::Vector& ),
                            Traits::BaseClass_t<AlgBase> >
      {

      public:

        /// Standard constructor
        RayTraceCherenkovCones( const std::string& name,
                                ISvcLocator* pSvcLocator );

        /// Initialization after creation
        StatusCode initialize() override;

      public:

        /// Functional operator
        MassHypoRingsVector
          operator()( const LHCb::RichTrackSegment::Vector& segments,
                      const CherenkovAngles::Vector& ckAngles ) const override;

      private: // helper classes

        /** @class CosSinPhi RichRayTraceCherenkovCone.h
         *
         *  Utility class to cache cos and sin values
         *
         *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
         *  @date   17/02/2008
         */
        template< typename TYPE >
        class CosSinPhi
        {
        public:
          typedef std::vector<CosSinPhi> Vector;
        public:
          explicit CosSinPhi( const TYPE _phi ) : phi(_phi)
          {
            vdt::fast_sincos( phi, sinPhi, cosPhi );
          }
        public:
          TYPE phi    = 0; ///< CK phi
          TYPE cosPhi = 0; ///< Cos(CK phi)
          TYPE sinPhi = 0; ///< Sin(CK phi)
        };

      private:

        /// Number of points to ray trace on each ring, for each radiator
        Gaudi::Property< RadiatorArray<unsigned int> > m_nPoints
        { this, "NRingPoints", { 96u, 96u, 96u } };

        /// Bailout number
        RadiatorArray<unsigned int> m_nBailout = {{}};

        /// Cached cos and sin values around the ring, for each radiator
        RadiatorArray< CosSinPhi<double>::Vector > m_cosSinPhiV;

        /// Cached trace modes for each radiator
        RadiatorArray<LHCb::RichTraceMode> m_traceModeRad = {{}};

        /// Flag to turn on or off checking of intersections with beampipe
        Gaudi::Property<bool> m_checkBeamPipe { this, "CheckBeamPipe", true };

        /// Flag to switch between simple or detail HPD description in ray tracing
        Gaudi::Property<bool> m_useDetailedHPDsForRayT { this, "UseDetailedHPDs", false };

        /** Bailout fraction. If no ray tracings have worked after this fraction have been
         *  perfromed, then give up */
        Gaudi::Property< RadiatorArray<float> > m_bailoutFrac
        { this, "BailoutFraction", { 0.75, 0.75, 0.75 } };

        /// Use the scalar or vector ray tracing
        Gaudi::Property<bool> m_useVectorised
        { this, "UseVectorisedRayTracing", true };

        /// Ray tracing tool
        ToolHandle<const IRayTracing> m_rayTrace
        { "Rich::Future::RayTracing/RayTracing", this };

        /// RichSmartID Tool
        ToolHandle<const ISmartIDTool> m_idTool
        { "Rich::Future::SmartIDTool/SmartIDTool:PUBLIC", this };

      };

    }
  }
}
