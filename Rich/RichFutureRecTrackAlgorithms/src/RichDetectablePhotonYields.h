
#pragma once

// STL
#include <tuple>
#include <array>

// Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/PhysicalConstants.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Kernel
#include "Kernel/RichDetectorType.h"

// Event Model
#include "RichFutureRecEvent/RichRecPhotonYields.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"

// Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"
#include "RichUtils/StlArray.h"
#include "RichUtils/RichMap.h"

// RichDet
#include "RichDet/DeRich1.h"
#include "RichDet/DeRich2.h"
#include "RichDet/DeRichPD.h"
#include "RichDet/DeRichSphMirror.h"

// VDT
#include "vdt/exp.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

      namespace
      {
        /// Output data type
        using OutData = std::tuple<PhotonYields::Vector,PhotonSpectra::Vector>;
      }

      /** @class DetectablePhotonYields RichDetectablePhotonYields.h
       *
       *  Computes the emitted photon yield data from Track Segments.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */
      class DetectablePhotonYields final :
        public MultiTransformer< OutData( const LHCb::RichTrackSegment::Vector&,
                                          const PhotonSpectra::Vector&,
                                          const MassHypoRingsVector& ),
                                 Traits::BaseClass_t<AlgBase> >
      {

      public:

        /// Standard constructor
        DetectablePhotonYields( const std::string& name,
                                ISvcLocator* pSvcLocator );

        /// Initialization after creation
        StatusCode initialize() override;

      public:

        /// Algorithm execution via transform
        OutData operator()( const LHCb::RichTrackSegment::Vector& segments,
                            const PhotonSpectra::Vector& emittedSpectra,
                            const MassHypoRingsVector& massRings ) const override;

      private: // definitions

        /// Type for count by photon detector
        using PDCount = std::vector< std::pair<const DeRichPD*,unsigned int> >;

        /// Type for count by mirror segment
        using MirrorCount = std::vector< std::pair<const DeRichSphMirror *,unsigned int> >;

      private: // data

        /// Pointers to RICHes
        DetectorArray<const DeRich*> m_riches = {{}};

        /// Cached value storing product of quartz window eff. and digitisation pedestal loss
        double m_qEffPedLoss{0};

        /// Thickness of windows in each RICH
        DetectorArray<double> m_qWinZSize = {{}};

      };

    }
  }
}
