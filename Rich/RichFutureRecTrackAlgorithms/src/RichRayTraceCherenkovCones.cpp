
// local
#include "RichRayTraceCherenkovCones.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

RayTraceCherenkovCones::RayTraceCherenkovCones( const std::string& name, 
                                                ISvcLocator* pSvcLocator )
  : Transformer ( name, pSvcLocator,
                  { KeyValue{ "TrackSegmentsLocation",       LHCb::RichTrackSegmentLocation::Default },
                    KeyValue{ "CherenkovAnglesLocation",     CherenkovAnglesLocation::Emitted } },
                  { KeyValue{ "MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted } } )
{
  declareProperty( "RayTracingTool", m_rayTrace );
  // debugging
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode RayTraceCherenkovCones::initialize()
{
  // Sets up various tools and services
  auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // load tools
  sc = sc && m_rayTrace.retrieve() && m_idTool.retrieve();

  // loop over radiators
  for ( const auto rad : Rich::radiators() )
  {
    // Fill cos and sin values
    m_cosSinPhiV[rad].clear();
    m_cosSinPhiV[rad].reserve( m_nPoints[rad] );
    const auto incPhi = Gaudi::Units::twopi / static_cast<double>(m_nPoints[rad]);
    double ckPhi = 0.0;
    for ( unsigned int iPhot = 0; iPhot < m_nPoints[rad]; ++iPhot, ckPhi+=incPhi )
    { m_cosSinPhiV[rad].emplace_back( ckPhi ); }
    // bailout number
    m_nBailout[rad] = static_cast<unsigned int> ( m_bailoutFrac[rad] * m_nPoints[rad] ); 
  }

  // the ray-tracing mode
  LHCb::RichTraceMode tmpMode ( LHCb::RichTraceMode::RespectPDTubes,
                                ( m_useDetailedHPDsForRayT ?
                                  LHCb::RichTraceMode::SphericalPDs :
                                  LHCb::RichTraceMode::FlatPDs ) );
  if ( m_checkBeamPipe ) { tmpMode.setBeamPipeIntersects(true); }
  m_traceModeRad[Rich::Aerogel]  = tmpMode;
  m_traceModeRad[Rich::Aerogel].setAeroRefraction(true);
  m_traceModeRad[Rich::Rich1Gas] = tmpMode;
  m_traceModeRad[Rich::Rich2Gas] = tmpMode;
  _ri_debug << "Aerogel  Track " << m_traceModeRad[Rich::Aerogel]  << endmsg;
  _ri_debug << "Rich1Gas Track " << m_traceModeRad[Rich::Rich1Gas] << endmsg;
  _ri_debug << "Rich2Gas Track " << m_traceModeRad[Rich::Rich2Gas] << endmsg;

  // return
  return sc;
}

//=============================================================================

MassHypoRingsVector 
RayTraceCherenkovCones::operator()( const LHCb::RichTrackSegment::Vector& segments,
                                    const CherenkovAngles::Vector& ckAngles ) const 
{
  // The data to return
  MassHypoRingsVector ringsV;
  ringsV.reserve( segments.size() );

  // Temporary working photon object. Need to eventually remove need for this.
  GeomPhoton photon;

  // local position corrector
  // longer term need to remove this
  const Rich::Rec::RadPositionCorrector corrector;

  // loop over the input data
  for ( const auto && data : Ranges::ConstZip(segments,ckAngles) )
  {
    const auto & segment = std::get<0>(data);
    const auto & ckTheta = std::get<1>(data);

    // Add a set of mass hypo rings for this segment
    ringsV.emplace_back( );
    auto & rings = ringsV.back();

    // which rich and radiator
    const auto rich = segment.rich();
    const auto rad  = segment.radiator();

    // best emission point
    const auto & emissionPoint = segment.bestPoint();
     
     // Loop over PID types
    for ( const auto id : activeParticles() )
    {
      // Above threshold ?
      if ( ckTheta[id] > 0 )
      {

        // compute sin and cos theta
        double sinTheta(0), cosTheta(0);
        vdt::fast_sincos( ckTheta[id], sinTheta, cosTheta );

        // reserve size in the points container
        rings[id].reserve( m_nPoints[rad] );

        // Container for photon directions
        std::vector<Gaudi::XYZVector> vects;
        vects.reserve( m_nPoints[rad] );

        // loop around the ring to create the directions
        for ( const auto& P : m_cosSinPhiV[rad] )
        {
          // Photon direction around loop
          vects.emplace_back( segment.vectorAtCosSinThetaPhi( cosTheta, sinTheta,
                                                              P.cosPhi, P.sinPhi ) );
        }

        // Count the number of good photons
        unsigned int nOK(0);

        // Which ray tracing to run
        if ( m_useVectorised )
        {

          // The vectorised ray tracing
          const auto results =
            m_rayTrace.get()->traceToDetector( emissionPoint, vects, segment, m_traceModeRad[rad] );

          // loop over the results and fill
          unsigned int nPhot(0);
          for ( const auto && data : Ranges::ConstZip(results,m_cosSinPhiV[rad]) )
          {
            const auto & res    = std::get<0>(data);
            const auto & cosphi = std::get<1>(data);

            // count photons
            ++nPhot;

            // Add a new point
            const auto & gP = res.detectionPoint;
            rings[id].emplace_back ( gP,
                                     corrector.correct(m_idTool.get()->globalToPDPanel(gP),rad),
                                     res.smartID,
                                     (RayTracedCKRingPoint::Acceptance)(res.result),
                                     res.primaryMirror,
                                     res.secondaryMirror,
                                     res.photonDetector,
                                     cosphi.phi );

            // count raytraces that are in HPD panel
            if ( res.result >= LHCb::RichTraceMode::InPDPanel ) { ++nOK; }

            // bailout check
            if ( 0 == nOK && nPhot >= m_nBailout[rad] ) { break; }  
          }

        }
        else
        {

          // Loop over the directions and ray trace them using the scalar method
          unsigned int nPhot(0);
          for ( const auto && data : Ranges::ConstZip(vects,m_cosSinPhiV[rad]) )
          {
            const auto & photDir = std::get<0>(data);
            const auto & cosphi  = std::get<1>(data);

            // count photons
            ++nPhot;

            // do the ray tracing
            // really need to speed this up by removing virtual function call and need for photon. 
            const auto result =
              m_rayTrace.get()->traceToDetector( rich, emissionPoint, photDir, photon, 
                                               segment, 
                                               m_traceModeRad[rad], Rich::top ); // Note forced side is not used..

            // Add a new point
            const auto & gP = photon.detectionPoint();
            rings[id].emplace_back ( gP,
                                     corrector.correct(m_idTool.get()->globalToPDPanel(gP),rad),
                                     photon.smartID(),
                                     (RayTracedCKRingPoint::Acceptance)(result),
                                     photon.primaryMirror(),
                                     photon.secondaryMirror(),
                                     photon.photonDetector(),
                                     cosphi.phi );
            
            // count raytraces that are in HPD panel
            if ( result >= LHCb::RichTraceMode::InPDPanel ) { ++nOK; }
            
            // bailout check
            if ( 0 == nOK && nPhot >= m_nBailout[rad] ) break; 
          }

        }

        // if no good hits empty the container
        //if ( 0 == nOK ) { rings[id].clear(); } 
        
      }
      
    }
    
  }
  
  return ringsV;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( RayTraceCherenkovCones )

//=============================================================================
