
// local
#include "../RichQuarticPhotonReco.h"

// LHCbMath
#include "LHCbMath/AVXGuard.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future;
using namespace Rich::Future::Rec;


OutData 
QuarticPhotonReco::run_avx( const LHCb::RichTrackSegment::Vector& segments,
                            const CherenkovAngles::Vector& ckAngles,
                            const CherenkovResolutions::Vector& ckResolutions,
                            const SegmentPanelSpacePoints::Vector& trHitPntsLoc,
                            const SegmentPhotonFlags::Vector& segPhotFlags,
                            const Rich::PDPixelCluster::Vector& clusters,
                            const SpacePointVector& globalPoints,
                            const SpacePointVector& localPoints,
                            const Relations::TrackToSegments::Vector& tkToSegRels ) const
{
  LHCb::AVX::Guard guard{};
#include "../RichQuarticPhotonReco.icpp"
}
