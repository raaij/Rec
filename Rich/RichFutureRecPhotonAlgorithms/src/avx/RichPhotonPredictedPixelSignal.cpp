
// local
#include "../RichPhotonPredictedPixelSignal.h"

// LHCbMath
#include "LHCbMath/AVXGuard.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future;
using namespace Rich::Future::Rec;

OutData 
PhotonPredictedPixelSignal::run_avx( const CherenkovPhoton::Vector& photons,
                                     const Relations::PhotonToParents::Vector& photRels,
                                     const LHCb::RichTrackSegment::Vector& segments,
                                     const CherenkovAngles::Vector& ckAngles,
                                     const CherenkovResolutions::Vector& ckRes,
                                     const PhotonYields::Vector& photYields ) const
{
  LHCb::AVX::Guard guard{};
#include "../RichPhotonPredictedPixelSignal.icpp"
}
