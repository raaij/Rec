
#pragma once

// STL
#include <iomanip>

// Gaudi (must be first)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Rich Utils
#include "RichUtils/ZipRange.h"

// LHCbMath
#include "LHCbMath/CPUDispatch.h"

// Detector Description
#include "RichDet/DeRichSystem.h"
#include "RichDet/DeRich1.h"
#include "RichDet/DeRich2.h"

// VDT
#include "vdt/exp.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

      namespace { using OutData = PhotonSignals::Vector; }

      /** @class PhotonPredictedPixelSignal PhotonPredictedPixelSignal.h
       *
       *  Computes the expected pixel signals for photon candidates.
       *
       *  @author Chris Jones
       *  @date   2016-10-19
       */
      class PhotonPredictedPixelSignal final :
        public Transformer< OutData( const CherenkovPhoton::Vector&,
                                     const Relations::PhotonToParents::Vector&,
                                     const LHCb::RichTrackSegment::Vector&,
                                     const CherenkovAngles::Vector&,
                                     const CherenkovResolutions::Vector&,
                                     const PhotonYields::Vector& ),
                            Traits::BaseClass_t<AlgBase> >
      {
        
      public:

        /// Standard constructor
        PhotonPredictedPixelSignal( const std::string& name,
                                    ISvcLocator* pSvcLocator );

        /// Initialization of the tool after creation
        StatusCode initialize() override;

      public:

        /// Functional operator
        OutData operator()( const CherenkovPhoton::Vector& photons,
                            const Relations::PhotonToParents::Vector& photRels,
                            const LHCb::RichTrackSegment::Vector& segments,
                            const CherenkovAngles::Vector& ckAngles,
                            const CherenkovResolutions::Vector& ckRes,
                            const PhotonYields::Vector& photYields ) const override
        {
          return (this->*m_run)( photons, photRels, segments, ckAngles, ckRes, photYields ); 
        }

      private: // dispatch methods

        /** Pointer to dispatch function for the operator call
         *  Default to lowest common denominator. 
         *  Reset in initialize() according to runtime support */
        OutData (PhotonPredictedPixelSignal::*m_run)
          ( const CherenkovPhoton::Vector& photons,
            const Relations::PhotonToParents::Vector& photRels,
            const LHCb::RichTrackSegment::Vector& segments,
            const CherenkovAngles::Vector& ckAngles,
            const CherenkovResolutions::Vector& ckRes,
            const PhotonYields::Vector& photYields ) const
          = &PhotonPredictedPixelSignal::run_generic;

        /// Generic method
        OutData run_generic( const CherenkovPhoton::Vector& photons,
                             const Relations::PhotonToParents::Vector& photRels,
                             const LHCb::RichTrackSegment::Vector& segments,
                             const CherenkovAngles::Vector& ckAngles,
                             const CherenkovResolutions::Vector& ckRes,
                             const PhotonYields::Vector& photYields ) const;

        /// SSE4 method
        OutData run_sse4( const CherenkovPhoton::Vector& photons,
                          const Relations::PhotonToParents::Vector& photRels,
                          const LHCb::RichTrackSegment::Vector& segments,
                          const CherenkovAngles::Vector& ckAngles,
                          const CherenkovResolutions::Vector& ckRes,
                          const PhotonYields::Vector& photYields ) const;
        
        /// AVX method
        OutData run_avx( const CherenkovPhoton::Vector& photons,
                         const Relations::PhotonToParents::Vector& photRels,
                         const LHCb::RichTrackSegment::Vector& segments,
                         const CherenkovAngles::Vector& ckAngles,
                         const CherenkovResolutions::Vector& ckRes,
                         const PhotonYields::Vector& photYields ) const;

        /// AVX2 method
        OutData run_avx2( const CherenkovPhoton::Vector& photons,
                          const Relations::PhotonToParents::Vector& photRels,
                          const LHCb::RichTrackSegment::Vector& segments,
                          const CherenkovAngles::Vector& ckAngles,
                          const CherenkovResolutions::Vector& ckRes,
                          const PhotonYields::Vector& photYields ) const;
        
      private:

        /// The exponential function for float
        inline float  __attribute__((always_inline)) 
          myexp( const float  x ) const { return vdt::fast_expf(x); }

        /// The exponential function for double
        inline double __attribute__((always_inline)) 
          myexp( const double x ) const { return vdt::fast_exp(x);  }

      private:

        /// cached value of exp(minArg)
        double m_expMinArg = 0.0;

        /// cached scale factor / RoC^2
        DetectorArray<double> m_factor = {{}};

        /// Rich System detector element
        const DeRichSystem * m_richSys = nullptr;

        /// The minimum argument value for the probability value
        Gaudi::Property<double> m_minArg { this, "MinExpArg", -650 };

        /// minimum cut value for photon probability
        Gaudi::Property< RadiatorArray<double> > m_minPhotonProb
        { this, "MinPhotonProbability", { 1e-15, 1e-15, 1e-15 },
            "The minimum allowed photon probability values for each radiator (Aero/R1Gas/R2Gas)" };

        /// Scale factors
        Gaudi::Property< DetectorArray<double> > m_scaleFactor
        { this, "ScaleFactor", { 4.0, 4.0 } };

        /// Flag to control if CPU detection should be performed or not
        Gaudi::Property<bool> m_detectCPU { this, "DetectCPUCapabilities", true };
        
      };

    }
  }
}
