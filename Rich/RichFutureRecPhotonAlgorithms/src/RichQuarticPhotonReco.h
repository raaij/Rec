
#pragma once

// STL
#include <algorithm>
#include <functional>
#include <type_traits>
#include <array>
#include <cmath>

// Array properties
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Gaudi
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichBasePhotonReco.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecSpacePoints.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"

// Utils
#include "RichUtils/RichRayTracingUtils.h"

// LHCbMath
#include "LHCbMath/CPUDispatch.h"

// Rec Utils
#include "RichRecUtils/RichRadCorrLocalPositions.h"

// RichDet
#include "RichDet/DeRich1.h"
#include "RichDet/DeRich2.h"
#include "RichDet/DeRichSphMirror.h"

// Quartic Solver
#include "RichRecUtils/QuarticSolverNewton.h"

// interfaces
#include "RichInterfaces/IRichMirrorSegFinderLookUpTable.h"

// VDT
#include "vdt/asin.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

      // pull in methods from Rich::RayTracingUtils
      using namespace Rich::RayTracingUtils;

      namespace
      {
        /// Shortcut to the output data type
        using OutData = std::tuple<CherenkovPhoton::Vector,Relations::PhotonToParents::Vector>;
      }

      /** @class QuarticPhotonReco RichQuarticPhotonReco.h
       *
       *  Reconstructs photon candidates using the Quartic solution.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */

      class QuarticPhotonReco final :
        public MultiTransformer< OutData( const LHCb::RichTrackSegment::Vector&,
                                          const CherenkovAngles::Vector&,
                                          const CherenkovResolutions::Vector&,
                                          const SegmentPanelSpacePoints::Vector&,
                                          const SegmentPhotonFlags::Vector&,
                                          const Rich::PDPixelCluster::Vector&,
                                          const SpacePointVector&,
                                          const SpacePointVector&,
                                          const Relations::TrackToSegments::Vector& ),
                                 Traits::BaseClass_t<BasePhotonReco> >
      {

      public:

        /// Standard constructor
        QuarticPhotonReco( const std::string& name,
                           ISvcLocator* pSvcLocator );

        /// Initialization after creation
        StatusCode initialize() override;

      public:

        /// Functional operator
        OutData operator()( const LHCb::RichTrackSegment::Vector& segments,
                            const CherenkovAngles::Vector& ckAngles,
                            const CherenkovResolutions::Vector& ckResolutions,
                            const SegmentPanelSpacePoints::Vector& trHitPntsLoc,
                            const SegmentPhotonFlags::Vector& segPhotFlags,
                            const Rich::PDPixelCluster::Vector& clusters,
                            const SpacePointVector& globalPoints,
                            const SpacePointVector& localPoints,
                            const Relations::TrackToSegments::Vector& tkToSegRels ) const override
        {
          return (this->*m_run)( segments, ckAngles, ckResolutions, trHitPntsLoc,
                                 segPhotFlags, clusters, globalPoints, localPoints, tkToSegRels );
        }

      private: // dispatch methods

        /** Pointer to dispatch function for the operator call
         *  Default to lowest common denominator. 
         *  Reset in initialize() according to runtime support */
        OutData (QuarticPhotonReco::*m_run)
          ( const LHCb::RichTrackSegment::Vector& segments,
            const CherenkovAngles::Vector& ckAngles,
            const CherenkovResolutions::Vector& ckResolutions,
            const SegmentPanelSpacePoints::Vector& trHitPntsLoc,
            const SegmentPhotonFlags::Vector& segPhotFlags,
            const Rich::PDPixelCluster::Vector& clusters,
            const SpacePointVector& globalPoints,
            const SpacePointVector& localPoints,
            const Relations::TrackToSegments::Vector& tkToSegRels ) const 
          = &QuarticPhotonReco::run_generic;
        
        /// Generic method
        OutData run_generic( const LHCb::RichTrackSegment::Vector& segments,
                             const CherenkovAngles::Vector& ckAngles,
                             const CherenkovResolutions::Vector& ckResolutions,
                             const SegmentPanelSpacePoints::Vector& trHitPntsLoc,
                             const SegmentPhotonFlags::Vector& segPhotFlags,
                             const Rich::PDPixelCluster::Vector& clusters,
                             const SpacePointVector& globalPoints,
                             const SpacePointVector& localPoints,
                             const Relations::TrackToSegments::Vector& tkToSegRels ) const;

        /// SSE4 method
        OutData run_sse4( const LHCb::RichTrackSegment::Vector& segments,
                          const CherenkovAngles::Vector& ckAngles,
                          const CherenkovResolutions::Vector& ckResolutions,
                          const SegmentPanelSpacePoints::Vector& trHitPntsLoc,
                          const SegmentPhotonFlags::Vector& segPhotFlags,
                          const Rich::PDPixelCluster::Vector& clusters,
                          const SpacePointVector& globalPoints,
                          const SpacePointVector& localPoints,
                          const Relations::TrackToSegments::Vector& tkToSegRels ) const;
         
        /// AVX method
        OutData run_avx( const LHCb::RichTrackSegment::Vector& segments,
                         const CherenkovAngles::Vector& ckAngles,
                         const CherenkovResolutions::Vector& ckResolutions,
                         const SegmentPanelSpacePoints::Vector& trHitPntsLoc,
                         const SegmentPhotonFlags::Vector& segPhotFlags,
                         const Rich::PDPixelCluster::Vector& clusters,
                         const SpacePointVector& globalPoints,
                         const SpacePointVector& localPoints,
                         const Relations::TrackToSegments::Vector& tkToSegRels ) const;

        /// AVX2 method
        OutData run_avx2( const LHCb::RichTrackSegment::Vector& segments,
                          const CherenkovAngles::Vector& ckAngles,
                          const CherenkovResolutions::Vector& ckResolutions,
                          const SegmentPanelSpacePoints::Vector& trHitPntsLoc,
                          const SegmentPhotonFlags::Vector& segPhotFlags,
                          const Rich::PDPixelCluster::Vector& clusters,
                          const SpacePointVector& globalPoints,
                          const SpacePointVector& localPoints,
                          const Relations::TrackToSegments::Vector& tkToSegRels ) const;
        
      private: // methods

        /// Access the DeRich beam pipe objects
        inline const DeRichBeamPipe* __attribute__((always_inline))
          deBeam( const Rich::DetectorType rich ) const noexcept
        {
          return m_deBeam[rich];
        }

        /// acos for double
        inline double myacos( const double x ) const noexcept { return vdt::fast_acos(x);  }

        /// acos for float
        inline float  myacos( const float  x ) const noexcept { return vdt::fast_acosf(x); }

        /// Find mirror segments and reflection points for given data
        inline bool __attribute__((always_inline))
          findMirrorData( const Rich::DetectorType rich,
                          const Rich::Side side,
                          const Gaudi::XYZPoint & virtDetPoint,
                          const Gaudi::XYZPoint & emissionPoint,
                          const DeRichSphMirror *& sphSegment,
                          const DeRichSphMirror *& secSegment,
                          Gaudi::XYZPoint & sphReflPoint,
                          Gaudi::XYZPoint & secReflPoint ) const
        {
          // solve quartic equation with nominal values and find spherical mirror reflection point
          m_quarticSolver.solve<double,3,3>( emissionPoint,
                                             m_rich[rich]->nominalCentreOfCurvature(side),
                                             virtDetPoint,
                                             m_rich[rich]->sphMirrorRadius(),
                                             sphReflPoint );
          // find the spherical mirror segment
          sphSegment = m_mirrorSegFinder.get()->findSphMirror( rich, side, sphReflPoint );
          // Search for the secondary segment
          // Direction vector from primary mirror point to virtual detection point
          const auto dir ( virtDetPoint - sphReflPoint );
          // find the sec mirror intersction point and secondary mirror segment
          const bool OK = intersectPlane( sphReflPoint, dir,
                                          m_rich[rich]->nominalPlane(side),
                                          secReflPoint );
          if ( OK )
          {
            // find the secondary mirror
            secSegment = m_mirrorSegFinder.get()->findSecMirror( rich, side, secReflPoint );
          }
          // return
          return OK;
        }
        
        /// Compute the best emission point for the gas radiators using
        /// the given spherical mirror reflection points
        bool getBestGasEmissionPoint( const Rich::RadiatorType radiator,
                                      const Gaudi::XYZPoint & sphReflPoint1,
                                      const Gaudi::XYZPoint & sphReflPoint2,
                                      const Gaudi::XYZPoint & detectionPoint,
                                      const LHCb::RichTrackSegment & segment,
                                      Gaudi::XYZPoint & emissionPoint,
                                      float & fraction ) const;

        /// Truncate the give value to a given d.p.
        template < typename TYPE >
        inline TYPE truncate( const TYPE x ) const
        {
          constexpr long long int scale = 10000000; // 7 d.p.
          //const long long int i = std::round( x * scale );
          const long long int i = ( x * scale );
          return (TYPE)(i)/scale;  
        }

      private:

        /// Newton Quartic Solver 
        Rich::Rec::QuarticSolverNewton m_quarticSolver;

        /// Rich1 and Rich2 detector elements
        DetectorArray<const DeRich *> m_rich = {{}};

        /// RICH beampipe object for each radiator
        DetectorArray<const DeRichBeamPipe*> m_deBeam = {{}};

        /** @brief Flag to indicate if the unambiguous photon test should be performed
         *  for each radiator
         *
         *  If set to true the reconstruction is first performed twice, using the
         *  track segment start and end points as the assumed emission points and the
         *  nominal mirror geometry. If both calculations give the same mirror segments
         *  then this photon is flagged as unambiguous and the reconstruction is then
         *  re-performed using these mirror segments and the best-guess emission point
         *  (usually the track segment centre point)
         *
         *  If false, then the photon is first reconstructed using the segment centre
         *  point as the emission point and using the nominal mirror geometry. The mirror
         *  segments found for this calculation are then used to re-do the calculation.
         */
        Gaudi::Property< RadiatorArray<bool> > m_testForUnambigPhots
        { this, "FindUnambiguousPhotons", { true, true, true } };

        /** Flag to turn on rejection of ambiguous photons
         *
         *  If set true photons which are not unambiguous will be rejected
         *  Note, setting this true automatically means m_testForUnambigPhots
         *  will be set true
         */
        Gaudi::Property< RadiatorArray<bool> > m_rejectAmbigPhots
        { this, "RejectAmbiguousPhotons", { false, false, false } };

        /** @brief Flag to indicate if Cherenkov angles should be computed using the
         *  absolute mirror segment alignment.
         *
         *  If set to true, then the reconstruction will be performed twice,
         *  once with the nominal mirror allignment in order to find the appropriate
         *  mirror segments, and then again with thos segments.
         *
         *  If false, only the nominal mirror allignment will be used
         */
        Gaudi::Property< RadiatorArray<bool> > m_useAlignedMirrSegs
        { this, "UseMirrorSegmentAlignment", { true, true, true } };

        /** Maximum number of iterations of the quartic solution, for each radiator, in order
         *  to account for the non-flat secondary mirrors.
         */
        Gaudi::Property< RadiatorArray<int> > m_nMaxQits
        { this, "NQuarticIterationsForSecMirrors", { 3, 3, 3 } };

        /** Minimum number of iterations of the quartic solution, for each radiator, in order
         *  to account for the non-flat secondary mirrors.
         */
        Gaudi::Property< RadiatorArray<int> > m_nMinQits
        { this, "MinIterationsForTolCheck", { 0, 0, 0 } };

        /// Turn on/off the checking of photon trajectories against the beam pipe
        Gaudi::Property< RadiatorArray<bool> > m_checkBeamPipe
        { this, "CheckBeamPipe", { true, true, true } };

        /** Turn on/off the checking of photon trajectories against the mirror segments
         *  to verify if the photon hit the real actiave area (and not, for instance, the gaps).
         */
        Gaudi::Property< RadiatorArray<bool> > m_checkPrimMirrSegs
        { this, "CheckPrimaryMirrorSegments", { false, false, false } };

        /// Minimum active segment fraction in each radiator
        Gaudi::Property< RadiatorArray<double> > m_minActiveFrac
        { this, "MinActiveFraction", { 0.2, 0.2, 0.2 } };

        /// Minimum tolerance for mirror reflection point during iterations
        Gaudi::Property< RadiatorArray<double> > m_minSphMirrTolIt
        { this, "MinSphMirrTolIt", 
          { std::pow(0.10*Gaudi::Units::mm,2), 
            std::pow(0.08*Gaudi::Units::mm,2), 
            std::pow(0.05*Gaudi::Units::mm,2) } };

        /// Flag to control if the secondary mirrors are treated as if they are completely flat
        Gaudi::Property< DetectorArray<bool> > m_treatSecMirrsFlat
        { this, "AssumeFlatSecondaryMirrors", { true, false } };

        /// Mirror segment finder tool
        ToolHandle<const IMirrorSegFinderLookUpTable> m_mirrorSegFinder
        { "Rich::Future::MirrorSegFinderLookUpTable/MirrorFinder:PUBLIC", this };

        /// Flag to control if CPU detection should be performed or not
        Gaudi::Property<bool> m_detectCPU { this, "DetectCPUCapabilities", true };

      };

      //=========================================================================
      // Compute the best emission point for the gas radiators using
      // the given spherical mirror reflection points
      //=========================================================================
      inline bool __attribute__((always_inline))
      QuarticPhotonReco::
      getBestGasEmissionPoint( const Rich::RadiatorType radiator,
                               const Gaudi::XYZPoint& sphReflPoint1,
                               const Gaudi::XYZPoint& sphReflPoint2,
                               const Gaudi::XYZPoint& detectionPoint,
                               const LHCb::RichTrackSegment & segment,
                               Gaudi::XYZPoint & emissionPoint,
                               float & fraction ) const
      {
        double alongTkFrac = 0.5;
        
        if ( radiator == Rich::Rich1Gas )
        {
          // First reflection and hit point on same y side ?
          const bool sameSide1 = ( sphReflPoint1.y() * detectionPoint.y() > 0 );
          const bool sameSide2 = ( sphReflPoint2.y() * detectionPoint.y() > 0 );
          if ( sameSide1 && sameSide2 )
          {
            emissionPoint = segment.bestPoint();
          }
          else if ( sameSide1 )
          {
            fraction = (float)(std::fabs(sphReflPoint1.y()/(sphReflPoint1.y()-sphReflPoint2.y())));
            alongTkFrac = fraction/2.0;
            emissionPoint = segment.bestPoint(alongTkFrac);
          }
          else if ( sameSide2 )
          {
            fraction = (float)(std::fabs(sphReflPoint2.y()/(sphReflPoint1.y()-sphReflPoint2.y())));
            alongTkFrac = 1.0-fraction/2.0;
            emissionPoint = segment.bestPoint(alongTkFrac);
          }
          else
          {
            //Warning( "Rich1Gas : Both RICH spherical mirror hits opposite side to detection point" );
            return false;
          }
          
        }
        else if ( radiator == Rich::Rich2Gas )
        {
          // First sphReflPoint and hit point on same x side ?
          const bool sameSide1 = ( sphReflPoint1.x() * detectionPoint.x() > 0 );
          const bool sameSide2 = ( sphReflPoint2.x() * detectionPoint.x() > 0 );
          if ( sameSide1 && sameSide2 )
          {
            emissionPoint = segment.bestPoint();
          }
          else if ( sameSide1 )
          {
            fraction = (float)(std::fabs(sphReflPoint1.x()/(sphReflPoint1.x()-sphReflPoint2.x())));
            alongTkFrac = fraction/2.0;
            emissionPoint = segment.bestPoint(alongTkFrac);
          }
          else if ( sameSide2 )
          {
            fraction = (float)(std::fabs(sphReflPoint2.x()/(sphReflPoint1.x()-sphReflPoint2.x())));
            alongTkFrac = 1.0-fraction/2.0;
            emissionPoint = segment.bestPoint(alongTkFrac);
          }
          else
          {
            //Warning( "Rich2Gas : Both RICH spherical mirror hits opposite side to detection point" );
            return false;
          }
        }
        else { Error( "::getBestGasEmissionPoint() called for Aerogel segment !!" ).ignore(); }
        
        // if ( msgLevel(MSG::VERBOSE) )
        // {
        //   verbose() << radiator << " best emission point correction :- " << endmsg
        //             << " -> Photon detection point = " << detectionPoint << endmsg
        //             << " -> Sph. Mirror ptns       = " << sphReflPoint1 << " " << sphReflPoint2 << endmsg
        //             << " -> Segment entry/exit     = " << trSeg.entryPoint() << " " << trSeg.exitPoint() << endmsg
        //             << " -> Segment fraction       = " << fraction << endmsg
        //             << " -> Emm. Ptn. Along traj   = " << alongTkFrac << endmsg
        //             << " -> Best Emission point    = " << emissionPoint << endmsg;
        // }
        
        return true;
      }

    }
  }
}
