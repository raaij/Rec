
// local
#include "RichPhotonPredictedPixelSignal.h"

using namespace Rich::Future::Rec;

//-----------------------------------------------------------------------------
// Implementation file for class : RichPhotonPredictedPixelSignal
//
// 2016-09-30 : Chris Jones
//-----------------------------------------------------------------------------

//=============================================================================

PhotonPredictedPixelSignal::
PhotonPredictedPixelSignal( const std::string& name, ISvcLocator* pSvcLocator )
  : Transformer ( name, pSvcLocator,
                  { KeyValue{ "CherenkovPhotonLocation", CherenkovPhotonLocation::Default },
                    KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                    KeyValue{ "TrackSegmentsLocation",   LHCb::RichTrackSegmentLocation::Default },
                    KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                    KeyValue{ "CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default },
                    KeyValue{ "PhotonYieldLocation",     PhotonYieldsLocation::Detectable } },
                  { KeyValue{ "PhotonSignalsLocation",   PhotonSignalsLocation::Default } } )
{ 
  // init
  m_factor.fill(0);
  // debug
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode PhotonPredictedPixelSignal::initialize() 
{
  const auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // Get the CPU capabilities and set dispatch method
  if ( m_detectCPU )
  {
    const auto vbtl = { 
      std::make_pair( LHCb::CPU::AVX2,    &PhotonPredictedPixelSignal::run_avx2    ),
      std::make_pair( LHCb::CPU::AVX,     &PhotonPredictedPixelSignal::run_avx     ),
      std::make_pair( LHCb::CPU::SSE4,    &PhotonPredictedPixelSignal::run_sse4    ),
      std::make_pair( LHCb::CPU::GENERIC, &PhotonPredictedPixelSignal::run_generic )
    };
    m_run = LHCb::CPU::dispatch(vbtl);
  }

  // Get Rich DetElems
  m_richSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );
  const auto * Rich1DE = getDet<DeRich1>( DeRichLocations::Rich1 );
  const auto * Rich2DE = getDet<DeRich2>( DeRichLocations::Rich2 );

  // Mirror radii of curvature in mm
  const DetectorArray<double> radiusCurv = { Rich1DE->sphMirrorRadius(),
                                             Rich2DE->sphMirrorRadius() };

  // cache the factor for each RICH
  for ( const auto det : { Rich::Rich1, Rich::Rich2 } )
  {
    m_factor[det] = ( m_scaleFactor[det] /
                      ( std::pow(radiusCurv[det],2) * std::pow(2.0*M_PI,1.5) ) );
  }

  // exp params
  m_expMinArg = myexp( m_minArg );

  return sc;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( PhotonPredictedPixelSignal )

//=============================================================================
