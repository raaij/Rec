
// local
#include "RichCKEstiFromRadiusPhotonReco.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future;
using namespace Rich::Future::Rec;

//=============================================================================

CKEstiFromRadiusPhotonReco::CKEstiFromRadiusPhotonReco( const std::string& name, 
                                                        ISvcLocator* pSvcLocator )
  : MultiTransformer ( name, pSvcLocator,
                       { KeyValue{ "TrackSegmentsLocation",        LHCb::RichTrackSegmentLocation::Default },
                         KeyValue{ "CherenkovAnglesLocation",      CherenkovAnglesLocation::Emitted },
                         KeyValue{ "CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default },
                         KeyValue{ "TrackLocalPointsLocation",     SpacePointLocation::SegmentsLocal },
                         KeyValue{ "MassHypothesisRingsLocation",   MassHypoRingsLocation::Emitted },
                         KeyValue{ "SegmentPhotonFlagsLocation",   SegmentPhotonFlagsLocation::Default },
                         KeyValue{ "RichPixelClustersLocation",    Rich::PDPixelClusterLocation::Default },
                         KeyValue{ "RichPixelGlobalPositionsLocation", SpacePointLocation::PixelsGlobal },
                         KeyValue{ "RichPixelLocalPositionsLocation",  SpacePointLocation::PixelsLocal },
                         KeyValue{ "TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Selected } },
                       { KeyValue{ "CherenkovPhotonLocation",      CherenkovPhotonLocation::Default },
                         KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default } } )
{
  // Corrections for the intrinsic biases
  //                  Aerogel    Rich1Gas    Rich2Gas
  m_ckBiasCorrs = {   0.0,      -6.687e-5,   1.787e-6   };

  // debugging
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

OutData
CKEstiFromRadiusPhotonReco::operator()( const LHCb::RichTrackSegment::Vector& segments,
                                        const CherenkovAngles::Vector& ckAngles,
                                        const CherenkovResolutions::Vector& ckResolutions,
                                        const SegmentPanelSpacePoints::Vector& trHitPntsLoc,
                                        const MassHypoRingsVector& massRings,
                                        const SegmentPhotonFlags::Vector& segPhotFlags,
                                        const Rich::PDPixelCluster::Vector& clusters,
                                        const SpacePointVector& globalPoints,
                                        const SpacePointVector& localPoints,
                                        const Relations::TrackToSegments::Vector& tkToSegRels ) const
{
  // ranges namespace
  using namespace ranges::v3;

  // make the output data
  OutData outData;

  // Shortcut to the photons and relations
  auto & photons   = std::get<0>(outData);
  auto & relations = std::get<1>(outData);
  // guess at reserve size
  const auto resSize = segments.size() * globalPoints.size() / 20;
  photons.reserve( resSize );
  relations.reserve( resSize );

  // View range for pixel data
  const auto allPixels = Ranges::ConstZip( clusters, localPoints );

  // Make 'ranges' for RICH1 and RICH2. 
  const auto RPixs = richRanges( allPixels );
  const auto & RichRanges  = RPixs[0];   // Complete RICH ranges
  const auto & Rich1Ranges = RPixs[1];   // ranges for RICH1 [top,bottom]
  const auto & Rich2Ranges = RPixs[2];   // ranges for RICH2 [left,right]
  
  // local position corrector
  // longer term need to remove this
  const Rich::Rec::RadPositionCorrector corrector;

  // global photon index
  int photonIndex(-1);

  // Loop over the track->segment relations
  for ( const auto & inTkRel : tkToSegRels )
  {
    // loop over segments for this track
    for ( const auto & segIndex : inTkRel.segmentIndices )
    {
      // Get the data from the zipped container
      const auto & segment     = segments[segIndex];
      const auto & tkCkAngles  = ckAngles[segIndex];
      const auto & tkCkRes     = ckResolutions[segIndex];
      const auto & tkLocPtn    = trHitPntsLoc[segIndex];
      const auto & tkRings     = massRings[segIndex];
      const auto & segFlags    = segPhotFlags[segIndex];

      // Is this segment above threshold
      //if ( ! ( tkCkAngles[lightestActiveHypo()] > 0 ) ) continue;

      // which RICH and radiator
      const auto rich = segment.rich();
      const auto rad  = segment.radiator();

      // This implementiation does not support Aerogel
      if ( UNLIKELY( Rich::Aerogel == rad ) ) { Exception("Aerogel not supported"); }

      // get the best pixel range for this segment, based on where hits are expected
      const auto& pixR = ( segFlags.inBothPanels() ? RichRanges[rich] :
                           Rich::Rich1 == rich ? 
                           ( segFlags.inPanel(Rich::top) ? 
                             Rich1Ranges[Rich::top]  : Rich1Ranges[Rich::bottom] ) :
                           ( segFlags.inPanel(Rich::left) ? 
                             Rich2Ranges[Rich::left] : Rich2Ranges[Rich::right] ) );

      // Loop over pixel information
      int pixIndex = pixR.first - allPixels.begin() - 1; // (start index - 1) for this range
      for ( auto pix_it = pixR.first; pix_it != pixR.second; ++pix_it )
      {
        // load the data from the tuple
        const auto & cluster = std::get<0>(*pix_it);
        const auto & locPos  = std::get<1>(*pix_it);
        // increment the pixel index
        ++pixIndex;

        //_ri_verbo << "Trying Segment " << segIndex << " Pixel " << pixIndex << endmsg;

        // Apply pre-selection
      
        // Pixel position, in local HPD coords corrected for average radiator distortion
        // Might need to pre-compute and cache this as we repeat it here each segment...
        const auto pixPRad = corrector.correct(locPos,rad);

        // Track local hit point on the same panel as the hit
        const auto & segPSide = tkLocPtn.point( cluster.panel() );

        // x,y differences
        const auto diff_x = ( segPSide.x() - pixPRad.x() );
        const auto diff_y = ( segPSide.y() - pixPRad.y() );

        // compute the seperation squared
        const auto sep2 = ( std::pow( diff_x, 2 ) + std::pow( diff_y, 2 ) );

        // presel. default to rejected
        bool SelOK = false;

        // Check overall boundaries
        if ( sep2 < m_maxROI2PreSel[rad] && sep2 > m_minROI2PreSel[rad] )
        {
        
          // estimated CK theta
          const auto ckThetaEsti = std::sqrt(sep2)*m_scalePreSel[rad];

          // Is the hit close to any mass hypo in local coordinate space
          SelOK = any_of( activeParticles(),
                          [sigma=m_nSigmaPreSel[rad],&ckThetaEsti,&tkCkAngles,&tkCkRes]
                          ( const auto hypo )
                          { return fabs(tkCkAngles[hypo]-ckThetaEsti) < sigma*tkCkRes[hypo]; } );

        } // boundary check

        // Did we pass the pre-sel
        if ( !SelOK ) continue;

        // Move on to the reconstruction

        // make a photon object to work on
        photons.emplace_back();
        auto & gPhoton = photons.back();
        DataGuard<decltype(photons)> photGuard(photons);

        // fraction of segment path length accessible to the photon
        // cannot determine this here so set to 1
        constexpr float fraction(1);

        // The track - pixel seperation
        const auto track_pix_sep = std::sqrt( sep2 );

        // estimate phi from these hits
        const auto phiCerenkov = (float) ( Gaudi::Units::pi + vdt::fast_atan2f(diff_y,diff_x) );

        // Find the best ring and points to use as the calibration data
        const RayTracedCKRingPoint::Vector *ring{nullptr};
        //const RayTracedCKRingPoint *best_point{nullptr};
        Rich::ParticleIDType ring_pid{Rich::Unknown}, last_ring_pid{Rich::Unknown};
        float sep_diff2 = std::numeric_limits<float>::max();
        float sep_calib{0}, last_sep_calib{0}, thetaCerenkov{-1};
        bool unambigPhoton{false}, last_unambigPhoton{false};
        bool pointOutsideLargestRing{false};
        for ( const auto pid : activeParticles() )
        {
        
          // Load the ring and select the point for this PID type
          const auto & ring_tmp = tkRings[pid];
        
          // Did we find a ring (i.e. above threshold)
          if ( !ring_tmp.empty() )
          {
            // check how close to threshold the ring is. Do not use if too close.
            //if ( tkCkAngles[pid] > minCalibRingRadius(radiator) ) // todo
            if ( tkCkAngles[pid] > 0 )
            {
            
              // select the calibration point to use from this ring
              const auto points = getPointsClosestInAzimuth(ring_tmp,phiCerenkov);
            
              // if found see if this point is better than the last one tried
              if ( points.first && points.second &&
                   sameSide( rad, pixPRad, points.first->localPosition()  ) &&
                   sameSide( rad, pixPRad, points.second->localPosition() ) )
              {
                // get distance to each calibration point in phi
                const auto lD = fabs( points.first->azimuth()  - phiCerenkov );
                const auto hD = fabs( points.second->azimuth() - phiCerenkov );
                const auto Dinv = 1.0 / ( lD + hD );
              
                // corrected interpolated local calibration point position (x,y)
                const auto& lP    = points.first ->localPosition();
                const auto& hP    = points.second->localPosition();
                const auto calp_x = ( ( lP.x() * hD ) + ( hP.x() * lD ) ) * Dinv;
                const auto calp_y = ( ( lP.y() * hD ) + ( hP.y() * lD ) ) * Dinv;
              
                // pixel - calibration point seperation ^ 2
                const auto pix_calib_sep2 = ( std::pow( pixPRad.x() - calp_x, 2 ) +
                                              std::pow( pixPRad.y() - calp_y, 2 ) );
                const bool calibIsCloser = ( pix_calib_sep2 < sep_diff2 );
              
                // Is this point a better calibration point to use ?
                if ( m_useRingInterp[rad] || calibIsCloser )
                {
                  // update decision variable
                  sep_diff2 = pix_calib_sep2;
                
                  // First ring found ?
                  const bool firstFoundRing = ( ring == nullptr );
                
                  // Update best point pointer
                  //best_point = ( lD < hD ? points.first : points.second );
                
                  // Update calibration distance
                  last_sep_calib = sep_calib;
                  sep_calib = std::sqrt( std::pow( segPSide.x() - calp_x, 2 ) +
                                         std::pow( segPSide.y() - calp_y, 2 ) );
                
                  // Update best ring and pixel pointers
                  last_ring_pid = ring_pid;
                  ring      = &ring_tmp;
                  ring_pid  = pid;
                
                  // Ambiguous photon test, based on the mirrors used by both calibration points.
                  // Not quite the same as what is done in the Quartic tool (which tests
                  // reconstruction from the start and end of the track segment) but its better
                  // than doing nothing.
                  last_unambigPhoton = unambigPhoton;
                  unambigPhoton =
                    ( ( points.first->primaryMirror()   == points.second->primaryMirror()   ) &&
                      ( points.first->secondaryMirror() == points.second->secondaryMirror() ) );
                
                  // Is the data point outside the first ring ?
                  if ( firstFoundRing ) { pointOutsideLargestRing = track_pix_sep > sep_calib; }
                
                  // If interpolation is enabled, and we are between rings, use them
                  if ( m_useRingInterp[rad]            &&
                       !firstFoundRing                 &&
                       track_pix_sep >=      sep_calib &&
                       track_pix_sep <  last_sep_calib  )
                  {
                    // Interpolate between the two rings, then break out as this is the best we can do
                    thetaCerenkov = 
                      ( ( ( tkCkAngles[ring_pid]      * ( track_pix_sep - last_sep_calib ) ) +
                          ( tkCkAngles[last_ring_pid] * ( sep_calib     - track_pix_sep  ) ) ) / 
                        ( sep_calib - last_sep_calib ) );
                    break;
                  }
                  else
                  {
                    // Update CK theta value just using this ring
                    thetaCerenkov = tkCkAngles[ring_pid] * ( track_pix_sep / sep_calib );
                    // If this is the first found ring, and the point is further away than
                    // the calibration point, do not bother searching other (smaller) rings
                    // as they are only going to get further away.
                    if ( firstFoundRing && pointOutsideLargestRing ) { break; }
                  }
                
                }
                else if ( ring )
                {
                  // a ring is already found, and we are getting further away, so break out ...
                  break;
                }
              
              } // point found
            
            } // ring saturation check
          
          }
          else
          {
            // no ring was found. This means we are below threshold so there is no
            // point in searching other heavier rings, so break out.
            break;
          }
        
        } // loop over PID types

        // If a good calibration point was found, use its info to fill the photon
        if ( ring )
        {
        
          // check for ambiguous photons ?
          const bool unambig = unambigPhoton && last_unambigPhoton;

          // Check if alibration points used different mirrors, so reject
          if ( unambig || !m_rejAmbigPhots )
          {
          
            // Add bias correction to CK theta value
            thetaCerenkov += ckThetaCorrection(rad);

            // ------------------------------------------------------------------------
            // Final checks on the Cherenkov angles
            // ------------------------------------------------------------------------
            if ( UNLIKELY( !checkAngles( rad, tkCkAngles, tkCkRes,
                                         thetaCerenkov, phiCerenkov ) ) )
            {
              //_ri_debug << rad << " : Failed Final angles check" << endmsg;
            }
            else
            {
          
              // ------------------------------------------------------------------------
              // Set (remaining) photon parameters
              // ------------------------------------------------------------------------
              gPhoton.setCherenkovTheta         ( thetaCerenkov            );
              gPhoton.setCherenkovPhi           ( phiCerenkov              );
              gPhoton.setActiveSegmentFraction  ( fraction                 );
              gPhoton.setSmartID                ( cluster.primaryID()      );
              gPhoton.setUnambiguousPhoton      ( unambig                  );
              //_ri_verbo << "Created photon " << gPhoton << endmsg;
              // ------------------------------------------------------------------------
              
              // if get here keep the photon
              photGuard.setOK();
              // Save relations
              relations.emplace_back( ++photonIndex, pixIndex, segIndex, inTkRel.tkIndex );
              //_ri_verbo << relations.back() << endmsg;
 
            }
          
          }
        
        }
      
      }
    
    }

  }
  
  return outData;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( CKEstiFromRadiusPhotonReco )

//=============================================================================
