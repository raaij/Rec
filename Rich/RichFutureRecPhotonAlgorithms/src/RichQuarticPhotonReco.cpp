
// local
#include "RichQuarticPhotonReco.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future;
using namespace Rich::Future::Rec;

// pull in methods from Rich::RayTracingUtils
using namespace Rich::RayTracingUtils;

//=============================================================================

QuarticPhotonReco::QuarticPhotonReco( const std::string& name, 
                                      ISvcLocator* pSvcLocator )
  : MultiTransformer ( name, pSvcLocator,
                       { KeyValue{ "TrackSegmentsLocation",        LHCb::RichTrackSegmentLocation::Default },
                         KeyValue{ "CherenkovAnglesLocation",      CherenkovAnglesLocation::Signal },
                         KeyValue{ "CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default },
                         KeyValue{ "TrackLocalPointsLocation",     SpacePointLocation::SegmentsLocal },
                         KeyValue{ "SegmentPhotonFlagsLocation",   SegmentPhotonFlagsLocation::Default },
                         KeyValue{ "RichPixelClustersLocation",    Rich::PDPixelClusterLocation::Default },
                         KeyValue{ "RichPixelGlobalPositionsLocation", SpacePointLocation::PixelsGlobal },
                         KeyValue{ "RichPixelLocalPositionsLocation",  SpacePointLocation::PixelsLocal },
                         KeyValue{ "TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Selected } },
                       { KeyValue{ "CherenkovPhotonLocation",      CherenkovPhotonLocation::Default },
                         KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default } } )
{
  // init
  m_rich.fill(nullptr);
  m_ckBiasCorrs.fill(0);
  m_deBeam.fill(nullptr);

  // Corrections for the intrinsic biases
  //                  Aerogel      Rich1Gas   Rich2Gas
  m_ckBiasCorrs = { -0.000358914, -7.505e-5, -4.287e-5 };

  // debugging
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode QuarticPhotonReco::initialize()
{
  // Sets up various tools and services
  auto sc = MultiTransformer::initialize();
  if ( !sc ) return sc;

  // Get the CPU capabilities and set dispatch method
  if ( m_detectCPU )
  {
    const auto vbtl = { 
      std::make_pair( LHCb::CPU::AVX2,    &QuarticPhotonReco::run_avx2    ),
      std::make_pair( LHCb::CPU::AVX,     &QuarticPhotonReco::run_avx     ),
      std::make_pair( LHCb::CPU::SSE4,    &QuarticPhotonReco::run_sse4    ),
      std::make_pair( LHCb::CPU::GENERIC, &QuarticPhotonReco::run_generic )
    };
    m_run = LHCb::CPU::dispatch(vbtl);
  }

  // get the detector elements
  m_rich[Rich::Rich1] = getDet<DeRich>( DeRichLocations::Rich1 );
  m_rich[Rich::Rich2] = getDet<DeRich>( DeRichLocations::Rich2 );
  if ( m_checkBeamPipe[Rich::Rich1Gas] || m_checkBeamPipe[Rich::Aerogel] ) 
  { m_deBeam[Rich::Rich1] = getDet<DeRichBeamPipe>( DeRichLocations::Rich1BeamPipe ); }
  if ( m_checkBeamPipe[Rich::Rich2Gas] ) 
  { m_deBeam[Rich::Rich2] = getDet<DeRichBeamPipe>( DeRichLocations::Rich2BeamPipe ); }

  // load tools
  sc = sc && m_mirrorSegFinder.retrieve();

  // loop over radiators
  for ( const auto rad : Rich::radiators() )
  {

    // If rejection of ambiguous photons is turned on make sure test is turned on
    if ( m_rejectAmbigPhots[rad] && !m_testForUnambigPhots[rad] )
    {
      Warning( "Unambigous photon check will be enabled in order to reject ambiguous photons",
               StatusCode::SUCCESS ).ignore();
      m_testForUnambigPhots[rad] = true;
    }

    // If we are testing for photons that hit the beam pipe, turn on ambig photon test
    if ( m_checkBeamPipe[rad] && !m_testForUnambigPhots[rad] )
    {
      Warning( "Unambigous photon check will be enabled for beampipe check",
               StatusCode::SUCCESS ).ignore();
      m_testForUnambigPhots[rad] = true;
    }

    // information printout about configuration
    if ( m_testForUnambigPhots[rad] )
    {      _ri_debug << "Will test for unambiguous     " << rad << " photons" << endmsg; }
    else { _ri_debug << "Will not test for unambiguous " << rad << " photons" << endmsg; }

    if ( m_rejectAmbigPhots[rad] )
    {      _ri_debug << "Will reject ambiguous " << rad << " photons" << endmsg; }
    else { _ri_debug << "Will accept ambiguous " << rad << " photons" << endmsg; }

    if ( m_useAlignedMirrSegs[rad] )
    {      _ri_debug << "Will use fully alligned mirror segments for " << rad << " reconstruction" 
                     << endmsg;  }
    else { _ri_debug << "Will use nominal mirrors for " << rad << " reconstruction" << endmsg; }

    if ( m_checkBeamPipe[rad] )
    {      _ri_debug << "Will check for " << rad << " photons that hit the beam pipe" << endmsg; }

    if ( m_checkPrimMirrSegs[rad] )
    {      _ri_debug << "Will check for full intersecton with mirror segments for " 
                     << rad << endmsg; }

    _ri_debug << "Minimum active " << rad << " segment fraction = " << m_minActiveFrac[rad] 
              << endmsg;

  }

  _ri_debug << "# iterations (Aero/R1Gas/R2Gas) = " << m_nMaxQits << endmsg;

  // return
  return sc;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( QuarticPhotonReco )

//=============================================================================
