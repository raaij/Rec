
#pragma once

// Array properties
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Gaudi
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichBasePhotonReco.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecSpacePoints.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"

// Rec Utils
#include "RichRecUtils/RichRadCorrLocalPositions.h"

// VDT
#include "vdt/atan2.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

      namespace
      {
        /// Shortcut to the output data type
        using OutData = std::tuple<CherenkovPhoton::Vector,Relations::PhotonToParents::Vector>;
      }

      /** @class CKEstiFromRadiusPhotonReco RichCKEstiFromRadiusPhotonReco.h
       *
       *  Reconstructs photon candidates using the Quartic solution.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */

      class CKEstiFromRadiusPhotonReco final :
        public MultiTransformer< OutData( const LHCb::RichTrackSegment::Vector&,
                                          const CherenkovAngles::Vector&,
                                          const CherenkovResolutions::Vector&,
                                          const SegmentPanelSpacePoints::Vector&,
                                          const MassHypoRingsVector&,
                                          const SegmentPhotonFlags::Vector&,
                                          const Rich::PDPixelCluster::Vector&,
                                          const SpacePointVector&,
                                          const SpacePointVector&,
                                          const Relations::TrackToSegments::Vector& ),
                                 Traits::BaseClass_t<BasePhotonReco> >
      {

      public:

        /// Standard constructor
        CKEstiFromRadiusPhotonReco( const std::string& name,
                                    ISvcLocator* pSvcLocator );

      public:

        /// Functional operator
        OutData operator()( const LHCb::RichTrackSegment::Vector& segments,
                            const CherenkovAngles::Vector& ckAngles,
                            const CherenkovResolutions::Vector& ckResolutions,
                            const SegmentPanelSpacePoints::Vector& trHitPntsLoc,
                            const MassHypoRingsVector& massRings,
                            const SegmentPhotonFlags::Vector& segPhotFlags,
                            const Rich::PDPixelCluster::Vector& clusters,
                            const SpacePointVector& globalPoints,
                            const SpacePointVector& localPoints,
                            const Relations::TrackToSegments::Vector& tkToSegRels ) const override;

      private:

        /// Flag to turn on interpolation between two nearest rings, by radiator
        Gaudi::Property< RadiatorArray<bool> > m_useRingInterp
        { this, "UseRingInterpolation", { true, true, true } };

        /// Flag to turn on the rejection of 'ambiguous' photons
        Gaudi::Property<bool> m_rejAmbigPhots { this, "RejectAmbiguousPhotons", false };

      };

    }
  }
}
