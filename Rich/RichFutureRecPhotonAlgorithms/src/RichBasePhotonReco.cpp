
// local
#include "RichBasePhotonReco.h"

// boost
#include "boost/format.hpp"

using namespace Rich::Future::Rec;

//=============================================================================

BasePhotonReco::BasePhotonReco( const std::string& name, 
                                ISvcLocator* pSvcLocator )
  : AlgBase( name, pSvcLocator )
{
  // initialise - pre sel
  m_minROI2PreSel.fill( 0 );
  m_maxROI2PreSel.fill( 0 );
  m_scalePreSel  .fill( 0 );
  // initialise - sel
  m_ckBiasCorrs  .fill( 0 );
}

//=============================================================================

StatusCode BasePhotonReco::initialize()
{
  // Sets up various tools and services
  auto sc = AlgBase::initialize();
  if ( !sc ) return sc;

  // loop over radiators
  for ( const auto rad : Rich::radiators() )
  {
    // cache some numbers
    m_minROI2PreSel[rad] = std::pow(m_minROIPreSel[rad],2);
    m_maxROI2PreSel[rad] = std::pow(m_maxROIPreSel[rad],2);
    m_scalePreSel[rad]   = (m_ckThetaScale[rad]/m_sepGScale[rad]);
    if ( msgLevel(MSG::DEBUG) )
    {
      // printout for this rad
      auto trad = Rich::text(rad);
      trad.resize(8,' ');
      debug() << trad << " : Pre-Sel. Sep. range     "
              << boost::format("%5.1f") % m_minROIPreSel[rad] << " -> "
              << boost::format("%5.1f") % m_maxROIPreSel[rad] << " mm  : Tol. "
              << boost::format("%5.1f") % m_nSigmaPreSel[rad] << " # sigma" << endmsg;
    }
  }

  // return
  return sc;
}

//=============================================================================
