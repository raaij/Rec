
// local
#include "../RichQuarticPhotonReco.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future;
using namespace Rich::Future::Rec;

OutData 
QuarticPhotonReco::run_sse4( const LHCb::RichTrackSegment::Vector& segments,
                             const CherenkovAngles::Vector& ckAngles,
                             const CherenkovResolutions::Vector& ckResolutions,
                             const SegmentPanelSpacePoints::Vector& trHitPntsLoc,
                             const SegmentPhotonFlags::Vector& segPhotFlags,
                             const Rich::PDPixelCluster::Vector& clusters,
                             const SpacePointVector& globalPoints,
                             const SpacePointVector& localPoints,
                             const Relations::TrackToSegments::Vector& tkToSegRels ) const
{
#include "../RichQuarticPhotonReco.icpp"
}
