
#ifndef MUONINFORMATION_H
#define MUONINFORMATION_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
//#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiAlgorithm.h"

#include "IMuonInformation.h"            // Interface

//#include "RichParticleSearchMain.h"
#include "Kernel/RichRadiatorType.h"
#include "Event/RichRecSegment.h"
// base class
#include "RichRecBase/RichRecTupleAlgBase.h"

#include "Kernel/RichRadiatorType.h"

// Event
#include "Event/RichRecStatus.h"
#include "Event/MCRichOpticalPhoton.h"
#include "Event/RichRecTrack.h"
#include "Event/Track.h"
#include "Event/Particle.h"

// Histogramming
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"

// Interfaces
#include "MCInterfaces/IRichRecMCTruthTool.h"
#include "RichInterfaces/IRichParticleProperties.h"
#include "RichRecInterfaces/IRichCherenkovAngle.h"
#include "RichRecInterfaces/IRichTrackSelector.h"
#include "RichRecInterfaces/IRichIsolatedTrack.h"
#include "RichInterfaces/IRichRefractiveIndex.h"
#include "RichRecInterfaces/IRichRecGeomTool.h"
#include "CaloUtils/ICaloElectron.h"
#include "RichInterfaces/IRichSmartIDTool.h"

// RichDet
#include "RichDet/DeRichSphMirror.h"
#include "RichDet/DeRichHPD.h"
#include "RichDet/DeRichLocations.h"
#include "RichDet/DeRich.h"

// Kernel
#include "RichUtils/BoostArray.h"
#include "RichUtils/RichSmartIDCnv.h"

// boost
#include "boost/lexical_cast.hpp"
//#include <boost/foreach.hpp>

/** @class MuonInformation MuonInformation.h
 *
 *
 *  @author Matt Coombes
 *  @date   2010-10-04
 */

using namespace LHCb;
class MuonInformation final : public GaudiTool, virtual public IMuonInformation {
public:
  /// Standard constructor
  MuonInformation( const std::string& type,
                   const std::string& name,
                   const IInterface* parent);

  StatusCode initialize() override;    ///< Algorithm initialization

  int HasMuonInformation(const LHCb::Track*) override;

private:

  ICaloElectron* m_caloElectron = nullptr;

};
#endif // MUONINFORMATION_H
