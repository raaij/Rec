
//---------------------------------------------------------------------------------
/** @file RichRecoQC.h
 *
 *  Header file for RICH reconstruction monitoring algorithm : Rich::Rec::MC::RecoQC
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   2002-07-02
 */
//---------------------------------------------------------------------------------

#ifndef RICHRECQC_RICHRECOQC_H
#define RICHRECQC_RICHRECOQC_H 1

// Gaudi
#include "GaudiUtils/Aida2ROOT.h"
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichRecBase/RichRecHistoAlgBase.h"

// Interfaces
#include "MCInterfaces/IRichRecMCTruthTool.h"

// temporary histogramming numbers
#include "RichRecUtils/RichDetParams.h"

// interfaces
#include "RichInterfaces/IRichParticleProperties.h"
#include "RichRecInterfaces/IRichTrackSelector.h"
#include "RichRecInterfaces/IRichCherenkovAngle.h"
#include "RichRecInterfaces/IRichCherenkovResolution.h"
#include "RichRecInterfaces/IRichIsolatedTrack.h"
#include "RichRecInterfaces/IRichStereoFitter.h"

// Rich Utils
#include "RichUtils/RichHPDIdentifier.h"
#include "RichUtils/RichStatDivFunctor.h"
#include "RichUtils/StlArray.h"

// RichDet
#include "RichDet/DeRichMultiSolidRadiator.h"
#include "RichDet/DeRichAerogelRadiator.h"
#include "RichDet/DeRichSystem.h"

// ROOT
#include "TProfile.h"
#include "TH1D.h"
#include "TF1.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

// Redirect streams
#include "Kernel/STLOStreamRedirect.h"

namespace Rich
{
  namespace Rec
  {
    namespace MC
    {

      //---------------------------------------------------------------------------------
      /** @class RecoQC RichRecoQC.h
       *
       *  Quality control monitor for Rich Reconstruction
       *
       *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
       *  @date   2002-07-02
       */
      //---------------------------------------------------------------------------------

      class RecoQC final : public Rich::Rec::HistoAlgBase
      {

      public:

        /// Standard constructor
        RecoQC( const std::string& name, ISvcLocator* pSvcLocator );

        StatusCode initialize() override;    ///< Algorithm initialization
        StatusCode execute() override;       ///< Algorithm execution
        StatusCode finalize() override;      ///< Algorithm finalization

      protected:

        /// Pre-Book all (non-MC) histograms
        StatusCode prebookHistograms() override;

      private:

        /// Fit result
        class FitResult
        {
        public:
          /// Constructor
          FitResult() = default;
        public:
          bool   OK{false};     ///< Fit status
          double resolution{0}; ///< CK resolution
          double reserror{0};   ///< CK resolution error
          double mean{0};       ///< Mean
          double meanerror{0};  ///< Mean error
        };

      private: // methods

        /// access RichRecMCTruthTool tool on demand
        const Rich::Rec::MC::IMCTruthTool * richRecMCTool() const
        {
          if ( !m_richRecMCTruth ) { acquireTool( "RichRecMCTruthTool", m_richRecMCTruth ); }
          return m_richRecMCTruth;
        }

        /// Fit the resolution histograms
        void fitResolutionHistos();

        /// Get the per PD resolution histogram ID
        std::string pdResPlotID( const LHCb::RichSmartID hpd ) const;

        /// Fit the given histogram to extract the resolution
        FitResult fit( TH1D * hist, const Rich::RadiatorType rad );

      private: // data

        // Pointers to tool instances
        const IParticleProperties * m_richPartProp = nullptr; ///< Rich Particle properties
        const ICherenkovAngle * m_ckAngle = nullptr;          ///< Pointer to RichCherenkovAngle tool
        const ICherenkovResolution * m_ckRes = nullptr;       ///< Cherenkov angle resolution tool
        mutable const Rich::Rec::MC::IMCTruthTool* m_richRecMCTruth = nullptr;  ///< Pointer to RichRecMCTruthTool interface
        const ITrackSelector * m_trSelector = nullptr;        ///< Track selector
        const IIsolatedTrack * m_isoTrack = nullptr;          ///< Isolated track tool
        const IStereoFitter * m_fitter = nullptr;             ///< Stereographic refitting tool

        /// Pointer to RICH system detector element
        const DeRichSystem * m_RichSys = nullptr;

        // job options selection cuts
        RadiatorArray<double> m_minBeta = {{}}; ///< minimum beta value for tracks
        RadiatorArray<double> m_maxBeta = {{}}; ///< maximum beta value for tracks

        RadiatorArray<unsigned int> m_truePhotCount = {{}}; ///< Total number of true cherenkov photons per radiator
        RadiatorArray<unsigned int> m_nSegs = {{}};         ///< Total number of track segments per radiator

        RadiatorArray<double> m_ckThetaMax = {{}}; ///< Max theta limit for histos for each rad
        RadiatorArray<double> m_ckThetaMin = {{}}; ///< Min theta limit for histos for each rad

        RadiatorArray<bool> m_rads = {{}}; ///< Which radiators to monitor

        RadiatorArray<unsigned int> m_minRadSegs = {{}}; ///< Minimum segments per radiator
        RadiatorArray<unsigned int> m_maxRadSegs = {{}}; ///< Maximum segments per radiator

        /// Histogram fitting event frequency
        long long m_histFitFreq;

        /// Histogram ranges for CK resolution plots
        RadiatorArray<double> m_ckResRange = {{}};

        /// Enable aerogel tile plots
        bool m_aeroTilePlots;

        /// Enable per PD resolution plots
        bool m_pdResPlots;

        /// Enable per PD column resolution plots
        bool m_pdColResPlots;

        /// Enable Fitted CK theta resolution plots per PD
        bool m_fittedPDResPlots;

        /// Reject ambiguous photons
        bool m_rejectAmbigPhots;

        /// Event count
        unsigned long long m_nEvts{0};

        typedef std::pair<Rich::RadiatorType,LHCb::RichSmartID> PDPlotKey;
        typedef Rich::HashMap<PDPlotKey,TH1D*> PDPlotMap;
        PDPlotMap m_pdPlots; ///< PD resolution plots

      };

      //-------------------------------------------------------------------------------

      inline std::string RecoQC::pdResPlotID( const LHCb::RichSmartID hpd ) const
      {
        const Rich::DAQ::HPDIdentifier hid(hpd);
        std::ostringstream id;
        id << "PDs/pd-" << hid.number();
        return id.str();
      }

      //-------------------------------------------------------------------------------

    }
  }
}

#endif // RICHRECQC_RICHRECOQC_H
