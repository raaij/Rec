
//-----------------------------------------------------------------------------
/** @file RichRecPixelQC.h
 *
 *  Header file for algorithm class : Rich::Rec::MC::PixelQC
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   05/04/2002
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECMONITOR_RichRecPixelQC_H
#define RICHRECMONITOR_RichRecPixelQC_H 1

// STD
#include <sstream>

// base class
#include "RichRecBase/RichRecHistoAlgBase.h"

// RichUtils
#include "RichUtils/RichPoissonEffFunctor.h"
#include "RichUtils/RichStatDivFunctor.h"
#include "RichUtils/StlArray.h"

// temporary histogramming numbers
#include "RichRecUtils/RichDetParams.h"

// Event
#include "Event/MCRichDigitSummary.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Interfaces
#include "MCInterfaces/IRichMCTruthTool.h"
#include "MCInterfaces/IRichRecMCTruthTool.h"
#include "RichInterfaces/IRichRawBufferToSmartIDsTool.h"

namespace Rich
{
  namespace Rec
  {
    namespace MC
    {

      //-----------------------------------------------------------------------------
      /** @class PixelQC RichRecPixelQC.h
       *
       *  Monitor class for the pixel reconstruction
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   05/04/2002
       */
      //-----------------------------------------------------------------------------

      class PixelQC final : public Rich::Rec::HistoAlgBase
      {

      public:

        /// Standard constructor
        PixelQC( const std::string& name,
                 ISvcLocator* pSvcLocator );

        StatusCode initialize() override;    // Algorithm initialization
        StatusCode execute() override;       // Algorithm execution
        StatusCode finalize() override;      // Algorithm finalization

      protected:

        /// Pre-Book all (non-MC) histograms
        StatusCode prebookHistograms() override;

      private: // helper classes

        /// Simple class containing MC flags for a given pixel (reconstructed hit)
        class MCFlags
        {
        public:
          /// Default constructor
          MCFlags() = default;
          // data members
          bool isBkg{false};           ///< Hit is background, of any sort
          bool isHPDQCK{false};        ///< Hit is from CK radiation from the HPD quartz windows
          bool isGasCK{false};         ///< Hit is from CK radiation from the gas volume quartz windows
          bool isN2CK{false};          ///< Hit is from CK radiation from N2
          bool isChargedTk{false};     ///< Hit is from a charged track hitting the HPD
          bool isChargeShare{false};   ///< Hit is from charge sharing in the HPD silicon detector
          bool isAeroFiltCK{false};    ///< Hit is from CK radiator in the aerogel Filter
          bool isSiBackScatter{false}; ///< Hit is from back-scattering from the silicon chip
          bool isHPDIntReflect{false}; ///< Hit is from internal reflections inside the HPD
          bool isSignal{false};        ///< Hit is from (unscattered) CK radiation from one of the main radiators
          bool isAerogelCK{false};     ///< Hit is from (unscattered) CK radiation from the aerogel radiator
          bool isRich1GasCK{false};    ///< Hit is from (unscattered) CK radiation from the Rich1 gas radiator
          bool isRich2GasCK{false};    ///< Hit is from (unscattered) CK radiation from the Rich2 gas radiator
          bool isRadScint{false};      ///< Hit is from Radiator Scintillation
        };

        /// Stores tallys opf various types
        class Tallys
        {
        public:
          /// Constructor
          Tallys()
          {
            pixels          .fill(0);
            bkgs            .fill(0);
            npdqcks         .fill(0);
            ngasck          .fill(0);
            n2ck            .fill(0);
            ntrack          .fill(0);
            nchargeshare    .fill(0);
            naerofilter     .fill(0);
            nbackscatter    .fill(0);
            nhpdintreflect  .fill(0);
            signal          .fill(0);
            radHits         .fill(0);
            radScint        .fill(0);
          }
        public:
          // count type
          using RichTally = std::array<unsigned long long,Rich::NRiches>;
          using RadTally  = std::array<unsigned long long,Rich::NRadiatorTypes>;
        public:
          // data members
          RichTally pixels = {{}};   ///< Total number of pixels hit
          RichTally bkgs = {{}};     ///< Total number of background hits
          RichTally npdqcks = {{}};  ///< Total number of HPD quartz window CK hits
          RichTally ngasck = {{}};   ///< Total number of gas volume quartz window CK hits
          RichTally n2ck = {{}};     ///< Total number of N2 CK hits
          RichTally ntrack = {{}};   ///< Total number of charged track on HPD hits
          RichTally nchargeshare = {{}}; ///< Total number of silicon charge share hits
          RichTally naerofilter = {{}}; ///< Total number of aerogel filter CK hits
          RichTally nbackscatter = {{}}; ///< Total number of back-scattered hits
          RichTally nhpdintreflect = {{}}; ///< Total number of HPD internal reflection hits
          RichTally signal = {{}};   ///< Total number of signal hits
          RadTally  radHits = {{}};  ///< Total number of signal hits for each radiator
          RichTally radScint = {{}}; ///< Total number of hits from Scintillation
        };

      private: // methods

        /// Get various MC history flags for given RichSmartID
        MCFlags getHistories( const LHCb::RichSmartID id ) const;

        /// Print summary for given RICH
        void printRICH( const Rich::DetectorType rich ) const;

      private: // data

        /// Pointer to RichRecMCTruthTool interface
        const Rich::Rec::MC::IMCTruthTool* m_richRecMCTruth = nullptr;

        /// Pointer to RichMCTruthTool
        const Rich::MC::IMCTruthTool * m_truth = nullptr;

        /// Raw Buffer Decoding tool
        const Rich::DAQ::IRawBufferToSmartIDsTool * m_decoder = nullptr;

        /// Pointer to RICH system detector element
        const DeRichSystem * m_richSys = nullptr;

        // tallies

        Tallys m_recoTally; ///< tally for reconstructed pixels
        Tallys m_rawTally;  ///< tally for all raw pixels

        unsigned long long m_nEvts{0}; ///< Total number of events processed

        unsigned int m_maxPixels; ///< Maximum pixels

      };

    }
  }
}

#endif // RICHRECMONITOR_RichRecPixelQC_H
