
//---------------------------------------------------------------------------------
/** @file RichRecSummaryCKResMoni.h
 *
 *  Header file for RICH reconstruction monitoring algorithm : Rich::Rec::MC::SummaryCKResMoni
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   23/11/2007
 */
//---------------------------------------------------------------------------------

#ifndef RICHRECQC_RichRecSummaryCKResMoni_H
#define RICHRECQC_RichRecSummaryCKResMoni_H 1

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichRecBase/RichRecHistoAlgBase.h"

// temporary histogramming numbers
#include "RichRecUtils/RichDetParams.h"

// interfaces
#include "RichRecInterfaces/IRichTrackSelector.h"
#include "RichInterfaces/IRichParticleProperties.h"
#include "MCInterfaces/IRichRecMCTruthTool.h"
#include "RichInterfaces/IRichParticleProperties.h"

// RichUtils
#include "RichUtils/RichStatDivFunctor.h"
#include "RichUtils/StlArray.h"

// Event
#include "Event/RichSummaryTrack.h"
#include "Event/MCParticle.h"

namespace Rich
{
  namespace Rec
  {
    namespace MC
    {

      //---------------------------------------------------------------------------------
      /** @class SummaryCKResMoni RichRecSummaryCKResMoni.h
       *
       *  Quality control monitor for RICH summary information.
       *  Produces CK resolution plots, that can be compared to the 'standard' ones
       *
       *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
       *  @date   23/11/2007
       */
      //---------------------------------------------------------------------------------

      class SummaryCKResMoni final : public Rich::Rec::HistoAlgBase
      {

      public:

        /// Standard constructor
        SummaryCKResMoni( const std::string& name, ISvcLocator* pSvcLocator );

        StatusCode initialize() override;    // Algorithm initialization
        StatusCode execute() override;       // Algorithm execution

      private:

        /// access RichRecMCTruthTool tool on demand
        const Rich::Rec::MC::IMCTruthTool * richRecMCTool() const
        {
          if ( !m_richRecMCTruth ) { acquireTool( "RichRecMCTruthTool", m_richRecMCTruth ); }
          return m_richRecMCTruth;
        }

      private: // data

        const IParticleProperties * m_richPartProp = nullptr; ///< Rich Particle properties

        /// Pointer to RichRecMCTruthTool interface
        mutable const Rich::Rec::MC::IMCTruthTool* m_richRecMCTruth = nullptr;

        /// Track selector
        const ITrackSelector * m_trSelector = nullptr;

        /// Location of the summary tracks
        std::string m_summaryLoc;

        // Job Options

        /// minimum beta value for 'saturated' tracks
        RadiatorArray<double> m_minBeta = {{}};  
        /// Histogram ranges for CK resolution plots
        RadiatorArray<double> m_ckResRange = {{}}; 

      };

    }
  }
}

#endif // RICHRECQC_RichRecSummaryCKResMoni_H
