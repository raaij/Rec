
//---------------------------------------------------------------------------
/** @file RichTracklessRingMoni.h
 *
 *  Header file for algorithm class : Rich::Rec::MC::TracklessRingMoni
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   05/04/2002
 */
//---------------------------------------------------------------------------

#ifndef RICHRECQC_RichTracklessRingMoni_H
#define RICHRECQC_RichTracklessRingMoni_H 1

//STD
#include <sstream>
#include <cmath>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichRecBase/RichRecHistoAlgBase.h"

// Event
#include "Event/RichRecStatus.h"
#include "Event/RichRecTrack.h"

// Interfaces
#include "RichRecInterfaces/IRichTrackSelector.h"
#include "RichInterfaces/IRichRayTracing.h"
#include "RichInterfaces/IRichSmartIDTool.h"
#include "MCInterfaces/IRichRecMCTruthTool.h"
#include "RichRecInterfaces/IRichCherenkovAngle.h"

// Kernel
#include "RichRecBase/FastRingFitter.h"

// Utils
#include "RichUtils/StlArray.h"

namespace Rich
{
  namespace Rec
  {
    namespace MC
    {

      //---------------------------------------------------------------------------
      /** @class TracklessRingMoni RichTracklessRingMoni.h
       *
       *  Monitor for trackless rings
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   05/04/2002
       */
      //---------------------------------------------------------------------------

      class TracklessRingMoni final : public Rich::Rec::HistoAlgBase
      {

      public:

        /// Standard constructor
        TracklessRingMoni( const std::string& name,
                           ISvcLocator* pSvcLocator );

        StatusCode initialize() override;    // Algorithm initialization
        StatusCode execute() override;       // Algorithm execution

      protected:

        /// Pre-Book all (non-MC) histograms
        StatusCode prebookHistograms() override;

      private:

        /// access RichRecMCTruthTool tool on demand
        const Rich::Rec::MC::IMCTruthTool * richRecMCTool() const
        {
          if ( !m_richRecMCTruth )
          {
            acquireTool( "RichRecMCTruthTool", m_richRecMCTruth );
          }
          return m_richRecMCTruth;
        }

      private: // data

        /// Pointer to RichRecMCTruthTool interface
        mutable const Rich::Rec::MC::IMCTruthTool* m_richRecMCTruth = nullptr;

        /// Pointer to RichCherenkovAngle tool
        const ICherenkovAngle * m_ckAngle = nullptr;

        /// Location of Rings in TES
        std::string m_ringLoc;

        RadiatorArray<double> m_ckThetaMax = {{}}; ///< Max theta limit for histos for each rad
        RadiatorArray<double> m_ckThetaMin = {{}}; ///< Min theta limit for histos for each rad

        RadiatorArray<double> m_radiiMax = {{}};   ///< Max radii limit for histos for each rad
        RadiatorArray<double> m_radiiMin = {{}};   ///< Min radii limit for histos for each rad

        RadiatorArray<double> m_ckThetaRes = {{}}; ///< Ring radius - Expected CK theta resolution limits

        // Max fit variance
        RadiatorArray<double> m_maxFitVariance = {{}};

        /// MC association fraction for rings
        double m_mcAssocFrac;

      };

    }
  }
}

#endif // RICHRECQC_RichTracklessRingMoni_H
