
#pragma once

// STL
#include <type_traits>
#include <numeric>
#include <algorithm>
#include <array>
#include <iomanip> 

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecPixelBackgrounds.h"
#include "RichFutureRecEvent/RichRecTrackPIDInfo.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// VDT
#include "vdt/exp.h"
#include "vdt/log.h"

// boost
#include "boost/format.hpp"
#include "boost/numeric/conversion/bounds.hpp"
#include "boost/limits.hpp"

// Rich Utils
#include "RichUtils/ZipRange.h"
#include "RichUtils/RichMap.h"

namespace Rich 
{
  namespace Future 
  { 
    namespace Rec
    { 
      namespace GlobalPID
      {

        // Use the functional framework
        using namespace Gaudi::Functional;

        namespace
        {
          /// Type for output data
          using OutData = std::tuple< TrackPIDHypos, TrackDLLs::Vector >;
        }

        /** @class LikelihoodMinimiser RichGlobalPIDRecoSummary.h
         *
         *  Performs the RICH global PID likelihood minimisation.
         *
         *  @author Chris Jones
         *  @date   2016-10-25
         */

        class LikelihoodMinimiser final : 
          public MultiTransformer< OutData( const Summary::Track::Vector&,
                                            const Summary::Pixel::Vector&,
                                            const TrackPIDHypos&,
                                            const TrackDLLs::Vector&,
                                            const PixelBackgrounds&,
                                            const Relations::PhotonToParents::Vector&,
                                            const PhotonSignals::Vector& ),
                                   Traits::BaseClass_t<AlgBase> >
        {
          
        public:
          
          /// Standard constructor
          LikelihoodMinimiser( const std::string& name, ISvcLocator* pSvcLocator );

          /// Initialize method
          StatusCode initialize() override;

        public:
          
          /// Functional operator
          OutData
            operator()( const Summary::Track::Vector& gTracks,
                        const Summary::Pixel::Vector& gPixels,
                        const TrackPIDHypos& inTkHypos,
                        const TrackDLLs::Vector& inTkDLLs,
                        const PixelBackgrounds& pixelBkgs,
                        const Relations::PhotonToParents::Vector& photRel,
                        const PhotonSignals::Vector& photSignals ) const override;

        private: // definitions

          /// Working type for floating point numbers
          using FloatType = double;

          /// type for local pixel data containers
          using PixelData = std::vector<FloatType>;

          /// Track list entry. Its current best DLL change and a pointer to the track
          using TrackPair = std::pair<FloatType,const Summary::Track*>;
          
          /// List of all track list entries
          using TrackList = std::vector<TrackPair>;

          /// Struct to pass around the photon information
          class PhotConts final
          {
          public:
            PhotConts() = delete;
            PhotConts( const Relations::PhotonToParents::Vector& _photRel,
                       const PhotonSignals::Vector& _photSignals )
              : photRel(_photRel), photSignals(_photSignals) { }
          public:
            const Relations::PhotonToParents::Vector& photRel;
            const PhotonSignals::Vector&          photSignals;
          };

          /// Struct to pass around pixel containers
          class PixelConts final
          {
          public:
            PixelConts() = delete;
            PixelConts( const Summary::Pixel::Vector& gPixs,
                        const PixelBackgrounds& pBkgs ) 
              : gPixels       ( gPixs           ),
                pixelBkgs     ( pBkgs           ),
                pixSignals    ( pBkgs.size(), 0 ),
                pixCurrlogExp ( pBkgs.size(), 0 ) { }
          public:
            const Summary::Pixel::Vector& gPixels;
            const PixelBackgrounds& pixelBkgs;
            PixelData pixSignals;
            PixelData pixCurrlogExp;
          };

          /// Struct to pass around track containers
          class TrackConts final
          {
          public:
            TrackConts() = delete;
            TrackConts( OutData& outD, const Summary::Track::Vector& gTs ) 
              : outData(outD), gTracks(gTs) { }
          public:
            TrackPIDHypos&          tkHypos()       noexcept { return std::get<0>(outData); }
            const TrackPIDHypos&    tkHypos() const noexcept { return std::get<0>(outData); }
            TrackDLLs::Vector&       tkDLLs()       noexcept { return std::get<1>(outData); }
            const TrackDLLs::Vector& tkDLLs() const noexcept { return std::get<1>(outData); }
          public:
            OutData&                      outData;
            const Summary::Track::Vector& gTracks;
          };

          /// Container for changes to be made following an event iterations
          /// Contains a pointer to a track and the its new hypothesis
          using MinTrList = Rich::Map<const Summary::Track*,Rich::ParticleIDType>;

        private: // helpers

          /// Lookup table for log(exp(x)-1)
          template< typename TYPE,
                    size_t NSAMPLES = 10000,
                    typename = typename std::enable_if<std::is_floating_point<TYPE>::value>::type >
          class LogExpLookUp final
          {
          public:
            /// Default Constructor
            LogExpLookUp() = default;
          private:
            /// A single data point
            class Data
            {
            public:
              /// Drault constructor
              Data() = default;
              /// Constructor from bin low/high edges
              Data( const TYPE lowX, const TYPE highX )
              {
                const TYPE absMinX = 1e-20; // avoid log(0) in next line
                const auto lowY  = logexp( lowX > absMinX ? lowX : absMinX );
                const auto highY = logexp( highX );
                m_slope = ( lowY - highY ) / ( lowX - highX );
                m_const = lowY - ( lowX * m_slope );
              }
              /// Get the y value for a given x for this Data point
              inline TYPE getY( const TYPE x ) const noexcept
              {
                return ( ( x * m_slope ) + m_const );
              }
            public:
              /// The log(exp(x)-1) function to initialise the map
              template< typename T = TYPE >
              inline static
              typename std::enable_if< std::is_same<T,float>::value, T >::type
              logexp( const T x ) noexcept
              {
                return vdt::fast_logf( vdt::fast_expf(x) - 1.0f );
              }
              /// The log(exp(x)-1) function to initialise the map
              template< typename T = TYPE >
              inline static 
              typename std::enable_if< !std::is_same<T,float>::value, T >::type
              logexp( const T x ) noexcept
              {
                return vdt::fast_log( vdt::fast_exp(x) - 1.0 );
              }
            private:
              /// The slope parameter
              TYPE m_slope{0};
              /// The constant parameter
              TYPE m_const{0};
            public:
              /// type for storage of data points
              typedef std::array<Data,NSAMPLES> Storage;
            };
          public:
            /// Initialise. Range is from 0 to maxX.
            void init( const TYPE maxX )
            {
              // clear the current interpolation data
              clear();
              // reset the cached parameters
              // not that minX is implicitly 0
              m_maxX = maxX;
              m_incXinv = (TYPE)NSAMPLES / m_maxX ;
              // refill the data vector
              for ( unsigned int i = 0; i < NSAMPLES; ++i )
              { m_data[i] = Data( binLowX(i), binHighX(i) ); }
            }
            /** get the log(exp(x)-1) value for a given x from the look up table
             *  @attention No min value range check, as this is done elsewhere */
            inline TYPE lookup( const TYPE x ) const noexcept
            {
              // Use fast interpolator for configure x range (covers almost all calls)
              // y = log( exp(x) - 1 ) tends to y = x for large(ish) x so use this
              // if x is 'big'
              return ( x < m_maxX    ? m_data[xIndex(x)].getY(x) :
                       x < m_absMaxX ? LogExpLookUp::Data::logexp(x) : x );
            }
          private:
            /// Clear this object
            void clear()
            {
              m_data.fill( Data() );
              m_maxX = m_incXinv = (TYPE)0;
            }
            /// Get the low x value for a given bin index
            inline TYPE binLowX( const unsigned int i ) const noexcept
            {
              return (i/m_incXinv);
            }
            /// Get the high x value for a given bin index
            inline TYPE binHighX( const unsigned int i ) const noexcept
            {
              return binLowX(i) + (1.0/m_incXinv);
            }
            /** Get the look up index for a given x
             *  Note NO range checking is done. Assumed done beforehand */
            inline unsigned int xIndex( const TYPE x ) const noexcept
            {
              return (unsigned int)(x*m_incXinv);
            }
          private:
            /// The look up storage of data points
            typename Data::Storage m_data;
            /// The maximum valid x
            TYPE m_maxX{0};
            /// 1 / the bin increment
            TYPE m_incXinv{0};
            /** The abs max X value for this float type to calculate log(exp(x)-1)
             *  Above this value use the good approximation that y=log(exp(x)-1) -> y=x 
             *  The factor of 2.0 is to add a little safety margin */
            const TYPE m_absMaxX 
            { (TYPE)vdt::fast_log(boost::numeric::bounds<TYPE>::highest())/((TYPE)2.0) };
          };

          /// Stores information associated to a GlobalPID Track
          class InitTrackInfo final
          {
          public:
            /// Container
            typedef std::vector<InitTrackInfo> Vector;
            /// Constructor
            InitTrackInfo( const Summary::Track * track,
                           const Rich::ParticleIDType h,
                           const FloatType mindll )
              : pidTrack(track), hypo(h), minDLL(mindll) { }
          public:
            const Summary::Track * pidTrack{nullptr};    ///< Pointer to the track
            Rich::ParticleIDType hypo{Rich::Pion}; ///< Track hypothesis
            FloatType minDLL{0};                      ///< The DLL value
          };
          
        private: // methods

          /// Returns the force change Dll value
          inline FloatType forceChangeDll() const noexcept { return m_forceChangeDll; }
          
          /// Returns the freeze out Dll value
          inline FloatType freezeOutDll() const noexcept { return m_freezeOutDll; }

          /// Prefered implementation of log( e^x -1 )
          inline FloatType logExp( const FloatType x ) const noexcept
          {
            return m_logExpLookUp.lookup(x);
          }

          /// log( exp(x) - 1 ) or an approximation for small signals
          inline FloatType sigFunc( const FloatType x ) const noexcept
          {
            return ( x > m_minSig ? logExp(x) : m_logMinSig );
          }

          /// Calculates logLikelihood for event with a given set of track hypotheses.
          /// Performs full loop over all tracks and hypotheses
          FloatType logLikelihood( const TrackConts & tkC,
                                   const PixelConts & pixC ) const;
          
          /** Starting with all tracks pion, calculate logLikelihood. Then for
           *  each track in turn, holding all others to pion, calculate new
           *  logLikelihood for each particle code. If less than with all pion,
           *  set new minimum track hypothesis.
           *  @return Number of tracks that changed mass hypothesis
           */
          unsigned int initBestLogLikelihood( TrackList & trackList,
                                              TrackConts & tkC,
                                              PixelConts & pixC,
                                              PhotConts & photC ) const;
          
          /// Do the event iterations
          unsigned int doIterations( FloatType & currentBestLL,
                                     TrackList & trackList,
                                     TrackConts & tkC,
                                     PixelConts & pixC,
                                     PhotConts & photC ) const;

          // Get the active RICH flags
          inline DetectorArray<bool> getRICHFlags( MinTrList & minTracks ) const
          {
            // RICH flags. Default to flase
            DetectorArray<bool> inR = {{ false, false }};
            if ( !minTracks.empty() )
            {
              for ( const auto& T : minTracks )
              {
                // check if this track is in both RICHes
                for ( const auto rich : Rich::detectors() )
                { if ( T.first->richActive()[rich] ) { inR[rich] = true; } }
                // if both flags now set, stop the loop
                if ( inR[Rich::Rich1] && inR[Rich::Rich2] ) { break; }
              }
            } else { inR = { true, true }; }
            return inR;
          }

          /** Starting with all tracks with best hypotheses as set by
           *  initBestLogLikelihood(), for each track in turn get
           *  logLikelihood for each particle code, and return the track and
           *  particle code which gave the optimal log likelihood.
           *  @return The overall event LL change
           */
          FloatType findBestLogLikelihood( const DetectorArray<bool>& inR,
                                           MinTrList & minTracks,
                                           TrackList & trackList,
                                           TrackConts & tkC,
                                           PixelConts & pixC,
                                           PhotConts & photC ) const;
          
          /// Computes the change in the logLikelihood produced by changing given
          /// track to the given hypothesis
          FloatType deltaLogLikelihood( const Summary::Track & track,
                                        const Rich::ParticleIDType curHypo,
                                        const Rich::ParticleIDType newHypo,
                                        PixelConts & pixC,
                                        PhotConts & photC ) const;
          
          /// Update the best hypothesis for a given track
          void setBestHypo( const Summary::Track & track,
                            TrackPIDHypos& tkHypos,
                            const Rich::ParticleIDType newHypo,
                            PixelConts & pixC,
                            PhotConts & photC ) const;

          /// Print the current track list
          void printTrackList( const TrackList & trackList,
                               const TrackConts & tkC,
                               const MSG::Level level ) const;

          /// Print the track data
          void print( const TrackConts & tkC,
                      const MSG::Level level ) const;

          /// Print the pixel data
          void print( const PixelConts & pixC,
                      const MSG::Level level ) const;
          
        private: // data

          /// Cached value of log( exp(m_minSig) - 1 ) for efficiency
          FloatType m_logMinSig{0};
          
          /// Look up table for log(exp(x)-1) function
          LogExpLookUp<FloatType,10000> m_logExpLookUp;

          /// Minimum signal value for full calculation of log(exp(signal)-1)
          Gaudi::Property<FloatType> m_minSig
          { this, "MinSignalForNoLLCalc", 1e-3,
              "Minimum signal value for full calculation of log(exp(-signal)-1)" };

          /// Threshold for likelihood maximisation
          Gaudi::Property<FloatType> m_epsilon 
          { this, "LikelihoodThreshold", -1e-3,
              "Threshold for likelihood maximisation" };

          /// Maximum number of track iterations
          Gaudi::Property<unsigned int> m_maxEventIterations
          { this, "MaxEventIterations", 500u, 
              "Maximum number of track iterations" };

          /// Track DLL value to freeze track out from future iterations
          Gaudi::Property<FloatType> m_freezeOutDll
          { this, "TrackFreezeOutDLL", 4,
              "Track freeze out value (The point at which it is no longer considered for change)" };
          
          /// Track DLL value for a forced change
          Gaudi::Property<FloatType> m_forceChangeDll
          { this, "TrackForceChangeDLL", -2,
              "Track DLL Thresdhold for forced change" };

          /// Flag to turn on final DLL and hypothesis check
          Gaudi::Property<bool> m_doFinalDllCheck
          { this, "FinalDLLCheck", false,
              "Flag to turn on final DLL and hypothesis check" };
          
          /// Flag to turn on RICH check in LL minimisation
          Gaudi::Property<bool> m_richCheck
          { this, "RichDetCheck", true,
              "Flag to turn on RICH check in LL minimisation (turn off for max precision)" };

          /// Maximum number of tracks to change in a single event iteration
          Gaudi::Property<unsigned int> m_maxTkChanges
          { this, "MaxTrackChangesPerIt", 5u,
              "Maximum number of tracks to change in a single event iteration" };
          
          /// Maximum number of iteration retries
          Gaudi::Property<unsigned int> m_maxItRetries
          { this, "MaxIterationRetries", 10u, "Maximum retries" }; 
          
        };
        
        //=============================================================================

        inline LikelihoodMinimiser::FloatType  __attribute__((always_inline))
        LikelihoodMinimiser::deltaLogLikelihood( const Summary::Track & track,
                                                 const Rich::ParticleIDType curHypo,
                                                 const Rich::ParticleIDType newHypo,
                                                 PixelConts & pixC,
                                                 PhotConts & photC ) const
        {
          // Note because we no longer support Aerogel, its no longer possible to have
          // the same pixel associated to the same track more than once. 
          // This allows better optimisation of this method with 1 loop not 2 ;)
          
          // Change due to track expectation
          FloatType deltaLL = ( track.totalSignals()[newHypo] -
                                track.totalSignals()[curHypo] );
          
          // loop over the photons for this track
          for ( const auto & iPhot : track.photonIndices() )
          {
            // index for the pixel associated to this photon
            const auto & iPix = photC.photRel[iPhot].pixelIndex();
            
            // signals for this photon
            const auto & sigs = photC.photSignals[iPhot];
            
            // test signal for this pixel
            const auto pixTestSig = ( pixC.pixSignals[iPix] + sigs[newHypo] - sigs[curHypo] );
            
            // update the DLL
            deltaLL += ( pixC.pixCurrlogExp[iPix] -
                         sigFunc( pixC.pixelBkgs[iPix] + pixTestSig ) );
          }
          
          // return the DLL
          return deltaLL;
        }

        //=============================================================================
        
      }
    }
  }
}

