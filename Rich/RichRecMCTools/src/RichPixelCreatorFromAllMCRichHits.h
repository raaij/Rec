
//--------------------------------------------------------------------------------------
/** @file RichPixelCreatorFromAllMCRichHits.h
 *
 *  Header file for RICH reconstruction tool : Rich::Rec::PixelCreatorFromAllMCRichHits
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   08/07/2004
 */
//--------------------------------------------------------------------------------------

#ifndef RICHRECMCTOOLS_RichPixelCreatorFromAllMCRichHits_H
#define RICHRECMCTOOLS_RichPixelCreatorFromAllMCRichHits_H 1

// base class
#include "RichRecBase/RichPixelCreatorBase.h"

// interfaces
#include "MCInterfaces/IRichMCTruthTool.h"

// Event
#include "Event/MCRichOpticalPhoton.h"

namespace Rich
{
  namespace Rec
  {
    namespace MC
    {

      //--------------------------------------------------------------------------------------
      /** @class PixelCreatorFromAllMCRichHits RichPixelCreatorFromAllMCRichHits.h
       *
       *  Tool for the creation and book-keeping of RichRecPixel objects.
       *
       *  Creates a single RichRecPixel object for each MCRichHit in the event.
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   15/09/2003
       */
      //--------------------------------------------------------------------------------------

      class PixelCreatorFromAllMCRichHits final : public Rich::Rec::PixelCreatorBase
      {

      public: // methods for Gaudi framework

        /// Standard constructor
        PixelCreatorFromAllMCRichHits( const std::string& type,
                                       const std::string& name,
                                       const IInterface* parent );

        // Initialize method
        StatusCode initialize() override;

      protected: // methods

        /// Build a new RichRecPixel from an Rich::PDPixelCluster
        LHCb::RichRecPixel * buildPixel ( const Rich::PDPixelCluster& cluster ) const override;

        /// Build a new RichRecPixel from a single LHCb::RichSmartID
        LHCb::RichRecPixel * buildPixel( const LHCb::RichSmartID& id ) const override;

      private: // data

        /// MC Truth tool
        const Rich::MC::IMCTruthTool * m_mcTool = nullptr;

      };

    }
  }
}

#endif // RICHRECMCTOOLS_RichPixelCreatorFromAllMCRichHits_H
