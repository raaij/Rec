
//-----------------------------------------------------------------------------
/** @file RichRecMCTruthTool.cpp
 *
 *  Implementation file for RICH reconstruction tool : RichRecMCTruthTool
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   08/07/2004
 */
//-----------------------------------------------------------------------------

// local
#include "RichRecMCTruthTool.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Rec::MC;

//-----------------------------------------------------------------------------

DECLARE_TOOL_FACTORY( MCTruthTool )

// Standard constructor
MCTruthTool::MCTruthTool( const std::string& type,
                          const std::string& name,
                          const IInterface* parent )
  : ToolBase     ( type, name, parent ),
    m_trLoc      ( "/Event/"+LHCb::TrackLocation::Default ),
    m_otherTrLoc ( "" )
{
  // interface
  declareInterface<Rich::Rec::MC::IMCTruthTool>(this);

  // Context specific track locations
  if ( contextContains("HLT") )
  {
    m_otherTrLoc = "/Event/"+LHCb::TrackLocation::Default;
  }

  // job options
  declareProperty( "TrackLocation", m_trLoc );
  declareProperty( "OtherTrackLocation", m_otherTrLoc );
}

// initalization
StatusCode MCTruthTool::initialize()
{
  // Sets up various tools and services
  const StatusCode sc = ToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  // Acquire instances of tools
  acquireTool( "RichMCTruthTool", m_truth, nullptr, true );

  // Setup incident services
  incSvc()->addListener( this, IncidentType::BeginEvent );

  return sc;
}

// Method that handles various Gaudi "software events"
void MCTruthTool::handle ( const Incident& /* incident */ )
{
  // Only one Incident type, so skip type check
  //if ( IncidentType::BeginEvent == incident.type() ) { InitNewEvent(); }
  InitNewEvent();
}

MCTruthTool::TrackToMCP *
MCTruthTool::trackToMCPLinks( const LHCb::Track * track ) const
{
  if ( !track ) return nullptr;

  // TES location for the container for this track
  const auto & loc = ( track->parent() ? objectLocation(track->parent()) : m_trLoc );
  _ri_verbo << "trackToMCPLinks for Track " << track->key()
            << " at TES '" << loc << "'" << endmsg;

  // See if linker has already been loaded for this TES location
  const auto iLinks = m_trToMCPLinks.find(loc);
  if ( m_trToMCPLinks.end() == iLinks )
  {
    _ri_verbo << " -> failed to find linker for this location. Attempting to load one" << endmsg;

    // try and load a new linker for this location
    auto * linker = trackToMCPLinks(loc);
    if ( !linker && loc != m_trLoc )
    {
      _ri_verbo << "  -> No linker available for Track TES container. Returning default at "
                << m_trLoc << endmsg;
      linker = trackToMCPLinks(m_trLoc);
    }

    // return this new linker
    return linker;
  }

  // return found linker
  return iLinks->second.get();
}

MCTruthTool::TrackToMCP *
MCTruthTool::trackToMCPLinks( const std::string & loc ) const
{
  // See if linker has already been loaded for this TES location
  const auto iLinks = m_trToMCPLinks.find(loc);
  if ( m_trToMCPLinks.end() == iLinks )
  {
    _ri_verbo << " -> Attempting to load TrackToMCP Linker for '" << loc << "'" << endmsg;

    auto * linker = new TrackToMCP( evtSvc(), loc );
    if ( !linker->direct() )
    {
      _ri_verbo << "  -> Linker for Tracks to MCParticles not found" << endmsg;
      // delete object and set to nullptr, to force retrying next time this method is called.
      delete linker;
      linker = nullptr;
    }
    else
    {
      _ri_verbo << "  -> Found " << linker->direct()->relations().size()
                << " Track to MCParticle associations" << endmsg;
      // Save this linker in the map
      m_trToMCPLinks[loc].reset( linker );
    }

    // return this new linker
    return linker;
  }

  // return found linker
  return iLinks->second.get();
}

MCTruthTool::MCRichSegToMCCKRing *
MCTruthTool::mcSegToRingLinks() const
{
  if ( !m_mcSegToRingLinks.get() )
  {
    const auto & loc = LHCb::MCRichSegmentLocation::Default+"2MCCKRings";
    _ri_debug << "Attempting to load MCRichSegToMCCKRing Linker for '"
              << loc << "'" << endmsg;
    m_mcSegToRingLinks.reset( new MCRichSegToMCCKRing( evtSvc(), loc ) );
    if ( !m_mcSegToRingLinks->direct() )
    {
      _ri_debug << "Linker for MCRichTracks to MCCKRings not found for '"
                << loc << "'" << endmsg;
      // delete object and set to nullptr, to force retrying next time this method is called.
      m_mcSegToRingLinks.reset( nullptr );
    }
    else if ( msgLevel(MSG::DEBUG) )
    {
      debug() << "Found " << m_mcSegToRingLinks->direct()->relations().size()
              << " Track to MCRichTrack->MCCKRings associations" << endmsg;
    }
  }
  return m_mcSegToRingLinks.get();
}

const LHCb::MCParticle *
MCTruthTool::mcParticle( const LHCb::Track * track,
                         const double minWeight ) const
{
  // Try with linkers
  auto * linker = trackToMCPLinks(track);
  if ( linker && linker->direct() )
  {
    auto range = linker->direct()->relations(track);
    _ri_debug << "Found " << range.size() << " association(s) for Track " << track->key()
              << " " << track->type() << " " << track->history() << endmsg;
    const LHCb::MCParticle * mcp = nullptr;
    double bestWeight = -1;
    for ( auto iMC = range.begin(); iMC != range.end(); ++iMC )
    {
      if ( iMC->to() )
      {
        _ri_debug << " -> Found MCParticle key=" << iMC->to()->key()
                  << " weight=" << iMC->weight() << endmsg;
        if ( iMC->weight() > minWeight && iMC->weight() > bestWeight )
        {
          bestWeight = iMC->weight();
          mcp = iMC->to();
        }
      }
    }
    return mcp;
  }
  // If get here MC association failed
  _ri_debug << "No MC association available for Tracks" << endmsg;
  return nullptr;
}

const LHCb::MCParticle *
MCTruthTool::mcParticle( const LHCb::RichRecTrack * richTrack,
                         const double minWeight ) const
{
  if ( !richTrack ) return nullptr;
  const auto * obj = richTrack->parentTrack();
  if ( !obj )
  {
    _ri_debug << "RichRecTrack " << richTrack->key()
              <<  " has nullptr pointer to parent" << endmsg;
    return nullptr;
  }

  // What sort of track is this ...

  if ( const auto * offTrack = dynamic_cast<const LHCb::Track*>(obj) )
  {
    // Track
    _ri_debug << "RichRecTrack " << richTrack->key()
              << " has parent track Track " << offTrack->key() << endmsg;
    return mcParticle( offTrack, minWeight );
  }
  else if ( const auto * mcPart = dynamic_cast<const LHCb::MCParticle*>(obj) )
  {
    // MCParticle
    _ri_debug << "RichRecTrack " << richTrack->key()
              << " has parent track MCParticle " << mcPart->key() << endmsg;
    return mcPart;
  }
  else
  {
    Warning( "Unknown RichRecTrack parent track type" );
    return nullptr;
  }

}

void
MCTruthTool::mcRichHits( const LHCb::RichRecPixel * richPixel,
                         SmartRefVector<LHCb::MCRichHit> & hits ) const
{
  m_truth->mcRichHits( richPixel->pdPixelCluster(), hits );
}

bool
MCTruthTool::mcParticle( const LHCb::RichRecPixel * richPixel,
                         std::vector<const LHCb::MCParticle*> & mcParts ) const
{
  return m_truth->mcParticles( richPixel->pdPixelCluster(), mcParts );
}

const LHCb::MCParticle *
MCTruthTool::trueRecPhoton( const LHCb::RichRecPhoton * photon ) const
{
  return ( !photon ? nullptr :
           trueRecPhoton( photon->richRecSegment(), photon->richRecPixel() ) );
}

const LHCb::MCParticle *
MCTruthTool::trueRecPhoton( const LHCb::RichRecSegment * segment,
                            const LHCb::RichRecPixel * pixel ) const
{
  // protect against bad input data
  if ( !segment || !pixel ) { return nullptr; }

  const auto * track = segment->richRecTrack();
  const auto * mcTrack = ( track ? mcParticle(track) : nullptr );
  if ( !mcTrack ) return nullptr;

  return trueRecPhoton( mcTrack, pixel->pdPixelCluster() );
}

const LHCb::MCParticle *
MCTruthTool::trueRecPhoton( const LHCb::MCParticle * mcPart,
                            const Rich::PDPixelCluster& cluster ) const
{
  for ( const auto S : cluster.smartIDs() )
  {
    const auto * mcP = trueRecPhoton( mcPart, S );
    if ( nullptr != mcP ) return mcP;
  }
  return nullptr;
}

const LHCb::MCParticle *
MCTruthTool::trueRecPhoton( const LHCb::MCParticle * mcPart,
                            const LHCb::RichSmartID id ) const
{
  // Get MCParticles for the pixel
  std::vector<const LHCb::MCParticle *> mcParts;
  m_truth->mcParticles(id,mcParts);

  // Loop over all MCParticles associated to the pixel
  for ( const auto * MCP : mcParts )
  {
    if ( mcPart == MCP ) return mcPart;
  }

  return nullptr;
}

const LHCb::MCParticle *
MCTruthTool::trueCherenkovPhoton( const LHCb::RichRecPhoton * photon ) const
{
  return ( !photon ? nullptr :
           trueCherenkovPhoton( photon->richRecSegment(), photon->richRecPixel() ) );
}

const LHCb::MCParticle *
MCTruthTool::trueCherenkovPhoton( const LHCb::RichRecSegment * segment,
                                  const LHCb::RichRecPixel * pixel ) const
{
  if ( !segment || !pixel ) return nullptr;
  _ri_debug << "Testing RichRecSegment " << segment->key()
            << " and RichRecPixel " << pixel->key() << endmsg;

  const auto * mcPart = trueRecPhoton( segment, pixel );
  return ( !mcPart ? nullptr :
           trueCherenkovRadiation( pixel, segment->trackSegment().radiator() ) );
}

const LHCb::MCParticle *
MCTruthTool::trueCherenkovPhoton( const LHCb::MCParticle * mcPart,
                                  const LHCb::RichSmartID id,
                                  const Rich::RadiatorType rad ) const
{
  const auto * mcPart2 = trueRecPhoton( mcPart, id );
  return ( !mcPart2 ? nullptr : trueCherenkovRadiation( id, rad ) );
}

const LHCb::MCParticle *
MCTruthTool::trueCherenkovRadiation( const LHCb::RichRecPixel * pixel,
                                     const Rich::RadiatorType rad ) const
{
  return trueCherenkovRadiation( pixel->pdPixelCluster(), rad );
}

const LHCb::MCParticle *
MCTruthTool::trueCherenkovRadiation( const Rich::PDPixelCluster& cluster,
                                     const Rich::RadiatorType rad ) const
{
  for ( const auto S : cluster.smartIDs() )
  {
    const auto * mcP = trueCherenkovRadiation(S,rad);
    if ( nullptr != mcP ) return mcP;
  }
  return nullptr;
}

const LHCb::MCParticle *
MCTruthTool::trueCherenkovRadiation( const LHCb::RichSmartID id,
                                     const Rich::RadiatorType rad ) const
{
  // Test if hit is background
  if ( m_truth->isBackground(id) ) return nullptr;

  // Test if hit is from correct radiator
  if ( !m_truth->isCherenkovRadiation(id,rad) ) return nullptr;

  // All OK so find correct MCParticle
  std::vector<const LHCb::MCParticle *> mcParts;
  m_truth->mcParticles(id,mcParts);

  // Should do something better than just return first, but what ??
  return ( mcParts.empty() ? nullptr : mcParts.front() );
}

Rich::ParticleIDType
MCTruthTool::mcParticleType( const LHCb::Track * track,
                             const double minWeight ) const
{
  return m_truth->mcParticleType( mcParticle(track,minWeight) );
}

Rich::ParticleIDType
MCTruthTool::mcParticleType( const LHCb::RichRecTrack * richTrack,
                             const double minWeight ) const
{
  return m_truth->mcParticleType( mcParticle(richTrack,minWeight) );
}

Rich::ParticleIDType
MCTruthTool::mcParticleType( const LHCb::RichRecSegment * richSegment,
                             const double minWeight ) const
{
  return ( richSegment ?
           mcParticleType(richSegment->richRecTrack(),minWeight) :
           Rich::Unknown );
}

const LHCb::MCParticle *
MCTruthTool::mcParticle( const LHCb::RichRecSegment * richSegment,
                         const double minWeight ) const
{
  return ( richSegment ?
           mcParticle( richSegment->richRecTrack(), minWeight ) :
           nullptr );
}

bool
MCTruthTool::mcRichOpticalPhoton( const LHCb::RichRecPixel * richPixel,
                                  SmartRefVector<LHCb::MCRichOpticalPhoton> & phots ) const
{
  phots.clear();
  SmartRefVector<LHCb::MCRichHit> hits;
  mcRichHits(richPixel,hits);
  for ( auto iHit = hits.begin(); iHit != hits.end(); ++iHit )
  {
    // protect against bad hits
    if ( !(*iHit) ) continue;

    // Find MC photon
    const auto * phot = m_truth->mcOpticalPhoton( *iHit );
    if ( phot ) phots.push_back( phot );
  }

  // Return boolean indicating if any photons were found
  return !phots.empty();
}

const LHCb::MCRichSegment *
MCTruthTool::mcRichSegment( const LHCb::RichRecSegment * segment ) const
{
  if ( !segment ) return nullptr;
  const auto * mcTrack = mcRichTrack( segment );
  return ( mcTrack ?
           mcTrack->segmentInRad(segment->trackSegment().radiator()) : nullptr );
}

const SmartRefVector<LHCb::MCRichSegment> *
MCTruthTool::mcRichSegments( const LHCb::RichRecTrack * track ) const
{
  const auto * mcTrack = mcRichTrack( track );
  return ( mcTrack ? &(mcTrack->mcSegments()) : nullptr );
}

const LHCb::MCRichTrack *
MCTruthTool::mcRichTrack( const LHCb::RichRecSegment * segment ) const
{
  return ( segment ? mcRichTrack( segment->richRecTrack() ) : nullptr );
}

const LHCb::MCRichTrack *
MCTruthTool::mcRichTrack( const LHCb::RichRecTrack * track ) const
{
  return m_truth->mcRichTrack( mcParticle(track) );
}

bool
MCTruthTool::isBackground( const LHCb::RichRecPixel * pixel ) const
{
  return m_truth->isBackground( pixel->pdPixelCluster() );
}

bool
MCTruthTool::isRadScintillation( const LHCb::RichRecPixel * pixel ) const
{
  return m_truth->isRadScintillation( pixel->pdPixelCluster() );
}

// CRJ : Remove use of MCRichHits ??
const LHCb::MCRichHit *
MCTruthTool::trueCherenkovHit( const LHCb::RichRecPhoton * photon ) const
{
  // Track MCParticle
  const auto * trackMCP = mcParticle( photon->richRecSegment() );
  if ( trackMCP )
  {
    // Loop over all MCRichHits for the pixel associated to this photon
    SmartRefVector<LHCb::MCRichHit> hits;
    mcRichHits( photon->richRecPixel(), hits );
    for ( auto iHit = hits.begin(); iHit != hits.end(); ++iHit )
    {
      if ( !(*iHit) ) continue;
      const auto * pixelMCP = (*iHit)->mcParticle();
      if ( pixelMCP == trackMCP ) return *iHit;
    }

  }

  // Not a true combination...
  return nullptr;
}

const LHCb::MCRichOpticalPhoton *
MCTruthTool::trueOpticalPhoton( const LHCb::RichRecPhoton * photon ) const
{
  return ( !photon ? nullptr :
           trueOpticalPhoton( photon->richRecSegment(), photon->richRecPixel() ) );
}

const LHCb::MCRichOpticalPhoton *
MCTruthTool::trueOpticalPhoton( const LHCb::RichRecSegment * segment,
                                const LHCb::RichRecPixel * pixel ) const
{
  // local container of photons
  std::vector<const LHCb::MCRichOpticalPhoton *> photons;

  // Is this a true cherenkov combination
  const auto * mcPart = trueCherenkovPhoton(segment,pixel);
  if ( mcPart )
  {
    _ri_verbo << "Finding MCRichOpticalPhotons for pixel " << pixel->pdPixelCluster() << endmsg;

    // Now find associated MCRichOpticalPhoton
    const SmartRefVector<LHCb::MCRichHit> & hits = m_truth->mcRichHits(mcPart);
    _ri_verbo << "Found " << hits.size() << " MCRichHits" << endmsg;
    for ( auto iHit = hits.begin(); iHit != hits.end(); ++iHit )
    {
      if ( !(*iHit) ) continue;
      _ri_verbo << " -> Found mc hit in " << (*iHit)->radiator() << endmsg;
      // compare mc hit id to pixel id(s)
      for ( const auto S : pixel->pdPixelCluster().smartIDs() )
      {
        const LHCb::RichSmartID hitid ( S.pixelSubRowDataIsValid() ?
                                        (*iHit)->sensDetID() :
                                        (*iHit)->sensDetID().pixelID() );
        _ri_verbo << "  -> " << (*iHit)->radiator() << " MCRichHit " << hitid << endmsg;
        if ( S == hitid )
        {
          // check radiator type
          if ( segment->trackSegment().radiator() == (*iHit)->radiator() )
          {
            _ri_verbo << "   -> hit selected" << endmsg;
            photons.push_back( m_truth->mcOpticalPhoton(*iHit) );
          }
        } // same as hit ID
      } // loop over cluster smart IDs
    } // loop over hits
  }

  // did we find more than one photon ?
  // this is not handled well at the moment
  if ( photons.size() >= 2 )
  {
    Warning( "Found more than one associated MCRichOpticalPhoton. This is not handled well at present -> Returning first",
             StatusCode::SUCCESS, 1 ).ignore();
  }

  return ( photons.empty() ? nullptr : photons.front() );
}

const LHCb::RichRecRing *
MCTruthTool::mcCKRing( const LHCb::RichRecSegment * segment ) const
{
  const auto * mcSeg = mcRichSegment(segment);
  if ( !mcSeg ) return nullptr;

  if ( mcSegToRingLinks() )
  {
    auto range = mcSegToRingLinks()->direct()->relations(mcSeg);
    _ri_debug << "Found " << range.size() << " MCCKRing association(s) for MCRichTrack "
              << mcSeg->key() << endmsg;
    const LHCb::RichRecRing * ring(nullptr);
    double bestWeight = -999999;
    for ( auto iMC = range.begin(); iMC != range.end(); ++iMC )
    {
      if ( iMC->to() )
      {
        _ri_debug << " Found key=" << iMC->to()->key() << " weight=" << iMC->weight() << endmsg;
        if ( iMC->weight() > bestWeight )
        {
          bestWeight = iMC->weight();
          ring = iMC->to();
        }
      }
    }
    return ring;
  }
  // If get here MC association failed
  _ri_debug << "No MC association available for MCRichSegment -> MCCKRings" << endmsg;
  return nullptr;
}

// Access the MCParticle associated to a given RichRecRing
IMCTruthTool::MCPartAssocInfo
MCTruthTool::mcParticle( const LHCb::RichRecRing * ring,
                         const double assocFrac ) const
{
  IMCTruthTool::MCPartAssocInfo info;
  if ( ring )
  {
    if ( ring->type() == LHCb::RichRecRing::RayTracedCK )
    {
      // This ring is ray traced from a track segment, so use the MC association for that
      info.mcParticle = mcParticle( ring->richRecSegment(), assocFrac );
    }
    else if ( ring->type() == LHCb::RichRecRing::TracklessRing )
    {
      // trackless ring.

      // See what the pixels have to say
      typedef Rich::Map<const LHCb::MCParticle *, unsigned int> MCPMap;
      MCPMap mcpMap;
      unsigned int totMCPs(0);
      for ( const auto & pix : ring->richRecPixels() )
      {
        // get the MCPs for this pixel
        std::vector<const LHCb::MCParticle*> mcParts;
        if ( mcParticle( pix.pixel() , mcParts ) )
        {
          for ( const auto * mcp : mcParts )
          {
            ++totMCPs;
            ++mcpMap[mcp];
          }
        }
      } // loop over pixels

      const LHCb::MCParticle * pixel_mcP = nullptr;
      double best_mcAssoc(0);
      if ( totMCPs > 0 )
      {
        for ( auto iMCP = mcpMap.begin(); iMCP != mcpMap.end(); ++iMCP )
        {
          const double assoc = (double)(iMCP->second) / (double)totMCPs;
          if ( assoc > assocFrac && assoc > best_mcAssoc )
          {
            pixel_mcP = iMCP->first;
            best_mcAssoc = assoc;
          }
        }
      }

      // It might still have an associated segment via secondary matching
      // so lets see what track that gives us
      //const LHCb::MCParticle * track_mcP = mcParticle( ring->richRecSegment() );

      // decide best association
      info.mcParticle      = pixel_mcP;
      info.associationFrac = best_mcAssoc;

    } // trackless ring type

  } // ring OK

  // return final association
  return info;
}

bool MCTruthTool::trackToMCPAvailable() const
{
  auto * linker = trackToMCPLinks(m_trLoc);
  if ( linker && linker->direct() ) return true;
  linker = trackToMCPLinks(m_otherTrLoc);
  return ( linker && linker->direct() );
}

bool MCTruthTool::pixelMCHistoryAvailable() const
{
  return m_truth->richMCHistoryAvailable();
}

bool MCTruthTool::photonMCAssocAvailable() const
{
  return ( trackToMCPAvailable() && pixelMCHistoryAvailable() );
}

bool MCTruthTool::extendedMCAvailable() const
{
  return m_truth->extendedMCAvailable();
}
