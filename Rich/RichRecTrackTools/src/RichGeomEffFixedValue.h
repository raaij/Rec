
//-----------------------------------------------------------------------------
/** @file RichGeomEffFixedValue.h
 *
 *  Header file for tool : Rich::Rec::GeomEffFixedValue
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECTOOLS_RICHGEOMEFFFIXEDVALUE_H
#define RICHRECTOOLS_RICHGEOMEFFFIXEDVALUE_H 1

// STL
#include <array>

// base class
#include "RichRecBase/RichRecToolBase.h"

// Event model
#include "Event/RichRecSegment.h"

// interfaces
#include "RichRecInterfaces/IRichCherenkovAngle.h"
#include "RichRecInterfaces/IRichGeomEff.h"
#include "RichRecInterfaces/IRichRecGeomTool.h"
#include "RichInterfaces/IRichParticleProperties.h"

namespace Rich
{
  namespace Rec
  {

    //-----------------------------------------------------------------------------
    /** @class GeomEffFixedValue RichGeomEffFixedValue.h
     *
     *  Tool to perform a fast determination of the geometrical efficiency for
     *  a given RichRecSegment and mass hypothesis.
     *
     *  Uses a fixed value of the signal and scattered efficiencies on the HPD panel,
     *  and also takes into account the HPD panel boundaries.
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   15/03/2002
     */
    //-----------------------------------------------------------------------------

    class GeomEffFixedValue final : public Rich::Rec::ToolBase,
                                    virtual public IGeomEff
    {

    public: // Methods for Gaudi Framework

      /// Standard constructor
      GeomEffFixedValue( const std::string& type,
                         const std::string& name,
                         const IInterface* parent );

      /// Initialize method
      StatusCode initialize() override;

    public: // methods (and doxygen comments) inherited from public interface

      // Obtain geometrical efficiency for this track and hypothesis
      double geomEfficiency ( LHCb::RichRecSegment * segment,
                              const Rich::ParticleIDType id ) const override;

      // Obtain scattered geometrical efficiency for this track and hypothesis
      double geomEfficiencyScat ( LHCb::RichRecSegment * segment,
                                  const Rich::ParticleIDType id ) const override;

    private: // Private data

      // Pointers to tool instances
      const ICherenkovAngle * m_ckAngle = nullptr;   ///< Cherenkov angle tool
      const IGeomTool * m_geomTool = nullptr;     ///< Pointer to the Geometry tool
      /// Pointer to RichParticleProperties interface
      const IParticleProperties * m_richPartProp = nullptr;

      /// vector of fixed radiator geometrical efficiencies
      RadiatorArray<double> m_fixedValue = {{}};
      double m_fixedScatValue; ///< fixed radiator geometrical scatter efficiency

      /// Flag to turn on checking of HPD panel boundaries
      bool m_checkBoundaries;

      /// Flag to turn on/off checking of which regions photons can be in
      bool m_checkPhotonRegions;

      /// Particle ID types to consider in the photon creation checks
      Rich::Particles m_pidTypes;

    };

  }
}

#endif // RICHRECTOOLS_RICHGEOMEFFFIXEDVALUE_H
