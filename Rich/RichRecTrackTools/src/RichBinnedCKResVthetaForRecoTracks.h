
//-----------------------------------------------------------------------------
/** @file RichBinnedCKResVthetaForRecoTracks.h
 *
 *  Header file for tool : Rich::Rec::BinnedCKResVthetaForRecoTracks
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

#ifndef RICHRECTOOLS_RICHBINNEDCKRESVTHETAFORTRSTOREDTRACKS_H
#define RICHRECTOOLS_RICHBINNEDCKRESVTHETAFORTRSTOREDTRACKS_H 1

// Array properties
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// STL
#include <vector>
#include <array>

// base class
#include "RichRecBase/RichRecToolBase.h"

// Event model
#include "Event/RichRecSegment.h"

// interfaces
#include "RichRecInterfaces/IRichCherenkovResolution.h"
#include "RichRecInterfaces/IRichCherenkovAngle.h"
#include "RichInterfaces/IRichParticleProperties.h"

namespace Rich
{
  namespace Rec
  {

    //-----------------------------------------------------------------------------
    /** @class BinnedCKResVthetaForRecoTracks RichBinnedCKResVthetaForRecoTracks.h
     *
     *  Tool to calculate the Cherenkov angle resolution. This version is
     *  for reconstructed Tracks and implements an approach that uses simple bins in the
     *  cherenkov angle theta space and also varies according to the track algorithm type.
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   15/03/2002
     */
    //-----------------------------------------------------------------------------

    class BinnedCKResVthetaForRecoTracks final : public Rich::Rec::ToolBase,
                                                 virtual public ICherenkovResolution
    {

    public: // Methods for Gaudi Framework

      /// Standard constructor
      BinnedCKResVthetaForRecoTracks( const std::string& type,
                                      const std::string& name,
                                      const IInterface* parent );

      // Initialize method
      StatusCode initialize() override;

    public: // methods (and doxygen comments) inherited from public interface

      // Photon resolution
      double ckThetaResolution( LHCb::RichRecSegment * segment,
                                const Rich::ParticleIDType id = Rich::Pion ) const override;

    private: // methods

      // Photon resolution
      bool ckThetaResolution_Imp( LHCb::RichRecSegment * segment,
                                  const Rich::ParticleIDType id,
                                  double & res ) const;

    private: // definitions

      using BinData    = std::vector<double>;
      using BinDataMap = Track::TrackTypeArray<BinData>;
      using BinEdge    = std::pair<double,double>;
      using BinEdges   = std::vector<BinEdge>;

    private: // Private data

      /// Pointer to RichCherenkovAngle interface
      const ICherenkovAngle * m_ckAngle = nullptr;

      /// Pointer to RichParticleProperties interface
      const IParticleProperties * m_richPartProp = nullptr;

      /// The averged resolutions in each bin, for each track type
      mutable RadiatorArray<BinDataMap> m_theerr;

      /// The boundaries for the resolution bins
      RadiatorArray<BinEdges> m_binEdges = {{}};

      /// make sure all hypo resolutions are the same
      bool m_normalise;

      /// Particle ID types to consider in the photon creation checks
      Rich::Particles m_pidTypes;

      /// Overall factors for each radiator
      RadiatorArray<double> m_scale = {{}};

    };

  }
}

#endif // RICHRECTOOLS_RICHBINNEDCKRESVTHETAFORTRSTOREDTRACKS_H
