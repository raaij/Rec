
//---------------------------------------------------------------------------------
/** @file RichFlippedDetailedTrSegMakerFromRecoTracks.h
 *
 *  Header file for tool : Rich::Rec::FlippedDetailedTrSegMakerFromRecoTracks
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   06/07/2016
 */
//---------------------------------------------------------------------------------

#ifndef RICHRECTRACKTOOLS_RichFlippedDetailedTrSegMakerFromRecoTracks_H
#define RICHRECTRACKTOOLS_RichFlippedDetailedTrSegMakerFromRecoTracks_H 1

// base class
#include "RichDetailedTrSegMakerFromRecoTracks.h"

namespace Rich
{
  namespace Rec
  {

    //---------------------------------------------------------------------------------
    /** @class FlippedDetailedTrSegMakerFromRecoTracks RichFlippedDetailedTrSegMakerFromRecoTracks.h
     *
     *  Tool to create RichTrackSegments from Tracks.
     *
     *  Takes the results from RichDetailedTrSegMakerFromRecoTracks and 'flips' the track
     *  information in X and Y. Useful for background studies.
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   06/07/2016
     */
    //---------------------------------------------------------------------------------

    class FlippedDetailedTrSegMakerFromRecoTracks final : public DetailedTrSegMakerFromRecoTracks
    {

    public: // Methods for Gaudi Framework

      /// Standard Constructor
      FlippedDetailedTrSegMakerFromRecoTracks( const std::string& type,
                                               const std::string& name,
                                               const IInterface* parent );

      ///  initialize
      StatusCode initialize() override;

    public: // methods (and doxygen comments) inherited from interface

      // Create RichTrackSegments for a given tracking object
      int
      constructSegments( const ContainedObject* track,
                 std::vector<LHCb::RichTrackSegment*>& segments ) const override;

    private: // methods

      /// Flip a given point or vector object
      template< typename TYPE >
      inline TYPE flip( const TYPE & v ) const
      {
        // Flip the X and Y coordinates
        return TYPE( -v.X(), -v.Y(), v.Z() );
      }

    };

  }
}

#endif // RICHRECTRACKTOOLS_RichFlippedDetailedTrSegMakerFromRecoTracks_H
