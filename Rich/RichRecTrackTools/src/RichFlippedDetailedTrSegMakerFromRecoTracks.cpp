
//---------------------------------------------------------------------------------
/** @file RichFlippedDetailedTrSegMakerFromRecoTracks.cpp
 *
 * Implementation file for class : Rich::Rec::FlippedDetailedTrSegMakerFromRecoTracks
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 06/07/2016
 */
//---------------------------------------------------------------------------------

// local
#include "RichFlippedDetailedTrSegMakerFromRecoTracks.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Rec;

//===============================================================================
// Standard constructor, initializes variables
//===============================================================================
FlippedDetailedTrSegMakerFromRecoTracks::
FlippedDetailedTrSegMakerFromRecoTracks( const std::string& type,
                                         const std::string& name,
                                         const IInterface* parent )
  : DetailedTrSegMakerFromRecoTracks ( type, name, parent ) { }

//=============================================================================
// Initialisation.
//=============================================================================
StatusCode FlippedDetailedTrSegMakerFromRecoTracks::initialize()
{
  // Sets up various tools and services
  const auto sc = DetailedTrSegMakerFromRecoTracks::initialize();
  // return with a warning
  return Warning( "Flipping track state information in X and Y", sc );
}

//=============================================================================
// Constructs the track segments for a given Track
//=============================================================================
int
FlippedDetailedTrSegMakerFromRecoTracks::
constructSegments( const ContainedObject * obj,
                   std::vector<LHCb::RichTrackSegment*>& segments ) const
{
  // Execute the base class method, to create the segments
  DetailedTrSegMakerFromRecoTracks::constructSegments(obj,segments);

  // flip the state information in each segment created
  for ( auto * tkSeg : segments )
  {
    tkSeg->setEntryState ( flip(tkSeg->entryPoint()),
                           flip(tkSeg->entryMomentum())  );
    tkSeg->setMiddleState( flip(tkSeg->middlePoint()),
                           flip(tkSeg->middleMomentum()) );
    tkSeg->setExitState  ( flip(tkSeg->exitPoint()),
                           flip(tkSeg->exitMomentum())   );
  }

  // return value is number of segments formed
  return segments.size();
}

//=============================================================================

DECLARE_TOOL_FACTORY( FlippedDetailedTrSegMakerFromRecoTracks )

//=============================================================================
