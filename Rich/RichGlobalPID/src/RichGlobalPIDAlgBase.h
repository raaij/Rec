
//--------------------------------------------------------------------------
/** @file RichGlobalPIDAlgBase.h
 *
 *  Header file for RICH global PID algorithm base class : Rich::Rec::GlobalPID::AlgBase
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2002-11-30
 */
//--------------------------------------------------------------------------

#ifndef RICHGLOBALPID_RichGlobalPIDAlgBase_H
#define RICHGLOBALPID_RichGlobalPIDAlgBase_H 1

// base class
#include "RichRecBase/RichRecHistoAlgBase.h"
#include "RichGlobalPIDCommonBase.h"

#ifdef GAUDI_SYSEXECUTE_WITHCONTEXT
/// \fixme backward compatibility with Gaudi <= v28r1
#include "GaudiKernel/EventContext.h"
#endif

namespace Rich
{
  namespace Rec
  {
    namespace GlobalPID
    {

      //--------------------------------------------------------------------------
      /** @class AlgBase RichGlobalPIDAlgBase.h
       *
       *  Abstract base class for GlobalPID algorithms
       *
       *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
       *  @date   2002-11-30
       */
      //--------------------------------------------------------------------------

      class AlgBase : public CommonBase<Rich::Rec::HistoAlgBase>
      {

      public:

        ///< Standard constructor
        AlgBase( const std::string& name,
                 ISvcLocator* pSvcLocator )
          : CommonBase<Rich::Rec::HistoAlgBase>( name, pSvcLocator ) { }

        /// \fixme backward compatibility with Gaudi <= v28r1
        #ifdef GAUDI_SYSEXECUTE_WITHCONTEXT
        StatusCode sysExecute(const EventContext&) override; ///< system execute
        #else
        StatusCode sysExecute() override; ///< system execute
        #endif

      };

    }
  }
}

#endif // RICHGLOBALPID_RichGlobalPIDAlgBase_H
