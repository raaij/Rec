
//----------------------------------------------------------------------
/** @file QuarticSolverNewton.h
 *
 *  @author Christina Quast, Rainer Schwemmer
 *  @date   2017-02-03
 */
//----------------------------------------------------------------------

#pragma once

// Gaudi
#include "GaudiKernel/Kernel.h"

// VectorClass
#include "VectorClass/vectorclass.h"

// STL
#include <math.h>
#include <type_traits>
#include <array>

namespace Rich
{
  namespace Rec
  {

    //-----------------------------------------------------------------------------
    /** @class QuarticSolverNewton
     *
     *  Utility class that implements the solving of the Quartic equation for the RICH
     *  Based on original code by Chris Jones
     *
     *  @author Christina Quast, Rainer Schwemmer
     *  @date   2017-02-03
     */
    //-----------------------------------------------------------------------------
    class QuarticSolverNewton
    {

    public:

      /** Solves the characteristic quartic equation for the RICH optical system.
       *
       *  See note LHCB/98-040 RICH section 3 for more details
       *
       *  @param emissionPoint Assumed photon emission point on track
       *  @param CoC           Spherical mirror centre of curvature
       *  @param virtDetPoint  Virtual detection point
       *  @param radius        Spherical mirror radius of curvature
       *  @param sphReflPoint  The reconstructed reflection pont on the spherical mirror
       *
       *  @return boolean indicating status of the quartic solution
       *  @retval true  Calculation was successful. sphReflPoint is valid.
       *  @retval false Calculation failed. sphReflPoint is not valid.
       */
      template< class TYPE,
                std::size_t BISECTITS = 3, std::size_t NEWTONITS = 4,
                class POINT = Gaudi::XYZPoint >
      inline void __attribute__((always_inline)) solve( const POINT& emissionPoint,
                                                        const POINT& CoC,
                                                        const POINT& virtDetPoint,
                                                        const TYPE radius,
                                                        POINT& sphReflPoint ) const
      {
        using namespace std;

        // vector from mirror centre of curvature to assumed emission point
        const auto evec( emissionPoint - CoC );
        const TYPE e2 = evec.Dot(evec);

        // vector from mirror centre of curvature to virtual detection point
        const auto dvec( virtDetPoint - CoC );
        const TYPE d2 = dvec.Dot(dvec);

        // various quantities needed to create quartic equation
        // see LHCB/98-040 section 3, equation 3
        const TYPE       ed2 = e2 * d2;
        const TYPE cosgamma2 = ( ed2 > TYPE(0.0) ? pow(evec.Dot(dvec),2)/ed2 : TYPE(1.0) );
        // vectorise 4 square roots into 1
        using Vec4x = typename std::conditional<std::is_same<TYPE,float>::value,Vec4f,Vec4d>::type;
        const auto tmp_sqrt = sqrt( Vec4x( e2, d2,
                                           cosgamma2 < TYPE(1.0) ? TYPE(1.0)-cosgamma2 : TYPE(0.0),
                                           cosgamma2 ) );
        const TYPE e         = tmp_sqrt[0];
        const TYPE d         = tmp_sqrt[1];
        const TYPE singamma  = tmp_sqrt[2];
        const TYPE cosgamma  = tmp_sqrt[3];
        // scalar versions
        //const TYPE e         = std::sqrt(e2);
        //const TYPE d         = std::sqrt(d2);
        //const TYPE singamma  = std::sqrt( cosgamma2 < TYPE(1.0) ? TYPE(1.0)-cosgamma2 : TYPE(0.0) );
        //const TYPE cosgamma  = std::sqrt(cosgamma2);

        const TYPE dx        = d * cosgamma;
        const TYPE dy        = d * singamma;
        const TYPE r2        = radius * radius;
        const TYPE dy2       = dy * dy;
        const TYPE edx       = e + dx;

        // Fill array for quartic equation
        //const TYPE a0        = TYPE(4.0) * ed2;
        //Newton solver doesn't care about a0 being not 1.0. Remove costly division and several multiplies.
        //This has some downsides though. The a-values are hovering around a numerical value of 10^15. single
        //precision float max is 10^37. A single square and some multiplies will push it over the limit of what
        //single precision float can handle. It's ok for the newton method, but Halley or higher order Housholder
        //will fail without this normalization.
        //const auto inv_a0  =   ( a0 > 0 ? 1.0 / a0 : std::numeric_limits<TYPE>::max() );
        const TYPE dyrad2  = TYPE(2.0) * dy * radius;
        const TYPE aa[5] = { TYPE(4.0) * ed2,
                             - ( TYPE(2.0) * dyrad2 * e2 ),
                             ( (dy2 * r2) + ( edx * edx * r2 ) - ( TYPE(4.0) * ed2 ) ),
                             ( dyrad2 * e * (e-dx) ),
                             ( ( e2 - r2 ) * dy2 ) };

        //Use optimized newton solver on quartic equation.
        const auto sinbeta = solve_quartic_newton_RICH<TYPE,BISECTITS,NEWTONITS>( aa );

        //TODO: This method should be better but has problems still for some reasons
        //const auto sinbeta = solve_quartic_housholder_RICH<TYPE, 3>(a1, a2, a3, a4);

        // construct rotation transformation
        // Set vector magnitude to radius
        // rotate vector and update reflection point
        //rotation matrix uses sin(beta) and cos(beta) to perform rotation
        //even fast_asinf (which is only single precision and defeats the purpose
        //of this class being templatable to double btw) is still too slow
        //plus there is a cos and sin call inside AngleAxis ...
        //We can do much better by just using the cos(beta) we already have to calculate
        //sin(beta) and do our own rotation. On top of that we rotate non-normalized and save several
        //Divisions by normalizing only once at the very end
        //Again, care has to be taken since we are close to float_max here without immediate normalization.
        //As far as we have tried with extreme values in the rich coordinate systems this is fine.
        const TYPE nx  = (evec.y()*dvec.z()) - (evec.z()*dvec.y());
        const TYPE ny  = (evec.z()*dvec.x()) - (evec.x()*dvec.z());
        const TYPE nz  = (evec.x()*dvec.y()) - (evec.y()*dvec.x());

        const TYPE nx2 = nx*nx; 
        const TYPE ny2 = ny*ny; 
        const TYPE nz2 = nz*nz; 

        const TYPE norm      = nx2 + ny2 + nz2;
        const TYPE norm_sqrt = sqrt(norm);

        const TYPE        a = sinbeta * norm_sqrt;
        const TYPE sinbeta2 = sinbeta * sinbeta;
        const TYPE        b = ( sinbeta2 < TYPE(1.0) ? ( TYPE(1.0) - sqrt( TYPE(1.0) - sinbeta2 ) ) : TYPE(1.0) );
        const TYPE    enorm = radius/(e*norm);

        const TYPE bnxny = b*nx*ny;
        const TYPE bnxnz = b*nx*nz;
        const TYPE bnynz = b*ny*nz;

        //Perform non-normalized rotation
        const std::array<TYPE,9> M = 
          { norm - b*(nz2+ny2), a*nz+bnxny,         -a*ny+bnxnz,
            -a*nz+bnxny,       norm - b*(nx2+nz2),  a*nx+bnynz,
            a*ny+bnxnz,       -a*nx+bnynz,         norm-b*(ny2+nx2) };
        
        //re-normalize rotation and scale to radius in one step
        const TYPE ex = enorm*(evec.x()*M[0]+evec.y()*M[3]+evec.z()*M[6]);
        const TYPE ey = enorm*(evec.x()*M[1]+evec.y()*M[4]+evec.z()*M[7]);
        const TYPE ez = enorm*(evec.x()*M[2]+evec.y()*M[5]+evec.z()*M[8]);
        sphReflPoint = { CoC.x() + ex, CoC.y() + ey, CoC.z() + ez };

      }

    private:

      //A newton iteration solver for the Rich quartic equation
      //Since the polynomial that is evaluated here is extremely constrained
      //(root is in small interval, one root guaranteed), we can use a much more
      //efficient approximation (which still has the same precision) instead of the
      //full blown mathematically absolute correct method and still end up with
      //usable results

      template < class TYPE >
      inline TYPE __attribute__((always_inline)) f4( const TYPE (&a)[5], const TYPE x ) const
      {
        //return (a0 * x*x*x*x + a1 * x*x*x + a2 * x*x + a3 * x + a4);
        //A bit more FMA friendly
        return ( ( ( ( ( ( ( a[0] * x ) + a[1] ) * x ) + a[2] ) * x ) + a[3] ) * x ) + a[4];
      }

      //template < class TYPE >
      //inline TYPE __attribute__((always_inline)) df4( const TYPE &a0, const TYPE &a1, const TYPE &a2, const TYPE &a3, TYPE &x ) const
      //{
      //  //return (4.0f*a0 * x*x*x + 3.0f*a1 * x*x + 2.0f*a2 * x + a3);
      // return ( ( ( ( TYPE(4.0) * a0*x ) + TYPE(3.0) * a1 ) * x + ( TYPE(2.0) * a2 ) ) * x ) + a3;
      //}

      /** Horner's method to evaluate the polynomial and its derivatives with as little math operations as
       *  possible. We use a template here to allow the compiler to unroll the for loops and produce code
       *  that is free from branches and optimized for the grade of polynomial and derivatives as necessary.
       */
      template < class TYPE, std::size_t ORDER = 4, std::size_t DIFFGRADE = 3 >
      inline void __attribute__((always_inline)) 
      evalPolyHorner( const TYPE (&a)[ORDER+1], TYPE (&res)[DIFFGRADE+1], const TYPE x ) const
      {
        for ( std::size_t i = 0; i <= DIFFGRADE; ++i )
        {
          res[i] = a[0];
        }

        for ( std::size_t j = 1; j <= ORDER; ++j )
        {
          res[0] = res[0] * x + a[j];
          const int l = (ORDER - j) > DIFFGRADE ? DIFFGRADE : ORDER - j;
          for ( int i = 1; i <= l ; ++i )
          {
            res[i] = res[i] * x + res[i-1];
          }
        }

        //TODO: Check assembly to see if this is optimized away if DIFFGRADE is <= 2
        TYPE l = 1.0;
        for ( std::size_t i = 2; i <= DIFFGRADE; ++i )
        {
          l *= i;
          res[i] = res[i] * l;
        }

      }

      /** Newton-Rhapson method for calculating the root of the rich polynomial. It uses the bisection method in the beginning
       *  to get close enough to the root to allow the second stage newton method to converge faster. After 4 iterations of newton
       *  precision is as good as single precision floating point will get you. We have introduced a few tuning parameters like the
       *  newton gain factor and a slightly skewed bisection division, which in this particular case help to speed things up.
       *  TODO: Once we are happy with the number of newton and bisection iterations, this function should be templated to the number of
       *  these iterations to allow loop unrolling and elimination of unnecessary branching
       *  TODO: These tuning parameters have been found by low effort experimentation on random input data. A more detailed
       *  study should be done with real data to find the best values
       */
      template < class TYPE, std::size_t BISECTITS = 3, std::size_t NEWTONITS = 4 >
      inline TYPE __attribute__((always_inline)) solve_quartic_newton_RICH( const TYPE (&a)[5] ) const
      {
        //Use N steps of bisection method to find starting point for newton
        TYPE l(0.0);
        TYPE u(0.5);
        TYPE m(0.2); //We start a bit off center since the distribution of roots tends to be more to the left side
        for ( std::size_t i = 0; i < BISECTITS; ++i )
        {
          const auto oppositeSign = std::signbit( f4(a,m) * f4(a,l) );
          l = oppositeSign ? l : m;
          u = oppositeSign ? m : u;
          //0.4 instead of 0.5 to speed up convergence. Most roots seem to be closer to 0 than to the extreme end
          m = ( u + l ) * TYPE(0.4);
        }

        //Newton for the rest
        // CRJ : why copy the value. original value is not needed later on...
        //TYPE x = m;

        //Most of the times we are approaching the root of the polynomial from one side
        //and fall short by a certain fraction. This fraction seems to be around 1.04 of the
        //quotient which is subtracted from x. By scaling it up, we take bigger steps towards
        //the root and thus converge faster.
        //TODO: study this factor more closely it's pure guesswork right now. We might get
        //away with 3 iterations if we can find an exact value
        const TYPE gain = 1.04;
        TYPE res[2] = {0,0};
        for ( std::size_t i = 0; i < NEWTONITS; ++i )
        {
          evalPolyHorner<TYPE, 4, 1>( a, res, m );
          //epsilon = f4(a0,a1,a2,a3,a4,x) / df4(a0,a1,a2,a3,x);
          m -= gain * ( res[0] / res[1] );
        }

        return m;
      }

      // /** 3rd grade Housholder's method for iteratively finding the root of a function. Tests have shown that we seem to be
      //  *  already too constrained in our polynomial and input data that we are not gaining anything over newton. In fact it seems
      //  *  to make performance worse.
      //  *  TODO: find out why performance of this is so bad. We might be missing something. It should converge much faster than newton.
      //  */
      // template < class TYPE, std::size_t ITER = 3 >
      // inline TYPE __attribute__((always_inline)) householder(const TYPE (&a)[5], TYPE x0) const
      // {
      //   TYPE res[4];
      //   for ( std::size_t i = 0; i < ITER; ++i )
      //   {
      //     evalPolyHorner<TYPE, 4, 3>(a, res, x0);
      //     const auto res0sq = std::pow( res[0], 2 );
      //     const auto res1sq = std::pow( res[1], 2 );
      //     x0 -= ((TYPE(6.0) * res[0]*res1sq - TYPE(3.0) * res0sq*res[2])/
      //            (TYPE(6.0) * res[1]*res1sq - TYPE(6.0) * res[0]*res[1]*res[2] + res0sq*res[3]));
      //   }
      //   return x0;
      // }

      // /** Use Housholder's method to solve the rich quartic equation. Important! If this function is used, the coefficients of the
      //  *  polynomial have to be normalized. They are typically around 10^15 and floating point overflows will occur in here if these
      //  *  large values are used.
      //  */
      // template < class TYPE, std::size_t ITER = 3 >
      // inline TYPE__attribute__((always_inline))  solve_quartic_housholder_RICH( const TYPE& a0,
      //                                            const TYPE& a1,
      //                                            const TYPE& a2,
      //                                            const TYPE& a3) const
      // {
      //   //Starting value for housholder iteration. Empirically values seem to be
      //   //between 0 0and 0.4 so we chose the value in the middle as starting point
      //   TYPE x0 = TYPE(0.2);
      //   TYPE a[5] = { TYPE(1.0), a0, a1, a2, a3 };
      //   return householder<TYPE, ITER>( a, x0 );
      // }

    };

  }
}
