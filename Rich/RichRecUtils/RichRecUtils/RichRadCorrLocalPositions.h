// $Id: RichRadCorrLocalPositions.h,v 1.4 2007-09-04 16:46:57 jonrob Exp $
#ifndef RICHRECBASE_RICHRADCORRLOCALPOSITIONS_H
#define RICHRECBASE_RICHRADCORRLOCALPOSITIONS_H 1

// STD
#include <ostream>
#include <vector>

// Gaudi
#include "GaudiKernel/Point3DTypes.h"

// Kernel
#include "Kernel/RichRadiatorType.h"
#include "Kernel/MemPoolAlloc.h"

namespace Rich
{
  namespace Rec
  {

    /** @class RadPositionCorrector RichRecBase/RichRadCorrLocalPositions.h
     *
     *  Simple class to compute the local coordinate correction.
     *
     *  Need to migrate away from this for the upgrade, but keep for now.
     *
     *  @author Chris Jones   (Christopher.Rob.Jones@cern.ch)
     *  @date   2007-03-28
     */
    class RadPositionCorrector final 
    {
    public:
      /// Returns the corrected local position, for the given radiator
      inline Gaudi::XYZPoint correct( const Gaudi::XYZPoint & point,
                                      const Rich::RadiatorType rad ) const
      {
        return Gaudi::XYZPoint( (1-m_radScale[rad]) * point.x(),
                                (1+m_radScale[rad]) * point.y(),
                                point.z() );
      }
    private:
      /// The radiator correction parameters
      const RadiatorArray<double> m_radScale = {{ 0.03, 0.017, -0.014 }};
    };

    /** @class RadCorrLocalPositions RichRecBase/RichRadCorrLocalPositions.h
     *
     *  Simple class to store three local coordinate positions, the corrected positions for each radiator
     *
     *  @author Chris Jones   (Christopher.Rob.Jones@cern.ch)
     *  @date   2007-03-28
     */

    class RadCorrLocalPositions final : public LHCb::MemPoolAlloc<Rich::Rec::RadCorrLocalPositions>
    {

    public:

      /// Standard constructor
      RadCorrLocalPositions( ) = default;

      /// Constructor from a default position
      RadCorrLocalPositions( const Gaudi::XYZPoint & point ) { m_pos.fill(point); }

      /// Get the position for the given radiator (const)
      inline const Gaudi::XYZPoint & position( const Rich::RadiatorType rad ) const noexcept
      {
        return m_pos[rad];
      }

      /// Get the position for the given radiator (non const)
      inline Gaudi::XYZPoint & position( const Rich::RadiatorType rad ) noexcept
      {
        return m_pos[rad];
      }

      /// Set the position for the given radiator
      inline void setPosition( const Rich::RadiatorType rad,
                               const Gaudi::XYZPoint & point ) noexcept
      {
        m_pos[rad] = point;
      }

      /// overload printout to ostream operator <<
      friend inline std::ostream& operator << ( std::ostream& s,
                                                const Rich::Rec::RadCorrLocalPositions & pos )
      {
        return s << "[ " 
                 << pos.position(Rich::Aerogel)  << ", " 
                 << pos.position(Rich::Rich1Gas) << ", " 
                 << pos.position(Rich::Rich2Gas)
                 << " ]";
      }

    private:

      /// The corrected local positions
      Rich::RadiatorArray<Gaudi::XYZPoint> m_pos;

    };

  }
}

#endif // RICHRECBASE_RICHRADCORRLOCALPOSITIONS_H
