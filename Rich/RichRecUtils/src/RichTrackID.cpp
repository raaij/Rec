
//-----------------------------------------------------------------------------
/** @file RichTrackID.cpp
 *
 *  Implementation file for class : RichTrackID
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2003-09-23
 */
//-----------------------------------------------------------------------------

// local
#include "RichRecUtils/RichTrackID.h"

// STD
#include <sstream>

// from Gaudi
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/GaudiException.h"

// Text conversion for Rich::RecTrack::ParentType enumeration
std::string Rich::Rec::text( const Rich::Rec::TrackParent::Type parent )
{
  switch( parent )
  {
  case Rich::Rec::TrackParent::Type::Track:          return "Track";
  case Rich::Rec::TrackParent::Type::MCParticle:     return "MCParticle";
  case Rich::Rec::TrackParent::Type::MCRichTrack:    return "MCRichTrack";
  default:                                     return "SHOULD NEVER SEE THIS";
  }
}

// Text conversion for Rich::RecTrack::ParentType enumeration
std::string Rich::Rec::text( const Rich::Rec::Track::Type track )
{
  switch( track )
  {
  case Rich::Rec::Track::Type::Trigger:      return "Trigger"; // Place first for speed
  case Rich::Rec::Track::Type::Forward:      return "Forward";
  case Rich::Rec::Track::Type::Match:        return "Match";
  case Rich::Rec::Track::Type::KsTrack:      return "KsTrack";
  case Rich::Rec::Track::Type::VeloTT:       return "VeloTT";
  case Rich::Rec::Track::Type::Seed:         return "Seed";
  case Rich::Rec::Track::Type::Follow:       return "Follow";
  case Rich::Rec::Track::Type::Velo:         return "Velo";
  case Rich::Rec::Track::Type::MuonID:       return "MuonID";
  case Rich::Rec::Track::Type::Unknown:      return "Unknown";
  case Rich::Rec::Track::Type::Unusable:     return "Unusable";
  case Rich::Rec::Track::Type::MCRichTrack:  return "MCRichTrack";
  default:                             return "SHOULD NEVER SEE THIS";
  }
}

Rich::Rec::Track::Type Rich::Rec::Track::type( const std::string & name )
{
  if      ( "Trigger"     == name ) { return Rich::Rec::Track::Type::Trigger;     }
  else if ( "Forward"     == name ) { return Rich::Rec::Track::Type::Forward;     }
  else if ( "Match"       == name ) { return Rich::Rec::Track::Type::Match;       }
  else if ( "KsTrack"     == name ) { return Rich::Rec::Track::Type::KsTrack;     }
  else if ( "VeloTT"      == name ) { return Rich::Rec::Track::Type::VeloTT;      }
  else if ( "Seed"        == name ) { return Rich::Rec::Track::Type::Seed;        }
  else if ( "Follow"      == name ) { return Rich::Rec::Track::Type::Follow;      }
  else if ( "Velo"        == name ) { return Rich::Rec::Track::Type::Velo;        }
  else if ( "MuonID"      == name ) { return Rich::Rec::Track::Type::MuonID;      }
  else if ( "Unknown"     == name ) { return Rich::Rec::Track::Type::Unknown;     }
  else if ( "Unusable"    == name ) { return Rich::Rec::Track::Type::Unusable;    }
  else if ( "MCRichTrack" == name ) { return Rich::Rec::Track::Type::MCRichTrack; }
  else
  { // Should not get here ...
    throw GaudiException( "Unknown track string name '"+name+"'",
                          "*Rich::Rec::Track::type*", StatusCode::FAILURE );
    return Rich::Rec::Track::Type::Unknown;
  }
}

// Returns the enumerated type for a given Track
Rich::Rec::Track::Type Rich::Rec::Track::type( const LHCb::Track * track )
{
  if ( track )
  {
    // track algorithm type
    const LHCb::Track::History hist = (LHCb::Track::History)track->history();
    // check all known track types (order according to abundance)
    // Forward types
    if      ( LHCb::Track::History::PatForward    == hist ||
              LHCb::Track::PrForward     == hist )  { return Rich::Rec::Track::Type::Forward;  }
    // match track types
    else if ( LHCb::Track::History::TrackMatching == hist ||
              LHCb::Track::History::PatMatch      == hist ||
              LHCb::Track::PrMatch       == hist )  { return Rich::Rec::Track::Type::Match;    }
    // seed types
    else if ( LHCb::Track::History::TsaTrack      == hist ||
              LHCb::Track::History::PatSeeding    == hist ||
              LHCb::Track::PrSeeding     == hist )  { return Rich::Rec::Track::Type::Seed;     }
    // veloTT types
    else if ( LHCb::Track::History::PatVeloTT     == hist ||
              LHCb::Track::PrVeloUT      == hist )  { return Rich::Rec::Track::Type::VeloTT;   }
    // Ks Tracks
    else if ( LHCb::Track::History::PatKShort     == hist ||
              LHCb::Track::History::PatDownstream == hist ||
              LHCb::Track::PrDownstream  == hist )  { return Rich::Rec::Track::Type::KsTrack;  }
    // Velo only tracks (from any source)
    else if ( LHCb::Track::Types::Velo  == track->type() ||
              LHCb::Track::VeloR == track->type() ) { return Rich::Rec::Track::Type::Velo;     }
    // Muon ID
    else if ( LHCb::Track::MuonID        == hist )  { return Rich::Rec::Track::Type::MuonID;   }
    // MC or ideal tracking
    else if ( LHCb::Track::TrackIdealPR  == hist )  { return Rich::Rec::Track::Type::MCRichTrack; }
    // Imported HLT tracks
    else if ( LHCb::Track::History::HLTImportedTrack == hist )
    {
      switch( track->type() )
      {
      case LHCb::Track::Types::Downstream:
        return Rich::Rec::Track::Type::KsTrack;
      case LHCb::Track::Types::VeloR:
      case LHCb::Track::Types::Velo:
        return Rich::Rec::Track::Type::Velo;
      case LHCb::Track::Types::Upstream:
        return Rich::Rec::Track::Type::VeloTT;
      case LHCb::Track::Types::Ttrack:
        return Rich::Rec::Track::Type::Seed;
      case LHCb::Track::Types::Long:
      default:
        return Rich::Rec::Track::Type::Forward;
      }
    }
    else
    { // Should not get here ...
      std::ostringstream mess;
      mess << "Unknown Track type : Track::History = " << track->history();
      throw GaudiException( mess.str(), "*Rich::Rec::Track::type*", StatusCode::FAILURE );
    }
  }

  // Should not get here either ...
  throw GaudiException( "Null Track pointer", "*Rich::Rec::Track::type*", StatusCode::FAILURE );
}

Rich::Rec::Track::Type Rich::Rec::Track::type( const LHCb::MCRichTrack * track )
{
  if ( UNLIKELY(!track) )
  {
    throw GaudiException( "Null Track pointer", "*Rich::Rec::Track::type*", StatusCode::FAILURE );
  }
  return Rich::Rec::Track::Type::MCRichTrack;
}
