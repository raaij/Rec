
#pragma once

// Utils
#include "RichRecUtils/RichPhotonSpectra.h"
#include "RichFutureUtils/RichHypoData.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      /// Type for photon yield data
      using PhotonYields = Rich::Future::HypoData<double>;

      /// photon yield TES locations
      namespace PhotonYieldsLocation
      {
        /// Location in TES for the emitted photon spectra
        static const std::string Emitted    = "Rec/RichFuture/PhotonYields/Emitted";
        /// Location in TES for the signal photon spectra
        static const std::string Signal     = "Rec/RichFuture/PhotonYields/signal";
        /// Location in TES for the detectable photon spectra
        static const std::string Detectable = "Rec/RichFuture/PhotonYields/Detectable";
      }

      /// The number of bins in the photon spectra data
      constexpr unsigned int NPhotonSpectraBins = 5;

      /// Type for photon spectra data
      using PhotonSpectra = Rich::PhotonSpectra<double,NPhotonSpectraBins>;

      /// photon spectra TES locations
      namespace PhotonSpectraLocation
      {
        /// Location in TES for the emitted photon spectra
        static const std::string Emitted    = "Rec/RichFuture/PhotonSpectra/Emitted";
        /// Location in TES for the signal photon spectra
        static const std::string Signal     = "Rec/RichFuture/PhotonSpectra/signal";
        /// Location in TES for the detectable photon spectra
        static const std::string Detectable = "Rec/RichFuture/PhotonSpectra/Detectable";
      }

    }
  }
}
