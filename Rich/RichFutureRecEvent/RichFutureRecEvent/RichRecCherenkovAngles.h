
#pragma once

#include "RichFutureUtils/RichHypoData.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      /// Type for Cherenkov angles
      using CherenkovAngles = Rich::Future::HypoData<double>;

      /// Cherenkov angles TES locations
      namespace CherenkovAnglesLocation
      {
        /// Location in TES for the emitted photon spectra
        static const std::string Emitted = "Rec/RichFuture/CherenkovAngles/Emitted";
        /// Location in TES for the signal photon spectra
        static const std::string Signal  = "Rec/RichFuture/CherenkovAngles/signal";
      }

      /// type for expected Cherenkov resolutions
      using CherenkovResolutions = Rich::Future::HypoData<double>;

      /// Cherenkov resolutions TES locations
      namespace CherenkovResolutionsLocation
      {
        /// Location in TES for the Cherenkov resolutions
        static const std::string Default = "Rec/RichFuture/CherenkovResolutions/Default";
      }

    }
  }
}
