
#pragma once

// STL
#include <ostream>
#include <array>

// Kernel
#include "Kernel/RichSmartID.h"
#include "Kernel/RichParticleIDType.h"
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"
#include "Kernel/FastAllocVector.h"

// Utils
#include "RichUtils/RichMap.h"
#include "RichRecUtils/RichPhotonSpectra.h"
#include "RichFutureUtils/RichHypoData.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      //=============================================================================

      /// Type for geometrical efficiencies
      using GeomEffs = Rich::Future::HypoData<double>;

      /// geometrical efficiencies TES locations
      namespace GeomEffsLocation
      {
        /// Location in TES for the default geometrical efficiencies
        static const std::string Default = "Rec/RichFuture/GeomEffs/Default";
      }

      //=============================================================================

      /// Type for the per PD Geom Effs for each mass hypothesis
      using GeomEffsPerPD = Rich::ParticleArray< Rich::Map<LHCb::RichSmartID,double> >;

      /// Vector of GeomEffsPerPD
      using GeomEffsPerPDVector = LHCb::STL::Vector<GeomEffsPerPD>;

      /// geometrical efficiencies per PD TES locations
      namespace GeomEffsPerPDLocation
      {
        /// Location in TES for the default geometrical efficiencies
        static const std::string Default = "Rec/RichFuture/GeomEffsPerPD/Default";
      }

      //=============================================================================

      /** @class SegmentPhotonFlags
       *  Type for storing segment 'photon regions' data
       *  Indicates if the segment in question has photon in either of the two
       *  RICH detector sides [0,1] = ( top,bottom RICH1, Left,Right in RICH2).
       *  regions. */
      class SegmentPhotonFlags final
      {
      public:

        /// Type for container
        using Vector = LHCb::STL::Vector<SegmentPhotonFlags>;

      public:

        /// Get the side for a given position and RICH type
        template< typename POINT >
          inline Rich::Side side( const Rich::DetectorType rich,
                                  const POINT& p ) const noexcept
        {
          return ( Rich::Rich1 == rich ?
                   p.y() > 0 ? Rich::top  : Rich::bottom :
                   p.x() > 0 ? Rich::left : Rich::right );
        }

        /// Set the flag for the given side
        inline void setInAcc( const Rich::Side side, const bool ok = true ) noexcept
        { m_photsEachSide[side] = ok; }

        /// Set the flag for a given RICH and point
        template< typename POINT >
          inline void setInAcc( const Rich::DetectorType rich,
                                const POINT& p,
                                const bool ok = true ) noexcept
        {
          setInAcc( side(rich,p), ok );
        }

        /// Access the flag for the given side
        inline bool inPanel( const Rich::Side side ) const noexcept
        { return m_photsEachSide[side]; }

        // Is this segment active in both side ?
        inline bool inBothPanels() const noexcept
        { return m_photsEachSide[0] && m_photsEachSide[1]; }

        /// Access the flags array
        inline const std::array<bool,2>& flags() const noexcept
        { return m_photsEachSide; }

      public:

        /// overload printout to ostream operator <<
        friend inline std::ostream& operator << ( std::ostream& s,
                                                  const SegmentPhotonFlags & flags )
        {
          return s << "[" << flags.flags()[0] << "," <<flags.flags()[1] << "]";
        }

      private:

        /// Stores the flag for each RICH side
        std::array<bool,2> m_photsEachSide = {{false,false}};

      };

      /// Locations in TES for segment photon side flags
      namespace SegmentPhotonFlagsLocation
      {
        /// Location in TES for the default segment photon flags
        static const std::string Default = "Rec/RichFuture/SegmentPhotonFlags/Default";
      }

      //=============================================================================

    }
  }
}
