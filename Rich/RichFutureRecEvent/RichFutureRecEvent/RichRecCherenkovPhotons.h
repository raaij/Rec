
#pragma once

// STL
#include <string>

#include "RichFutureUtils/RichGeomPhoton.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {
      
      /** @class CherenkovPhoton RichFutureRecEvent/RichRecCherenkovPhotons.h
       *
       *  A reconstructed photon object.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */
      using CherenkovPhoton = Rich::Future::RecoPhoton;

      /// TES locations
      namespace CherenkovPhotonLocation
      {
        /// Default Location in TES for the global space points
        static const std::string Default = "Rec/RichFuture/CherenkovPhotons/Default";
      }

    }
  }
}
