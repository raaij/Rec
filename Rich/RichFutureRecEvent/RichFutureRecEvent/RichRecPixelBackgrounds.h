
#pragma once

// Kernel
#include "Kernel/FastAllocVector.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      /// Type for pixel background values
      using PixelBackgrounds = LHCb::STL::Vector<double>;

      /// TES locations
      namespace PixelBackgroundsLocation
      {
        /// Default TES location for pixel backgrounds
        static const std::string Default = "Rec/RichFuture/PixelBackgrounds/Default";
      }

    }
  }
}
