
//-----------------------------------------------------------------------------
/** @file RichSellmeirFunc.cpp
 *
 *  Implementation file for tool : Rich::Rec::SellmeirFunc
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

// local
#include "RichSellmeirFunc.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Rec;

//-----------------------------------------------------------------------------

// Standard constructor
SellmeirFunc::SellmeirFunc ( const std::string& type,
                             const std::string& name,
                             const IInterface* parent )
  : ToolBase ( type, name, parent )
{
  // interface
  declareInterface<ISellmeirFunc>(this);
}

StatusCode SellmeirFunc::initialize()
{
  // Sets up various tools and services
  StatusCode sc = ToolBase::initialize();
  if ( sc.isFailure() ) { return sc; }

  // Get Rich1 Detector element
  m_Rich1DE = getDet<DeRich1>( DeRichLocations::Rich1 );

  // Acquire instances of tools
  acquireTool( "RichParticleProperties", m_partProp );

  // register for UMS updates
  //updMgrSvc()->registerCondition( this, m_Rich1DE, &SellmeirFunc::umsUpdate );
  // force first updates
  //sc = updMgrSvc()->update(this);
  // CRJ : UMS does not work because Rich1 has no conditions (yet)
  sc = umsUpdate();
 
  // return
  return ( sc.isSuccess() ? sc : Error ("Failed first UMS update",sc) );
}

StatusCode SellmeirFunc::umsUpdate()
{
  _ri_debug << "Sellmeir parameter update triggered" << endmsg;

  // Retrieve square of particle masses
  for ( const auto pid : Rich::particles() )
  {
    m_particleMassSq[pid] = m_partProp->massSq(pid);
  }

  // Load radiator sellmeir parameters from XML
  const RadArray selF1 =
    { m_Rich1DE->param<double>("SellAgelF1Param"),
      m_Rich1DE->param<double>("SellC4F10F1Param"),
      m_Rich1DE->param<double>("SellCF4F1Param") };
  const RadArray selF2 =
    { m_Rich1DE->param<double>("SellAgelF2Param"),
      m_Rich1DE->param<double>("SellC4F10F2Param"),
      m_Rich1DE->param<double>("SellCF4F2Param") };
  const RadArray selE1 =
    { m_Rich1DE->param<double>("SellAgelE1Param"),
      m_Rich1DE->param<double>("SellC4F10E1Param"),
      m_Rich1DE->param<double>("SellCF4E1Param") };
  const RadArray selE2 = 
    { m_Rich1DE->param<double>("SellAgelE2Param"),
      m_Rich1DE->param<double>("SellC4F10E2Param"),
      m_Rich1DE->param<double>("SellCF4E2Param") };
  const RadArray molW  =   
    { 0,
      m_Rich1DE->param<double>("GasMolWeightC4F10Param"),
      m_Rich1DE->param<double>("GasMolWeightCF4Param") };
  const RadArray rho   =
    { 0,
      m_Rich1DE->param<double>("RhoEffectiveSellC4F10Param"),
      m_Rich1DE->param<double>("RhoEffectiveSellCF4Param") };
  const auto selLorGasFac = m_Rich1DE->param<double>("SellLorGasFacParam");

  // Initialise the calculations and cache as much as possible for efficiency
  for ( const auto rad : Rich::radiators() )
  {
    const bool isAero = ( Rich::Aerogel == rad );
    const auto RC = ( isAero ? 1.0 : selLorGasFac * rho[rad] / molW[rad] );
    const auto RF = selF1[rad] + selF2[rad];
    const auto RE02 = ( selF1[rad]*selE2[rad]*selE2[rad] +
                        selF2[rad]*selE1[rad]*selE1[rad] ) / RF;
    const auto RE = ( selE2[rad]*selE2[rad] +
                      selE1[rad]*selE1[rad] ) / RF;
    const auto RG = ( selE1[rad]*selE1[rad] *
                      selE2[rad]*selE2[rad] ) / (RF*RE02);
    const auto RH = RE02/RF;
    const auto RM = RE + (2.0*RC);
    const auto RS = RG + (2.0*RC);
    const auto RT = sqrt( 0.25*RM*RM - RH*RS );
    const auto RXSP = std::sqrt( (RM/2. + RT)/RH );
    const auto RXSM = std::sqrt( (RM/2. - RT)/RH );
    m_REP[rad] = std::sqrt(RE02) * RXSP;
    m_REM[rad] = std::sqrt(RE02) * RXSM;
    m_RXSPscale[rad] = (RXSP - 1./RXSP);
    m_RXSMscale[rad] = (RXSM - 1./RXSM);
    m_X[rad] = (3.0*RC*std::sqrt(RE02)/(4.*RT));
  }

  return StatusCode::SUCCESS;
}

double SellmeirFunc::photonsInEnergyRange( const LHCb::RichRecSegment * segment,
                                           const Rich::ParticleIDType id,
                                           const double botEn,
                                           const double topEn ) const
{
  // protect against below threshold case
  if ( Rich::BelowThreshold == id ) return 0;

  // Some parameters of the segment
  const auto momentum2  = segment->trackSegment().bestMomentum().Mag2();
  const auto length     = segment->trackSegment().pathLength();
  const auto rad        = segment->trackSegment().radiator();
  const auto Esq        = momentum2 + m_particleMassSq[id];
  const auto invBetaSq  = ( momentum2>0 ? Esq/momentum2 : 0 );
  const auto invGammaSq = ( Esq>0 ? m_particleMassSq[id]/Esq : 0 );

  // Compute number of photons
  auto nPhot = ( 37.0 * invBetaSq ) * ( paraW(rad,topEn) -
                                        paraW(rad,botEn) -
                                        ((topEn-botEn)*invGammaSq) );

  // correct for wavelength independant transmission coeff. in aerogel
  if ( UNLIKELY( Rich::Aerogel == rad ) )
  {
    // get the radiator intersections
    const auto & radInts = segment->trackSegment().radIntersections();

    // normalise over each intersection
    double totPlength(0), waveIndepTrans(0);
    if ( !radInts.empty() )
    {
      // average energy for this range
      const auto avEn = 0.5 * (botEn+topEn) * Gaudi::Units::eV;

      // average over all intersections
      for ( const auto& R : radInts )
      {
        const auto pLen = R.pathLength();
        const auto absL = R.radiator()->absorption()->value(avEn);
        totPlength       += pLen;
        waveIndepTrans   += pLen * vdt::fast_exp( -pLen / absL );
      }
      if ( totPlength>0 ) waveIndepTrans /= totPlength;

      // scale the expected photon yield
      nPhot *= waveIndepTrans;
    }
  }

  // return
  return length * ( nPhot < 0 ? 0 : nPhot );
}

//-----------------------------------------------------------------------------

DECLARE_TOOL_FACTORY( SellmeirFunc )

//-----------------------------------------------------------------------------
