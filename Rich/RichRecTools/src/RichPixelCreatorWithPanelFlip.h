
//---------------------------------------------------------------------------------
/** @file RichPixelCreatorWithPanelFlip.h
 *
 *  Header file for tool : Rich::Rec::PixelCreatorWithPanelFlip
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   18/02/2011
 */
//---------------------------------------------------------------------------------

#ifndef RICHRECTOOLS_RichPixelCreatorWithPanelFlip_H
#define RICHRECTOOLS_RichPixelCreatorWithPanelFlip_H 1

// from Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/IRndmGenSvc.h"

// base class
#include "RichPixelCreatorFromRawBuffer.h"

// Rich Utils
#include "RichUtils/StlArray.h"

namespace Rich
{
  namespace Rec
  {

    //---------------------------------------------------------------------------------
    /** @class PixelCreatorWithPanelFlip RichPixelCreatorWithPanelFlip.h
     *
     *  Tool for the creation and book-keeping of RichRecPixel objects.
     *
     *  Inherits from RichPixelCreatorFromRawBuffer, but flips the HPD panel
     *  information ( So left <-> right in RICH2, up <-> down in RICH1 )
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   18/02/2011
     */
    //---------------------------------------------------------------------------------

    class PixelCreatorWithPanelFlip final : public PixelCreatorFromRawBuffer
    {

    public: // Methods for Gaudi Framework

      /// Standard constructor
      PixelCreatorWithPanelFlip( const std::string& type,
                                 const std::string& name,
                                 const IInterface* parent );

      // Initialize method
      StatusCode initialize() override;

    protected: // methods

      /// Build a new RichRecPixel from a Rich::PDPixelCluster
      LHCb::RichRecPixel * buildPixel ( const Rich::PDPixelCluster& cluster ) const override;

      /// Build a new RichRecPixel a single LHCb::RichSmartID
      LHCb::RichRecPixel * buildPixel ( const LHCb::RichSmartID & id ) const override;

    private: // methods

      /// Flip the given RichSmartID
      LHCb::RichSmartID flip( const LHCb::RichSmartID & id ) const;

    private: // data

      /// Flags to turn on the flipping for RICH1 and RICH2
      DetectorArray<bool> m_flipRICH = {{}};

    };

  }
}

#endif // RICHRECTOOLS_RichPixelCreatorWithPanelFlip_H
