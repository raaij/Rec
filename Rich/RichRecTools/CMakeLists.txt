################################################################################
# Package: RichRecTools
################################################################################
gaudi_subdir(RichRecTools v4r30)

gaudi_depends_on_subdirs(Det/RichDet
                         GaudiKernel
                         Kernel/LHCbKernel
                         Rich/RichRecBase)

find_package(GSL)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(RichRecTools
                 src/*.cpp
                 INCLUDE_DIRS GSL
                 LINK_LIBRARIES GSL RichDetLib GaudiKernel LHCbKernel RichRecBase)

