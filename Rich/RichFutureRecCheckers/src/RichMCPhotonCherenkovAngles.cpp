
// local
#include "RichMCPhotonCherenkovAngles.h"

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : PhotonCherenkovAngles
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

PhotonCherenkovAngles::
PhotonCherenkovAngles( const std::string& name, ISvcLocator* pSvcLocator )
  : Consumer( name, pSvcLocator,
              { KeyValue{ "SummaryTracksLocation", Summary::TESLocations::Tracks },
                KeyValue{ "TracksLocation",          LHCb::TrackLocation::Default },
                KeyValue{ "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default },
                KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                KeyValue{ "CherenkovPhotonLocation", CherenkovPhotonLocation::Default },
                KeyValue{ "TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles },
                KeyValue{ "RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default } } )
{
  // print some stats on the final plots
  setProperty ( "HistoPrint", true ); 
}

//-----------------------------------------------------------------------------

StatusCode PhotonCherenkovAngles::finalize()
{
  // Protect against FPEs from histogram printing
  // Can be removed once
  // https://gitlab.cern.ch/gaudi/Gaudi/merge_requests/270/diffs
  // is accepted and released.
  FPE::Guard guard(true);
  // return
  return Consumer::finalize();
}

//-----------------------------------------------------------------------------

StatusCode PhotonCherenkovAngles::prebookHistograms()
{

  // Loop over radiators
  for ( const auto rad : Rich::radiators() )
  {
    if ( m_rads[rad] )
    {
      richHisto1D( HID("ckResAll",rad), "Rec-Exp Cktheta | True Type",
                   -m_ckResRange[rad], m_ckResRange[rad], nBins1D() );
      richHisto1D( HID("ckResTrue",rad), "Rec-Exp Cktheta | MC true photons | True Type",
                   -m_ckResRange[rad], m_ckResRange[rad], nBins1D() );
      richHisto1D( HID("ckResFake",rad), "Rec-Exp Cktheta | MC fake photons | True Type",
                   -m_ckResRange[rad], m_ckResRange[rad], nBins1D() );
      richHisto1D( HID("ckResAllPion",rad), "Rec-Exp Cktheta | Pion Type",
                   -m_ckResRange[rad], m_ckResRange[rad], nBins1D() );
      richHisto1D( HID("ckResTruePion",rad), "Rec-Exp Cktheta | MC true photons | Pion Type",
                   -m_ckResRange[rad], m_ckResRange[rad], nBins1D() );
      richHisto1D( HID("ckResFakePion",rad), "Rec-Exp Cktheta | MC fake photons | Pion Type",
                   -m_ckResRange[rad], m_ckResRange[rad], nBins1D() );
    }
  }
 
  return StatusCode::SUCCESS;
}

//-----------------------------------------------------------------------------

void 
PhotonCherenkovAngles::operator()( const Summary::Track::Vector& sumTracks,
                                   const LHCb::Track::Selection& tracks,
                                   const Rich::PDPixelCluster::Vector& clusters,
                                   const Relations::PhotonToParents::Vector& photToSegPix,
                                   const LHCb::RichTrackSegment::Vector& segments,
                                   const CherenkovAngles::Vector& expTkCKThetas,
                                   const CherenkovPhoton::Vector& photons,
                                   const Rich::Future::MC::Relations::TkToMCPRels& tkrels,
                                   const LHCb::MCRichDigitSummarys & digitSums ) const
{

  // Make a local MC helper object
  Helper mcHelper( tkrels, digitSums );

  // loop over the track info
  for ( const auto && data : Ranges::ConstZip(sumTracks,tracks) )
  {
    const auto & sumTk = std::get<0>(data);
    const auto & tk    = std::get<1>(data);

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() )
    {
      // photon data
      const auto & phot = photons[photIn];
      const auto & rels = photToSegPix[photIn];

      // Associated cluster
      const auto & clus = clusters[rels.pixelIndex()];

      // the segment for this photon
      const auto & seg = segments[rels.segmentIndex()];

      // Radiator info
      const auto rad = seg.radiator();
      if ( !m_rads[rad] ) continue;

      // get the expected CK theta values for this segment
      const auto & expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // reconstructed theta
      const auto thetaRec = phot.CherenkovTheta();

      // Get the MCParticles for this track
      const auto mcPs = mcHelper.mcParticles(*tk,true,0.5);

      // do we have an true MC Cherenkov photon
      const auto trueCKMCPs = mcHelper.trueCherenkovPhoton( *tk, rad, clus );

      // Weight per MCP
      const double mcPW = ( !mcPs.empty() ? 1.0/(double)mcPs.size() : 1.0 );

      // loop over MCPs
      for ( const auto mcP : mcPs )
      {

        // The True MCParticle type
        auto pid = mcHelper.mcParticleType(mcP);
        // If MC type not known, assume Pion (as in real data)
        if ( Rich::Unknown == pid ) pid = Rich::Pion;
        // skip electrons which are reconstructed badly..
        if ( m_skipElectrons && Rich::Electron == pid ) continue; 
        
        // beta cut for true MC type
        const auto mcbeta = richPartProps()->beta( pTot, pid );
        if ( mcbeta >= m_minBeta[rad] && mcbeta <= m_maxBeta[rad] )
        {
          
          // expected CK theta ( for true type )
          const auto thetaExp = expCKangles[pid];
          
          // delta theta
          const auto deltaTheta = thetaRec - thetaExp;

          // true Cherenkov signal ?
          const bool trueCKSig = std::find( trueCKMCPs.begin(), 
                                            trueCKMCPs.end(), mcP ) != trueCKMCPs.end();
          
          // fill some plots
          richHisto1D( HID("ckResAll",rad) ) -> fill( deltaTheta, mcPW );
          if ( !trueCKSig )
          {
            richHisto1D( HID("ckResFake",rad) ) -> fill( deltaTheta, mcPW );
          }
          else
          {
            richHisto1D( HID("ckResTrue",rad) ) -> fill( deltaTheta, mcPW );
          }
          
        }
        
      } // loop over associated MCPs
      
      // Now plots when assuming all tracks are pions ( as in real data )
      const auto pionbeta = richPartProps()->beta( pTot, Rich::Pion );
      if ( pionbeta >= m_minBeta[rad] && pionbeta <= m_maxBeta[rad] )
      {

        // expected CK theta ( for Pion )
        const auto thetaExp = expCKangles[Rich::Pion];

        // delta theta
        const auto deltaTheta = thetaRec - thetaExp;
        
        // fill some plots   
        richHisto1D( HID("ckResAllPion",rad) ) -> fill( deltaTheta );
        if ( trueCKMCPs.empty() )
        {
          richHisto1D( HID("ckResFakePion",rad) ) -> fill( deltaTheta );
        }
        else
        {
          richHisto1D( HID("ckResTruePion",rad) ) -> fill( deltaTheta );
        }

      }

    }
  }
  
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( PhotonCherenkovAngles )

//-----------------------------------------------------------------------------
