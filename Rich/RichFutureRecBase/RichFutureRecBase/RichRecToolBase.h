
//-----------------------------------------------------------------------------
/** @file RichRecToolBase.h
 *
 * Header file for reconstruction tool base class : RichRecToolBase
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date   2002-07-26
 */
//-----------------------------------------------------------------------------

#ifndef RICHFUTURERECBASE_RICHRECTOOLBASE_H
#define RICHFUTURERECBASE_RICHRECTOOLBASE_H 1

// Base classes
#include "RichFutureKernel/RichToolBase.h"
#include "RichFutureRecBase/RichRecBase.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      //-----------------------------------------------------------------------------
      /** @class ToolBase RichRecToolBase.h RichRecBase/RichRecToolBase.h
       *
       *  Abstract base class for RICH reconstruction tools providing
       *  some basic functionality.
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   2002-07-26
       */
      //-----------------------------------------------------------------------------

      class ToolBase : public Rich::Future::ToolBase,
                       public Rich::Future::Rec::CommonBase<Rich::Future::ToolBase>
      {

      public:

        /// Standard constructor
        ToolBase( const std::string& type,
                  const std::string& name,
                  const IInterface* parent );

        // Initialize method
        virtual StatusCode initialize() override;

        // Finalize method
        virtual StatusCode finalize() override;

      };

    }
  }
}

#endif // RICHFUTURERECBASE_RICHRECTOOLBASE_H
