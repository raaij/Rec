
//-----------------------------------------------------------------------------
/** @file RichRecHistoToolBase.h
 *
 * Header file for reconstruction tool base class : RichRecHistoToolBase
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date   2005/01/13
 */
//-----------------------------------------------------------------------------

#ifndef RICHFUTURERECBASE_RICHRECHISTOTOOLBASE_H
#define RICHFUTURERECBASE_RICHRECHISTOTOOLBASE_H 1

// Base class
#include "RichFutureKernel/RichHistoToolBase.h"
#include "RichFutureRecBase/RichRecBase.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      //-----------------------------------------------------------------------------
      /** @class HistoToolBase RichRecHistoToolBase.h RichRecBase/RichRecHistoToolBase.h
       *
       *  Abstract base class for RICH reconstruction tools providing
       *  some basic functionality (identical to RichRecToolBase) but with additional
       *  histogram functionality provided by GaudiHistoTool base class.
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   2005/01/13
       */
      //-----------------------------------------------------------------------------

      class HistoToolBase : public Rich::Future::HistoToolBase,
                            public Rich::Future::Rec::CommonBase<Rich::Future::HistoToolBase>
      {

      public:

        /// Standard constructor
        HistoToolBase( const std::string& type,
                       const std::string& name,
                       const IInterface* parent );

        /** Initialization of the tool after creation
         *
         * @return The status of the initialization
         * @retval StatusCode::SUCCESS Initialization was successful
         * @retval StatusCode::FAILURE Initialization failed
         */
        virtual StatusCode initialize() override;

        /** Finalization of the tool before deletion
         *
         * @return The status of the finalization
         * @retval StatusCode::SUCCESS Finalization was successful
         * @retval StatusCode::FAILURE Finalization failed
         */
        virtual StatusCode finalize() override;

      };

    }
  }
}

#endif // RICHFUTURERECBASE_RICHRECHISTOTOOLBASE_H
