
//-----------------------------------------------------------------------------
/** @file RichRecAlgBase.cpp
 *
 *  Implementation file for RICH reconstruction algorithm base class : RichRecAlgBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2003-05-10
 */
//-----------------------------------------------------------------------------

// local
#include "RichFutureRecBase/RichRecAlgBase.h"

// ============================================================================
// Force creation of templated class
#include "RichRecBase.icpp"
template class Rich::Future::Rec::CommonBase<Rich::Future::AlgBase> ;
// ============================================================================

// ============================================================================
// Standard constructor
// ============================================================================
Rich::Future::Rec::AlgBase::AlgBase( const std::string& name,
                                     ISvcLocator* pSvcLocator )
  : Rich::Future::AlgBase                                ( name, pSvcLocator ),
    Rich::Future::Rec::CommonBase<Rich::Future::AlgBase> ( this              )
{
}
// ============================================================================

// ============================================================================
// Initialise
// ============================================================================
StatusCode Rich::Future::Rec::AlgBase::initialize()
{
  // Initialise base class
  StatusCode sc = Rich::Future::AlgBase::initialize();
  if ( sc.isFailure() ) return Error( "Failed to initialise Rich::AlgBase", sc );

  // Common initialisation
  return initialiseRichReco();
}
// ============================================================================

// ============================================================================
// Main execute method
// ============================================================================
StatusCode Rich::Future::Rec::AlgBase::execute()
{
  // All algorithms should re-implement this method
  return Error ( "Default Rich::RecAlgBase::execute() called !!" );
}
// ============================================================================

// ============================================================================
// Finalise
// ============================================================================
StatusCode Rich::Future::Rec::AlgBase::finalize()
{
  // Common finalisation
  const StatusCode sc = finaliseRichReco();
  if ( sc.isFailure() ) return Error( "Failed to finalise RichRecBase", sc );

  // Finalize base class
  return Rich::Future::AlgBase::finalize();
}
// ============================================================================
