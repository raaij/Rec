
// local
#include "RichPixelBackgroundsEstiAvHPD.h"

using namespace Rich::Future::Rec;

//-----------------------------------------------------------------------------
// Implementation file for class : RichPixelbackgroundsEstiAvHPD
//
// 2016-10-25 : Chris Jones
//-----------------------------------------------------------------------------

PixelBackgroundsEstiAvHPD::
PixelBackgroundsEstiAvHPD( const std::string& name, ISvcLocator* pSvcLocator )
 : Transformer( name, pSvcLocator,
                { KeyValue{ "TrackToSegmentsLocation",       Relations::TrackToSegmentsLocation::Selected },
                  KeyValue{ "TrackPIDHyposLocation",         TrackPIDHyposLocation::Default },
                  KeyValue{ "TrackSegmentsLocation",         LHCb::RichTrackSegmentLocation::Default },
                  KeyValue{ "GeomEffsPerPDLocation",         GeomEffsPerPDLocation::Default },
                  KeyValue{ "DetectablePhotonYieldLocation", PhotonYieldsLocation::Detectable },
                  KeyValue{ "RichPixelClustersLocation",     PDPixelClusterLocation::Default } },
                { KeyValue{ "PixelBackgroundsLocation",      PixelBackgroundsLocation::Default } } )
{
  // debug
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//-----------------------------------------------------------------------------

StatusCode PixelBackgroundsEstiAvHPD::initialize()
{
  // Sets up various tools and services
  auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // Rich System
  m_richSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );

  // Make the default working data object
  m_pdData = { PanelArray<PDData::Vector>{ PDData::Vector(maxPdIndex(Rich::Rich1,Rich::top)),
                                           PDData::Vector(maxPdIndex(Rich::Rich1,Rich::bottom)) },
               PanelArray<PDData::Vector>{ PDData::Vector(maxPdIndex(Rich::Rich2,Rich::left)),
                                           PDData::Vector(maxPdIndex(Rich::Rich2,Rich::right)) } };
  // initialise the static data in the working object
  for ( const auto rich : Rich::detectors() )
  {
    auto & richD = m_pdData[rich];
    for ( const auto side : Rich::sides() )
    {
      auto & panelD = richD[side];
      unsigned int index(0);
      for ( auto & pd : panelD )
      {
        // Create the Copy Number for this entry
        const Rich::DAQ::PDPanelIndex copyN(index++);
        // Get the DePD
        // try / catch needed for MaPMTs as not all indices are valid...
        // To be improved ...
        try { pd.dePD = dePD( rich, side, copyN ); }
        catch ( const GaudiException & ) { pd.dePD = nullptr; }
      }
    }
  }
  
  if ( m_ignoreExpSignal )
  { _ri_debug << "Will ignore expected signals when computing backgrounds" << endmsg; }
  
  return sc;
}

//-----------------------------------------------------------------------------

PixelBackgrounds
PixelBackgroundsEstiAvHPD::operator()( const Relations::TrackToSegments::Vector& tkToSegs,
                                       const TrackPIDHypos& tkHypos,
                                       const LHCb::RichTrackSegment::Vector& segments,
                                       const GeomEffsPerPDVector& geomEffsPerPD,
                                       const PhotonYields::Vector& detYieldsV,
                                       const Rich::PDPixelCluster::Vector& clusters ) const
{
  // the backgrounds to return. Initialize to 0
  PixelBackgrounds backgrounds( clusters.size(), 0.0 );

  // local cache of cluster indices
  std::vector<Rich::DAQ::HPDCopyNumber::Type> indices( clusters.size(), 0 );

  // The working data, copied from the default instance
  auto pdData = m_pdData;
 
  // Zip the segment data together
  const auto segRange = Ranges::ConstZip(segments,geomEffsPerPD,detYieldsV);

  // -----------------------------------------------------------
  // Fill the observed data
  // -----------------------------------------------------------
  for ( auto && pixData : Ranges::Zip(clusters,indices) )
  {
    // get the cluster and background value for this cluster
    const auto & cluster = std::get<0>(pixData);
    auto       & index   = std::get<1>(pixData);

    // RICH and HPD IDs
    const auto rich = cluster.rich();
    const auto side = cluster.panel();
    const auto pd   = cluster.pdID();

    // Get the PD copy number index
    index = pdCopyNumber(pd).data();

    // Get the data vector for this panel
    auto & dataV = (pdData[rich])[side];

    // Sanity check
    if ( index < dataV.size() )
    {
      // the working data object for this HPD
      auto & data = dataV[index];
      // count the number of hits in each HPD, in each RICH
      ++(data.obsSignal);
    }
    else
    {
      Error( "Pixel : PD index " + std::to_string(index) + " out of range !!" ).ignore();
    }

  }

  // -----------------------------------------------------------
  // Now the expected signals, based on the track information
  // loop over tracks (via the hypo values)
  // -----------------------------------------------------------
  if ( !m_ignoreExpSignal )
  {
    for ( const auto && tk : Ranges::ConstZip(tkToSegs,tkHypos) )
    {
      // get the track data
      const auto & tkRels = std::get<0>(tk);
      const auto & tkHypo = std::get<1>(tk);

      //_ri_debug << " -> Track " << tkRels.tkKey << " " << tkHypo << endmsg;
      
      // Loop over the segments for this track
      for ( const auto & iSeg : tkRels.segmentIndices )
      {
        // the segment data tuple
        const auto && segData = segRange[iSeg];
        // extract from the tuple
        const auto & segment  = std::get<0>(segData);
        const auto & geomEffs = std::get<1>(segData);
        const auto & detYield = std::get<2>(segData);
        
        // which RICH
        const auto rich = segment.rich();

        //_ri_debug << "  -> Segment " << iSeg << " " << rich
        //          << " " << segment.radiator()
        //          << " DetPhots=" << detYield[tkHypo] << endmsg;
        
        // Loop over the per PD geom. effs. for this track hypo
        for ( const auto & PD : geomEffs[tkHypo] )
        {
          // expected signal for this PD
          const auto sig = detYield[tkHypo] * PD.second; 
          // HPD ID
          const LHCb::RichSmartID pdID(PD.first);
          // index
          const auto index = pdCopyNumber(pdID).data();
          // panel data vector
          auto & dataV = (pdData[rich])[pdID.panel()];
          // Update the HPD data map with this value
          if ( index < dataV.size() )
          {
            auto & data = dataV[index];
            data.expSignal += sig;
            //_ri_debug << "   -> " << LHCb::RichSmartID(PD.first) << " DetPhots=" << sig << endmsg;
          }
          else
          {
            Error( "Track : PD index " + std::to_string(index) + " out of range !!" ).ignore();
          }
        }        
      }
    }
  }

  // -----------------------------------------------------------
  // Now compute the background terms
  // -----------------------------------------------------------
  
  // Obtain background term PD by PD
  for ( const auto rich : Rich::detectors() )
  {
    //_ri_debug << "Computing HPD backgrounds in " << rich << endmsg;
    
    int iter = 1;
    bool cont = true;
    double rnorm = 0.0;
    while ( cont )
    {
      //_ri_debug << " -> Iteration " << iter << endmsg;
      
      int nBelow(0), nAbove(0);
      double tBelow = 0.0;
      // loop over panels
      for ( auto& panelData : pdData[rich] )
      {
        // Loop over PD in this panel
        for ( auto& iPD : panelData )
        {
          // Only process PDs with observed hits
          if ( iPD.obsSignal > 0 )
          {
            
            // The background for this PD
            auto & bkg = iPD.expBackgrd;
            
            if ( 1 == iter )
            {
              // First iteration, just set background for this HPD to the difference
              // between the observed and and expected number of hits in the HPD
              //_ri_debug << "  -> HPD " << pd << " obs. = " << obs << " exp. = " << exp << endmsg;
              bkg = (double)iPD.obsSignal - iPD.expSignal;
            }
            else
            {
              // For additional interations apply the normalisation factor
              bkg = ( bkg > 0 ? bkg-rnorm : 0 );
            }
            
            if ( bkg < 0.0 )
            {
              // Count the number of HPDs below expectation for this iteration
              ++nBelow;
              // save the total amount below expectation
              tBelow += fabs( bkg );
            }
            else if ( bkg > 0.0 )
            {
              // count the number of HPDs above expectation
              ++nAbove;
            }
            
          } // with observed hits
          
        } // end loop over signal PDs
      } // end loop over panels
      
      //_ri_debug << "  -> Above = " << nAbove << " Below = " << nBelow << endmsg;
      
      if ( nBelow > 0 && nAbove > 0 )
      {
        // we have some HPDs above and below expectation
        // calculate the amount of signal below per above HPD
        rnorm = tBelow / ( static_cast<double>(nAbove) );
        //_ri_debug << "   -> Correction factor per HPD above = " << rnorm << endmsg;
      }
      else
      {
        //_ri_debug << "  -> Aborting iterations" << endmsg;
        cont = false;
      }
      
      // Final protection against infinite loops
      if ( ++iter > m_maxBkgIterations ) { cont = false; }

    } // while loop

  } // end rich loop

  // -----------------------------------------------------------
  // Normalise the PD backgrounds
  // -----------------------------------------------------------

  // Loop over the RICH data maps
  for ( auto & richData : pdData )
  {
    // loop over panels
    for ( auto & panel : richData )
    {
      // Loop over the PD data objects
      for ( auto & pd : panel )
      {
        if ( pd.obsSignal > 0 && pd.dePD )
        {
          // normalise background for this HPD
          pd.expBackgrd = 
            std::max( ( pd.expBackgrd > 0 ? 
                        pd.expBackgrd/pd.dePD->effectiveNumActivePixels() : 0 ),
                      m_minPixBkg.value() );
        }
      }
    }
  }

  // -----------------------------------------------------------
  // Fill the background values into the output data structure
  // -----------------------------------------------------------

  for ( auto && pixData : Ranges::Zip(clusters,backgrounds,indices) )
  {
    // get the cluster and background value for this cluster
    const auto & cluster = std::get<0>(pixData);
    auto       & bkg     = std::get<1>(pixData);
    const auto & index   = std::get<2>(pixData);

    // RICH flags
    const auto rich = cluster.rich();
    const auto side = cluster.panel();

    // get the panel data vector
    auto & dataV = (pdData[rich])[side];

    // get the data object for this PD
    if ( index < dataV.size() )
    {
      auto & data = dataV[index];
      // update the pixel background
      bkg = data.expBackgrd;
    }
    else
    {
      Error( "Bkg : PD index " + std::to_string(index) + " out of range !!" ).ignore();
      bkg = 0;
    }
  }
  
  return backgrounds;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( PixelBackgroundsEstiAvHPD )

//=============================================================================
