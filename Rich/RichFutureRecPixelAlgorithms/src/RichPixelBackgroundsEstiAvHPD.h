
#pragma once

// STL
#include <algorithm>
#include <vector>
#include <ostream>

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecPixelBackgrounds.h"
#include "RichFutureRecEvent/RichRecTrackPIDInfo.h"
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"
#include "RichFutureRecEvent/RichRecRelations.h"

// Rich Utils
#include "RichUtils/ZipRange.h"
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichMap.h"

// RicDet
#include "RichDet/DeRich1.h"
#include "RichDet/DeRichSystem.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

      /** @class PixelBackgroundsEstiAvHPD RichPixelBackgroundsEstiAvHPD.h
       *
       *  Computes an estimate of the background contribution to each pixel
       *  given the set of track mass hypotheses.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */
      class PixelBackgroundsEstiAvHPD final :
        public Transformer< PixelBackgrounds( const Relations::TrackToSegments::Vector&,
                                              const TrackPIDHypos&,
                                              const LHCb::RichTrackSegment::Vector&,
                                              const GeomEffsPerPDVector&,
                                              const PhotonYields::Vector&,
                                              const Rich::PDPixelCluster::Vector& ),
                            Traits::BaseClass_t<AlgBase> >
      {

      public:

        /// Standard constructor
        PixelBackgroundsEstiAvHPD( const std::string& name, ISvcLocator* pSvcLocator );

        /// Initialization after creation
        StatusCode initialize() override;

      public:

        /// Algorithm execution via transform
        PixelBackgrounds operator()( const Relations::TrackToSegments::Vector& tkToSegs,
                                     const TrackPIDHypos& tkHypos,
                                     const LHCb::RichTrackSegment::Vector& segments,
                                     const GeomEffsPerPDVector& geomEffsPerPD,
                                     const PhotonYields::Vector& detYieldsV,
                                     const Rich::PDPixelCluster::Vector& clusters ) const override;

      private: // definitions

        /// Data values for a single PD
        struct PDData
        {
          unsigned int obsSignal{0};  ///< Number of observed hits in this PD
          double expSignal{0};        ///< Expected signal in this PD
          double expBackgrd{0};       ///< Expected background in this PD
          const DeRichPD * dePD{nullptr};     ///< The PD detector element
          using Vector = std::vector<PDData>; ///< Vector type
        };

      private : // methods

        /// Gets the per RICH index for the given PD ID
        decltype(auto) pdCopyNumber( const LHCb::RichSmartID pdID ) const
        {
          // Panel index
          return m_richSys->dePDPanel(pdID)->pdNumber(pdID);
        }

        /// Get the max Index value for each panel
        decltype(auto) maxPdIndex( const Rich::DetectorType rich,
                                   const Rich::Side side ) const
        {
          // Need to add one to handle fact for MaPMTs numbering scheme starts at 1...
          return m_richSys->dePDPanel(rich,side)->maxPdNumber().data() + 1;
        }

        /// Get the DePD object
        inline const DeRichPD * dePD( const Rich::DetectorType rich,
                                      const Rich::Side side  ,
                                      Rich::DAQ::PDPanelIndex copyN ) const
        {
          return m_richSys->dePDPanel(rich,side)->dePD(copyN);
        }

        /// Get the DePD object
        inline const DeRichPD * dePD( LHCb::RichSmartID pdID,
                                      Rich::DAQ::PDPanelIndex copyN ) const
        {
          return dePD(pdID.rich(),pdID.panel(),copyN);
        }

      private : // data

        /// Maximum number of iterations in background normalisation
        Gaudi::Property<int> m_maxBkgIterations 
        { this, "MaxBackgroundNormIterations", 10,
            "Maximum number of iterations in background normalisation" };

        /** Minimum pixel background value.
         *  Any value below this will be set to this value */
        Gaudi::Property<double> m_minPixBkg
        { this, "MinPixelBackground", 0, "Minimum pixel background" };

        /** Ignore the expected signal when computing the background terms.
            Effectively, will assume all observed hists are background */
        Gaudi::Property<bool> m_ignoreExpSignal
        { this, "IgnoreExpectedSignals", false,
            "Ignore expectations when calculating backgrounds" };

        /// Rich System detector element
        const DeRichSystem * m_richSys = nullptr;

        /// Default working data object
        DetectorArray< PanelArray<PDData::Vector> > m_pdData;

      };

    }
  }
}
