
//---------------------------------------------------------------------------
/** @file RichPhotonGeomMonitor.h
 *
 *  Header file for algorithm class : Rich::rec::MC::PhotonGeomMonitor
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   05/04/2002
 */
//---------------------------------------------------------------------------

#ifndef RICHRECMONITOR_RichPhotonGeomMonitor_H
#define RICHRECMONITOR_RichPhotonGeomMonitor_H 1

// base class
#include "RichRecBase/RichRecHistoAlgBase.h"

// Event
#include "Event/MCRichOpticalPhoton.h"

// Interfaces
#include "RichRecInterfaces/IRichTrackSelector.h"
#include "MCInterfaces/IRichRecMCTruthTool.h"
#include "RichRecInterfaces/IRichCherenkovAngle.h"
#include "RichRecInterfaces/IRichRecGeomTool.h"

// temporary histogramming numbers
#include "RichRecUtils/RichDetParams.h"

namespace Rich
{
  namespace Rec
  {
    namespace MC
    {

      //---------------------------------------------------------------------------
      /** @class PhotonGeomMonitor RichPhotonGeomMonitor.h
       *
       *  Monitor the general reconstructed photons properties
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   05/04/2002
       */
      //---------------------------------------------------------------------------

      class PhotonGeomMonitor final : public Rich::Rec::HistoAlgBase
      {

      public:

        /// Standard constructor
        PhotonGeomMonitor( const std::string& name,
                           ISvcLocator* pSvcLocator );

        StatusCode initialize() override;    // Algorithm initialization
        StatusCode execute() override;       // Algorithm execution

      private: // data

        /// Pointer to RichRecMCTruthTool interface
        const Rich::Rec::MC::IMCTruthTool* m_richRecMCTruth = nullptr;

        /// Rich Cherenkov angle calculator tool
        const ICherenkovAngle * m_ckAngle = nullptr;         

        /// Simple geometrical questions
        const IGeomTool * m_geomTool = nullptr;         

        /// Track selector
        const ITrackSelector * m_trSelector = nullptr; 

      };

    }
  }
}

#endif // RICHRECMONITOR_RichPhotonGeomMonitor_H
