
## @package SegmentCreator
#  RichRecSegment creator options for RICH Reconstruction
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   15/08/2008

__version__ = "$Id: SegmentCreator.py,v 1.5 2009-09-16 13:37:28 jonrob Exp $"
__author__  = "Chris Jones <Christopher.Rob.Jones@cern.ch>"

from RichKernel.Configuration import *
from Configurables import ( Rich__Rec__SegmentCreator )

# ----------------------------------------------------------------------------------

## @class RichSegmentCreatorConf
#  RichRecSegment creator options for RICH Reconstruction
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   15/08/2008
class RichSegmentCreatorConf(RichConfigurableUser):

    ## Steering options
    __slots__ = {
        "Context"     : "Offline"  # The context within which to run
       ,"OutputLevel" : INFO    # The output level to set all algorithms and tools to use
        }

    ## Initialize 
    def initialize(self):
        
        pass

    ## @brief Apply the configuration
    def applyConf(self):
        
        context = self.getProp("Context")

        nickname = "RichSegmentCreator"
        
        segCreator = Rich__Rec__SegmentCreator( "ToolSvc."+context+"_"+nickname )

        if self.isPropertySet("OutputLevel") :
            segCreator.OutputLevel = self.getProp("OutputLevel")
