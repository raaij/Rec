#ifndef RECINIT_H
#define RECINIT_H 1

// Include files
// from LHCbKernel
#include "Kernel/LbAppInit.h"

class IGenericTool;
class IIncidentSvc;

/** @class RecInit RecInit.h
 *  Algorithm to initialize the reconstruction sequence
 *  Creates RecHeader and ProcStatus
 *
 *  @author Marco Cattaneo
 *  @date   2006-03-14
 */
class RecInit final : public LbAppInit
{
public:

  /// Standard constructor
  using LbAppInit::LbAppInit;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:

  IGenericTool* m_memoryTool = nullptr;   ///< Pointer to (private) memory histogram tool
  IIncidentSvc* m_incidentSvc = nullptr;  ///< Pointer to the incident service.

  /// Location in the transient store of the RawEvent object.
  /// @warning Obsolete: use m_rawEventLocations
  Gaudi::Property<std::string> m_rawEventLocation
  { this, "RawEventLocation",  "", "OBSOLETE. Use RawEventLocations instead" };

  /// List of locations in the transient store to search the RawEvent object.
  Gaudi::Property<std::vector<std::string>> m_rawEventLocations
  { this, "RawEventLocations", { },
    "List of possible locations of the RawEvent object in the"
    " transient store. By default it is LHCb::RawEventLocation::Calo,"
    " LHCb::RawEventLocation::Default."};

  Gaudi::Property<bool> m_abortOnFID  ///< If I can't find the raw file ID, do I abort?
  { this, "AbortOnFID", true, "If I can't find the raw file ID, do I abort? Default true=yes."};

};
#endif // RECINIT_H
