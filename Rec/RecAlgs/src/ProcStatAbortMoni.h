#ifndef RECALGS_PROCSTATABORTMONI_H
#define RECALGS_PROCSTATABORTMONI_H 1

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

// GaudiUtils
#include "GaudiUtils/HistoLabels.h"

// Event model
#include "Event/ProcStatus.h"

// AIDA
#include "AIDA/IProfile1D.h"

/** @class ProcStatAbortMoni ProcStatAbortMoni.h
 *
 *  Monitor for abort rates in ProcStat
 *
 *  @author Chris Jones
 *  @date   2010-07-16
 */

class ProcStatAbortMoni final : public GaudiHistoAlg
{
public:

  /// Standard constructor
  using GaudiHistoAlg::GaudiHistoAlg;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;   ///< Algorithm execution

private:

  /// List of subsystems
  Gaudi::Property<std::vector<std::string>> m_subSystems
  { this,  "SubSystems", { "Overall","Hlt","VELO","TT","IT","OT",
                           "Tracking","Vertex", "RICH","CALO",
                           "MUON","PROTO" } };

  /// Location of processing status object in TES
  Gaudi::Property<std::string> m_procStatLocation
  { this, "ProcStatusLocation", LHCb::ProcStatusLocation::Default };

  /// cache the histogram pointer
  AIDA::IProfile1D * m_h = nullptr;

};

#endif // RECALGS_PROCSTATABORTMONI_H
