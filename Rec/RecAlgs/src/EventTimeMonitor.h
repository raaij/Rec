#ifndef EVENTTIMEMONITOR_H
#define EVENTTIMEMONITOR_H 1

// STL
#include <array>

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"


/** @class EventTimeMonitor EventTimeMonitor.h
 *
 *  Creates histograms of event time
 *
 *  @author Patrick Koppenburg
 *  @date   2012-04-19
 */
class EventTimeMonitor final : public GaudiHistoAlg
{

public:

  /// Standard constructor
  using GaudiHistoAlg::GaudiHistoAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  /// the histogram definition (as property)
  Gaudi::Property<Gaudi::Histo1DDef> m_histoS  {this, "SecondsPlot", { "GPS Seconds" ,  0 , 3600., 3600 }, "The parameters of 'delta memory' histogram" } ;           ///< the histogram definition (as property)
  Gaudi::Histo1DDef m_histoH  { "GPS Hours" , -0.5 , 23.5,   24 } ;           ///< the histogram definition (as property)
  Gaudi::Histo1DDef m_histoD  { "GPS Days" ,  0.5 , 365.5, 366 }  ;           ///< the histogram definition (as property)
  Gaudi::Histo1DDef m_histoY  { "GPS Year" , 2008.5 , 2028.5, 20 };           ///< the histogram definition (as property)
  AIDA::IHistogram1D* m_plotS = nullptr; ///< the histogram of seconds
  AIDA::IHistogram1D* m_plotH = nullptr; ///< the histogram of hours
  AIDA::IHistogram1D* m_plotD = nullptr; ///< the histogram of day of year
  AIDA::IHistogram1D* m_plotY = nullptr; ///< the histogram of year

  /// month offsets. Isn't there a method doing that?
  const std::array<unsigned int,12> m_moffsets{{ 0,31,60,91,121,152,182,213,244,274,305,335 }};

};

#endif // EVENTTIMEMONITOR_H
