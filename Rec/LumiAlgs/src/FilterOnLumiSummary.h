#ifndef FILTERONLUMISUMMARY_H
#define FILTERONLUMISUMMARY_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// event model
#include "Event/HltLumiSummary.h"

/** @class FilterOnLumiSummary FilterOnLumiSummary.h
 *
 *
 *  @author Jaap Panman
 *  @date   2010-01-29
 */
class FilterOnLumiSummary : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

private:

  Gaudi::Property<std::string> m_DataName { this, "InputDataContainer", LHCb::HltLumiSummaryLocation::Default }; // input location of summary data
  Gaudi::Property<std::string> m_ValueName { this, "ValueName"};                                                 // value required
  Gaudi::Property<std::string> m_CounterName { this, "CounterName", "Method" };                                  // counter looked at
  int m_Counter = 0;                  // int value required
  int m_Value = 0;			          // int counter looked at
};
#endif // FILTERONLUMISUMMARY_H
