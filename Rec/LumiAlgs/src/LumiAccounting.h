#ifndef LUMIACCOUNTING_H
#define LUMIACCOUNTING_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// for DB
#include "GaudiKernel/IDetDataSvc.h"

// event model
#include "Event/HltLumiSummary.h"
#include "Event/LumiFSR.h"


/** @class LumiAccounting LumiAccounting.h
 *
 *
 *  @author Jaap Panman
 *  @date   2009-01-19
 */
class LumiAccounting : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization


protected:
  virtual StatusCode registerDB();    ///< register DB conditions
private:
  StatusCode i_cacheThresholdData();  ///< Function extracting data from Condition

protected:
  /// Reference to file records data service
  IDataProviderSvc* m_fileRecordSvc = nullptr;

  LHCb::LumiFSRs* m_lumiFSRs = nullptr;     ///< TDS container
  LHCb::LumiFSR* m_lumiFSR = nullptr;       ///< FSR for current file

  // database conditions and calibration factors
  SmartIF<IDetDataSvc> m_dds;                   ///< DetectorDataSvc
  Condition *m_condThresholds = nullptr;        ///< Condition for relative calibration
  std::vector<double> m_calibThresholds;        ///< relative calibration factors
  int m_statusThresholds = 0;                   ///<

  std::string m_current_fname;        		///< current file ID string
  int         m_count_files = 0;      		///< number of files read

  Gaudi::Property<std::string> m_rawEventLocation { this, "RawEventLocation", LHCb::RawEventLocation::Default };    		///< Location where we get the RawEvent
  Gaudi::Property<std::string> m_DataName { this, "InputDataContainer", LHCb::HltLumiSummaryLocation::Default };          		///< input location of summary data
  // it is assumed that we are only called for a single BXType and that the
  // output data container gets this name
  Gaudi::Property<std::string> m_FSRName { this, "OutputDataContainer", LHCb::LumiFSRLocation::Default }; ///< output location of summary data in FSR

};
#endif // LUMIACCOUNTING_H
