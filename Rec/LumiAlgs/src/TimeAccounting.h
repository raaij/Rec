#ifndef TIMEACCOUNTING_H
#define TIMEACCOUNTING_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// event model
#include "Event/TimeSpanFSR.h"


/** @class TimeAccounting TimeAccounting.h
 *
 *
 *  @author Jaap Panman
 *  @date   2009-01-19
 */
class TimeAccounting : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

private:
  /// Reference to file records data service
  SmartIF<IDataProviderSvc> m_fileRecordSvc;

  Gaudi::Property<std::string> m_rawEventLocation { this, "RawEventLocation", LHCb::RawEventLocation::Default };    // Location where we get the RawEvent
  Gaudi::Property<std::string> m_FSRName{ this, "OutputDataContainer", LHCb::TimeSpanFSRLocation::Default };              // output location of summary data in FSR

  LHCb::TimeSpanFSRs* m_timeSpanFSRs = nullptr; // TDS container
  LHCb::TimeSpanFSR* m_timeSpanFSR = nullptr;   // FSR for current file

  std::string m_current_fname;        // current file ID string
  int         m_count_files = 0 ;     // number of files read

};
#endif // TIMEACCOUNTING_H
