#ifndef FILTERFILLINGSCHEME_H
#define FILTERFILLINGSCHEME_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class FilterFillingScheme FilterFillingScheme.h
 *
 *
 *  @author Jaap Panman
 *  @date   2011-08-09
 */
class FilterFillingScheme : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization

protected:
  virtual StatusCode registerDB();    ///< register DB conditions
  virtual bool processDB();           ///< DB checking code

private:
  StatusCode i_cacheFillingData();    ///< Function extracting data from Condition
  StatusCode i_cacheMagnetData();     ///< Function extracting data from Condition

protected:

  Gaudi::Property<std::string> m_beam { this, "Beam", "0" };     ///< Beam looked at
  Gaudi::Property<std::string> m_MagnetState { this, "MagnetState", "NONE" };     ///< Magnet state looked at (if empty: all)
  Gaudi::Property<int> m_BXOffset { this, "BXOffset", 0 };       ///< bcid offset imspected
  long m_bxid = 0;                        ///< BX id from ODIN

  // database conditions and calibration factors
  SmartIF<IDetDataSvc> m_dds;         ///< DetectorDataSvc

  Condition *m_condMagnet = nullptr;            ///< Condition for magnet
  std::string m_parMagnetState;       ///< magnet state (UP/DOWN)

  Condition *m_condFilling = nullptr;           ///< Condition for LHC filling scheme
  std::string m_B1FillingScheme;      ///< filled bunches 00101010....
  std::string m_B2FillingScheme;      ///< filled bunches 00101010....

};
#endif // FILTERFILLINGSCHEME_H
