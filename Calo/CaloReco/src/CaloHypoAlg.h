#ifndef CaloReco_CaloHypoAlg_H 
#define CaloReco_CaloHypoAlg_H 1
// Include files
// from STL
#include <string>
#include <vector>

#include "CaloInterfaces/ICounterLevel.h"
#include "GaudiAlg/GaudiAlgorithm.h"
// forward declaration
struct ICaloHypoTool ;

/** @class CaloHypoAlg CaloHypoAlg.h
 *  
 *  Generic CaloHypo Algorithm.
 *  It is just applies a set of ICaloHypoTools 
 *  to a container of CaloHypo objects
 *  @see ICaloHypoTool
 *  @see  CaloHypo
 *  @see  CaloAlgorithm
 *  @see      Algorithm
 *  @see     IAlgorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/09/2002
 */

class CaloHypoAlg : public GaudiAlgorithm 
{
  /// friend factory for instantiation
  friend class AlgFactory<CaloHypoAlg>;

public:
  
  /** standard algorithm initialization 
   *  @see CaloAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code 
   */
  StatusCode initialize() override;   
  
  /** standard algorithm execution 
   *  @see CaloAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code 
   */
  StatusCode execute() override;   
  
  /** standard algorithm finalization 
   *  @see CaloAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code 
   */
  StatusCode finalize() override;   
  
protected:
  
  /** Standard constructor
   *  @param   name   algorithm name 
   *  @param   svcloc pointer to service locator 
   */
  CaloHypoAlg( const std::string& name   , 
              ISvcLocator*       svcloc );
  
private:

  /// default  construstor  is  private 
  CaloHypoAlg(); 
  /// copy     construstor  is  private 
  CaloHypoAlg
  ( const CaloHypoAlg& );
  /// assignement operator  is  private 
  CaloHypoAlg& operator=
  ( const CaloHypoAlg& );

private:
  
  typedef std::vector<std::string>    Names ;
  typedef std::vector<ICaloHypoTool*> Tools ;
  
  /// list of tool type/names 
  Gaudi::Property<Names> m_names 
    {this, "Tools", {}, "The list of generic Hypo-tools to be applied"};
    
  /// list of tools 
  Tools   m_tools ;
  Gaudi::Property<std::string> m_inputData {this, "InputData"};
  Gaudi::Property<std::string> m_type      {this, "HypoType"};
  ICounterLevel* counterStat = nullptr;
};
// ============================================================================
#endif // CaloHypoAlg_H
