#ifndef CALORECO_CALOElectronALG_H 
#define CALORECO_CALOElectronALG_H 1
// Include files
// from STL
#include <string>
#include <memory>
// from GaudiAlg
#include "CaloDet/DeCalorimeter.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloInterfaces/ICounterLevel.h"
// forward delcarations 
struct ICaloClusterSelector ;
struct ICaloHypoTool        ;

/** @class CaloElectronAlg CaloElectronAlg.h
 *  
 *  The simplest algorithm of reconstruction of 
 *  electrons in electromagnetic calorimeter.
 *
 *  @author Vanya Belyaev      Ivan.Belyaev@itep.ru
 *  @date   31/03/2002
 */
class CaloElectronAlg : public GaudiAlgorithm
{  
  /// friend factory for instantiation 
  friend class AlgFactory<CaloElectronAlg>; 
public:
  
  /// container of names
  typedef std::vector<std::string>           Names       ;
  /// container of selector tools 
  typedef std::vector<ICaloClusterSelector*> Selectors   ;
  /// containers of hypo tools 
  typedef std::vector<ICaloHypoTool*>        HypoTools   ;
  /// container of correction tools (S-,L-,...)
  typedef HypoTools                          Corrections ;
  
public:

  /**  standard Algorithm initialization
   *   @see CaloAlgorithm
   *   @see     Algorithm
   *   @see    IAlgorithm
   *   @return status code 
   */
  StatusCode initialize() override;  
  
  /**  standard Algorithm execution
   *   @see     Algorithm
   *   @see    IAlgorithm
   *   @return status code 
   */
  StatusCode execute() override;  
  
  /**  standard Algorithm finalization
   *   @see CaloAlgorithm
   *   @see     Algorithm
   *   @see    IAlgorithm
   *   @return status code 
   */
  StatusCode finalize() override;
  
protected:
  
  /** Standard constructor
   *  @param name algorithm name 
   *  @param pSvc service locator 
   */
  CaloElectronAlg
  ( const std::string&  name , 
    ISvcLocator*        pSvc );
  
private:
  
  // cluster selectors 
  Selectors              m_selectors;
  Gaudi::Property<Names> m_selectorsTypeNames {this, "SelectionTools", {
    "CaloSelectCluster/ElectronCluster",
    "CaloSelectChargedClusterWithSpd/ChargedWithSpd",
    "CaloSelectClusterWithPrs/ClusterWithPrs",
    "CaloSelectorNOT/ChargedWithTracks"
  }, "List of Cluster selector tools"};
  
  // corrections
  Corrections            m_corrections;
  Gaudi::Property<Names> m_correctionsTypeNames 
    {this, "CorrectionTools", {}, "List of primary correction tools"};
  
  // other hypo tools 
  HypoTools              m_hypotools;
  Gaudi::Property<Names> m_hypotoolsTypeNames 
    {this, "HypoTools", {"CaloExraDigits/SpdPrsExtraE"}, 
    "List of generic Hypo-tools to apply for newly created hypos"};

  // corrections
  Corrections            m_corrections2;
  Gaudi::Property<Names> m_correctionsTypeNames2 {this, "CorrectionTools2", { 
    "CaloECorrection/ECorrection" ,
    "CaloSCorrection/SCorrection" ,
    "CaloLCorrection/LCorrection" 
  }, "List of tools for 'fine-corrections"};
  
  // other hypo tools 
  HypoTools    m_hypotools2;
  Gaudi::Property<Names> m_hypotoolsTypeNames2 
    {this, "HypoTools2", {}, 
    "List of generi Hypo-tools to apply for corrected hypos"};

  Gaudi::Property<std::string> m_inputData  {this, "InputData" };
  Gaudi::Property<std::string> m_outputData {this, "OutputData"};
  Gaudi::Property<std::string> m_detData    {this, "Detector"  };
  const DeCalorimeter*  m_det = nullptr;
  Gaudi::Property<float> m_eTcut {this, "EtCut", 0, "Threshold on cluster & hypo ET"};
  ICounterLevel* counterStat = nullptr;
};

// ============================================================================
#endif // CALOElectronALG_H
