// ============================================================================
#include "CaloSelectClusterWithSpd.h"
// ============================================================================
/** @class CaloSelectChargedClusterWithSpd
 *  Simple seleclton of newural clusters based on Spd information 
 *  @author Olivier Deschamps 
 *  @author Vanya BELYAEV 
 */
// ============================================================================

class CaloSelectChargedClusterWithSpd : public CaloSelectClusterWithSpd 
{
  // ==========================================================================
  /// friend factory for instantiation
  friend struct ToolFactory<CaloSelectChargedClusterWithSpd>;
  // ==========================================================================
public:
  // ==========================================================================
  bool select ( const LHCb::CaloCluster* cluster ) const override
  { return (*this) ( cluster ) ; }
  // ==========================================================================
  bool operator()( const LHCb::CaloCluster* cluster ) const override
  {
    if ( 0 == cluster ) 
    {
      Warning ( "CaloCluster* points to NULL, return false" );  
      return false ;                                                  // RETURN 
    }
    //
    bool sel = cut() < n_hit ( *cluster ) ;
    if(counterStat->isVerbose())counter("selected clusters") += (int) sel;
    return sel;
  }
  // ==========================================================================
protected:
  // ==========================================================================
  /// constructor 
  CaloSelectChargedClusterWithSpd
  ( const std::string& type   , 
    const std::string& name   ,
    const IInterface*  parent )
    : CaloSelectClusterWithSpd ( type , name , parent )
  {}
  // ==========================================================================
};
// ============================================================================
DECLARE_TOOL_FACTORY( CaloSelectChargedClusterWithSpd )
// ============================================================================
