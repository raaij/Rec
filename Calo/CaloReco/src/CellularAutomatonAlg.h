#ifndef CELLULARAUTOMATONALG_H 
#define CELLULARAUTOMATONALG_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

#include "CaloInterfaces/ICaloClusterization.h"
#include "CaloInterfaces/ICounterLevel.h"

/** @class CellularAutomatonAlg CellularAutomatonAlg.h
 *  
 *
 *  @author Victor Egorychev
 *  @date   2008-04-03
 */
class CellularAutomatonAlg : public GaudiAlgorithm {

public: 
  /// Standard constructor
  CellularAutomatonAlg( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization

private:
  Gaudi::Property<std::string> m_inputData  {this, "InputData" , LHCb::CaloDigitLocation::Ecal};
  Gaudi::Property<std::string> m_outputData {this, "OutputData", LHCb::CaloClusterLocation::Ecal};
  Gaudi::Property<std::string> m_detData    {this, "Detector"  , DeCalorimeterLocation::Ecal};
  const DeCalorimeter* m_detector = nullptr;

  Gaudi::Property<bool> m_sort     {this, "Sort"    , true};
  Gaudi::Property<bool> m_sortByET {this, "SortByET", false};
  
  Gaudi::Property<std::string> m_toolName {this, "Tool", "CaloClusterizationTool"};
  ICaloClusterization* m_tool = nullptr;
  ICounterLevel* counterStat = nullptr;
  
  Gaudi::Property<unsigned int> m_neig_level {this, "Level", 0};
  unsigned long m_passMin = 999999;
  unsigned long m_passMax = 0;
  double m_pass = 0.;
  double m_clus = 0.; 
  double m_event= 0.;
};
#endif // CELLULARAUTOMATONALG_H
