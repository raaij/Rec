// $Id: CaloExtraDigits.h,v 1.8 2009-04-16 16:10:11 odescham Exp $
// ============================================================================
#ifndef CALORECO_CALOEXTRADIGITS_H
#define CALORECO_CALOEXTRADIGITS_H 1
// Include files
// from STL
#include <string>
// Kernel
#include "GaudiAlg/GaudiTool.h"
#include "CaloInterfaces/ICaloHypoTool.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloInterfaces/ICounterLevel.h"

/** @class CaloExtraDigits CaloExtraDigits.h
 *
 *
 *  @author Vanya Belyaev Ivan Belyaev
 *  @date   31/03/2002
 */
class CaloExtraDigits :
  public virtual     ICaloHypoTool ,
  public                  GaudiTool
{
  /// friend factory for instantiation
  friend struct ToolFactory<CaloExtraDigits>;

public:

  StatusCode initialize() override;
  StatusCode process    ( LHCb::CaloHypo* hypo  ) const override;
  StatusCode operator() ( LHCb::CaloHypo* hypo  ) const override;

protected:

  CaloExtraDigits( const std::string& type,
                   const std::string& name,
                   const IInterface* parent);

private:

  /// default constructor is private
  CaloExtraDigits();
  /// copy constructor is private
  CaloExtraDigits( const CaloExtraDigits& );
  /// assignement operator is private
  CaloExtraDigits& operator=( const CaloExtraDigits& );

private:

  Gaudi::Property<std::vector<std::string>> m_toDet {this, "ExtraDigitFrom"};
  std::map<std::string,ICaloHypo2Calo*> m_toCalo;
  Gaudi::Property<std::string> m_det {this, "Detector", "Ecal"};
  ICounterLevel* counterStat = nullptr;
};
#endif // CALOEXTRADIGITS_H
