#ifndef CALORECO_CALOSELECTOR_H
#define CALORECO_CALOSELECTOR_H 1
// Include files
// from STL
#include <string>
// from GaudiAlg
#include "GaudiAlg/GaudiTool.h"
// from CaloInterfaces
#include "CaloInterfaces/ICaloClusterSelector.h"
// forward declaratiosn
struct ICaloLikelihood ; // from CaloInterfaces ;

/** @class CaloSelector CaloSelector.h
 *
 *  Concrete cluster-selector tool, to select CaloClusters
 *  which satisfy the given calorimeter hypothesis
 *  Tool in turn uses the tool with ICaloLikelihood
 *  interaface to calculate the likelihood.
 *  @see ICaloClusterSelector
 *  @see ICaloLikelyhood
 *  @see CaloCluster
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   31/03/2002
 */
class CaloSelector :
  public virtual   ICaloClusterSelector ,
  public                       GaudiTool
{
  /// friend factory for instantiation`
  friend struct ToolFactory<CaloSelector>;
public:

  /** "select"/"preselect" method
   *  @see ICaloClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select     ( const LHCb::CaloCluster* cluster ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @see ICaloClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator() ( const LHCb::CaloCluster* cluster ) const override;

  /** standard initialization of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode initialize() override;

  /** standard finalization  of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode finalize() override;

protected:

  /** Standard constructor
   *  @param    type   tool type (?)
   *  @param    name   tool name
   *  @param    parent tool parent
   */
  CaloSelector
  ( const std::string&  type   ,
    const std::string&  name   ,
    const IInterface*   parent );

private:

  /// default constructor is private
  CaloSelector();
  /// copy constructor is private
  CaloSelector
  ( const CaloSelector& );
  /// assignement operator is private
  CaloSelector& operator=
  ( const CaloSelector& );

private:

  // type of Likelyhood tool to be used
  Gaudi::Property<std::string> m_lhType 
    {this, "LikelihoodType", "", "type of Likelyhood tool to be used"};

  // name of Likelyhood tool to be used
  Gaudi::Property<std::string> m_lhName 
    {this, "LikelihoodName", "", "name of Likelyhood tool to be used"};

  // Likelyhood tool to be used
  ICaloLikelihood* m_likelihood = nullptr;
  
  // cut on likelyhood
  Gaudi::Property<double> m_cut {this, "LikelihoodCut", 1.e+50, "cut on likelyhood"};

};

// ============================================================================
#endif // CALORECO_CALOSELECTOR_H
// ============================================================================
