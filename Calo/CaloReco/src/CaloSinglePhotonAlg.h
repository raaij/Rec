// ============================================================================
#ifndef CALOALGS_CALOSINGLEPHOTONALG_H 
#define CALOALGS_CALOSINGLEPHOTONALG_H 1
// ============================================================================
#include <string>
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloInterfaces/ICounterLevel.h"
struct ICaloClusterSelector ;
struct ICaloHypoTool        ;

/** @class CaloSinglePhotonAlg CaloSinglePhotonAlg.h
 *  
 *  The simplest algorithm of reconstruction of 
 *  single photon in electromagnetic calorimeter.
 *  The implementation is based on F.Machefert's codes.
 *
 *  @author Frederic Machefert machefer@in2p3.fr
 *  @author Vanya Belyaev      Ivan.Belyaev@itep.ru
 *  @date   31/03/2002
 */
class CaloSinglePhotonAlg : 
  public GaudiAlgorithm
{  
  /// friend factory for instantiation 
  friend class AlgFactory<CaloSinglePhotonAlg>; 
public:
  
  /// container of names
  typedef std::vector<std::string>           Names       ;
  /// container of selector tools 
  typedef std::vector<ICaloClusterSelector*> Selectors   ;
  /// containers of hypo tools 
  typedef std::vector<ICaloHypoTool*>        HypoTools   ;
  /// container of correction tools (S-,L-,...)
  typedef HypoTools                          Corrections ;
  
public:

  StatusCode initialize() override;  
  StatusCode execute() override;  
  StatusCode finalize() override;
  
protected:
  
  CaloSinglePhotonAlg( const std::string&  name , ISvcLocator*        pSvc );  
  
private:
  
  Gaudi::Property<float> m_eTcut {this, "EtCut", 0., "Threshold on cluster & hypo ET"};

  // cluster selectors 
  Selectors    m_selectors                 ;
  Gaudi::Property<Names> m_selectorsTypeNames
    {this, "SelectionTools", {
      "CaloSelectCluster/PhotonCluster",
      "CaloSelectNeutralClusterWithTracks/NeutralCluster"
    }, "List of tools for selection of clusters"};
  
  // corrections
  Corrections  m_corrections                ;
  Gaudi::Property<Names> m_correctionsTypeNames
    {this, "CorrectionTools", {}, 
    "List of tools for primary corrections"};
  
  // other hypo tools 
  HypoTools    m_hypotools                  ;
  Gaudi::Property<Names> m_hypotoolsTypeNames 
    {this, "HypoTools", {"CaloExtraDigits/SpdPrsExtraG"}, 
    "List of generic Hypo-tools to apply to newly created hypos"};

  // corrections
  Corrections  m_corrections2               ;
  Gaudi::Property<Names> m_correctionsTypeNames2 
    {this, "CorrectionTools2", {
      "CaloECorrection/ECorrection", 
      "CaloSCorrection/SCorrection", 
      "CaloLCorrection/LCorrection",
    }, "List of tools for 'fine-corrections'"};
  
  // other hypo tools 
  HypoTools    m_hypotools2                 ;
  Gaudi::Property<Names> m_hypotoolsTypeNames2 
    {this, "HypoTools2", {}, 
    "List of generic Hypo-tools to apply to corrected hypos"};

  Gaudi::Property<std::string> m_inputData  {this, "InputData"};
  Gaudi::Property<std::string> m_outputData {this, "OutputData"};
  Gaudi::Property<std::string> m_detData    {this, "Detector"};
  const DeCalorimeter*  m_det = nullptr;
  ICounterLevel* counterStat = nullptr;
};
// ============================================================================
#endif // CALOSINGLEPHOTONALG_H
