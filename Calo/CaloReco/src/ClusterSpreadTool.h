#ifndef CALORECO_CLUSTERSPREADTOOL_H
#define CALORECO_CLUSTERSPREADTOOL_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Property.h"
// ============================================================================
// CaloInterfaces
// ============================================================================
#include  "CaloInterfaces/ICaloClusterTool.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include  "GaudiAlg/GaudiTool.h"
// ============================================================================
// CaloUtil
// ============================================================================
#include  "CaloUtils/SpreadEstimator.h"
// ============================================================================
/** @class ClusterSpreadTool ClusterSpreadTool.h
 *
 *  Concrete tool for estimation of the
 *  effective cluster size ("spread")
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   22/11/2001
 */
// ============================================================================
class ClusterSpreadTool:
  public virtual ICaloClusterTool ,
  public                 GaudiTool
{
  // ==========================================================================
  /// friend class for instantiation
  friend struct ToolFactory<ClusterSpreadTool>;
  // ==========================================================================
public:
  // ==========================================================================
  /** standard initialization method
   *  @return status code
   */
  StatusCode initialize() override;
  // ==========================================================================
  /** standard finalization method
   *  @return status code
   */
  StatusCode finalize() override;
  // ==========================================================================
  /** The main processing method
   *  @param cluster pointer to CaloCluster object to be processed
   *  @return status code
   */
  StatusCode process    ( LHCb::CaloCluster* cluster ) const override;
  // ==========================================================================
  /** The main processing method (functor interface)
   *  @param cluster pointer to CaloCluster object to be processed
   *  @return status code
   */
  StatusCode operator() ( LHCb::CaloCluster* cluster ) const override;
  // ==========================================================================
protected:
  // ==========================================================================
  /** Standard constructor
   *  @param type tool type (useless)
   *  @param name tool name
   *  @param parent pointer to parent object (service, algorithm or tool)
   */
  ClusterSpreadTool
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent );
  // ==========================================================================
private:
  // ==========================================================================
  /// default constructor   is private
  ClusterSpreadTool();
  /// copy    constructor   is private
  ClusterSpreadTool
  ( const ClusterSpreadTool& );
  /// assignement operator  is private
  ClusterSpreadTool& operator=
  ( const ClusterSpreadTool& );
  // ==========================================================================
private:
  // ==========================================================================
  SpreadEstimator    m_estimator ;
  Gaudi::Property<std::string> m_detData {this, "Detector"};
  const DeCalorimeter* m_det = nullptr;
  // ==========================================================================
};
// ============================================================================
// The End
// ============================================================================
#endif // CALORECO_CLUSTERSPREADTOOL_H
// ============================================================================
