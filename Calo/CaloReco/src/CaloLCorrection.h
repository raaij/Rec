// $Id: CaloLCorrection.h,v 1.5 2007-03-02 15:31:46 cattanem Exp $
// ============================================================================
#ifndef CALORECO_CALOLCORRECTION_H
#define CALORECO_CALOLCORRECTION_H 1

// from STL
#include <string>
#include <cmath>

// Calo
#include "CaloInterfaces/ICaloHypoTool.h"
#include "CaloCorrectionBase.h"
#include "CaloDet/DeCalorimeter.h"

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// Event
#include "Event/CaloHypo.h"

/** @class CaloLCorrection CaloLCorrection.h
 *
 *
 *  @author Deschamps Olivier
 *  @date   2003-03-10
 *  revised 2010
 */

class CaloLCorrection :
  public virtual ICaloHypoTool ,
  public              CaloCorrectionBase
{

  /// friend factory for instantiation
  friend struct ToolFactory<CaloLCorrection>;

public:

  StatusCode process    ( LHCb::CaloHypo* hypo  ) const override;
  StatusCode operator() ( LHCb::CaloHypo* hypo  ) const override;

public:

  StatusCode initialize() override;
  StatusCode finalize() override;

protected:

  /** Standard constructor
   *  @see GaudiTool
   *  @see  AlgTool
   *  @param type tool type (?)
   *  @param name tool name
   *  @param parent  tool parent
   */
  CaloLCorrection ( const std::string& type   ,
                    const std::string& name   ,
                    const IInterface*  parent ) ;

  /// destructor
  virtual ~CaloLCorrection () = default;

private:

  CaloLCorrection () ;
  CaloLCorrection           ( const CaloLCorrection& ) ;
  CaloLCorrection& operator=( const CaloLCorrection& ) ;

};
// ============================================================================
#endif // CALORECO_CALOLCORRECTION_H
