// Include files

// local
#include "CaloMoniAlg.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloMoniAlg
//
// 2008-09-03 : Olivier Deschamps
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloMoniAlg::CaloMoniAlg( const std::string& name,
                          ISvcLocator* pSvcLocator)
    : Calo2Dview(name, pSvcLocator )
{
  // Areas
  m_mcount.reserve(m_nAreas);
  m_scount.reserve(2);

  //set default detectorName
  m_detData = LHCb::CaloAlgUtils::CaloNameFromAlg( name );
  if ( m_detData == "Prs" )
    m_energyMax = 300.* Gaudi::Units::MeV;
  if ( m_detData == "Spd" )
    m_energyMax = 10.* Gaudi::Units::MeV;
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloMoniAlg::initialize() {
  StatusCode sc = Calo2Dview::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by Calo2Dview
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Initialize" << endmsg;

  counterStat = tool<ICounterLevel>("CounterLevel");
  if("" == histoTopDir()) setHistoTopDir("CaloMoniDst/");

  if( m_split && m_splitSides ){
    warning() << "Cannot split simultaneously the calo sides and areas, so far - Area splitting wins" << endmsg;
    m_splitSides=false;
  }

  return StatusCode::SUCCESS;
}
