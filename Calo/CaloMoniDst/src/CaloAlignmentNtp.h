#ifndef CALOALIGNEMENTNTP_H
#define CALOALIGNEMENTNTP_H 1

// Include files
// from Gaudi
#include "Event/ProtoParticle.h"
#include "CaloUtils/ICaloElectron.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloInterfaces/ICaloHypoEstimator.h"
#include "CaloNtpBase.h"

/** @class CaloAlignmentNtp CaloAlignmentNtp.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2009-12-11
 */
class CaloAlignmentNtp : public CaloNtpBase {
public:
  /// Standard constructor
  CaloAlignmentNtp( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  ICaloHypoEstimator* m_estimator = nullptr;
  ICaloHypoEstimator* estimator(){return m_estimator;}

  Gaudi::Property<std::pair<double,double>> m_eop   {this, "EoPFilter", {0.7, 1.3}};
  Gaudi::Property<std::pair<double,double>> m_dlle  {this, "DLLeFilter", {-9999999.,9999999.}};
  Gaudi::Property<std::pair<double,double>> m_rdlle {this, "RichDLLeFilter", {0.,99999.}};
  Gaudi::Property<std::pair<double,double>> m_bMatch{this, "BremMatchFilter", {-99999999.,99999999.}};
  Gaudi::Property<std::pair<double,double>> m_eMatch{this, "ElectronMatchFilter", {-999999.,999999.}};
  Gaudi::Property<std::pair<double,double>> m_cMatch{this, "ClusterMatchFilter", {-999999.,999999.}};
  Gaudi::Property<std::pair<double,double>> m_mas   {this, "MassFilter", {-99999.,99999.}};
  Gaudi::Property<std::pair<double,double>> m_dist  {this, "BremEleDistFilter", {4.,99999.}};

  Gaudi::Property<bool>   m_pairing{this, "EmlectronPairing", true};
  Gaudi::Property<float>  m_min  {this, "DeltaMin", -150.};
  Gaudi::Property<float>  m_max  {this, "DeltaMax", +150.}; 
  Gaudi::Property<int>    m_thBin{this, "ThetaBin", 14};
  Gaudi::Property<int>    m_bin  {this, "DeltaBin", 150};
  Gaudi::Property<bool>   m_brem {this, "ElectronWithBremOnly", false};
  Gaudi::Property<float>  m_r    {this, "SshapeRange", 0.7};
  Gaudi::Property<int>    m_b    {this, "SshapeBin"  , 50};
};
#endif // CALOALIGNEMENTNTP_H
