#ifndef CALOELECTRONNTP_H
#define CALOELECTRONNTP_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "Event/ProtoParticle.h"
#include "CaloUtils/ICaloElectron.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloInterfaces/ICounterLevel.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "CaloDet/DeCalorimeter.h"
#include "GaudiKernel/IEventTimeDecoder.h"

/** @class CaloElectronNtp CaloElectronNtp.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2009-12-11
 */
class CaloElectronNtp : public GaudiTupleAlg {
public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

  ITrackExtrapolator* extrapolator(){return m_extrapolator;};

private:
  ICounterLevel* counterStat = nullptr;
  bool inRange( std::pair<double,double> range, double value){
    return ( value >= range.first) && (value <= range.second);
  }

  ITrackExtrapolator*  m_extrapolator = nullptr;
  ICaloElectron* m_caloElectron = nullptr;

  Gaudi::Property<std::string> m_extrapolatorType {this, "ExtrapolatorType", "TrackRungeKuttaExtrapolator"};
  Gaudi::Property<std::string> m_input {this, "InputContainer", "LHCb::ProtoParticleLocation::Charged"};
  Gaudi::Property<std::pair<double,double>> m_e  {this, "EFilter"  , {0.,99999999}};
  Gaudi::Property<std::pair<double,double>> m_et {this, "EtFilter" , {200.,999999.}};
  Gaudi::Property<std::pair<double,double>> m_prs{this, "PrsFilter", {50.,9999999.}};
  Gaudi::Property<std::pair<double,double>> m_eop{this, "EoPFilter", {0.,2.5}};
  
  ICaloHypo2Calo* m_toSpd = nullptr;
  ICaloHypo2Calo* m_toPrs = nullptr;
  DeCalorimeter* m_calo = nullptr;
  IEventTimeDecoder* m_odin = nullptr;

  Gaudi::Property<bool> m_pairing {this, "ElectronPairing", true};
  Gaudi::Property<bool> m_histo   {this, "Histo", true};
  Gaudi::Property<bool> m_tuple   {this, "Tuple", true};
  Gaudi::Property<bool> m_trend   {this, "Trend", false};
  Gaudi::Property<bool> m_usePV3D {this, "UsePV3D", false};
  Gaudi::Property<bool> m_splitFEBs{this, "splitFEBs", false};
  Gaudi::Property<bool> m_splitE  {this, "splitE", false};

  Gaudi::Property<std::string> m_vertLoc 
    {this, "VertexLoc", LHCb::RecVertexLocation::Primary};
  Gaudi::Property<std::vector<int>> m_tracks 
    {this, "TrackTypes", {LHCb::Track::Types::Long, LHCb::Track::Types::Downstream}};

  void fillH(double eOp,Gaudi::LorentzVector t, LHCb::CaloCellID id,std::string hat="");
};

#endif // CALOELECTRONNTP_H
