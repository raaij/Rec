 #include <vector>
#include "GaudiAlg/GaudiTupleAlg.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloInterfaces/ICounterLevel.h"
#include "GaudiKernel/IEventTimeDecoder.h"
// ============================================================================

class CaloPi0Ntp : public GaudiTupleAlg{
  /// friend factory for instantiation
  friend class AlgFactory<CaloPi0Ntp>;
public:
 /// standard algorithm initialization
  CaloPi0Ntp( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  StatusCode execute() override;
private:
  ICounterLevel* counterStat = nullptr;
  /// default  constructor is private
  CaloPi0Ntp();
  /// copy     constructor is private
  CaloPi0Ntp( const CaloPi0Ntp& );
  /// assignement operator is private
  CaloPi0Ntp &operator=( const CaloPi0Ntp& );
private:
  bool inRange( std::pair<double,double> range, double value);
  bool inRange( std::pair<int, int > range, int value);
  std::vector<double> toVector(const Gaudi::XYZPoint& point);
  std::vector<double> toVector(const Gaudi::LorentzVector& vec);

  void hTuning(std::string base, int spd,double prs1, double prs2,
               const Gaudi::LorentzVector c1 , const LHCb::CaloCellID id1,
               const Gaudi::LorentzVector c2 , const LHCb::CaloCellID id2,int nVert);

  Gaudi::Property<std::pair<double,double>> m_ppt
    {this, "PhotonPt", {250., 15000.}};

  Gaudi::Property<std::pair<double,double>> m_isol
    {this, "Isolation", {0., 9999.}, "Warning: a cut biases the pi0 mass"};

  Gaudi::Property<std::pair<int,int>> m_conv
    {this, "Conversion", {1, 1}};

  Gaudi::Property<std::pair<double,double>> m_prsE
    {this, "PrsE", {0., 9999.}};

  Gaudi::Property<std::pair<double,double>> m_pt
    {this, "Pt", {200., 15000}};

  Gaudi::Property<std::pair<double,double>> m_e
    {this, "E", {0., 500000}};

  Gaudi::Property<std::pair<double,double>> m_mass
    {this, "Mass", {50., 900.}};

  Gaudi::Property<bool> m_bkg
    {this, "Background", false};

  Gaudi::Property<float>  m_leBin {this, "leBin", 0.25};
  Gaudi::Property<float>  m_etBin {this, "etBin", 150.};
  Gaudi::Property<float>  m_thBin {this, "thBin", 0.005};

  Gaudi::Property<float>  m_hMin    {this, "hMin", 0.};
  Gaudi::Property<float>  m_hMax    {this, "hMax", 900.};
  Gaudi::Property<int>    m_hBin    {this, "hBin", 450};
  Gaudi::Property<int>    m_spdBin  {this, "spdBin", 50};
  Gaudi::Property<bool>   m_tuple   {this, "Tuple", true};
  Gaudi::Property<bool>   m_histo   {this, "Histo", true};
  Gaudi::Property<bool>   m_trend   {this, "Trend", false};
  Gaudi::Property<bool>   m_usePV3D {this, "UsePV3D", false};
  Gaudi::Property<std::string> m_vertLoc {this, "VertexLoc", ""};

  int m_spdMult = 0;

  IEventTimeDecoder* m_odin = nullptr;
  DeCalorimeter* m_calo     = nullptr;
  ICaloHypo2Calo* m_toSpd   = nullptr;
  ICaloHypo2Calo* m_toPrs   = nullptr;

};
