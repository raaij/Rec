#ifndef CALOPROTOELECTRONMONITOR_H
#define CALOPROTOELECTRONMONITOR_H 1

// Include files
// from Gaudi
#include "CaloMoniAlg.h"
#include "Event/ProtoParticle.h"
#include "CaloUtils/ICaloElectron.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

/** @class CaloProtoElectronMonitor CaloProtoElectronMonitor.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2009-12-11
 */
class CaloProtoElectronMonitor : public CaloMoniAlg {
public:
  /// Standard constructor
  CaloProtoElectronMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution


  ITrackExtrapolator* extrapolator(){return m_extrapolator;};

private:

  ITrackExtrapolator*  m_extrapolator = nullptr;
  ICaloElectron* m_caloElectron = nullptr;

  Gaudi::Property<float> m_eOpMin
    {this, "HistoEoPMin", 0.};

  Gaudi::Property<float> m_eOpMax
    {this, "HistoEoPMax", 3.};

  Gaudi::Property<int> m_eOpBin
    {this, "HistoEoPBin", 100};

  Gaudi::Property<float> m_prsCut
    {this, "PrsCut", 50.* Gaudi::Units::MeV};

  Gaudi::Property<bool> m_pairing
    {this, "ElectronPairing", false};

  Gaudi::Property<std::string> m_extrapolatorType
    {this, "ExtrapolatorType", "TrackRungeKuttaExtrapolator"};

  Gaudi::Property<std::vector<int>> m_tracks
    {this, "TrackTypes", {LHCb::Track::Types::Long, LHCb::Track::Types::Downstream}};

};
#endif // CALOPROTOELECTRONMONITOR_H
