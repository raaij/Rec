#ifndef CALOPHOTONCHECKER_H
#define CALOPHOTONCHECKER_H 1
// ============================================================================
// Include files
// ============================================================================
// Relations
// ============================================================================
#include "Relations/IRelationWeighted.h"
#include "Relations/IRelation.h"
//#include "Relations/IAssociatorWeighted.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
#include "Event/MCParticle.h"
// ============================================================================
// Local
// ============================================================================
#include "CaloMoniAlg.h"

// forward declarations
class ICaloHypoLikelyhood;
class DeCalorimeter;
class IIncidentSvc;


/** @class CaloPhotonChecker CaloPhotonChecker.h
 *
 *  Photon Selection Monitoring
 *  (LHCb 2004-03)
 *
 *  @author Frederic Machefert frederic.machefert@in2p3.fr
 *  @date   2004-15-04
 */

class CaloPhotonChecker : public CaloMoniAlg
{
  // friend factory for instantiation
  friend class AlgFactory<CaloPhotonChecker>;
public:

  /** standard algorithm initialization
   *  @see CaloAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode initialize() override;

  /** standard algorithm execution
   *  @see CaloAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode execute() override;

  /** standard algorithm finalization
   *  @see CaloAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode finalize() override;

protected:

  /** Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  CaloPhotonChecker( const std::string& name ,
                          ISvcLocator*       pSvc );

  std::vector<AIDA::IHistogram2D*> defHisto(const unsigned int,
                                      const double, const double,
                                      const unsigned int,
                                      const std::string);

  inline double transform(double e){
    const double epsilon=1.e-10;
    if (e<epsilon) return 0.;
    double val=log(1.35914*e)-6.21461;
    if (val>5.5) return 5.5;
    return  val;
  }

private:
  unsigned int m_nEvents = 0;
  unsigned int m_nCandidates = 0;
  unsigned int m_nPhotons = 0;
  unsigned int m_nMCPhotons = 0;
  unsigned int m_nWrongs = 0;

  // Detector Information
  DeCalorimeter* m_ecal = nullptr;
  DeCalorimeter* m_spd = nullptr;
  DeCalorimeter* m_prs = nullptr;

  Gaudi::Plane3D m_ecalPlane;
  Gaudi::Plane3D m_prsPlane;
  Gaudi::Plane3D m_spdPlane;

  double         m_zConv = 0.;
  unsigned int   m_area = 0;

  // Tools
  std::string m_IDTableName = LHCb::CaloIdLocation::PhotonID;
  Gaudi::Property<std::string> m_TrTableName 
    {this, "CC2TrTableName", LHCb::CaloIdLocation::ClusterMatch};
  Gaudi::Property<std::string> m_MCTableName 
    {this, "CC2MCPTableName", "Relations/" + LHCb::CaloClusterLocation::Default};

  // Particle Properties
  std::string      m_gammaName {"gamma"};
  LHCb::ParticleID m_gammaID {0};
  std::string      m_pi0Name {"pi0"};
  LHCb::ParticleID m_pi0ID {0};

  // histogramming related variables
  Gaudi::Property<bool>                m_pdf     {this, "Pdf", false};
  Gaudi::Property<std::vector<double>> m_prsbin  {this, "EPrsBin"};
  Gaudi::Property<std::vector<double>> m_chi2bin {this, "Chi2Bin"};
  Gaudi::Property<std::vector<double>> m_seedbin {this, "SeedBin"};

  // Signal/background definitions
  Gaudi::Property<float> m_etmin     {this, "Etmin", 200.*Gaudi::Units::MeV};
  Gaudi::Property<float> m_dr        {this, "Dr", -1.};
  Gaudi::Property<float> m_dz        {this, "Dz", -1.};
  Gaudi::Property<float> m_de        {this, "DE", 0.25};
  Gaudi::Property<float> m_mergedDist{this, "MergedDist", 1.5};

  // histograms

  AIDA::IHistogram1D*   m_lhSig    = nullptr;
  AIDA::IHistogram1D*   m_lhSigSpd = nullptr;
  AIDA::IHistogram1D*   m_lhBkg    = nullptr;
  AIDA::IHistogram1D*   m_lhBkgSpd = nullptr;

  Gaudi::Property<int> m_nbinlh {this, "LhNBin", 20};
  AIDA::IHistogram2D*   m_effpur       = nullptr;
  AIDA::IHistogram2D*   m_effpur_spd   = nullptr;
  AIDA::IHistogram2D*   m_effpur_nospd = nullptr;

  Gaudi::Property<int> m_nbinpt   {this, "PtNBin", 20};
  Gaudi::Property<float> m_lhcut {this, "LhCut", 0.3};
  Gaudi::Property<float> m_ptmin {this, "PtMinHisto", 0. *Gaudi::Units::MeV};
  Gaudi::Property<float> m_ptmax {this, "PtMaxHisto", 10.*Gaudi::Units::GeV};
  AIDA::IHistogram1D*   m_efficiency = nullptr;
  AIDA::IHistogram1D*   m_purity     = nullptr;

  std::vector<double> m_mc_g;
  std::vector<double> m_rec_bkg;
  std::vector<double> m_rec_sig;

  double              m_lh_mcg;
  std::vector<double> m_lh_recsig;
  std::vector<double> m_lh_recbkg;
  double              m_lh_mcg_conv;
  std::vector<double> m_lh_recsig_conv;
  std::vector<double> m_lh_recsig_spd;
  std::vector<double> m_lh_recbkg_spd;
  double              m_lh_mcg_noconv;
  std::vector<double> m_lh_recsig_noconv;
  std::vector<double> m_lh_recsig_nospd;
  std::vector<double> m_lh_recbkg_nospd;

  std::vector<AIDA::IHistogram2D*>    m_signalEPrs2D     ;
  std::vector<AIDA::IHistogram2D*>    m_backgrEPrs2D     ;
  std::vector<AIDA::IHistogram2D*>    m_signalChi22D     ;
  std::vector<AIDA::IHistogram2D*>    m_backgrChi22D     ;
  std::vector<AIDA::IHistogram2D*>    m_signalSeed2D     ;
  std::vector<AIDA::IHistogram2D*>    m_backgrSeed2D     ;

  std::vector<AIDA::IHistogram2D*>    m_signalEPrsSpd2D     ;
  std::vector<AIDA::IHistogram2D*>    m_backgrEPrsSpd2D     ;
  std::vector<AIDA::IHistogram2D*>    m_signalChi2Spd2D     ;
  std::vector<AIDA::IHistogram2D*>    m_backgrChi2Spd2D     ;
  std::vector<AIDA::IHistogram2D*>    m_signalSeedSpd2D     ;
  std::vector<AIDA::IHistogram2D*>    m_backgrSeedSpd2D     ;
};

#endif // CALOPHOTONCHECKER_H
