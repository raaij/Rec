#ifndef CALOENERGYFLOWMONITOR_H
#define CALOENERGYFLOWMONITOR_H 1

// Include files
// ============================================================================
// from Gaudi
// ============================================================================
#include "GaudiAlg/GaudiHistoAlg.h"
#include "Kernel/CaloCellID.h"
// ============================================================================
// Event
// ============================================================================
#include  "Event/CaloDigit.h"
// ============================================================================
// CaloDet
// ============================================================================
#include "CaloDet/DeCalorimeter.h"
// ============================================================================
// CaloUtils
// ============================================================================
#include  "CaloMoniAlg.h"
// ============================================================================

/** @class CaloEFlowAlg CaloEFlowAlg.h
 *
 *
 *  The algorithm for dedicated "EnergyFlow" monitoring of "CaloDigits" containers.
 *  The algorithm produces the following histograms:
 *   1. CaloDigit multiplicity
 *   2. CaloDigit ocupancy 2D plot per area
 *   3. CaloDigit energy and transverse energy 2D plot per area
 *  The same set of histograms, but with cut on Et (or E), is produced if specified
 *
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @p "Name" is the name of the algorithm
 *
 *  @see   CaloMoniAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see      Algorithm
 *  @see     IAlgorithm
 *
 *  @author Aurelien Martens
 *  @date   2009-04-08
 */
class CaloEFlowAlg : public CaloMoniAlg{
public:

  /** Standard constructor
   *  @param   name        algorithm name
   *  @param   pSvcLocator pointer to service locator
   */
  CaloEFlowAlg( const std::string &name, ISvcLocator *pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:

  /// default  construstor  is  private
  CaloEFlowAlg();
  /// copy     construstor  is  private
  CaloEFlowAlg( const CaloEFlowAlg& );
  /// assignement operator  is  private
  CaloEFlowAlg &operator=( const CaloEFlowAlg& );

  DeCalorimeter *m_calo = nullptr;

  Gaudi::Property<float>  m_eFilterMin {this, "EnergyFilterMin", -999};
  Gaudi::Property<float>  m_etFilterMin{this, "EtFilterMin", -999};
  Gaudi::Property<float>  m_eFilterMax {this, "EnergyFilterMax", -999};
  Gaudi::Property<float>  m_etFilterMax{this, "EtFilterMax", -999};
  Gaudi::Property<int>    m_ADCFilterMin{this, "ADCFilterMin", -999};
  Gaudi::Property<int>    m_ADCFilterMax{this, "ADCFilterMax", -999};

  Gaudi::Property<bool> m_mctruth {this, "MCTruth", false};
  Gaudi::Property<bool> m_simulation {this, "Simulation", true};
  Gaudi::Property<bool> m_ignoreNonBeamCrossing {this, "IgnoreNonBeamCrossing", true};
  Gaudi::Property<bool> m_ignoreTAE {this, "IgnoreTAE", true};
  Gaudi::Property<bool> m_ignoreNonPhysicsTrigger {this, "IgnoreNonPhysicsTrigger", true};

  const short int m_pidKplus = 321;
  const short int m_pidKminus = -321;
  const short int m_pidPiplus = 211;
  const short int m_pidPiminus = -211;

  std::string m_detectorName;
  Gaudi::Property<std::string> m_slot {this, "Slot", "", "pile up evetn '' or Prev/ or Next/"};

};
#endif // CALOENERGYFLOWMONITOR_H
