#ifndef CALOHYPONTP_H
#define CALOHYPONTP_H 1
#include <vector>
#include "GaudiAlg/GaudiTupleAlg.h"
#include "CaloInterfaces/ICaloHypoEstimator.h"
#include "CaloInterfaces/ICounterLevel.h"
#include "CaloInterfaces/ICalo2MCTool.h"
#include "GaudiKernel/IEventTimeDecoder.h"
// ============================================================================

class CaloHypoNtp : public GaudiTupleAlg{
  /// friend factory for instantiation
  friend class AlgFactory<CaloHypoNtp>;
public:
 /// standard algorithm initialization
  using GaudiTupleAlg::GaudiTupleAlg;
  StatusCode initialize() override;
  StatusCode execute() override;
private:
  /// default  constructor is private
  CaloHypoNtp();
  /// copy     constructor is private
  CaloHypoNtp( const CaloHypoNtp& );
  /// assignement operator is private
  CaloHypoNtp &operator=( const CaloHypoNtp& );
private:
  ICounterLevel* counterStat = nullptr;
  bool inRange( std::pair<double,double> range, double value);
  bool inRange( std::pair<int, int > range, int value);

  ICaloHypoEstimator* m_estimator = nullptr;
  ICaloHypoEstimator* estimator(){return m_estimator;}
  IEventTimeDecoder* m_odin = nullptr;

  ICalo2MCTool* m_2MC = nullptr;
  ICalo2MCTool* calo2MC(){return m_2MC;}

  std::string m_tabLoc;

  Gaudi::Property<bool> m_extrapol {this, "Extrapolation", true};
  Gaudi::Property<bool> m_seed {this, "AddSeed"     , false};
  Gaudi::Property<bool> m_neig {this, "AddNeighbors", false};

  Gaudi::Property<std::pair<double,double>> m_et   {this, "RangePt"  , {100., 15000.  }};
  Gaudi::Property<std::pair<double,double>> m_e    {this, "RangeE"   , {0.  , 5000000 }};
  Gaudi::Property<std::pair<double,double>> m_spdM {this, "RangeSpdM", {0.  , 5000000.}};
  Gaudi::Property<std::pair<double,double>> m_prsE {this, "RangePrsE", {0.  , 9999.   }};

  Gaudi::Property<std::vector<std::string>> m_locs {this, "Locations"};
  Gaudi::Property<std::vector<std::string>> m_hypos
    {this, "Hypos", {"Electrons", "Photons", "MergedPi0s"}};

  Gaudi::Property<std::string> m_vrtLoc {this, "VertexLoc", LHCb::RecVertexLocation::Primary};
  Gaudi::Property<std::string> m_trkLoc {this, "TrackLoc" , LHCb::TrackLocation::Default};

  Gaudi::Property<bool> m_tupling {this, "Tupling", true};
  Gaudi::Property<bool> m_checker {this, "CheckerMode", false};
  Gaudi::Property<bool> m_print   {this, "Printout", false};
  Gaudi::Property<bool> m_stat    {this, "Statistics", true};
  Gaudi::Property<int> m_mcID     {this, "MCID", -99999999};
  
};
#endif // CALOHYPONTP_H
