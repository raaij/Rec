#ifndef CALONTPBASE_H
#define CALONTPBASE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloInterfaces/ICounterLevel.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "CaloUtils/CaloMomentum.h"
#include "CaloUtils/ICaloElectron.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Event/Track.h"
#include "Event/RecVertex.h"
#include "Event/ProtoParticle.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "Event/ODIN.h"

/** @class CaloNtpBase CaloNtpBase.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-10-12
 */
class CaloNtpBase : public GaudiTupleAlg {
public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override;    ///< Algorithm initialization

  ICaloHypo2Calo* toSpd(){return m_toSpd;} ;
  ICaloHypo2Calo* toPrs(){return m_toPrs;} ;
  DeCalorimeter* calo(){return m_calo;};
  bool inRange( std::pair<double,double> range, double value){
    return ( value >= range.first) && (value <= range.second);
  }
  bool inRange( std::pair<int,int> range, int value){
    return ( value == 0 &&   range.first == 1 ) || (value > 0 && range.second == 1);
  }

  unsigned int nSpd(){
    std::string loc =   LHCb::CaloAlgUtils::CaloDigitLocation( "SPD" );
    if( !exist<LHCb::CaloDigits>(loc) )return 0;
    const LHCb::CaloDigits* digits = get<LHCb::CaloDigits> (loc );
    return (NULL != digits) ? digits->size() : 0;
  };
  unsigned int nTracks(){
    std::string loc =  LHCb::TrackLocation::Default;
    if( !exist<LHCb::Tracks>(loc) )return 0;
    LHCb::Tracks* tracks= get<LHCb::Tracks>(loc);
    return (NULL != tracks) ? tracks->size() : 0;
  }
  unsigned int nVertices();
  ITrackExtrapolator* extrapolator(){return m_extrapolator;};
  ICaloElectron* caloElectron(){return m_caloElectron;}
  bool acceptTrack(const LHCb::Track*);
  bool eventProcessing();
  bool hypoProcessing(const LHCb::CaloHypo* hypo);
  bool inArea(const LHCb::CaloHypo* hypo);

protected:
  ICounterLevel* counterStat = nullptr;
  Gaudi::Property<bool> m_histo {this, "Histo", true};
  Gaudi::Property<bool> m_tuple {this, "Tuple", true};
  Gaudi::Property<bool> m_profil{this, "Profile", true};
  Gaudi::Property<bool> m_inArea {this, "inAreaAcc", false};

  Gaudi::Property<std::string> m_vertLoc {this, "VertexLoc", ""};
  Gaudi::Property<bool> m_usePV3D {this, "UsePV3D", false};
  int m_run = 0;
  ulonglong m_evt = 0;
  int m_tty = 0;

  Gaudi::Property<std::vector<int>> m_tracks 
    {this, "TrackTypes", {LHCb::Track::Types::Long, LHCb::Track::Types::Downstream}};
  Gaudi::Property<std::string> m_extrapolatorType 
    {this, "ExtrapolatorType", "TrackRungeKuttaExtrapolator"};
  
  // global cuts
  Gaudi::Property<std::pair<double,double>> m_nTrk {this, "nTracks"  , {-1.,999999.}};
  Gaudi::Property<std::pair<double,double>> m_nSpd {this, "SpdMult"  , {-1.,999999.}};
  Gaudi::Property<std::pair<double,double>> m_nVtx {this, "nVertices", {-1.,999999.}};
  
  // hypo cuts
  Gaudi::Property<std::pair<double,double>> m_e  {this, "EFilter", {0.,99999999.}};
  Gaudi::Property<std::pair<double,double>> m_et {this, "EtFilter", {150.,999999.}};
  Gaudi::Property<std::pair<double,double>> m_prs{this, "PrsFilter",{-1.,1024.}};
  Gaudi::Property<std::pair<double,double>> m_spd{this, "SpdFilter",{-1.,9999.}};
  Gaudi::Property<std::string> m_input {this, "InputContainer"};

private:
  ITrackExtrapolator* m_extrapolator = nullptr;
  DeCalorimeter* m_calo = nullptr;
  ICaloElectron* m_caloElectron = nullptr;
  ICaloHypo2Calo* m_toSpd = nullptr;
  ICaloHypo2Calo* m_toPrs = nullptr;
  IEventTimeDecoder* m_odin = nullptr;

};
#endif // CALONTPBASE_H
