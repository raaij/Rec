// ============================================================================
#ifndef CALOPIDS_CALOID2DLL_H
#define CALOPIDS_CALOID2DLL_H 1
// ============================================================================
// Include files
// ============================================================================
/// STD& STL
// ============================================================================
#include <cmath>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/VectorMap.h"
// ============================================================================
// AIDA
// ============================================================================
#include "AIDA/IHistogram2D.h"
// ============================================================================
// Relations
// ============================================================================
#include "Relations/IRelation.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// CaloUtils
// ============================================================================
#include "CaloUtils/Calo2Track.h"
#include "CaloTrackAlg.h"
#include "CaloUtils/CaloAlgUtils.h"
// ============================================================================
#include "ToString.h"
#include "ToVector.h"
// ============================================================================
#include "DetDesc/Condition.h"
// ============================================================================
#include "TH2D.h"
// ============================================================================

class ITHistSvc;
// ============================================================================
/** @class CaloID2DLL CaloID2DLL.h
 *
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-18
 */
// ============================================================================
class CaloID2DLL : public CaloTrackAlg
{
  /// friend factory for instantiation
  friend class AlgFactory<CaloID2DLL>;
public:
  /// Algorithm initialization
  StatusCode initialize() override;
  /// Algorithm execution
  StatusCode execute() override;
  StatusCode dumpDLLsToFile();
  /// callback function invoked by the UpdateManagerSvc
  StatusCode  i_updateDLL();
protected:
  /// Standard protected constructor
  CaloID2DLL
  ( const std::string& name ,
    ISvcLocator*       pSvc ) ;
private:
  CaloID2DLL () ;
  CaloID2DLL            ( const CaloID2DLL& ) ;
  CaloID2DLL& operator= ( const CaloID2DLL& ) ;
  StatusCode  initializeWithCondDB   ();
  StatusCode  initializeWithoutCondDB();
protected:
  //
  inline double dLL
  ( const double             p ,
    const double             v ,
    const LHCb::Track::Types t ) const ;
protected:
  // input/output relation table
  Gaudi::Property<std::string> m_input  {this, "Input", "", "input relation table"};
  Gaudi::Property<std::string> m_output {this, "Output", "", "output relation table"};

  // histogram title
  Gaudi::Property<std::string> m_title_lt {this, "HistogramL", "", "histogram title long"  };
  Gaudi::Property<std::string> m_title_dt {this, "HistogramD", "", "histogram title down"  };
  Gaudi::Property<std::string> m_title_tt {this, "HistogramT", "", "histogram title TTrack"};
  Gaudi::Property<std::string> m_title_ut {this, "HistogramU", "", "histogram title upstr" };
  Gaudi::Property<std::string> m_title_vt {this, "HistogramV", "", "histogram title velo"  };

  Gaudi::Property<std::string> m_title_lt_ths {this, "HistogramL_THS", "", "histogram title long if DLLs are read from a root file via THS"};
  Gaudi::Property<std::string> m_title_dt_ths {this, "HistogramD_THS", "", "histogram title down if DLLs are read from a root file via THS"};
  Gaudi::Property<std::string> m_title_tt_ths {this, "HistogramT_THS", "", "histogram title TTrack if DLLs are read from a root file via THS"};
  Gaudi::Property<std::string> m_title_ut_ths {this, "HistogramU_THS", "", "histogram title upstr if DLLs are read from a root file via THS"};
  Gaudi::Property<std::string> m_title_vt_ths {this, "HistogramV_THS", "", "histogram title velo if DLLs are read from a root file via THS"};

  Gaudi::Property<float> m_pScale_lt {this, "nMlong"  , -1, "scale for mom long"};
  Gaudi::Property<float> m_pScale_dt {this, "nMdown"  , -1, "scale for mom down"};
  Gaudi::Property<float> m_pScale_tt {this, "nMTtrack", -1, "scale for mom TT"};
  Gaudi::Property<float> m_pScale_ut {this, "nMupstr" , -1, "scale for val upstr"};
  Gaudi::Property<float> m_pScale_vt {this, "nMvelo"  , -1, "scale for val velo"};

  Gaudi::Property<float> m_vScale_lt {this, "nVlong"  , -1, "scale for val long"};
  Gaudi::Property<float> m_vScale_dt {this, "nVdown"  , -1, "scale for val down"};
  Gaudi::Property<float> m_vScale_tt {this, "nVTtrack", -1, "scale for val TT"};
  Gaudi::Property<float> m_vScale_ut {this, "nVupstr" , -1, "scale for val upstr"};
  Gaudi::Property<float> m_vScale_vt {this, "nVvelo"  , -1, "scale for val velo"};

private:

  const TH2D *m_histo_lt = nullptr;
  const TH2D *m_histo_dt = nullptr;
  const TH2D *m_histo_tt = nullptr;
  const TH2D *m_histo_ut = nullptr;
  const TH2D *m_histo_vt = nullptr;

  Gaudi::Property<bool> m_useCondDB
    {this, "UseCondDB", true, "if true - use CondDB, otherwise get the DLLs via THS from a root file"};

  Gaudi::Property<std::string> m_conditionName {this, "ConditionName"};
  Condition * m_cond = nullptr;
} ;
// ============================================================================
inline double CaloID2DLL::dLL
( const double             p ,
  const double             v ,
  const LHCb::Track::Types t ) const
{

  const TH2D* histo = 0 ;
  double pScale = -1.;
  double vScale = -1.;

  switch ( t )
  {
  case LHCb::Track::Types::Long :
    histo  = m_histo_lt ;
    pScale = m_pScale_lt;
    vScale = m_vScale_lt;
    break ;
  case LHCb::Track::Types::Downstream :
    histo  = m_histo_dt ;
    pScale = m_pScale_dt;
    vScale = m_vScale_dt;
    break ;
  case LHCb::Track::Types::Upstream :
    histo  = m_histo_ut ;
    pScale = m_pScale_ut;
    vScale = m_vScale_ut;
    break ;
  case LHCb::Track::Types::Velo :
    histo  = m_histo_vt ;
    pScale = m_pScale_vt;
    vScale = m_vScale_vt;
    break ;
  case LHCb::Track::Types::Ttrack :
    histo  = m_histo_tt ;
    pScale = m_pScale_tt;
    vScale = m_vScale_tt;
    break ;
  default :
    Error ("Injvald track type, return 0").ignore();
    return 0 ;
  }

  if ( 0 == histo )
  {
    Error ("Histogram is not specified return 0").ignore();
    return 0 ;
  }

  const double _x = ::tanh ( p / pScale ) ;
  const double _y = ::tanh ( v / vScale ) ;
//const int    ix = histo->coordToIndexX ( _x ) ;
//const int    iy = histo->coordToIndexY ( _y ) ;
//return histo->binHeight(  ix , iy ) ;
  const int    ii = const_cast<TH2D *>(histo)->FindBin(_x, _y) ;
  return histo->GetBinContent( ii ) ;

}
// ============================================================================
#endif // CALOID2DLL_H
// ============================================================================

