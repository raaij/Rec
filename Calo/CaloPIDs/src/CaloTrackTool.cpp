// ============================================================================
// Include files
// ============================================================================
// Local
// ============================================================================
#include "CaloTrackTool.h"
// ============================================================================

// ============================================================================
/** @file
 *  Implementation file for class Calo::CaloTrackTool
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-05-28
 */
// ============================================================================
/// standard constructor
// ============================================================================
Calo::CaloTrackTool::CaloTrackTool( const std::string& type   , // ?
                                    const std::string& name   ,
                                    const IInterface*  parent )
  : GaudiTool ( type , name , parent )
{
  _setProperty     ( "CheckTracks"          , "false"               ) ;
}
// ============================================================================
/// initialize the tool
// ============================================================================
StatusCode Calo::CaloTrackTool::initialize ()
{
  StatusCode sc = GaudiTool::initialize() ;
  if ( sc.isFailure() ) { return sc ; }                             // RETURN
  //
  if ( propsPrint() || msgLevel ( MSG::DEBUG ) || m_use.check() )
  { info () << m_use << endmsg ; } ;
  //
  if ( !m_detectorName.empty() )
  { m_calo = getDet<DeCalorimeter> ( detectorName()  ) ; }
  else { Warning("empty detector name!") ; }
  //
  return StatusCode::SUCCESS ;
}


void Calo::CaloTrackTool::_setProperty(const std::string& p ,const std::string& v){
  StatusCode sc = setProperty(p,v);
  if(sc.isSuccess()){
    //    if( msgLevel(MSG::DEBUG) ) debug() << " setting Property "<<p<< " to " << v <<endmsg ;
  }
  else{
    warning() << " setting Property "<<p<< " to " << v << " FAILED" <<endmsg ;
  }
}

