// ============================================================================
#ifndef CALOUTILS_CALO_CALOTRACKTOOL_H
#define CALOUTILS_CALO_CALOTRACKTOOL_H 1
// ============================================================================
// Include files
// ============================================================================
// STD&STL
// ============================================================================
#include <vector>
#include <string>
#include <algorithm>
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
// LHCbMath
// ============================================================================
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
#include "Event/TrackFunctor.h"
#include "Event/State.h"
#include "Event/TrackUse.h"
// ============================================================================
// CaloInterfaces
// ============================================================================
#include "CaloInterfaces/ICaloTrackMatch.h"
#include "CaloInterfaces/ICaloDigits4Track.h"
// ============================================================================
// TrackInterfaces
// ============================================================================
#include "TrackInterfaces/ITrackExtrapolator.h"
// ============================================================================
// CaloDet
// ============================================================================
#include "CaloDet/DeCalorimeter.h"
// ============================================================================
namespace Calo
{
  // ==========================================================================
  /** @class Calo::CaloTrackTool CaloTrackTool.h
   *
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-05-28
   */
  class CaloTrackTool : public GaudiTool
  {
  public:
    // ========================================================================
    typedef std::vector<LHCb::Track::Types>               TrackTypes ;
    typedef ICaloDigits4Track::Line                             Line ;
    // ========================================================================
  public:
    // ========================================================================
    /// initialize the tool
    StatusCode initialize() override;
    // ========================================================================
    void _setProperty(const std::string& p ,const std::string& v);
    // ========================================================================
  protected:
    // ========================================================================
    /// standard constructor
    CaloTrackTool
    ( const std::string& type   , // ?
      const std::string& name   ,
      const IInterface*  parent ) ;
    // ========================================================================
  private:
    // ========================================================================
    /// the default constructor is disabled ;
    CaloTrackTool() ;
    // ========================================================================
  protected:
    // ========================================================================
    inline ITrackExtrapolator* extrapolator      () const ;
    inline ITrackExtrapolator* fastExtrapolator  () const ;
    // ========================================================================
  protected:
    // ========================================================================
    /// Propagate track to a given 3D-place
    inline StatusCode propagate
    ( const LHCb::Track&      track ,
      const Gaudi::Plane3D&   plane ,
      LHCb::State&            state ,
      const LHCb::ParticleID& pid   = LHCb::ParticleID(211) ) const ;
    /// Propagate state to a given 3D-place
    inline StatusCode propagate
    ( LHCb::State&            state ,
      const Gaudi::Plane3D&   plane ,
      const LHCb::ParticleID& pid   = LHCb::ParticleID(211) ) const ;
    /// Propagate state to a given Z
    inline StatusCode propagate
    ( LHCb::State&            state ,
      const double            z     ,
      const LHCb::ParticleID& pid   = LHCb::ParticleID(211) ) const ;
    // ========================================================================
  protected:
    // ========================================================================
    /// construct the straight line from the state
    inline Line line ( const LHCb::State& state ) const
    { return Line ( state.position() , state.slopes () ) ; } ;
    // ========================================================================
  protected:
    // ========================================================================
    /** get  a pointer to the satte for the given track at given location
     *  it shodul be faster then double usage of
     *  LHCb::Track::hasStateAt ( location )  and LHCb::stateAt ( location )
     *  In addition it scans the list of states fro the end -
     *  it is good for calorimeter
     */
    inline const LHCb::State* state
    ( const LHCb::Track&          track ,
      const LHCb::State::Location loc   ) const ;
    // ========================================================================
    /** get  a pointer to the satte for the given track at given location
     *  it shodul be faster then double usage of
     *  LHCb::Track::hasStateAt ( location )  and LHCb::stateAt ( location )
     *  In addition it scans the list of states fro the end -
     *  it is good for calorimeter
     */
    inline       LHCb::State* state
    ( LHCb::Track&                track ,
      const LHCb::State::Location loc   ) const ;
    // ========================================================================
  protected:
    // ========================================================================
    /// check if the track to be used @see TrackUse
    inline bool use  ( const LHCb::Track* track ) const { return m_use( track) ; }
    /// print the short infomration about track flags
    inline MsgStream& print
    ( MsgStream&         stream ,
      const LHCb::Track* track  ) const ;
    /// print the short infomration about track flags
    inline MsgStream& print
    ( const LHCb::Track* track             ,
      const MSG::Level   level = MSG::INFO ) const ;
    // ========================================================================
  protected:
    // ========================================================================
    double tolerance() const { return m_tolerance ; }
    const std::string& detectorName() const { return m_detectorName ; }
    const DeCalorimeter* calo() const { return m_calo ; }
    // ========================================================================
  private:
    // ========================================================================
    // extrapolator name
    Gaudi::Property<std::string> m_extrapolatorName {this, "Extrapolator"};
    // the actual extrapolator
    mutable ITrackExtrapolator* m_extrapolator = nullptr ; ///< the actual extrapolator
    // fast extrapolator name
    Gaudi::Property<std::string> m_fastExtrapolatorName {this, "FastExtrapolator", "TrackLinearExtrapolator/Linear:PUBLIC"};
    // the actual extrapolator
    mutable ITrackExtrapolator* m_fastExtrapolator = nullptr; ///< the fast extrapolator

    Gaudi::Property<float> m_fastZ
      {this, "zForFastExtrapolator", 10.0 * Gaudi::Units::meter, "z-position of 'linear' extrapolation"};

    Gaudi::Property<float> m_tolerance
      {this, "Tolerance", 2.0 * Gaudi::Units::mm, "plane extrapolation tolerance"};

    Gaudi::Property<float> m_cosTolerance
      {this, "CosTolerance", ::cos( 0.1 * Gaudi::Units::mrad ), "plane extrapolation angular tolerance"};

    Gaudi::Property<unsigned int> m_maxIter
      {this, "MaxPlaneIterations", 5, "maximal number of iterations"};

    // detector element
    const DeCalorimeter*     m_calo = nullptr       ;
    Gaudi::Property<std::string> m_detectorName {this, "Calorimeter"};
    // track selector
    TrackUse                 m_use { *this }        ;
    // local storages
    mutable Gaudi::XYZPoint  m_pos                  ;
    mutable Gaudi::XYZVector m_mom                  ;
    // ========================================================================
  } ;
  // ==========================================================================
} // end of namespace Calo
// ============================================================================
// get the extrapolator
// ============================================================================
inline ITrackExtrapolator*
Calo::CaloTrackTool::extrapolator() const
{
  if ( 0 != m_extrapolator     ) { return m_extrapolator ; }
  m_extrapolator = tool<ITrackExtrapolator>( m_extrapolatorName , this ) ;
  return m_extrapolator ;
}
// ============================================================================
// get the fast extrapolator
// ============================================================================
inline ITrackExtrapolator*
Calo::CaloTrackTool::fastExtrapolator() const
{
  if ( 0 != m_fastExtrapolator ) { return m_fastExtrapolator ; }
  m_fastExtrapolator =
    tool<ITrackExtrapolator>( m_fastExtrapolatorName , this ) ;
  return m_fastExtrapolator ;
}
// ============================================================================
// Propagate track to a given 3D-place
// ============================================================================
inline StatusCode
Calo::CaloTrackTool::propagate
( const LHCb::Track&      track ,
  const Gaudi::Plane3D&   plane ,
  LHCb::State&            state ,
  const LHCb::ParticleID& pid   ) const
{
  state = track.closestState ( plane ) ;
  if ( ::fabs( plane.Distance ( state.position() ) ) < tolerance() )
  { return StatusCode::SUCCESS ; }
  return propagate ( state , plane , pid ) ;
}
// ============================================================================
// Propagate state to a given 3D-place
// ============================================================================
inline StatusCode
Calo::CaloTrackTool::propagate
( LHCb::State&            state ,
  const Gaudi::Plane3D&   plane ,
  const LHCb::ParticleID& pid   ) const
{
  // check the plane: if it is "almost Z=const"-plane
  const Gaudi::XYZVector& normal = plane.Normal() ;
  if ( m_cosTolerance < ::fabs ( normal.Z() ) )
  {
    // use the standard method
    const double Z =  -1*plane.HesseDistance()/normal.Z() ;
    return propagate (  state , Z , pid ) ;
  }
  Gaudi::XYZPoint point ;
  for ( unsigned int iter = 0 ; iter < m_maxIter ; ++iter )
  {
    const double distance = ::fabs ( plane.Distance( state.position() ) );
    if ( distance <m_tolerance ) { return StatusCode::SUCCESS ; }   // RETURN
    double mu = 0.0 ;
    if ( !Gaudi::Math::intersection ( line ( state ) , plane , point , mu ) )
    { return Warning ( "propagate: line does not cross the place" ) ; }// RETURN
    StatusCode sc = propagate ( state , point.Z() , pid ) ;
    if ( sc.isFailure() )
    { return Warning ( "propagate: failure from propagate" , sc  ) ; } // RETURN
  }
  return Warning ( "propagate: no convergency has been reached" ) ;
}
// ============================================================================
// Propagate state to a given Z
// ============================================================================
inline StatusCode
Calo::CaloTrackTool::propagate
( LHCb::State&            state ,
  const double            z     ,
  const LHCb::ParticleID& pid   ) const
{
  if      ( std::max ( state.z() , z ) <  m_fastZ )
  { // use the regular extrapolator
    return extrapolator     () -> propagate ( state , z , pid ) ;
  }
  else if ( std::min ( state.z() , z ) > m_fastZ  )
  { // use the fast (linear) extrapolator
    return fastExtrapolator () -> propagate ( state , z , pid ) ;
  }
  // use the pair of extrapolators
  StatusCode sc1 ;
  StatusCode sc2 ;
  if ( state.z () < z )
  {
    sc1 = extrapolator     () -> propagate ( state  , m_fastZ , pid ) ;
    sc2 = fastExtrapolator () -> propagate ( state  , z       , pid ) ;
  }
  else
  {
    sc2 = fastExtrapolator () -> propagate ( state  , m_fastZ , pid ) ;
    sc1 = extrapolator     () -> propagate ( state  , z       , pid ) ;
  }

  StatusCode sc = StatusCode::SUCCESS;
  std::string errMsg = "";
  if ( sc2.isFailure() )
  {
    errMsg = "Error from FastExtrapolator";
    sc = sc2;
  }
  if ( sc1.isFailure() )
  {
    errMsg = "Error from extrapolator";
    sc = sc1;
  }
  //
  if( sc.isFailure() )
    return Warning( errMsg, sc );
  else
    return sc;
}
// ============================================================================
// print the short infomration about track flags
// ============================================================================
inline MsgStream&
Calo::CaloTrackTool::print
( MsgStream&         stream ,
  const LHCb::Track* track  ) const
{ return stream.isActive() ? m_use.print ( stream , track ) : stream ; }
// ============================================================================
// print the short infomration about track flags
// ============================================================================
inline MsgStream&
Calo::CaloTrackTool::print
( const LHCb::Track* track ,
  const MSG::Level   level ) const
{ return print ( msgStream ( level ) , track ) ; }
// ============================================================================
// get  a pointer to the state for the given track at given location
// ============================================================================
inline const LHCb::State*
Calo::CaloTrackTool::state
( const LHCb::Track& track , const LHCb::State::Location loc   ) const
{
  const auto& states = track.states()  ;
  // loop in reverse order: for calo should be a bit more efficient
  auto found = std::find_if( states.rbegin(), states.rend(),
                             [&](const LHCb::State* s)
                             { return s->checkLocation(loc); } );
  //
  return found != states.rend() ? *found : nullptr ;           // RETURN
}
// ============================================================================
// The END
// ============================================================================
#endif // CALOUTILS_CALO_CALOTRACKTOOL_H
// ============================================================================
