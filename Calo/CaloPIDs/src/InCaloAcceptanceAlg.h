#ifndef CALOPIDS_INCALOACCEPTANCEALG_H
#define CALOPIDS_INCALOACCEPTANCEALG_H 1
// ============================================================================
// Include files
// ============================================================================
// Relations
// ============================================================================
#include "Relations/IRelation.h"
#include "Relations/IsConvertible.h"
// ============================================================================
// LHCbKernel
// ============================================================================
#include "Kernel/IInAcceptance.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// CaloUtils
// ============================================================================
#include "CaloUtils/Calo2Track.h"
#include "CaloTrackAlg.h"
#include "CaloUtils/CaloAlgUtils.h"
// ============================================================================
// Local
// ============================================================================
#include "ToVector.h"
// ============================================================================
/** @class InCaloAcceptanceAlg InCaloAcceptanceAlg.h
 *
 *  the trivial algorithm to fill "InCaloAcceptance" table
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================
class InCaloAcceptanceAlg : public CaloTrackAlg
{
  /// friend factory for instantiation
  friend class AlgFactory<InCaloAcceptanceAlg>;
public:
  /// algorithm initialization
  StatusCode initialize() override;
  /// algorithm execution
  StatusCode execute() override;
protected:
  /// Standard protected constructor
  InCaloAcceptanceAlg
  ( const std::string& name ,
    ISvcLocator*       pSvc ) ;
private:
  InCaloAcceptanceAlg() ;
  InCaloAcceptanceAlg           ( const InCaloAcceptanceAlg& ) = delete;
  InCaloAcceptanceAlg& operator=( const InCaloAcceptanceAlg& ) = delete;
protected:
  typedef std::vector<std::string> Inputs ;
protected:
  // input containers of the tracks ;
  Gaudi::Property<Inputs>      m_inputs   {this, "Inputs"};
  Gaudi::Property<std::string> m_output   {this, "Output"};
  Gaudi::Property<std::string> m_toolName {this, "Tool"};
  const IInAcceptance* m_tool = nullptr;
} ;
// ============================================================================

// ============================================================================
#endif // CALOPIDS_INCALOACCEPTANCEALG_H
// ============================================================================
