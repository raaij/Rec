#ifndef CALOPHOTONIDALG_H
#define CALOPHOTONIDALG_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
// from LHCb
#include "CaloInterfaces/ICaloHypoEstimator.h"
#include "CaloInterfaces/ICounterLevel.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloUtils/ClusterFunctors.h"
#include "DetDesc/Condition.h"
#include "Relations/Relation1D.h"
#include "Relations/IRelationWeighted.h"
#include "CaloUtils/Calo2Track.h"
// Histogramming
#include "TH2D.h"
#include <GaudiUtils/Aida2ROOT.h>
#include "AIDA/IHistogram2D.h"




/** @class CaloPhotonIdAlg CaloPhotonIdAlg.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-02-27
 */
class CaloPhotonIdAlg: public GaudiAlgorithm {
public:

  enum TYPE {
    SIGNAL, BACKGROUND, SIGNAL_SPD, BACKGROUND_SPD
  };

  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override; ///< Algorithm execution

  /// callback function invoked by the UpdateManagerSvc
  StatusCode i_updateDLL();

protected:
  StatusCode initializeWithCondDB();
  StatusCode initializeWithoutCondDB();

private:
  double likelihood(const LHCb::CaloHypo* hypo) ;

  inline double dLL(const double, const double, const double, const double,
                    const CaloPhotonIdAlg::TYPE, const int) ;

  StatusCode evalParam(const LHCb::CaloHypo*, double &, double &, double &, int &, double &, unsigned int &) const;

  double evalLikelihood(double, double, double, int, double, double, unsigned int) ;

  TH2D *locateHistoOnDisk(std::string);

  TH2D *locateHistoOnDB(std::string);

private:
  ICounterLevel* counterStat = nullptr;

  bool m_extrapol = true;
  bool m_seed = false;
  bool m_neig = false;
  bool m_isRunnable = true;

  Gaudi::Property<std::string> m_type {this, "Type", "PhotonID"};

  Gaudi::Property<bool> m_tracking {this, "Tracking", true};
  Gaudi::Property<bool> m_pdfEPrs  {this, "PdfEPrs" , true};
  Gaudi::Property<bool> m_pdfChi2  {this, "PdfChi2" , true};
  Gaudi::Property<bool> m_pdfSeed  {this, "PdfSeed" , true};
  Gaudi::Property<bool> m_dlnL     {this, "DlnL"    , true};

  //
  Gaudi::Property<std::vector<std::string>> m_inputs
    {this, "Inputs", {}, "CaloHypo location (default set in initialize)"};
  Gaudi::Property<std::string> m_output
    {this, "Output", "", "relation table output location (default set in initialize)"};

  // Condition dbase
  Gaudi::Property<bool> m_useCondDB 
    {this, "UseCondDB", true, "if true - use CondDB, otherwise get the DLLs via THS from a root file"};
  Gaudi::Property<std::string> m_conditionName
    {this, "ConditionName", "Conditions/ParticleID/Calo/PhotonID"};
  Condition * m_cond = nullptr;


  ICaloHypoEstimator* m_estimator = nullptr;

  // Photon Pdf

  std::string m_histo_path = "CaloNeutralPIDs/PhotonID/";

  std::string m_title_Sig_EPrs_10 = "Signal_Prs_noSpdHit_10";
  std::string m_title_Sig_EPrs_11 = "Signal_Prs_noSpdHit_11";
  std::string m_title_Sig_EPrs_12 = "Signal_Prs_noSpdHit_12";
  std::string m_title_Sig_EPrs_15 = "Signal_Prs_SpdHit_15";
  std::string m_title_Sig_EPrs_16 = "Signal_Prs_SpdHit_16";
  std::string m_title_Sig_EPrs_17 = "Signal_Prs_SpdHit_17";

  std::string m_title_Sig_Chi2_20 = "Signal_Chi2Tk_noSpdHit_20";
  std::string m_title_Sig_Chi2_21 = "Signal_Chi2Tk_noSpdHit_21";
  std::string m_title_Sig_Chi2_22 = "Signal_Chi2Tk_noSpdHit_22";
  std::string m_title_Sig_Chi2_25 = "Signal_Chi2Tk_SpdHit_25";
  std::string m_title_Sig_Chi2_26 = "Signal_Chi2Tk_SpdHit_26";
  std::string m_title_Sig_Chi2_27 = "Signal_Chi2Tk_SpdHit_27";

  std::string m_title_Sig_Seed_30 = "Signal_ESeed_noSpdHit_30";
  std::string m_title_Sig_Seed_31 = "Signal_ESeed_noSpdHit_31";
  std::string m_title_Sig_Seed_32 = "Signal_ESeed_noSpdHit_32";
  std::string m_title_Sig_Seed_35 = "Signal_ESeed_SpdHit_35";
  std::string m_title_Sig_Seed_36 = "Signal_ESeed_SpdHit_36";
  std::string m_title_Sig_Seed_37 = "Signal_ESeed_SpdHit_37";

  std::string m_title_Bkg_EPrs_110 = "Background_Prs_noSpdHit_110";
  std::string m_title_Bkg_EPrs_111 = "Background_Prs_noSpdHit_111";
  std::string m_title_Bkg_EPrs_112 = "Background_Prs_noSpdHit_112";
  std::string m_title_Bkg_EPrs_115 = "Background_Prs_SpdHit_115";
  std::string m_title_Bkg_EPrs_116 =  "Background_Prs_SpdHit_116";
  std::string m_title_Bkg_EPrs_117 = "Background_Prs_SpdHit_117";

  std::string m_title_Bkg_Chi2_120 = "Background_Chi2Tk_noSpdHit_120";
  std::string m_title_Bkg_Chi2_121 = "Background_Chi2Tk_noSpdHit_121";
  std::string m_title_Bkg_Chi2_122 = "Background_Chi2Tk_noSpdHit_122";
  std::string m_title_Bkg_Chi2_125 = "Background_Chi2Tk_SpdHit_125";
  std::string m_title_Bkg_Chi2_126 = "Background_Chi2Tk_SpdHit_126";
  std::string m_title_Bkg_Chi2_127 = "Background_Chi2Tk_SpdHit_127";

  std::string m_title_Bkg_Seed_130 = "Background_ESeed_noSpdHit_130";
  std::string m_title_Bkg_Seed_131 = "Background_ESeed_noSpdHit_131";
  std::string m_title_Bkg_Seed_132 = "Background_ESeed_noSpdHit_132";
  std::string m_title_Bkg_Seed_135 = "Background_ESeed_SpdHit_135";
  std::string m_title_Bkg_Seed_136 = "Background_ESeed_SpdHit_136";
  std::string m_title_Bkg_Seed_137 = "Background_ESeed_SpdHit_137";

  const TH2D *m_Sig_EPrs_10 = nullptr;
  const TH2D *m_Sig_EPrs_11 = nullptr;
  const TH2D *m_Sig_EPrs_12 = nullptr;
  const TH2D *m_Sig_EPrs_15 = nullptr;
  const TH2D *m_Sig_EPrs_16 = nullptr;
  const TH2D *m_Sig_EPrs_17 = nullptr;

  const TH2D *m_Sig_Chi2_20 = nullptr;
  const TH2D *m_Sig_Chi2_21 = nullptr;
  const TH2D *m_Sig_Chi2_22 = nullptr;
  const TH2D *m_Sig_Chi2_25 = nullptr;
  const TH2D *m_Sig_Chi2_26 = nullptr;
  const TH2D *m_Sig_Chi2_27 = nullptr;

  const TH2D *m_Sig_Seed_30 = nullptr;
  const TH2D *m_Sig_Seed_31 = nullptr;
  const TH2D *m_Sig_Seed_32 = nullptr;
  const TH2D *m_Sig_Seed_35 = nullptr;
  const TH2D *m_Sig_Seed_36 = nullptr;
  const TH2D *m_Sig_Seed_37 = nullptr;

  const TH2D *m_Bkg_EPrs_110 = nullptr;
  const TH2D *m_Bkg_EPrs_111 = nullptr;
  const TH2D *m_Bkg_EPrs_112 = nullptr;
  const TH2D *m_Bkg_EPrs_115 = nullptr;
  const TH2D *m_Bkg_EPrs_116 = nullptr;
  const TH2D *m_Bkg_EPrs_117 = nullptr;

  const TH2D *m_Bkg_Chi2_120 = nullptr;
  const TH2D *m_Bkg_Chi2_121 = nullptr;
  const TH2D *m_Bkg_Chi2_122 = nullptr;
  const TH2D *m_Bkg_Chi2_125 = nullptr;
  const TH2D *m_Bkg_Chi2_126 = nullptr;
  const TH2D *m_Bkg_Chi2_127 = nullptr;

  const TH2D *m_Bkg_Seed_130 = nullptr;
  const TH2D *m_Bkg_Seed_131 = nullptr;
  const TH2D *m_Bkg_Seed_132 = nullptr;
  const TH2D *m_Bkg_Seed_135 = nullptr;
  const TH2D *m_Bkg_Seed_136 = nullptr;
  const TH2D *m_Bkg_Seed_137 = nullptr;
};

// ============================================================================
inline double CaloPhotonIdAlg::dLL(const double energy, const double eprs,
    const double chi2, const double seed, const CaloPhotonIdAlg::TYPE type,
    const int area) {
  const TH2D* histo_eprs = 0;
  const TH2D* histo_chi2 = 0;
  const TH2D* histo_seed = 0;

  const double epsilon = 1.e-20;

  switch (area) {
  case 0:
    switch (type) {
    case SIGNAL:
      histo_eprs = m_Sig_EPrs_10;
      histo_chi2 = m_Sig_Chi2_20;
      histo_seed = m_Sig_Seed_30;
      break;
    case SIGNAL_SPD:
      histo_eprs = m_Sig_EPrs_15;
      histo_chi2 = m_Sig_Chi2_25;
      histo_seed = m_Sig_Seed_35;
      break;
    case BACKGROUND:
      histo_eprs = m_Bkg_EPrs_110;
      histo_chi2 = m_Bkg_Chi2_120;
      histo_seed = m_Bkg_Seed_130;
      break;
    case BACKGROUND_SPD:
      histo_eprs = m_Bkg_EPrs_115;
      histo_chi2 = m_Bkg_Chi2_125;
      histo_seed = m_Bkg_Seed_135;
      break;
    default:
      Error("Invalid cluster type").ignore();
      return epsilon;
    }
    break;
  case 1:
    switch (type) {
    case SIGNAL:
      histo_eprs = m_Sig_EPrs_11;
      histo_chi2 = m_Sig_Chi2_21;
      histo_seed = m_Sig_Seed_31;
      break;
    case SIGNAL_SPD:
      histo_eprs = m_Sig_EPrs_16;
      histo_chi2 = m_Sig_Chi2_26;
      histo_seed = m_Sig_Seed_36;
      break;
    case BACKGROUND:
      histo_eprs = m_Bkg_EPrs_111;
      histo_chi2 = m_Bkg_Chi2_121;
      histo_seed = m_Bkg_Seed_131;
      break;
    case BACKGROUND_SPD:
      histo_eprs = m_Bkg_EPrs_116;
      histo_chi2 = m_Bkg_Chi2_126;
      histo_seed = m_Bkg_Seed_136;
      break;
    default:
      Error("Invalid cluster type").ignore();
      return epsilon;
    }
    break;
  case 2:
    switch (type) {
    case SIGNAL:
      histo_eprs = m_Sig_EPrs_12;
      histo_chi2 = m_Sig_Chi2_22;
      histo_seed = m_Sig_Seed_32;
      break;
    case SIGNAL_SPD:
      histo_eprs = m_Sig_EPrs_17;
      histo_chi2 = m_Sig_Chi2_27;
      histo_seed = m_Sig_Seed_37;
      break;
    case BACKGROUND:
      histo_eprs = m_Bkg_EPrs_112;
      histo_chi2 = m_Bkg_Chi2_122;
      histo_seed = m_Bkg_Seed_132;
      break;
    case BACKGROUND_SPD:
      histo_eprs = m_Bkg_EPrs_117;
      histo_chi2 = m_Bkg_Chi2_127;
      histo_seed = m_Bkg_Seed_137;
      break;
    default:
      Error("Invalid cluster type").ignore();
      return epsilon;
    }
    break;
  default:
    Error("Invalid area").ignore();
    return epsilon;
  }

  if (0 == histo_eprs) {
    warning() << "Histogram EPrs is not found." << endmsg;
    warning() << "Stop evaluating the Dll." << endmsg;
    m_isRunnable=false;
    return -9999.;
  }
  if (0 == histo_chi2) {
    warning() << "Histogram Chi2 is not found." << endmsg;
    warning() << "Stop evaluating the Dll." << endmsg;
    m_isRunnable=false;
    return -9999.;
  }
  if (0 == histo_seed) {
    warning() << "Histogram Seed is not found." << endmsg;
    warning() << "Stop evaluating the Dll." << endmsg;
    m_isRunnable=false;
    return -9999.;
  }

  if (energy < epsilon)
    return epsilon;
  double e = log(1.4 * energy) - 6.2;
  if (e > 5.5) e=5.5;

  const int ieprs = const_cast<TH2D *> (histo_eprs)->FindBin(eprs, e);
  const int ichi2 = const_cast<TH2D *> (histo_chi2)->FindBin(chi2, e);
  const int iseed = const_cast<TH2D *> (histo_seed)->FindBin(seed, e);

  double tmp =
    histo_eprs->GetBinContent(ieprs)*
    histo_seed->GetBinContent(iseed);

  if (m_tracking) tmp*=histo_chi2->GetBinContent(ichi2);

  return tmp;
}

#endif // CALOPHOTONIDALG_H
