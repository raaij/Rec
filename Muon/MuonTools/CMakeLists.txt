################################################################################
# Package: MuonTools
################################################################################
gaudi_subdir(MuonTools v6r0)

gaudi_depends_on_subdirs(Det/MuonDet
                         Muon/MuonDAQ
                         Muon/MuonInterfaces
                         GaudiAlg)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(MuonTools
                 src/*.cpp
                 LINK_LIBRARIES MuonDetLib MuonInterfacesLib GaudiAlgLib)

