#ifndef IMVATOOL_H_
#define IMVATOOL_H_

#include "GaudiKernel/IAlgTool.h"
#include "MuonID/CommonMuonHit.h"

#include "Event/Track.h"

static const InterfaceID IID_IMVATool("IMVATool", 1, 0);

class IMVATool : virtual public IAlgTool {
 public:
  static const InterfaceID& interfaceID() { return IID_IMVATool; }

  virtual auto calcBDT(LHCb::Track&, CommonConstMuonHits&) const noexcept -> double = 0;
};

#endif  // IMVATOOL_H_
