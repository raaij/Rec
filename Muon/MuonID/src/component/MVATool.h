#ifndef MVATOOL_H
#define MVATOOL_H 1

#include <array>
#include <string>
#include <utility>
#include <vector>
#include <map>
#include <memory>

#include "Event/MuonPID.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"

#include "MuonID/IMVATool.h"  // Interface

#include "MuonID/ICommonMuonTool.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "MuonDet/DeMuonDetector.h"
//#include "weights/TMVAClassification_BDTD_mva_pion_1200_3_20_05_2_masscut_withPreweight.class.C"
//#include "weights/TMVAClassification_BDTD_mva_pion_300_5_25_05_2_masscut_withPreweight.class.C"
#include "weights/TMVAClassification_BDTD_mva_pion_300_5_25_05_2_masscut_withPreweight_NoNshared_NewMS.class.C"
//#include "weights/TMVAClassification_BDTD_mva_pion_2000_5_25_05_2_masscut_withPreweight.class.C"
//#include "weights/TMVAClassification_BDTD_mva_pion_4000_5_25_10_2_masscut_withPreweight.class.C"



/** @class MVATool MVATool.h
 * A tool that provides an MVA for muon identification 
 *
 * @author Ricardo Vazquez Gomez
 * @date 2017-05-29
 */

class MVATool final : public extends1<GaudiTool, IMVATool> {
//class MVATool final : public GaudiTool {
 public:
  MVATool(const std::string& type, const std::string& name, 
          const IInterface* parent);
  virtual ~MVATool() override = default;
  
  virtual auto initialize() -> StatusCode override;
  virtual auto calcBDT(LHCb::Track&, CommonConstMuonHits&) const noexcept -> double override;

  unsigned int nStations;

 private:

  std::unique_ptr<ReadBDTD_mva_pion_300_5_25_05_2_masscut_withPreweight_NoNshared_NewMS> m_BDTReader;
  DeMuonDetector* m_det;
  ICommonMuonTool *muonTool_;

  std::vector<double> m_stationZ;

  unsigned int firstUsedStation;

};

#endif  // MVATOOL_H


