#ifndef MUONIDPLUSTOOL_H
#define MUONIDPLUSTOOL_H 1

#include <map>

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Event/MuonCoord.h"
#include "Event/Track.h"
#include "Event/MuonPID.h"
#include "MuonID/CommonMuonHit.h"
#include "MuonID/IMuonIDTool.h"
#include "MuonID/IMuonMatchTool.h"
#include "Kernel/ParticleID.h"

// Forward declarations
namespace LHCb {
  class Particle;
  class MuonCoord;
  class MuonTileID;
  class State;
}
class DeMuonDetector;
struct ITrackExtrapolator;
class IMuonFastPosTool;
struct IMuonPadRec;
struct IMuonClusterRec;



/** @class MuonIDPlusTool MuonIDPlusTool.h
 *
 *  /brief Generic tool for MuonID, returning a MuonID object from a Track object
 *
 *  on the first call per event, this tool makes association tables between all tracks passing
 *  some preselction cuts (defined by options MinTrackMomentum, MaxGhostProb, MaxTrackChi2) and all muon hits.
 *  Matches within Nsigma < SearchSigmaCut are kept
 *
 *  Subsequent steps in muonID (association of best muon hits, chi^2 calculations, DLL, etc) are
 *  delegated to dedicated tools
 *
 *  Use ReleaseObjectOwnership = false
 *  if you want the tool to delete all created MuonID and Track objects (by default the tool assumes these objects will
 *  be stored in a container)
 *
 *
 *
 *  @author Giacomo Graziani
 *  @date   2015-11-10
 *
 */
class MuonIDPlusTool : public GaudiTool, public IMuonIDTool {
public:
  /// Standard constructor
  MuonIDPlusTool( const std::string& type,
                  const std::string& name,
                  const IInterface* parent);

  virtual ~MuonIDPlusTool(){ clearPIDs();}; ///< Destructor
  StatusCode initialize() override;

  StatusCode eventInitialize() override;

  LHCb::MuonPID* getMuonID(const LHCb::Track* track) override;

  double muonIDPropertyD(const LHCb::Track* track, const char* propertyName, int station=-1) override;
  int muonIDPropertyI(const LHCb::Track* track, const char* propertyName, int station=-1) override;

private:
  DeMuonDetector* m_mudet = nullptr;
  IMuonFastPosTool* m_posTool = nullptr;
  IMuonPadRec* m_padrectool = nullptr;
  IMuonClusterRec* m_clustertool = nullptr;
  ITrackExtrapolator* m_extrapolator = nullptr;
  IMuonMatchTool* m_matchTool = nullptr;

  unsigned int m_lastRun = 0;
  unsigned int m_lastEvent = 0;
  LHCb::ParticleID theMuon{13};
  const LHCb::Track* m_lasttrack = nullptr;

  static const unsigned int MAXHITS = 1024;
  std::vector<unsigned int> m_hitCounter;
  std::vector<CommonMuonHits> m_muhits;                 // muon hits grouped by stations
  std::map<const CommonMuonHit*, std::vector< std::pair< const LHCb::Track*,float> > > m_mutrkmatchTable;
  std::map<const LHCb::Track*, std::vector< TrackMuMatch > > m_trkmumatchTable;
  std::map<const LHCb::Track*, std::vector< TrackMuMatch > > m_trkmumatchTableSpares;
  std::map<const LHCb::Track*, std::vector<bool>> m_trkInAcceptance;

  std::vector<LHCb::MuonPID*> m_muonPIDs;
  LHCb::MuonPID* m_lastPID = nullptr;

  bool m_largeClusters = false;
  std::vector<bool> m_stationHasLargeCluster;
  int m_nStationsWithMatch = 0;

  void initVariables();
  void clearPIDs();
  void setIsMuon(double momentum, double chi2perdof);
  void checkMuIsolation(const LHCb::Track *pTrack, std::vector<CommonConstMuonHits>* mucoord);

  StatusCode getMuonHits();
  StatusCode matchHitsToTracks();
  bool isTrackInsideStation(const LHCb::State& state, unsigned int istation) const;

  double medianClusize();

  // variables for tuple:
  std::vector<int> m_matchM;
  std::vector<double> m_matchSigma;
  std::vector<int> m_matchX;
  std::vector<int> m_matchY;
  std::vector<double> m_matchT;
  std::vector<double> m_matchdT;
  std::vector<int> m_clusize;
  std::vector<double> m_isoM;
  double m_medianClusize = 0.;
  double m_maxisoM = -1.;
  int m_friendShares = 0;
  int m_nSmatched = 0;
  int m_nVmatched = 0;

  bool m_isMuon = false;
  bool m_isMuonLoose = false;
  bool m_isMuonTight = false;

  // options:
  Gaudi::Property<bool> m_useM1 
    {this, "UseFirstStation", false, 
    "also use M1 hits for muonID"};
  
  Gaudi::Property<float> m_searchSigmaCut 
    {this, "SearchSigmaCut", 9., 
    "number of sigma of extrap. error defining initial search window for muon hits"};
  
  Gaudi::Property<float> m_maxTrackChi2 
    {this, "MaxTrackChi2", 10., 
    "max chi2/dof for considered tracks"};
  
  Gaudi::Property<float> m_maxGhostProb 
    {this, "MaxGhostProb", .8, 
    "max ghost prob. for considered tracks"};
  
  Gaudi::Property<float> m_minTrackMomentum 
    {this, "MinTrackMomentum", 3.*Gaudi::Units::GeV, 
    "minimum momentum for considered tracks"};
  
  Gaudi::Property<int> m_maxCluSize 
    {this, "MaxCluSize", 8, 
    "do not consider cluster above this size for track fitting (just attach after fit)"};
  
  Gaudi::Property<bool> m_ReleaseObjectOwnership 
    {this, "ReleaseObjectOwnership", true, 
    "Assume all created MuonTrack and MuonPID objects will be stored to TES"};
  
  Gaudi::Property<std::string> m_matchToolName 
    {this, "MatchToolName", "MuonChi2MatchTool", 
    "Tool for track extrapolation and match to muon stations"};

  Gaudi::Property<std::string> m_BestTrackLocation 
    {this, "InputTracksLocation", LHCb::TrackLocation::Default, 
    "address of best tracks container"};

};
#endif // MUONIDPLUSTOOL_H
