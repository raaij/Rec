#ifndef MUONTRACKALIHMONITOR_H
#define MUONTRACKALIGMONITOR_H 1

#include <vector>
#include "GaudiAlg/GaudiHistoAlg.h"

/** @class MuonTrackAligMonitor MuonTrackAligMonitor.h
 *
 *
 *  @author
 *  @date   2009-02-25
 */
struct ITrackExtrapolator;
struct ITrackChi2Calculator;
class DeMuonDetector;

class MuonTrackAligMonitor : public GaudiHistoAlg {

public:
  /// Standard constructor
  using GaudiHistoAlg::GaudiHistoAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  AIDA::IHistogram1D  *m_h_chi2,  *m_h_p;
  std::vector<AIDA::IHistogram1D*> m_h_resxL_a,m_h_resyL_a,m_h_resxL_c,m_h_resyL_c;
  std::vector<AIDA::IHistogram1D*> m_h_resxM_a,m_h_resyM_a,m_h_resxM_c,m_h_resyM_c;
  AIDA::IHistogram2D  *m_h_xy, *m_h_txty;

  AIDA::IProfile1D *m_p_resxx, *m_p_resxy, *m_p_resxtx,*m_p_resxty, *m_p_restxx, *m_p_restxy, *m_p_restxtx,
    *m_p_restxty, *m_p_resyx, *m_p_resyy, *m_p_resytx, *m_p_resyty, *m_p_restyx, *m_p_restyy, *m_p_restytx, *m_p_restyty,
    *m_resxhsL, *m_resyhsL,*m_resxhsM, *m_resyhsM;

  ITrackExtrapolator   *m_extrapolator = nullptr; ///< extrapolator
  ITrackChi2Calculator *m_chi2Calculator = nullptr;

  DeMuonDetector*       m_muonDet = nullptr;
  Gaudi::Property<bool> m_LongToMuonMatch {this, "LongToMuonMatch", true};
  Gaudi::Property<bool> m_IsCosmics {this, "IsCosmics", false};
  
  Gaudi::Property<float> m_pCut        {this, "pCut"        , 0.};
  Gaudi::Property<float> m_chi2nCut    {this, "chi2nCut"    , 3};
  Gaudi::Property<float> m_chi2matchCut{this, "chi2matchCut", 10};
  double m_zM1;
  Gaudi::Property<std::string> m_histoLevel {this, "HistoLevel", "OfflineFull"};

  Gaudi::Property<std::string> m_extrapolatorName   {this, "Extrapolator"  , "TrackMasterExtrapolator"};
  Gaudi::Property<std::string> m_Chi2CalculatorName {this, "Chi2Calculator", "TrackChi2Calculator"};
  bool m_notOnline = true;
  bool m_expertMode = false;

};
#endif // MUONTRACKALIGMONITOR_H
