#ifndef VELORECMONITORS_VELOIPRESOLUTIONMONITORNT_H 
#define VELORECMONITORS_VELOIPRESOLUTIONMONITORNT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/IPVOfflineTool.h"
#include "TrackInterfaces/IMaterialLocator.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IVeloExpectation.h"

#include "Event/RecVertex.h"

#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Kernel/Particle2MCLinker.h"

/** @class VeloIPResolutionMonitorNT VeloIPResolutionMonitorNT.h
 *  
 *  An algorithm to monitor IP resolutions as a function of 1/PT.
 *  The IP of tracks used to make a primary vertex is taken on that primary vertex.
 *  Assuming these tracks are from prompt particles, the true IP should be 0, so the
 *  calculated IP is in fact the resolution on the IP. Histograms produced are the 
 *  mean of the absolute unsigned 3D IP resolution against 1/PT, 
 *  and the width of a Gaussian fitted to each 1D IP resolution against 1/PT.
 *
 *  @author Michael Thomas Alexander
 *  @date   2009-05-15
 */

namespace Velo
{
  
  class VeloIPResolutionMonitorNT : public GaudiTupleAlg {
  public: 
    /// Standard constructor
    VeloIPResolutionMonitorNT( const std::string& name, ISvcLocator* pSvcLocator );
        
    StatusCode initialize() override;    ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;    ///< Algorithm finalization

  protected:

  private:

    std::string m_vertexLocation ;
    std::string m_trackLocation ;

    //    const LHCb::RecVertex* m_pv;
    const LHCb::Track* m_track = nullptr;

    bool m_withMC = false;

    bool m_refitPVs = true;
    bool m_checkIDs = false;
    float m_checkFrac = 0.7;
    std::string m_trackExtrapolatorType ;
    std::string m_materialLocatorType ;

    ITrackExtrapolator* m_trackExtrapolator = nullptr;
    IPVOfflineTool* m_pvtool = nullptr;
    IMaterialLocator* m_materialLocator = nullptr;
    IHitExpectation* m_TTExpectTool = nullptr;
    IVeloExpectation* m_VeloExpectTool = nullptr;

    bool m_printToolProps = false;


    StatusCode calculateIPs( const LHCb::RecVertex*, const LHCb::Track*, 
                             double&, double&, double&, double&, double&, double&, LHCb::State&, LHCb::State& );
    void distance( const LHCb::RecVertex*, const LHCb::State& , double&, double&, int );
    
    StatusCode checkMCAssoc( const LHCb::Track*, const LHCb::RecVertex*, const LHCb::MCVertex*&, Gaudi::LorentzVector&, unsigned int& );

    const LHCb::Track* matchTrack(const LHCb::Track&, const LHCb::RecVertex&) const ;
  };
}

#endif // VELORECMONITORS_VELOIPRESOLUTIONMONITOR_H
